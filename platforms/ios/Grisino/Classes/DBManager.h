//
//  DBManager.h
//  Sarkany
//
//  Created by Nazareno Rodriguez on 3/30/16.
//
//

#ifndef DBManager_h
#define DBManager_h

@interface DBManager : NSObject

@property (nonatomic, strong) NSMutableArray *arrColumnNames;

@property (nonatomic) int affectedRows;

@property (nonatomic) long long lastInsertedRowID;

-(NSArray *)loadDataFromDB:(NSString *)query;

-(int)executeQuery:(NSString *)query;

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;

@end

#endif /* DBManager_h */
