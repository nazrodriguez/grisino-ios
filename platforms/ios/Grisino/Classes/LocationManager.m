//
//  LocationShareModel.m
//  Location
//
//  Created by Rick
//  Copyright (c) 2014 Location. All rights reserved.
//

#import "AppDelegate.h"
#import "LocationManager.h"
#import <UIKit/UIKit.h>
#import "GAI.h"
#import "GAIDictionaryBuilder.h"

#import <netinet/in.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import <SystemConfiguration/SCNetworkReachability.h>

@interface LocationManager () <CLLocationManagerDelegate>

@end

@implementation LocationManager

NSString *BRAND = @"Grisino";
Boolean LOGS = false;
Boolean PREVENT_DUPLICATES = true;
Boolean TIME_FILTERS = true;
Boolean GEOFENCE_FOR_EACH_SHOP = true;
NSString *SERVER_NAME_LOC = @"prod";
Boolean RELEVANCE_FILTERS = true;
Boolean UNRESTRICT_UPDATE = false;
Boolean DEBUG = false;

Boolean asked = false;

Boolean notifyBarrier = false;

//Class method to make sure the share model is synch across the app
+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    
    return sharedMyModel;
}


#pragma mark - CLLocationManager

-(CLBeaconRegion *)beaconRegion{
    CLBeaconRegion *beacon = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"] identifier:@"MiniBeacon_00003"];
    // a[self.anotherLocationManager startMonitoringForRegion:beacon];
    
    //beacon = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"E2C56DB5-DFFB-48D2-B060-D0F5A71096E1"] identifier:@"beacon-abcdfghiklmon-2"];
    [self.anotherLocationManager startMonitoringForRegion:beacon];
    
    return beacon;
}
 /*-(void) stopMonitoringBeacons {
    [self.anotherLocationManager stopMonitoringForRegion:<#(nonnull CLRegion *)#>]
}*/
 /*
-(BOOL)isUUID:(NSUUID *)uuid equalToCLBeacon:(CLBeacon *)beacon {
    if([[beacon.proximityUUID UUIDString] isEqualToString:[uuid UUIDString]]){
        
    }
}*/

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray<CLBeacon *> *)beacons inRegion:(CLBeaconRegion *)region {
    NSLog(@"REGION BEACONED: %@", region);
}

- (void)startMonitoringLocation {
    if (_anotherLocationManager)
        [_anotherLocationManager stopUpdatingLocation];
    
    self.anotherLocationManager = [[CLLocationManager alloc]init];
    _anotherLocationManager.delegate = self;
    _anotherLocationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    _anotherLocationManager.allowsBackgroundLocationUpdates = YES;
    _anotherLocationManager.pausesLocationUpdatesAutomatically = NO;
    _anotherLocationManager.distanceFilter = 100.0;
    _anotherLocationManager.activityType = CLActivityTypeFitness;

    [_anotherLocationManager startUpdatingLocation];
    
}

- (void)startSLC {
    if (_anotherLocationManager){
        [_anotherLocationManager stopMonitoringSignificantLocationChanges];
        NSLog(@"Already existant location manager. Stopping SLC monitoring.");
    }
    
    self.anotherLocationManager = [[CLLocationManager alloc]init];
    _anotherLocationManager.delegate = self;
    _anotherLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    //_anotherLocationManager.allowsBackgroundLocationUpdates = YES;
    _anotherLocationManager.pausesLocationUpdatesAutomatically = NO;
    _anotherLocationManager.distanceFilter = 100.0;
    _anotherLocationManager.activityType = CLActivityTypeOtherNavigation;
    
    if(asked == false)
        asked = true;
       
    [_anotherLocationManager startMonitoringSignificantLocationChanges];
    
}

- (void)restartSLC {
    [_anotherLocationManager stopMonitoringSignificantLocationChanges];
    [_anotherLocationManager startMonitoringSignificantLocationChanges];
}

- (void)restartMonitoringLocation {
    [_anotherLocationManager stopUpdatingLocation];
    [_anotherLocationManager startUpdatingLocation];
}

#pragma mark - CLLocationManager Delegate

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(nonnull NSError *)error {
    if(LOGS){
        /*Debug logging of geofences*/
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        localNotification.alertBody = [NSString stringWithFormat:@"ERROR updating location %@", error];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        /*Debug logging of geofences*/
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    if(LOGS > 1){
        /*Debug logging of geofences*/
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:10];
        localNotification.alertBody = [NSString stringWithFormat:@"New updated location %@", [locations lastObject]];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        /*Debug logging of geofences*/
    }
    for (int i = 0; i < [locations count]; i++) {
        CLLocation * newLocation = [locations objectAtIndex:i];
        CLLocationCoordinate2D theLocation = newLocation.coordinate;
        CLLocationAccuracy theAccuracy = newLocation.horizontalAccuracy;
        self.myLocation = theLocation;
        self.myLocationAccuracy = theAccuracy;
    }
    
    [self sendLogs];
    
    /*Look for nearby promos and notify them*/
    @synchronized (self) {
        NSLog(@"sync");
        [self NotifyNearPromos];
    }
    /*Look for nearby promos and notify them*/
    
    //ACTIVATE TO TEST MiniBeacon_0003
    //[self beaconRegion];
    
    [self addLocationToPList:_afterResume];
}

-(void) locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(nullable CLRegion *)region withError:(nonnull NSError *)error {
    
    if(LOGS){
        /*Debug logging of geofences amount*/
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        localNotification.alertBody = [NSString stringWithFormat:@"Error monitoring region. %@", error];     
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        /*Debug logging of geofences amount*/
    }
}

-(void) locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {

    if(LOGS){
        /*Debug logging of geofences*/
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.alertBody = [NSString stringWithFormat:@"Entered region %@", region.identifier];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        /*Debug logging of geofences*/
    }
    
    NSArray *notification = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM notificaciones WHERE geofence = '%@'", region.identifier]]];
    
    if([notification count] > 0){
        if(LOGS){
            /*Debug logging of geofences*/
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            localNotification.alertBody = [NSString stringWithFormat:@"Found notification where geofence identifier equals %@! Now looking for promo whose nid matches said notification", region.identifier];
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            /*Debug logging of geofences*/
        }
        
        NSArray *allPromos = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM promos"]]];
        
        if([allPromos count] == 0){
            if(LOGS){
                /*Debug logging of geofences*/
                UILocalNotification* localNotification2 = [[UILocalNotification alloc] init];
                localNotification2.alertBody = @"Entered region but cant find promos";
                localNotification2.timeZone = [NSTimeZone defaultTimeZone];
                localNotification2.soundName = UILocalNotificationDefaultSoundName;
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification2];
                /*Debug logging of geofences*/
            }
            return;
        }
        
        NSError *e = nil;
        NSArray *enteredRegionPromos;
        NSDictionary *enteredRegionPromo = nil;
        NSString *notificationId = notification[0][2];
        
        if(allPromos != nil && [allPromos count] && [allPromos[0] count] == 0 && [allPromos[0][2] dataUsingEncoding:NSUTF8StringEncoding] != nil){
            enteredRegionPromos = [NSJSONSerialization JSONObjectWithData:  [allPromos[0][2] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &e];
            
            for(NSDictionary *promo in enteredRegionPromos){
                if([[promo valueForKey:@"_nid"] longLongValue] == [notificationId longLongValue])
                    enteredRegionPromo = promo;
            }
        }
        
        if(LOGS){
            /*Debug logging of geofences*/
            UILocalNotification* localNotification2 = [[UILocalNotification alloc] init];
            localNotification2.alertBody = [NSString stringWithFormat:@"Found notification where geofence identifier equals %@! Now looking for promo whose nid matches said notification", region.identifier];
            localNotification2.timeZone = [NSTimeZone defaultTimeZone];
            localNotification2.soundName = UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification2];
            /*Debug logging of geofences*/
        }
        
        if(enteredRegionPromo != nil && enteredRegionPromo != NULL){
            if(LOGS){
                /*Debug logging of geofences*/
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.alertBody = [NSString stringWithFormat:@"Found a promo to notify in this region! It´s called %@", [enteredRegionPromo valueForKey:@"notification_text"]];
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                /*Debug logging of geofences*/
            }
            
            int distance = -1;
            
            // Look for the user´s position again
            [self restartSLC];
            self.haversine.lat1 = self.myLocation.latitude;
            self.haversine.lon1 = self.myLocation.longitude;
            
            /*Final geo notification when entered region*/
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            NSMutableString *str = (NSMutableString *)[([[[enteredRegionPromo valueForKey:@"notification_text"] isEqualToString:@""] ? [enteredRegionPromo valueForKey:@"description_title"] : [enteredRegionPromo valueForKey:@"notification_text"] stringByAppendingString:[NSString stringWithFormat:@" (Muy cerca de aquí!)"]]) stringByReplacingOccurrencesOfString:@"%" withString:@"%%"];
            localNotification.alertBody = str;//@" (A %d%@)", distance, distanceUnit]];
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.userInfo = [[NSDictionary alloc] initWithObjectsAndKeys: [enteredRegionPromo valueForKey:@"_id"], @"id", [enteredRegionPromo valueForKey:@"_nid"], @"nid", @"promos", @"type", nil];
            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            /*Final geo notification when entered region*/
            
            if(PREVENT_DUPLICATES){
                /*Tell db when promo should not be notified anymore*/
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *date = [dateFormatter stringFromDate:[NSDate date]];
                int success = [self.dbManager executeQuery:[NSString stringWithFormat:@"INSERT OR IGNORE INTO notificaciones (fecha,notificacionId, elementId, geofence) VALUES ('%@','%@','%@','none')", date, [enteredRegionPromo valueForKey:@"_nid"], [enteredRegionPromo valueForKey:@"_id"]]];
                
                if(success == 1)
                    NSLog(@"FAILED INSERTION");
                //PROBABLY SHOULDNT NOTIFY
                /*Tell db when promo should not be notified anymore*/
            }
        } else {
            NSArray *allNews = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM contenidos"]]];
            
            if([allNews count] == 0){
                if(LOGS){
                    /*Debug logging of geofences*/
                    UILocalNotification* localNotification2 = [[UILocalNotification alloc] init];
                    localNotification2.alertBody = @"Entered region but cant find news";
                    localNotification2.timeZone = [NSTimeZone defaultTimeZone];
                    localNotification2.soundName = UILocalNotificationDefaultSoundName;
                    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification2];
                    /*Debug logging of geofences*/
                }
                return;
            }
            
            NSError *e = nil;
            NSArray *enteredRegionNews;
            NSString *notificationId = notification[0][2];
            NSDictionary *enteredRegionNew = nil;
            
            if(allNews != nil && [allNews count] && [allNews[0] count] == 0 && [allNews[0][2] dataUsingEncoding:NSUTF8StringEncoding] != nil){
                enteredRegionNews = [NSJSONSerialization JSONObjectWithData:  [allNews[0][2] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &e];
                
                for(NSDictionary *news in enteredRegionNews){
                    if([[news valueForKey:@"_nid"] longLongValue] == [notificationId longLongValue])
                        enteredRegionNew = news;
                }
            }
            
            if(LOGS){
                /*Debug logging of geofences*/
                UILocalNotification* localNotification2 = [[UILocalNotification alloc] init];
                localNotification2.alertBody = [NSString stringWithFormat:@"Found notification where geofence identifier equals %@! Now looking for news whose nid matches said notification", region.identifier];
                localNotification2.timeZone = [NSTimeZone defaultTimeZone];
                localNotification2.soundName = UILocalNotificationDefaultSoundName;
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification2];
                /*Debug logging of geofences*/
            }
            
            if(enteredRegionNew != nil && enteredRegionNew != NULL){
                if(LOGS){
                    /*Debug logging of geofences*/
                    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                    localNotification.alertBody = [NSString stringWithFormat:@"Found a news to notify in this region! It´s called %@", [enteredRegionNew valueForKey:@"notification_text"]];
                    localNotification.timeZone = [NSTimeZone defaultTimeZone];
                    localNotification.soundName = UILocalNotificationDefaultSoundName;
                    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                    /*Debug logging of geofences*/
                }
                
                //NSLog(@"%@", enteredRegionNew);
                
                int distance = -1;
                
                // Look for the user´s position again
                [self restartSLC];
                self.haversine.lat1 = self.myLocation.latitude;
                self.haversine.lon1 = self.myLocation.longitude;
                
                /*Final geo notification when entered region*/
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                NSMutableString *str = (NSMutableString *)[([([[enteredRegionNew valueForKey:@"notification_text"] isEqualToString:@""] ? [enteredRegionNew valueForKey:@"title"] : [enteredRegionNew valueForKey:@"notification_text"]) stringByAppendingString:[NSString stringWithFormat:@" (Muy cerca de aquí!)"]]) stringByReplacingOccurrencesOfString:@"%" withString:@"%%"];
                localNotification.alertBody = str;//@" (A %d%@)", distance, distanceUnit]];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.userInfo = [[NSDictionary alloc] initWithObjectsAndKeys: [enteredRegionNew valueForKey:@"_id"], @"id", [enteredRegionNew valueForKey:@"_nid"], @"nid", @"news", @"type", nil];
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                
                NSLog(@"%@", [([[enteredRegionNew valueForKey:@"notification_text"] isEqualToString:@""] ? [enteredRegionNew valueForKey:@"title"] : [enteredRegionNew valueForKey:@"notification_text"]) stringByAppendingString:[NSString stringWithFormat:@" (Muy cerca de aquí!)"]]);
                /*Final geo notification when entered region*/
                
                if(PREVENT_DUPLICATES){
                    /*Tell db when promo should not be notified anymore*/
                    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSString *date = [dateFormatter stringFromDate:[NSDate date]];
                    int success = [self.dbManager executeQuery:[NSString stringWithFormat:@"INSERT OR IGNORE INTO notificaciones (fecha,notificacionId, elementId, geofence) VALUES ('%@','%@','%@','none')", date, [enteredRegionNew valueForKey:@"_nid"], [enteredRegionNew valueForKey:@"_id"]]];
                    
                    if(success == 1)
                        NSLog(@"FAILED INSERTION");
                    //PROBABLY SHOULDNT NOTIFY
                    /*Tell db when promo should not be notified anymore*/
                }
            } else {
                if(LOGS){
                    /*Debug logging of geofences*/
                    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                    localNotification.alertBody = [NSString stringWithFormat:@"Did not find a promo to notify in this region!"];
                    localNotification.timeZone = [NSTimeZone defaultTimeZone];
                    localNotification.soundName = UILocalNotificationDefaultSoundName;
                    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                    /*Debug logging of geofences*/
                }
                
                /*FOR WHEN BEACONS ARE UP*/
                /*NSArray *allShops = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM venues"]]];
                NSError *e = nil;
                NSArray *enteredRegionShops = [NSJSONSerialization JSONObjectWithData:  [allShops[0][2] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &e];
                NSString *notificationId = notification[0][2];

                NSDictionary *enteredRegionShop = nil;

                for(NSDictionary *shop in enteredRegionShops){
                if([[shop valueForKey:@"_nid"] isEqualToString:notificationId])
                enteredRegionShop = shop;
                }
                if(enteredRegionShop != nil){
                //[self searchForNearbyShop:@""];
                }*/
            }
        }
    } else {
        if(LOGS){
            /*Debug logging of geofences*/
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            localNotification.alertBody = @"Did not find notification where geofence identifier is contained.";
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            /*Debug logging of geofences*/            
        }
    }
}

-(void) searchForNearbyShop:(NSString *)shop{
    /*
    //Test SSID
    //Generate address
    struct sockaddr_in zeroAddr;
    bzero(&zeroAddr, sizeof(zeroAddr));
    zeroAddr.sin_len = sizeof(zeroAddr);
    zeroAddr.sin_family = AF_INET;
    
    //Target de network reachability
    SCNetworkReachabilityRef target = SCNetworkReachabilityCreateWithName(NULL, "evolfree");
    
    //Flags
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityGetFlags(target, &flags);
    
    //Filling of flag
    NSString *sNetworkReachable;
    if(flags & kSCNetworkFlagsReachable)
        sNetworkReachable = @"YES";
    else
        sNetworkReachable = @"NO";
    
    NSString *sCellNetwork;
    if(flags & kSCNetworkReachabilityFlagsIsWWAN)
        sCellNetwork = @"YES";
    else
        sCellNetwork = @"NO";
    NSString *s = [[NSString alloc initWithFormat:
                   @"Network reachable: %@\n"
                   @"Cell network: %@\n",sNetworkReachable, sCellNetwork];
    
    dispatch_queue_t q_background = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    double delayInSeconds = 5.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, q_background, ^(void){
        [self searchForNearbyShop:@""];
    });*/
    
    /*CFArrayRef *interfaceNames = CNCopySupportedInterfaces();
    CFDictionaryRef myDict = CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(*interfaceNames, 0));
    NSString *s = CFDictionaryGetValue(myDict, kCNNetworkInfoKeySSID);
    NSLog(@"%@", s);*/
    
}

-(void) locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    NSLog(@"EXITED REGION %@", region);
    
    if(LOGS){
        /*Final geo notification when entered region*/
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        localNotification.alertBody = @"(A region was left behind)";
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        /*Final geo notification when entered region*/
    }

}

#pragma mark - Plist helper methods

// Below are 3 functions that add location and Application status to PList
// The purpose is to collect location information locally

- (NSString *)appState {
    UIApplication* application = [UIApplication sharedApplication];
    
    NSString * appState;
    if([application applicationState]==UIApplicationStateActive)
        appState = @"UIApplicationStateActive";
    if([application applicationState]==UIApplicationStateBackground)
        appState = @"UIApplicationStateBackground";
    if([application applicationState]==UIApplicationStateInactive)
        appState = @"UIApplicationStateInactive";
    
    return appState;
}

- (void)addResumeLocationToPList {

    NSString * appState = [self appState];
    
    self.myLocationDictInPlist = [[NSMutableDictionary alloc]init];
    [_myLocationDictInPlist setObject:@"UIApplicationLaunchOptionsLocationKey" forKey:@"Resume"];
    [_myLocationDictInPlist setObject:appState forKey:@"AppState"];
    [_myLocationDictInPlist setObject:[NSDate date] forKey:@"Time"];
    
    [self saveLocationsToPlist];
}

- (void)addLocationToPList:(BOOL)fromResume {

    NSString * appState = [self appState];
    
    self.myLocationDictInPlist = [[NSMutableDictionary alloc]init];
    [_myLocationDictInPlist setObject:[NSNumber numberWithDouble:self.myLocation.latitude]  forKey:@"Latitude"];
    [_myLocationDictInPlist setObject:[NSNumber numberWithDouble:self.myLocation.longitude] forKey:@"Longitude"];
    [_myLocationDictInPlist setObject:[NSNumber numberWithDouble:self.myLocationAccuracy] forKey:@"Accuracy"];
    
    [_myLocationDictInPlist setObject:appState forKey:@"AppState"];
    
    if (fromResume) {
        [_myLocationDictInPlist setObject:@"YES" forKey:@"AddFromResume"];
    } else {
        [_myLocationDictInPlist setObject:@"NO" forKey:@"AddFromResume"];
    }
    
    [_myLocationDictInPlist setObject:[NSDate date] forKey:@"Time"];
    
    [self saveLocationsToPlist];
}

- (void)addApplicationStatusToPList:(NSString*)applicationStatus {

    NSString * appState = [self appState];
    
    self.myLocationDictInPlist = [[NSMutableDictionary alloc]init];
    [_myLocationDictInPlist setObject:applicationStatus forKey:@"applicationStatus"];
    [_myLocationDictInPlist setObject:appState forKey:@"AppState"];
    [_myLocationDictInPlist setObject:[NSDate date] forKey:@"Time"];
    
    [self saveLocationsToPlist];
}

- (void)saveLocationsToPlist {
    NSString *plistName = [NSString stringWithFormat:@"LocationArray.plist"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    if([paths count] == 0)
        return;
        
    NSString *docDir = [paths objectAtIndex:0];
    NSString *fullPath = [NSString stringWithFormat:@"%@/%@", docDir, plistName];
    
    NSMutableDictionary *savedProfile = [[NSMutableDictionary alloc] initWithContentsOfFile:fullPath];
    
    if (!savedProfile) {
        savedProfile = [[NSMutableDictionary alloc] init];
        self.myLocationArrayInPlist = [[NSMutableArray alloc]init];
    } else {
        self.myLocationArrayInPlist = [savedProfile objectForKey:@"LocationArray"];
    }
    
    if(_myLocationDictInPlist) {
        [_myLocationArrayInPlist addObject:_myLocationDictInPlist];
        [savedProfile setObject:_myLocationArrayInPlist forKey:@"LocationArray"];
    }
    
    if (![savedProfile writeToFile:fullPath atomically:FALSE]) {
        NSLog(@"Couldn't save LocationArray.plist" );
    }
}

-(void) NotifyNearPromos
{
    /* Set variables */
        NSMutableArray *orderedGeoPromos = nil;
        NSMutableArray *orderedGeoNews = nil;
        NSMutableArray *orderedShops = nil;
        NSArray *jsonArray;
        NSError *e;
        if(self.dbManager == NULL)
            self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"iLoveit.db"];
    /* Set variables */
    /* Setup Haversine */
        self.haversine = [[Haversine alloc] init];
        self.haversine.lat1 = self.myLocation.latitude;
        self.haversine.lon1 = self.myLocation.longitude;
    /* Setup Haversine */
    
    /* Barrier to avoid conflicts */
        if(notifyBarrier == true){
            NSLog(@"NOTIFY NEAR PROMOS OPEN - BARRIER UP");
            
            if(LOGS){
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                localNotification.alertBody = [NSString stringWithFormat:@"NOTIFY NEAR PROMOS OPEN - BARRIER UP"];
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            }
            return;
        }
        NSLog(@"NOTIFY NEAR PROMOS OPEN - NICE");
        notifyBarrier = true;
    /* Barrier to avoid conflicts */
    /* Log the amount of monitored regions */
        NSLog(@"AMOUNT OF REGIONS MONITORED START NNP NOW %lu", (unsigned long)[[_anotherLocationManager monitoredRegions] count]);
        if(LOGS){
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
            localNotification.alertBody = [NSString stringWithFormat:@"Amount of regions monitored currently is %lu", (unsigned long)[_anotherLocationManager.monitoredRegions count]];
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        }
    /* Log the amount of monitored regions */
    
    /* Update databases */
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        [self.dbManager executeQuery:@"CREATE TABLE IF NOT EXISTS updateLogs (fecha TEXT)"];
        NSArray *updateLogs = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:@"SELECT * FROM updateLogs"]];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        NSString *now = [dateFormat stringFromDate:[NSDate date]];
        NSDate *today = [dateFormat dateFromString:now];
        
        if(state != UIApplicationStateActive){
            if([updateLogs count] > 0){
                
                NSDate *lastUpdateDate = [dateFormat dateFromString:[updateLogs lastObject][0]];
                float elapsed = [today timeIntervalSinceDate:lastUpdateDate] / 60;
                
                if(UNRESTRICT_UPDATE || elapsed > 60){//min
                    [self.dbManager executeQuery:[NSString stringWithFormat:@"INSERT OR IGNORE INTO updateLogs (fecha) VALUES ('%@')", now]];
                    [self updateDB];
                }
            } else {
                [self.dbManager executeQuery:[NSString stringWithFormat:@"INSERT OR IGNORE INTO updateLogs (fecha) VALUES ('%@')", now]];
                [self updateDB];
            }
        }
    /* Update databases */
    
    /* IF - CANT CREATE TABLE FOR PROMOS; CONTENTS OR NOTIFICATIONS IN BASE, RETURN. */
        int successPromos = [self.dbManager executeQuery:@"CREATE TABLE IF NOT EXISTS promos (_id INTEGER NOT NULL PRIMARY KEY, fecha TEXT , content TEXT )"];
        if(successPromos == 1){
            notifyBarrier = false;
            NSLog(@"NOTIFY NEAR PROMOS CLOSE - CREATE TABLE PROMOS FAILED");
            if(LOGS){
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                localNotification.alertBody = [NSString stringWithFormat:@"NOTIFY NEAR PROMOS CLOSE - CREATE TABLE PROMOS FAILED"];
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            }
            return;
        }

        int successNews = [self.dbManager executeQuery:@"CREATE TABLE IF NOT EXISTS contenidos (_id INTEGER NOT NULL PRIMARY KEY, fecha TEXT , content TEXT )"];
        if(successNews == 1){
            notifyBarrier = false;
            NSLog(@"NOTIFY NEAR PROMOS CLOSE - CREATE TABLE NEWS FAILED");
            if(LOGS){
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                localNotification.alertBody = [NSString stringWithFormat:@"NOTIFY NEAR PROMOS CLOSE - CREATE TABLE NEWS FAILED"];
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            }
            return;
        }

        int successNotifications = [self.dbManager executeQuery:@"CREATE TABLE IF NOT EXISTS notificaciones (_id INTEGER NOT NULL PRIMARY KEY, fecha TEXT , notificacionId TEXT, elementId TEXT, geofence TEXT)"];
        if(successNotifications == 1){
            notifyBarrier = false;
            NSLog(@"NOTIFY NEAR PROMOS CLOSE - CREATE TABLE NOTIFICACIONES FAILED");
            if(LOGS){
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                localNotification.alertBody = [NSString stringWithFormat:@"NOTIFY NEAR PROMOS CLOSE - CREATE TABLE NOTIFICACIONES FAILED"];
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            }
            return;
        }
    /* IF - CANT CREATE TABLE FOR PROMOS; CONTENTS OR NOTIFICATIONS IN BASE, RETURN. */
    
    /* Get, process, workaround and order PROMOS */
    /*Get the results*/
    NSString *query = @"select * from promos";
    if (self.dbArray != nil) {
        self.dbArray = nil;
    }
    
    self.dbArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    /*Get the results*/
    
    /*If no promos in base, return nothing*/
    if(self.dbArray == nil || !self.dbArray || [self.dbArray count] == 0 ||[self.dbArray[0] count] == 0 || [self.dbArray[0][2] dataUsingEncoding:NSUTF8StringEncoding] == nil){
        NSLog(@"PROMOS DB IS EMPTY");
        notifyBarrier = false;
        if(LOGS){
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
            localNotification.alertBody = [NSString stringWithFormat:@"NOTIFY NEAR PROMOS CLOSE - NO PROMOS IN BASE"];
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        }
    } else {
        NSLog(@"IS FULL");
        
        /*Process the results*/
        e = nil;
        jsonArray = [NSJSONSerialization JSONObjectWithData:  [self.dbArray[0][2] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &e];
        orderedGeoPromos = (NSMutableArray *)jsonArray;
        NSLog(@"%lu PROMOS IN BASE", (unsigned long)[orderedGeoPromos count]);
        /*Process the results*/
        
        /*Get scheduled and notified promos so they are filterd*/
        NSArray *notifiedPromos = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:@"SELECT * FROM notificaciones WHERE geofence = 'none'"]];
        NSArray *scheduledPromos = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:@"SELECT * FROM notificaciones WHERE geofence = 'scheduledDate'"]];
        NSLog(@"Scheduled promos: %lu", (unsigned long)[scheduledPromos count]);
        NSLog(@"Notified promos: %lu", (unsigned long)[notifiedPromos count]);
        /*Get scheduled and notified promos so they are filterd*/
        
        /*Filter promos by validity, duplicates, within non-bothering hours and relevance*/
        [orderedGeoPromos filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            Boolean willGetGeofence = true;
            
            /*Check if item._nid is in notificaciones*/
            NSString *nid = [evaluatedObject valueForKey:@"_nid"];
            Boolean found = false;
            
            for(int i = 0; i < [notifiedPromos count]; i++){
                //NSLog(@"nid: %@, notifiedPromos´s nid: %@", nid, [notifiedPromos objectAtIndex:i][2]);
                if([[[NSString alloc] initWithFormat:@"%@", nid] isEqualToString: [notifiedPromos objectAtIndex:i][2]]){
                    found = true;
                }
            }
            
            for(int i = 0; i < [scheduledPromos count]; i++){
                //NSLog(@"nid: %@, scheduledPromos´s nid: %@", nid, [scheduledPromos objectAtIndex:i][2]);
                if([[[NSString alloc] initWithFormat:@"%@", nid] isEqualToString: [scheduledPromos objectAtIndex:i][2]]){
                    found = true;
                }
            }
            
            //NSLog(@" - Previously notified %lu times",(unsigned long)[notifiedPromos count]);
            if(found && PREVENT_DUPLICATES){
                willGetGeofence = false;
            }
            /*Check if item._nid is in notificaciones*/
            
            Boolean shouldNotify = [[evaluatedObject valueForKey:@"notify"] boolValue];
            Boolean isPermanent = [[evaluatedObject valueForKey:@"permanent"] boolValue];
            
            /*Check date validity*/
            NSString *start = [evaluatedObject valueForKey:@"start_date"];
            NSString *end = [evaluatedObject valueForKey:@"end_date"];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
            [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
            NSDate *startdate = [dateFormat dateFromString:start];
            NSString *now = [dateFormat stringFromDate:[NSDate date]];
            NSDate *nowdate = [dateFormat dateFromString:now];
            NSDate *enddate = [dateFormat dateFromString:end];
            /*Check date validity*/
            
            float elapsed = [nowdate timeIntervalSinceDate:startdate] / 86400;
            bool isOnlyDate = ([[[evaluatedObject objectForKey:@"triggers"] valueForKey:@"hotspot_data"] isKindOfClass:[NSDictionary class]]) || ((unsigned long)[[[[evaluatedObject objectForKey:@"audience"] objectForKey:@"shops"] componentsJoinedByString:@""] length] > 0) ? false : true;
            bool isBetweenDates = (startdate && enddate && ([nowdate compare:startdate] == NSOrderedDescending || isOnlyDate) && [nowdate compare:enddate] == NSOrderedAscending) || !TIME_FILTERS;
            
            /*Hours and minutes*/
            NSDate *date = [NSDate date];
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:(isOnlyDate ? startdate : date)];
            NSInteger hour = [components hour];
            /*Hours and minutes*/
            bool isBetweenAllowedHours = (hour <= 23 && hour >= 7) || !TIME_FILTERS;
            bool isStillRelevantOrToBeScheduled = (elapsed <= 3 || !TIME_FILTERS) && (elapsed >= 0 || isOnlyDate);
            if((!isBetweenDates && !isPermanent) || !isBetweenAllowedHours || !isStillRelevantOrToBeScheduled){
                willGetGeofence = false;
            }
            [evaluatedObject setValue:[NSString stringWithFormat:@"%d", isOnlyDate] forKey:@"isDate"];
            
            NSLog(@" ");
            NSLog(@"%@", [evaluatedObject valueForKey:@"notification_text"]);
            NSLog(@"isOnlyDate: %d", isOnlyDate);
            NSLog(@"isBetweenDates: %d", isBetweenDates);
            NSLog(@"isBetweenAllowedHours: %d", isBetweenAllowedHours);
            NSLog(@"isStillRelevantOrToBeScheduled: %d", isStillRelevantOrToBeScheduled);
            NSLog(@"ULTIMATELY GETS A GEOFENCE: %d", willGetGeofence);
            NSLog(@" ");
            
            /*Ultimately, if it not should be notified, assign a false*/
            if(!shouldNotify)
                willGetGeofence = false;
            /*Ultimately, if it not should be notified, assign a false*/
            
            /*PROMO´S SHOPS WORKAROUND*/
            if(willGetGeofence && !isOnlyDate){
                //NSLog(@"FOUND %@ TO BE GEO", [evaluatedObject objectForKey:@"notification_text"]);
                NSArray *shopPromos = [[evaluatedObject objectForKey:@"audience"] objectForKey:@"shops"];
                //NSLog(@"%lu", (unsigned long)[shopPromos count]);
                if([shopPromos count]){
                    Boolean hasAtLeastOne = false;
                    for(int i = 0; i < [shopPromos count]; i++){
                        NSDictionary *shop = [shopPromos objectAtIndex:i];
                        if([[shop objectForKey:@"map"] count] >= 1){
                            hasAtLeastOne = true;
                        }
                    }
                    if(!hasAtLeastOne){
                        //NSLog(@"%@ had INVALID map info in all of its shops set to notify at.", [evaluatedObject objectForKey:@"description_title"]);
                        return false;
                    } else {
                        //NSLog(@"%@ had VALID map info in at least one of the shops it´s set to notify at.", [evaluatedObject objectForKey:@"description_title"]);
                        
                    }
                }
            }
            /*PROMO´S SHOPS WORKAROUND*/
            
            return willGetGeofence;
        }]];
        /*Filter promos by validity, duplicates, within non-bothering hours and relevance*/
        
        /*Add aditional geofences for content with many shops*/
        NSMutableArray *aditionalPromoGeofencesForShops = [[NSMutableArray alloc] init];
        if(GEOFENCE_FOR_EACH_SHOP){
            for(NSDictionary *promo in orderedGeoPromos){
                
                NSArray *shopPromos = [[promo objectForKey:@"audience"] objectForKey:@"shops"];
                if([shopPromos count]){
                    for(int i = 0; i < [shopPromos count]; i++){
                        NSDictionary *shop = [shopPromos objectAtIndex:i];
                        if([[shop objectForKey:@"map"] count] >= 1){
                            
                            NSMutableDictionary *aux = [[NSMutableDictionary alloc] initWithDictionary:(NSMutableDictionary *)promo];
                            
                            NSDictionary *shopspotData = [[NSDictionary alloc] initWithObjectsAndKeys:[[shop objectForKey:@"map"] objectForKey:@"lat"],@"lat",[[shop objectForKey:@"map"] objectForKey:@"lng"],@"lng", nil];
                            
                            [[aux objectForKey:@"triggers"] setValue:shopspotData forKey:@"hotspot_data"];
                            
                            NSString *appendedShop = [[aux objectForKey:@"notification_text"] stringByAppendingString: [NSString stringWithFormat:@" %@",[shop objectForKey:@"name"]]];
                            
                            [aux setObject:appendedShop forKey:@"notification_text"];
                            
                            [aditionalPromoGeofencesForShops addObject:[[NSMutableDictionary alloc] initWithDictionary:aux copyItems:true]];
                            
                            aux = nil;
                        }
                    }
                }
            }
        }
        /*Add aditional geofences for content with many shops*/
        
        /*Log all aditional items and their hotspot data*/
        for(int i = 0; i < [aditionalPromoGeofencesForShops count]; i++){
            NSLog(@"%@", [[aditionalPromoGeofencesForShops objectAtIndex:i] objectForKey:@"notification_text"]);
            NSLog(@"%@", [[[aditionalPromoGeofencesForShops objectAtIndex:i] objectForKey:@"triggers"] objectForKey:@"hotspot_data"]);
        }
        /*Log all aditional items and their hotspot data*/
        
        /*Order de promos by proximity*/
        NSLog(@"ABOUT TO SORT %lu PROMOS", (unsigned long)[orderedGeoPromos count]);
        if([orderedGeoPromos count] >= 2){
            orderedGeoPromos = (NSMutableArray *)[orderedGeoPromos sortedArrayUsingComparator:^NSComparisonResult(id a, id b){
                
                int distanceA = -1;
                int distanceB = -1;
                
                /*--------------FIRST OBJECT ANALYSIS---------------*/
                if([[[a objectForKey:@"triggers"] valueForKey:@"hotspot_data"] isKindOfClass:[NSDictionary class]]){
                    
                    NSString *lng = [[[a objectForKey:@"triggers"] objectForKey:@"hotspot_data"] valueForKey:@"lng"];
                    NSString *lat = [[[a objectForKey:@"triggers"] objectForKey:@"hotspot_data"] valueForKey:@"lat"];
                    
                    //NSLog(@"A -> %@´s lat(%@) and lng(%@)", [a objectForKey:@"notification_text"], lat, lng);
                    
                    self.haversine.lat2 = [lat floatValue];
                    self.haversine.lon2 = [lng floatValue];
                    
                    distanceA = [self.haversine toMeters];
                    [a setObject:[NSNumber  numberWithInteger:([[[a objectForKey:@"triggers"] valueForKey:@"hotspot_radius"] intValue] < 200 ? 200 : [[[a objectForKey:@"triggers"] valueForKey:@"hotspot_radius"] intValue])] forKey:@"geofenceRadius"];
                    
                } else if((unsigned long)[[[[a objectForKey:@"audience"] objectForKey:@"shops"] componentsJoinedByString:@""] length] > 0){
                    
                    int minDistance = -1;
                    NSDictionary *shop;
                    int shopRadius;
                    
                    for(int i = 0; i < (unsigned long)[[[a objectForKey:@"audience"] objectForKey:@"shops"] count]; i++){
                        
                        shop = [[a objectForKey:@"audience"] objectForKey:@"shops"][i];
                        
                        if(shop == (NSDictionary *)[NSNull null] || [[shop objectForKey:@"map"] count] < 1){
                            NSLog(@"either the shop or its map info was null for %@", [a objectForKey:@"notification_text"]);
                            NSLog(@"%@", [shop objectForKey:@"map"]);
                            
                            if((unsigned long)[[[a objectForKey:@"audience"] objectForKey:@"shops"] count] == 1){
                                [a setObject:@"yes" forKey:@"shouldErase"];
                            }
                            
                            continue;
                        } else {
                            //NSLog(@"Everything is fine");
                        }
                        
                        shopRadius = 500;
                        
                        self.haversine.lat2 = [[[shop objectForKey:@"map"] valueForKey:@"lat"] floatValue];
                        self.haversine.lon2 = [[[shop objectForKey:@"map"] valueForKey:@"lng"] floatValue];
                        
                        //If its closer than 1km
                        if(i == 0 || [self.haversine toMeters] < minDistance)
                            minDistance = [self.haversine toMeters];
                        
                    }
                    
                    distanceA = minDistance;
                    [a setObject:[NSNumber  numberWithInteger:shopRadius] forKey:@"geofenceRadius"];
                }
                if(distanceA != -1){
                    [a setObject:[[[CLLocation alloc] init] initWithLatitude:self.haversine.lat2 longitude:self.haversine.lon2] forKey:@"calculatedLocation"];
                    //NSLog(@"Could calculate distance for %@ at %@, and its %d",[a objectForKey:@"notification_text"], [a objectForKey:@"calculatedLocation"] ,distanceA);
                }
                /*--------------FIRST OBJECT ANALYSIS---------------*/
                
                /*--------------SECOND OBJECT ANALYSIS---------------*/
                if([[[b objectForKey:@"triggers"] valueForKey:@"hotspot_data"] isKindOfClass:[NSDictionary class]]){
                    
                    NSString *lng = [[[b objectForKey:@"triggers"] objectForKey:@"hotspot_data"] valueForKey:@"lng"];
                    NSString *lat = [[[b objectForKey:@"triggers"] objectForKey:@"hotspot_data"] valueForKey:@"lat"];
                    
                    //NSLog(@"B -> %@´s lat(%@) and lng(%@)", [b objectForKey:@"notification_text"], lat, lng);
                    
                    self.haversine.lat2 = [lat floatValue];
                    self.haversine.lon2 = [lng floatValue];
                    
                    distanceB = [self.haversine toMeters];
                    [b setObject:[NSNumber  numberWithInteger:([[[b objectForKey:@"triggers"] valueForKey:@"hotspot_radius"] intValue] < 200 ? 200 : [[[b objectForKey:@"triggers"] valueForKey:@"hotspot_radius"] intValue])] forKey:@"geofenceRadius"];
                    
                    
                } else if((unsigned long)[[[[b objectForKey:@"audience"] objectForKey:@"shops"] componentsJoinedByString:@""] length] > 0){
                    
                    int minDistance = -1;
                    NSDictionary *shop;
                    int shopRadius;
                    
                    for(int i = 0; i < (unsigned long)[[[b objectForKey:@"audience"] objectForKey:@"shops"] count]; i++){
                        
                        shop = [[b objectForKey:@"audience"] objectForKey:@"shops"][i];
                        
                        if(shop == (NSDictionary *)[NSNull null] || [[shop objectForKey:@"map"] count] < 1){
                            NSLog(@"either the shop or its map info was null for %@", [a objectForKey:@"notification_text"]);
                            
                            if((unsigned long)[[[b objectForKey:@"audience"] objectForKey:@"shops"] count] == 1){
                                [b setObject:@"yes" forKey:@"shouldErase"];
                            }
                            continue;
                        }
                        
                        shopRadius = 500;
                        
                        self.haversine.lat2 = [[[shop objectForKey:@"map"] valueForKey:@"lat"] floatValue];
                        self.haversine.lon2 = [[[shop objectForKey:@"map"] valueForKey:@"lng"] floatValue];
                        
                        //If its closer than 1km
                        if(i == 0 || [self.haversine toMeters] < minDistance)
                            minDistance = [self.haversine toMeters];
                        
                    }
                    
                    distanceB = minDistance;
                    [b setObject:[NSNumber numberWithInteger:shopRadius] forKey:@"geofenceRadius"];
                    
                }
                if(distanceB != -1)
                    [b setObject:[[[CLLocation alloc] init] initWithLatitude:self.haversine.lat2 longitude:self.haversine.lon2] forKey:@"calculatedLocation"];
                /*--------------SECOND OBJECT ANALYSIS---------------*/
                
                /*--------------FINAL COMPARE---------------*/
                [a setObject:[NSNumber numberWithInt:distanceA] forKey:@"calculatedDistance"];
                [b setObject:[NSNumber numberWithInt:distanceB] forKey:@"calculatedDistance"];
                
                if(distanceA != -1 && distanceB != -1){//Ambas con geo
                    return distanceA <= distanceB ? NSOrderedAscending : NSOrderedDescending;
                } else if(distanceA != -1){//A tiene geo y B fecha
                    return NSOrderedAscending;
                } else if(distanceB != -1){//B tiene geo y A fecha
                    return NSOrderedDescending;
                } else {//Ambas tienen fecha
                    return NSOrderedSame;
                }
                /*--------------FINAL COMPARE---------------*/
                
            }];
        } else if([orderedGeoPromos count] == 1){
            int distanceA = -1;
            NSLog(@"Only 1 promo to set.");
            
            NSMutableDictionary *a = [orderedGeoPromos objectAtIndex:0];
            if([[[a objectForKey:@"triggers"] valueForKey:@"hotspot_data"] isKindOfClass:[NSDictionary class]]){
                
                NSString *lng = [[[a objectForKey:@"triggers"] objectForKey:@"hotspot_data"] valueForKey:@"lng"];
                NSString *lat = [[[a objectForKey:@"triggers"] objectForKey:@"hotspot_data"] valueForKey:@"lat"];
                self.haversine.lat2 = [lat floatValue];
                self.haversine.lon2 = [lng floatValue];
                
                distanceA = [self.haversine toMeters];
                [a setObject:[NSNumber  numberWithInteger:([[[a objectForKey:@"triggers"] valueForKey:@"hotspot_radius"] intValue] < 100 ? 100 : [[[a objectForKey:@"triggers"] valueForKey:@"hotspot_radius"] intValue])] forKey:@"geofenceRadius"];
            } else if((unsigned long)[[[[a objectForKey:@"audience"] objectForKey:@"shops"] componentsJoinedByString:@""] length] > 0){
                
                int minDistance = -1;
                NSDictionary *shop;
                int shopRadius;
                
                for(int i = 0; i < (unsigned long)[[[a objectForKey:@"audience"] objectForKey:@"shops"] count]; i++){
                    
                    shop = [[a objectForKey:@"audience"] objectForKey:@"shops"][i];
                    
                    if(shop == (NSDictionary *)[NSNull null]){
                        NSLog(@"shop was null");
                        
                        if((unsigned long)[[[a objectForKey:@"audience"] objectForKey:@"shops"] count] == 1)
                            [a setObject:@"yes" forKey:@"shouldErase"];
                        
                        continue;
                    }
                    
                    shopRadius = 500;
                    
                    self.haversine.lat2 = [[[shop objectForKey:@"map"] valueForKey:@"lat"] floatValue];
                    self.haversine.lon2 = [[[shop objectForKey:@"map"] valueForKey:@"lng"] floatValue];
                    
                    //If its closer than 1km
                    if(i == 0 || [self.haversine toMeters] < minDistance)
                        minDistance = [self.haversine toMeters];
                    
                }
                
                distanceA = minDistance;
                
                [a setObject:[NSNumber  numberWithInteger:shopRadius] forKey:@"geofenceRadius"];
            }
            if(distanceA != -1){
                [a setObject:[[[CLLocation alloc] init] initWithLatitude:self.haversine.lat2 longitude:self.haversine.lon2] forKey:@"calculatedLocation"];
            }
            
            [a setObject:[NSNumber numberWithInt:distanceA] forKey:@"calculatedDistance"];
            
            orderedGeoPromos = [[NSMutableArray alloc] initWithObjects:a, nil];
            
        }
        /*Order de promos by proximity*/
        
        /*Null shop workaround*/
        NSMutableArray * copy = [[NSMutableArray alloc] initWithArray:orderedGeoPromos];
        if([copy count]){
            [copy filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                return !([[evaluatedObject allKeys] containsObject:@"shouldErase"] && [[evaluatedObject objectForKey:@"shouldErase"] isEqualToString:@"yes"]);
            }]];
        }
        orderedGeoPromos = copy;
        /*Null shop workaround*/
    }
    /* Get, process, workaround and order PROMOS */
    
    /* Get, process, workaround and order SHOPS */
        /*Get the results*/
        query = @"select * from venues";
        if (self.dbArray != nil)
            self.dbArray = nil;
        self.dbArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
        /*Get the results*/
        
        /*Use the results*/
        Boolean HAS_SHOPS = (self.dbArray != nil && self.dbArray && [self.dbArray count] != 0 && [self.dbArray[0] count] != 0 && [self.dbArray[0][2] dataUsingEncoding:NSUTF8StringEncoding] != nil);
        if(HAS_SHOPS){
            /*Process the results*/
            jsonArray = [NSJSONSerialization JSONObjectWithData:  [self.dbArray[0][2] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &e];
            
            orderedShops = (NSMutableArray *)jsonArray;
            /*Process the results*/
            
            /*SHOPS WORKAROUND*/
            NSLog(@" ");
            long shopsInitialCount = (unsigned long)[orderedShops count];
            __block Boolean hasInvalid = false;
            
            [orderedShops filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                Boolean isNull = [[evaluatedObject objectForKey:@"map"] count] < 3;
                
                if(isNull){
                    NSLog(@"%@ has invalid map information.", [evaluatedObject objectForKey:@"name"]);
                    hasInvalid = true;
                }
                
                return !isNull;
            }]];
            if(hasInvalid){
                NSLog(@"There were %lu shops, and after filtering invalid ones, there are %lu remaining.", shopsInitialCount,(unsigned long)[orderedShops count]);
            }
            NSLog(@" ");
            /*SHOPS WORKAROUND*/
            
            /*Order shops by proximity*/
            orderedShops = (NSMutableArray *)[orderedShops sortedArrayUsingComparator:^NSComparisonResult(id a, id b){
                
                int distanceA = -1, distanceB = -1, minDistance = -1, shopRadius;
                
                /*FIRST OBJECT ANALYSIS*/
                minDistance = -1;
                
                shopRadius = [[a valueForKey:@"radius"] intValue];
                
                self.haversine.lat2 = [[[a objectForKey:@"map"] valueForKey:@"lat"] floatValue];
                self.haversine.lon2 = [[[a objectForKey:@"map"] valueForKey:@"lng"] floatValue];
                
                if(minDistance == -1 || [self.haversine toMeters] < minDistance)
                    minDistance = [self.haversine toMeters];
                
                distanceA = minDistance;
                [a setObject:[NSNumber  numberWithInteger:200] forKey:@"geofenceRadius"];
                
                if(distanceA != -1)
                    [a setObject:[[[CLLocation alloc] init] initWithLatitude:self.haversine.lat2 longitude:self.haversine.lon2] forKey:@"calculatedLocation"];
                /*FIRST OBJECT ANALYSIS*/
                
                /*SECOND OBJECT ANALYSIS*/
                minDistance = -1;
                
                shopRadius = [[b valueForKey:@"radius"] intValue];
                
                self.haversine.lat2 = [[[b objectForKey:@"map"] valueForKey:@"lat"] floatValue];
                self.haversine.lon2 = [[[b objectForKey:@"map"] valueForKey:@"lng"] floatValue];
                
                if(minDistance == -1 || [self.haversine toMeters] < minDistance)
                    minDistance = [self.haversine toMeters];
                
                distanceB = minDistance;
                [b setObject:[NSNumber  numberWithInteger:200] forKey:@"geofenceRadius"];
                
                if(distanceB != -1)
                    [b setObject:[[[CLLocation alloc] init] initWithLatitude:self.haversine.lat2 longitude:self.haversine.lon2] forKey:@"calculatedLocation"];
                /*SECOND OBJECT ANALYSIS*/
                
                /*FINAL COMPARE*/
                [a setObject:[NSNumber numberWithInt:distanceA] forKey:@"calculatedDistance"];
                [b setObject:[NSNumber numberWithInt:distanceB] forKey:@"calculatedDistance"];
                if(distanceA != -1 && distanceB != -1){//Ambas con geo
                    return distanceA <= distanceB ? NSOrderedAscending : NSOrderedDescending;
                } else if(distanceA != -1){
                    return NSOrderedAscending;
                } else if(distanceB != -1){
                    return NSOrderedDescending;
                } else {
                    return NSOrderedSame;
                }
                /*FINAL COMPARE*/
            }];
            /*Order shops by proximity*/
        }
        /*Use the results*/
    /* Get, process, workaround and order SHOPS */
    /* Get, process, workaround and order NEWS */
        /*Get the results*/
        query = @"select * from contenidos";
        if (self.dbArray != nil) {
            self.dbArray = nil;
        }
        
        self.dbArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
        /*Get the results*/
        
        if(self.dbArray == nil || !self.dbArray || [self.dbArray count] == 0 ||[self.dbArray[0] count] == 0 || [self.dbArray[0][2] dataUsingEncoding:NSUTF8StringEncoding] == nil){
            NSLog(@"CONTENTS DB IS EMPTY");
            notifyBarrier = false;
            if(LOGS){
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                localNotification.alertBody = [NSString stringWithFormat:@"NOTIFY NEAR PROMOS CLOSE - NO NEWS IN BASE"];
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            }
        } else {
            /*Process the results*/
            jsonArray = [NSJSONSerialization JSONObjectWithData:  [self.dbArray[0][2] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &e];
            orderedGeoNews = (NSMutableArray *)jsonArray;
            NSLog(@"%lu NEWS IN BASE", (unsigned long)[orderedGeoNews count]);
            /*Process the results*/
            
            /*Get scheduled and notified news so they are filterd*/
            NSArray *notifiedNews = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:@"SELECT * FROM notificaciones WHERE geofence = 'none'"]];
            NSArray *scheduledNews = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:@"SELECT * FROM notificaciones WHERE geofence = 'scheduledDate'"]];
            /*Get scheduled and notified news so they are filterd*/
            
            /*Filter news by validity, duplicates, within non-bothering hours and relevance*/
            [orderedGeoNews filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                Boolean willGetGeofence = true;
                
                // Use title for notification_text
                if([[evaluatedObject valueForKey:@"notification_text"] isEqualToString:@""]){
                    [evaluatedObject setValue:[evaluatedObject valueForKey:@"title"] forKey:@"notification_text"];
                }
                
                /*Check if item._nid is in notificaciones*/
                NSString *nid = [evaluatedObject valueForKey:@"_nid"];
                Boolean found = false;
                
                for(int i = 0; i < [notifiedNews count]; i++){
                    //NSLog(@"nid: %@, notifiedNews´s nid: %@", nid, [notifiedNews objectAtIndex:i][2]);
                    if([[[NSString alloc] initWithFormat:@"%@", nid] isEqualToString: [notifiedNews objectAtIndex:i][2]]){
                        found = true;
                    }
                }
                
                for(int i = 0; i < [scheduledNews count]; i++){
                    //NSLog(@"nid: %@, scheduledNews´s nid: %@", nid, [scheduledNews objectAtIndex:i][2]);
                    if([[[NSString alloc] initWithFormat:@"%@", nid] isEqualToString: [scheduledNews objectAtIndex:i][2]]){
                        found = true;
                    }
                }
                
                //NSLog(@" - Previously notified %lu times",(unsigned long)[notifiedPromos count]);
                if(found && PREVENT_DUPLICATES){
                    willGetGeofence = false;
                }
                /*Check if item._nid is in notificaciones*/
                
                Boolean shouldNotify = [[evaluatedObject valueForKey:@"notify"] boolValue];
                Boolean isPermanent = [[evaluatedObject valueForKey:@"permanent"] boolValue];
                
                /*Check date validity*/
                NSString *start = [evaluatedObject valueForKey:@"start_date"];
                NSString *end = [evaluatedObject valueForKey:@"end_date"];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
                [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
                NSDate *startdate = [dateFormat dateFromString:start];
                NSString *now = [dateFormat stringFromDate:[NSDate date]];
                NSDate *nowdate = [dateFormat dateFromString:now];
                NSDate *enddate = [dateFormat dateFromString:end];
                /*Check date validity*/
                
                float elapsed = [nowdate timeIntervalSinceDate:startdate] / 86400;
                bool isOnlyDate = ([[[evaluatedObject objectForKey:@"triggers"] valueForKey:@"hotspot_data"] isKindOfClass:[NSDictionary class]]) || ((unsigned long)[[[[evaluatedObject objectForKey:@"audience"] objectForKey:@"shops"] componentsJoinedByString:@""] length] > 0) ? false : true;
                bool isBetweenDates = (startdate && enddate && ([nowdate compare:startdate] == NSOrderedDescending || isOnlyDate) && [nowdate compare:enddate] == NSOrderedAscending) || !TIME_FILTERS;
                
                /*Hours and minutes*/
                NSDate *date = [NSDate date];
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:(isOnlyDate ? startdate : date)];
                NSInteger hour = [components hour];
                /*Hours and minutes*/
                bool isBetweenAllowedHours = (hour <= 23 && hour >= 7) || !TIME_FILTERS;
                bool isStillRelevantOrToBeScheduled = (elapsed <= 3 || !TIME_FILTERS) && (elapsed >= 0 || isOnlyDate);
                if((!isBetweenDates && !isPermanent) || !isBetweenAllowedHours || !isStillRelevantOrToBeScheduled){
                    willGetGeofence = false;
                }
                [evaluatedObject setValue:[NSString stringWithFormat:@"%d", isOnlyDate] forKey:@"isDate"];
                
                NSLog(@" ");
                NSLog(@"%@", [evaluatedObject valueForKey:@"notification_text"]);
                NSLog(@"isOnlyDate: %d", isOnlyDate);
                NSLog(@"isBetweenDates: %d", isBetweenDates);
                NSLog(@"isBetweenAllowedHours: %d", isBetweenAllowedHours);
                NSLog(@"isStillRelevantOrToBeScheduled: %d", isStillRelevantOrToBeScheduled);
                NSLog(@"ULTIMATELY GETS A GEOFENCE: %d", willGetGeofence);
                NSLog(@" ");
                
                /*Ultimately, if it not should be notified, assign a false*/
                if(!shouldNotify)
                    willGetGeofence = false;
                /*Ultimately, if it not should be notified, assign a false*/
                
                /*NEWS´S SHOPS WORKAROUND*
                 if(willGetGeofence && !isOnlyDate){
                 //NSLog(@"FOUND %@ TO BE GEO", [evaluatedObject objectForKey:@"notification_text"]);
                 NSArray *shopNews = [[evaluatedObject objectForKey:@"audience"] objectForKey:@"shops"];
                 //NSLog(@"%lu", (unsigned long)[shopPromos count]);
                 if([shopNews count]){
                 Boolean hasAtLeastOne = false;
                 for(int i = 0; i < [shopNews count]; i++){
                 NSDictionary *shop = [shopNews objectAtIndex:i];
                 if([[shop objectForKey:@"map"] count] >= 1){
                 hasAtLeastOne = true;
                 }
                 }
                 if(!hasAtLeastOne){
                 NSLog(@"%@ had INVALID map info in all of its shops set to notify at.", [evaluatedObject objectForKey:@"description_title"]);
                 return false;
                 } else {
                 NSLog(@"%@ had VALID map info in at least one of the shops it´s set to notify at.", [evaluatedObject objectForKey:@"description_title"]);
                 
                 }
                 }
                 }
                 /*NEWS SHOPS WORKAROUND*/
                
                return willGetGeofence;
            }]];
            /*Filter news by validity, duplicates, within non-bothering hours and relevance*/
            
            /*Add aditional geofences for content with many shops*
             NSMutableArray *aditionalNewsGeofencesForShops = [[NSMutableArray alloc] init];
             if(GEOFENCE_FOR_EACH_SHOP){
             for(NSDictionary *news in orderedGeoPromos){
             
             NSArray *shopNews = [[news objectForKey:@"audience"] objectForKey:@"shops"];
             if([shopNews count]){
             for(int i = 0; i < [shopNews count]; i++){
             NSDictionary *shop = [shopNews objectAtIndex:i];
             if([[shop objectForKey:@"map"] count] >= 1){
             
             NSMutableDictionary *aux = [[NSMutableDictionary alloc] initWithDictionary:(NSMutableDictionary *)news];
             
             NSDictionary *shopspotData = [[NSDictionary alloc] initWithObjectsAndKeys:[[shop objectForKey:@"map"] objectForKey:@"lat"],@"lat",[[shop objectForKey:@"map"] objectForKey:@"lng"],@"lng", nil];
             
             [[aux objectForKey:@"triggers"] setValue:shopspotData forKey:@"hotspot_data"];
             
             NSString *appendedShop = [[aux objectForKey:@"notification_text"] stringByAppendingString: [NSString stringWithFormat:@" %@",[shop objectForKey:@"name"]]];
             
             [aux setObject:appendedShop forKey:@"notification_text"];
             
             [aditionalNewsGeofencesForShops addObject:[[NSMutableDictionary alloc] initWithDictionary:aux copyItems:true]];
             
             aux = nil;
             }
             }
             }
             }
             }
             
             NSLog(@"There are %lu aditional promo geofences to be scheduled thanks to multiple pointed geo promos.", (unsigned long)[aditionalPromoGeofencesForShops count]);
             orderedGeoPromos = (NSMutableArray *)[orderedGeoPromos arrayByAddingObjectsFromArray:aditionalPromoGeofencesForShops];
             
             NSLog(@"There are %lu news geofences to be scheduled", (unsigned long)[orderedGeoNews count]);
             NSLog(@"There are %lu aditional news geofences to be scheduled thanks to multiple pointed geo content.", (unsigned long)[aditionalNewsGeofencesForShops count]);
             orderedGeoNews = (NSMutableArray *)[orderedGeoNews arrayByAddingObjectsFromArray:aditionalNewsGeofencesForShops];
             /*Add aditional geofences for content with many shops*/
            
            /*Log all aditional news and their hotspot data*
             for(int i = 0; i < [aditionalNewsGeofencesForShops count]; i++){
             NSLog(@"%@", [[aditionalNewsGeofencesForShops objectAtIndex:i] objectForKey:@"notification_text"]);
             NSLog(@"%@", [[[aditionalNewsGeofencesForShops objectAtIndex:i] objectForKey:@"triggers"] objectForKey:@"hotspot_data"]);
             }
             /*Log all aditional news and their hotspot data*/
            
            /*Order de news by proximity*/
            NSLog(@"ABOUT TO SORT %lu NEWS", (unsigned long)[orderedGeoNews count]);
            if([orderedGeoNews count] >= 2){
                orderedGeoNews = (NSMutableArray *)[orderedGeoNews sortedArrayUsingComparator:^NSComparisonResult(id a, id b){
                    
                    int distanceA = -1;
                    int distanceB = -1;
                    
                    /*--------------FIRST OBJECT ANALYSIS---------------*/
                    if([[[a objectForKey:@"triggers"] valueForKey:@"hotspot_data"] isKindOfClass:[NSDictionary class]]){
                        
                        NSString *lng = [[[a objectForKey:@"triggers"] objectForKey:@"hotspot_data"] valueForKey:@"lng"];
                        NSString *lat = [[[a objectForKey:@"triggers"] objectForKey:@"hotspot_data"] valueForKey:@"lat"];
                        
                        NSLog(@"A -> %@´s lat(%@) and lng(%@)", [a objectForKey:@"notification_text"], lat, lng);
                        NSLog(@"User position´s lat(%f) and lng(%f)", self.haversine.lat1, self.haversine.lon1);
                        
                        self.haversine.lat2 = [lat floatValue];
                        self.haversine.lon2 = [lng floatValue];
                        
                        distanceA = [self.haversine toMeters];
                        [a setObject:[NSNumber  numberWithInteger:([[[a objectForKey:@"triggers"] valueForKey:@"hotspot_radius"] intValue] < 200 ? 200 : [[[a objectForKey:@"triggers"] valueForKey:@"hotspot_radius"] intValue])] forKey:@"geofenceRadius"];
                        
                    } else if((unsigned long)[[[[a objectForKey:@"audience"] objectForKey:@"shops"] componentsJoinedByString:@""] length] > 0){
                        
                        int minDistance = -1;
                        NSDictionary *shop;
                        int shopRadius;
                        
                        for(int i = 0; i < (unsigned long)[[[a objectForKey:@"audience"] objectForKey:@"shops"] count]; i++){
                            
                            shop = [[a objectForKey:@"audience"] objectForKey:@"shops"][i];
                            
                            if(shop == (NSDictionary *)[NSNull null] || [[shop objectForKey:@"map"] count] < 1){
                                NSLog(@"either the shop or its map info was null for %@", [a objectForKey:@"notification_text"]);
                                NSLog(@"%@", [shop objectForKey:@"map"]);
                                
                                if((unsigned long)[[[a objectForKey:@"audience"] objectForKey:@"shops"] count] == 1){
                                    [a setObject:@"yes" forKey:@"shouldErase"];
                                }
                                
                                continue;
                            } else {
                                //NSLog(@"Everything is fine");
                            }
                            
                            shopRadius = 500;
                            
                            self.haversine.lat2 = [[[shop objectForKey:@"map"] valueForKey:@"lat"] floatValue];
                            self.haversine.lon2 = [[[shop objectForKey:@"map"] valueForKey:@"lng"] floatValue];
                            
                            //If its closer than 1km
                            if(i == 0 || [self.haversine toMeters] < minDistance)
                                minDistance = [self.haversine toMeters];
                            
                        }
                        
                        distanceA = minDistance;
                        [a setObject:[NSNumber  numberWithInteger:shopRadius] forKey:@"geofenceRadius"];
                    }
                    if(distanceA != -1){
                        [a setObject:[[[CLLocation alloc] init] initWithLatitude:self.haversine.lat2 longitude:self.haversine.lon2] forKey:@"calculatedLocation"];
                        //NSLog(@"Could calculate distance for %@ at %@, and its %d",[a objectForKey:@"notification_text"], [a objectForKey:@"calculatedLocation"] ,distanceA);
                    }
                    /*--------------FIRST OBJECT ANALYSIS---------------*/
                    
                    /*--------------SECOND OBJECT ANALYSIS---------------*/
                    if([[[b objectForKey:@"triggers"] valueForKey:@"hotspot_data"] isKindOfClass:[NSDictionary class]]){
                        
                        NSString *lng = [[[b objectForKey:@"triggers"] objectForKey:@"hotspot_data"] valueForKey:@"lng"];
                        NSString *lat = [[[b objectForKey:@"triggers"] objectForKey:@"hotspot_data"] valueForKey:@"lat"];
                        
                        //NSLog(@"B -> %@´s lat(%@) and lng(%@)", [b objectForKey:@"notification_text"], lat, lng);
                        
                        self.haversine.lat2 = [lat floatValue];
                        self.haversine.lon2 = [lng floatValue];
                        
                        distanceB = [self.haversine toMeters];
                        [b setObject:[NSNumber  numberWithInteger:([[[b objectForKey:@"triggers"] valueForKey:@"hotspot_radius"] intValue] < 200 ? 200 : [[[b objectForKey:@"triggers"] valueForKey:@"hotspot_radius"] intValue])] forKey:@"geofenceRadius"];
                        
                        
                    } else if((unsigned long)[[[[b objectForKey:@"audience"] objectForKey:@"shops"] componentsJoinedByString:@""] length] > 0){
                        
                        int minDistance = -1;
                        NSDictionary *shop;
                        int shopRadius;
                        
                        for(int i = 0; i < (unsigned long)[[[b objectForKey:@"audience"] objectForKey:@"shops"] count]; i++){
                            
                            shop = [[b objectForKey:@"audience"] objectForKey:@"shops"][i];
                            
                            if(shop == (NSDictionary *)[NSNull null] || [[shop objectForKey:@"map"] count] < 1){
                                NSLog(@"either the shop or its map info was null for %@", [a objectForKey:@"notification_text"]);
                                
                                if((unsigned long)[[[b objectForKey:@"audience"] objectForKey:@"shops"] count] == 1){
                                    [b setObject:@"yes" forKey:@"shouldErase"];
                                }
                                continue;
                            }
                            
                            shopRadius = 500;
                            
                            self.haversine.lat2 = [[[shop objectForKey:@"map"] valueForKey:@"lat"] floatValue];
                            self.haversine.lon2 = [[[shop objectForKey:@"map"] valueForKey:@"lng"] floatValue];
                            
                            //If its closer than 1km
                            if(i == 0 || [self.haversine toMeters] < minDistance)
                                minDistance = [self.haversine toMeters];
                            
                        }
                        
                        distanceB = minDistance;
                        [b setObject:[NSNumber numberWithInteger:shopRadius] forKey:@"geofenceRadius"];
                        
                    }
                    if(distanceB != -1)
                        [b setObject:[[[CLLocation alloc] init] initWithLatitude:self.haversine.lat2 longitude:self.haversine.lon2] forKey:@"calculatedLocation"];
                    /*--------------SECOND OBJECT ANALYSIS---------------*/
                    
                    /*--------------FINAL COMPARE---------------*/
                    [a setObject:[NSNumber numberWithInt:distanceA] forKey:@"calculatedDistance"];
                    [b setObject:[NSNumber numberWithInt:distanceB] forKey:@"calculatedDistance"];
                    
                    if(distanceA != -1 && distanceB != -1){//Ambas con geo
                        return distanceA <= distanceB ? NSOrderedAscending : NSOrderedDescending;
                    } else if(distanceA != -1){//A tiene geo y B fecha
                        return NSOrderedAscending;
                    } else if(distanceB != -1){//B tiene geo y A fecha
                        return NSOrderedDescending;
                    } else {//Ambas tienen fecha
                        return NSOrderedSame;
                    }
                    /*--------------FINAL COMPARE---------------*/
                    
                }];
            } else if([orderedGeoNews count] == 1){
                int distanceA = -1;
                NSLog(@"Only 1 news to set.");
                
                NSMutableDictionary *a = [orderedGeoNews objectAtIndex:0];
                if([[[a objectForKey:@"triggers"] valueForKey:@"hotspot_data"] isKindOfClass:[NSDictionary class]]){
                    
                    NSString *lng = [[[a objectForKey:@"triggers"] objectForKey:@"hotspot_data"] valueForKey:@"lng"];
                    NSString *lat = [[[a objectForKey:@"triggers"] objectForKey:@"hotspot_data"] valueForKey:@"lat"];
                    self.haversine.lat2 = [lat floatValue];
                    self.haversine.lon2 = [lng floatValue];
                    
                    NSLog(@"Only content to sort -> %@´s lat(%@) and lng(%@)", [a objectForKey:@"notification_text"], lat, lng);
                    NSLog(@"User position´s lat(%f) and lng(%f)", self.haversine.lat1, self.haversine.lon1);
                    
                    distanceA = [self.haversine toMeters];
                    [a setObject:[NSNumber  numberWithInteger:([[[a objectForKey:@"triggers"] valueForKey:@"hotspot_radius"] intValue] < 100 ? 100 : [[[a objectForKey:@"triggers"] valueForKey:@"hotspot_radius"] intValue])] forKey:@"geofenceRadius"];
                } else if((unsigned long)[[[[a objectForKey:@"audience"] objectForKey:@"shops"] componentsJoinedByString:@""] length] > 0){
                    
                    int minDistance = -1;
                    NSDictionary *shop;
                    int shopRadius;
                    
                    for(int i = 0; i < (unsigned long)[[[a objectForKey:@"audience"] objectForKey:@"shops"] count]; i++){
                        
                        shop = [[a objectForKey:@"audience"] objectForKey:@"shops"][i];
                        
                        if(shop == (NSDictionary *)[NSNull null]){
                            NSLog(@"shop was null");
                            
                            if((unsigned long)[[[a objectForKey:@"audience"] objectForKey:@"shops"] count] == 1)
                                [a setObject:@"yes" forKey:@"shouldErase"];
                            
                            continue;
                        }
                        
                        shopRadius = 500;
                        
                        self.haversine.lat2 = [[[shop objectForKey:@"map"] valueForKey:@"lat"] floatValue];
                        self.haversine.lon2 = [[[shop objectForKey:@"map"] valueForKey:@"lng"] floatValue];
                        
                        //If its closer than 1km
                        if(i == 0 || [self.haversine toMeters] < minDistance)
                            minDistance = [self.haversine toMeters];
                        
                    }
                    
                    distanceA = minDistance;
                    
                    [a setObject:[NSNumber  numberWithInteger:shopRadius] forKey:@"geofenceRadius"];
                }
                if(distanceA != -1){
                    [a setObject:[[[CLLocation alloc] init] initWithLatitude:self.haversine.lat2 longitude:self.haversine.lon2] forKey:@"calculatedLocation"];
                }
                
                [a setObject:[NSNumber numberWithInt:distanceA] forKey:@"calculatedDistance"];
                
                orderedGeoNews = [[NSMutableArray alloc] initWithObjects:a, nil];
                
            }
            /*Order de news by proximity*/
            
            /*Null news shop workaround*
             copy = [[NSMutableArray alloc] initWithArray:orderedGeoNews];
             if([copy count]){
             [copy filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
             return !([[evaluatedObject allKeys] containsObject:@"shouldErase"] && [[evaluatedObject objectForKey:@"shouldErase"] isEqualToString:@"yes"]);
             }]];
             }
             orderedGeoNews = copy;
             /*Null news shop workaround*/
        }
    /* Get, process, workaround and order NEWS */
    
    
    /* Concat filtered and ordered promos and news with ordered shops. Afterwards, order them all by their calculated distance provided they are to notify by geo. */
        NSMutableArray *notifiableElements;
        
        if(HAS_SHOPS){
            notifiableElements = orderedShops;
        }
        
        if(orderedGeoPromos != nil && (unsigned long)[orderedGeoPromos count] > 0){
            notifiableElements = (NSMutableArray *)[notifiableElements arrayByAddingObjectsFromArray:orderedGeoPromos];
        }
        
        if(orderedGeoNews != nil && (unsigned long)[orderedGeoNews count] > 0){
            notifiableElements = (NSMutableArray *)[notifiableElements arrayByAddingObjectsFromArray:orderedGeoNews];
        }

        if(HAS_SHOPS){
            notifiableElements = (NSMutableArray *)[notifiableElements sortedArrayUsingComparator:^NSComparisonResult(id a, id b){
                int distanceA = [[a objectForKey:@"calculatedDistance"] intValue];
                int distanceB = [[b objectForKey:@"calculatedDistance"] intValue];
                
                if(distanceA != -1 && distanceB != -1){//Ambas con geo
                    return distanceA <= distanceB ? NSOrderedAscending : NSOrderedDescending;
                } else if(distanceA != -1){//A tiene geo y B fecha
                    return NSOrderedAscending;
                } else if(distanceB != -1){//B tiene geo y A fecha
                    return NSOrderedDescending;
                } else {//Ambas tienen fecha
                    return NSOrderedSame;
                }
            }];
        }
    /* Concat filtered and ordered promos and news with ordered shops. Afterwards, order them all by their calculated distance provided they are to notify by geo. */
    
    /* Unsubscribe from any other geofence */
        NSLog(@"AMOUNT OF REGIONS MONITORED UP UNTIL NOW %lu", (unsigned long)[[_anotherLocationManager monitoredRegions] count]);
        for (CLRegion *monitored in [_anotherLocationManager monitoredRegions])
            [_anotherLocationManager stopMonitoringForRegion:monitored];
        NSLog(@"AMOUNT OF REGIONS MONITORED AFTER STOP MONITORING ALL REGIONS %lu", (unsigned long)[[_anotherLocationManager monitoredRegions] count]);
        if([[_anotherLocationManager monitoredRegions] count] != 0){
            for (CLRegion *monitored in [_anotherLocationManager monitoredRegions])
                NSLog(@"%@",monitored);
        }
    /* Unsubscribe from any other geofence */
    
    /* Set geofences */
        int relevantGeoFences = [self notifyOrSetPromoFromArray:notifiableElements withAlreadySetAmount:0];
        if(LOGS){
            /*Debug logging of geofences amount*/
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            localNotification.alertBody = [NSString stringWithFormat:@"Notify Near Promos is setting %d geofences", relevantGeoFences];
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            /*Debug logging of geofences amount*/
        }
    /* Set geofences */
    
    /* Release safety barrier */
        NSLog(@"AMOUNT OF REGIONS MONITORED AFTER NNP %lu", (unsigned long)[[_anotherLocationManager monitoredRegions] count]);
        notifyBarrier = false;
        NSLog(@"NOTIFY NEAR PROMOS CLOSE - NICELY");
    /* Release safety barrier */
}



-(int)notifyOrSetPromoFromArray:(NSMutableArray *)elements withAlreadySetAmount:(int)amount{
    
    /* IF - DEBUG is on, log how many elements came in the array */
    if(DEBUG){
        if([elements count] == 0)
            NSLog(@"NotifyOrSetPromoFromArray, no elements left. With an already set amount of elements of: %d", amount);
        else
            NSLog(@"NotifyOrSetPromoFromArray, trying with element: %@. With an already set amount of elements of: %d", [[elements objectAtIndex:0] valueForKey:@"notification_text"], amount);
    }
    /* IF - DEBUG is on, log how many elements came in the array */
    
    if([elements count] == 0)
        return amount;
    
    /* SET - variables */
    NSDictionary *elem = [elements objectAtIndex:0];
    int calculatedDistance = [[elem objectForKey:@"calculatedDistance"] intValue];
    int isOnlyDate = [[elem objectForKey:@"isDate"] intValue];
    int relevanceThreshold = 70000;
    /* SET - variables */
    
    /* IF - it is geolocalized element */
    if(isOnlyDate == 0){
        
        /*Prevent to register a geofence that is away for more than 50 km*/
        if(calculatedDistance > relevanceThreshold && RELEVANCE_FILTERS){
            
            NSLog(@"Would add geofence if but since it´s too far away, it is not considered relevant. It is %d meters away, while the threshold accepts less than %d.",calculatedDistance, relevanceThreshold);
            
            if([elements count] > 1)
                elements = (NSMutableArray *)[elements subarrayWithRange:NSMakeRange(1, [elements count] - 1)];
            else
                elements = [NSMutableArray array];
            return [self notifyOrSetPromoFromArray:elements withAlreadySetAmount:amount];
        }
        /*Prevent to register a geofence that is away for more than 50 km*/
        
        /*Prevent registering more than 18 geofences*/
        if(amount >= 18){
            
            NSLog(@"Would add geofence but there´s already 18 added.");
            
            if([elements count] > 1)
                elements = (NSMutableArray *)[elements subarrayWithRange:NSMakeRange(1, [elements count] - 1)];
            else
                elements = [NSMutableArray array];
            return [self notifyOrSetPromoFromArray:elements withAlreadySetAmount:amount];
        }
        amount++;
        /*Prevent registering more than 18 geofences*/
        
        /* Set variables */
        CLLocation *calculatedLocation = [elem objectForKey:@"calculatedLocation"];
        int geofenceRadius = [[elem valueForKey:@"geofenceRadius"] intValue];
        NSString *gfid = [[NSUUID UUID] UUIDString];
        NSString *nid = [elem valueForKey:@"_nid"];
        NSString *id = [elem valueForKey:@"_id"];
        /* Set variables */
        
        /* If its a shop´s fence you're already within, add a geofence regardless of whether you are within it or not; and move on */
        if(![[elem valueForKey:@"type"] isEqualToString:@"promos"] && ![[elem valueForKey:@"_type"] isEqualToString:@"content"]){
            
            //NSLog(@"WOULD BE ADDING A GEOFENCE THAT SETS BEACONS AROUND SHOP %@", [elem valueForKey:@"name"]);
            
            // Should also talk about beacons
            /*Tell db when element should not be notified anymore*
             NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
             [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
             NSString *date = [dateFormatter stringFromDate:[NSDate date]];
             [self.dbManager executeQuery:[NSString stringWithFormat:@"INSERT OR IGNORE INTO notificaciones (fecha,notificacionId, elementId, geofence) VALUES ('%@','%@','%@','%@')", date, nid, id, gfid]];
             /*Tell db when element should not be notified anymore*/
            
            /*CLRegion *region = [[CLRegion alloc] initCircularRegionWithCenter:[calculatedLocation coordinate] radius:geofenceRadius identifier:gfid];
             [self.anotherLocationManager startMonitoringForRegion:region];*/
            
            //TAKE THIS OUT WHEN BEACONS LOGIC WAS DEVELOPED
            amount--;
            
            if([elements count] > 1)
                elements = (NSMutableArray *)[elements subarrayWithRange:NSMakeRange(1, [elements count] - 1)];
            else
                elements = [NSMutableArray array];
            return [self notifyOrSetPromoFromArray:elements withAlreadySetAmount:amount];
        }
        /* If its a shop´s fence you're already within, add a geofence regardless of whether you are within it or not; and move on */
        
        /* Is already withing the required radius */
        if(calculatedDistance <= geofenceRadius){
            
            /* Catch distance calculation error */
            if(calculatedDistance == 0){
                NSLog(@"Would add geofence but it came with a calculated distance of 0 meters.");
                if([elements count] > 1)
                    elements = (NSMutableArray *)[elements subarrayWithRange:NSMakeRange(1, [elements count] - 1)];
                else
                    elements = [NSMutableArray array];
                return [self notifyOrSetPromoFromArray:elements withAlreadySetAmount:amount];
            }
            /* Catch distance calculation error */
            
            NSLog(@"ALREADY NOTIFYING CLOSE GEO: %@", [elem valueForKey:@"notification_text"]);
            
            /* Set variables */
            int distance = (int)(50.0 * floor(((float)calculatedDistance/50.0)+0.5));
            NSString *distanceUnit = distance > 1000 ? @"km" : @"m";
            distance = distance > 1000 ? distance / 1000 : distance;
            distance = distance == 0 ? 50 : distance;
            /* Set variables */
            
            /* Final geo notification when entered region */
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            NSMutableString *str = (NSMutableString *)[([[[elem valueForKey:@"notification_text"] isEqualToString:@""] ? [elem valueForKey:@"description_title"] : [elem valueForKey:@"notification_text"] stringByAppendingString:[NSString stringWithFormat:@" (Ya estás a %d%@)", distance, distanceUnit]]) stringByReplacingOccurrencesOfString:@"%" withString:@"%%"];
            localNotification.alertBody = str;
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.userInfo = [[NSDictionary alloc] initWithObjectsAndKeys: id, @"id", nid, @"nid", nil];
            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            /* Final geo notification when entered region */
            
            /* Save; this element has already been notified and shouldn´t be notified anymore */
            if(PREVENT_DUPLICATES){
                
                /*Tell db when element should not be notified anymore*/
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *date = [dateFormatter stringFromDate:[NSDate date]];
                int success = [self.dbManager executeQuery:[NSString stringWithFormat:@"INSERT OR IGNORE INTO notificaciones (fecha,notificacionId, elementId, geofence) VALUES ('%@','%@','%@','none')", date, nid, id]];
                /*Tell db when element should not be notified anymore*/
                
                if(success == 1){
                    NSLog(@"ERROR - Could not prevent element %@ from being duplicated in the future; could not insert into 'notificaciones' table.");
                    amount--;
                    return [self notifyOrSetPromoFromArray:elements withAlreadySetAmount:amount];
                }
            }
            /* Save; this element has already been notified and shouldn´t be notified anymore */
            
            /* Next iteration */
            amount--;
            if([elements count] > 1)
                elements = (NSMutableArray *)[elements subarrayWithRange:NSMakeRange(1, [elements count] - 1)];
            else
                elements = [NSMutableArray array];
            return [self notifyOrSetPromoFromArray:elements withAlreadySetAmount:amount];
            /* Next iteration */
        }
        /* Is already withing the required radius */
        
        /* Set this element to be notified upon entering geofence */
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *date = [dateFormatter stringFromDate:[NSDate date]];
        int success = [self.dbManager executeQuery:[NSString stringWithFormat:@"INSERT OR IGNORE INTO notificaciones (fecha,notificacionId, elementId, geofence) VALUES ('%@','%@','%@','%@')", date, nid, id, gfid]];

        
        if(success == 1){
            NSLog(@"ERROR - Could not set element %@ to be notified when the geofence is entered; could not insert into 'notificaciones' table.");
            amount--;
            return [self notifyOrSetPromoFromArray:elements withAlreadySetAmount:amount];
        }
        /* Set this element to be notified upon entering geofence */
        
        /* Set variables */
        CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:[calculatedLocation coordinate] radius:geofenceRadius identifier:gfid];
        region.notifyOnEntry = true;
        region.notifyOnExit = true;
        /* Set variables */
        
        NSLog(@"SETTING GEOFENCE FOR %@.", [elem valueForKey:@"notification_text"]);
        NSLog(@"%@, is gonna listen to a radius of %d", [elem valueForKey:@"notification_text"], geofenceRadius);
        [self.anotherLocationManager startMonitoringForRegion:region];
        
        /* Next iteration */
        if([elements count] > 1)
            elements = (NSMutableArray *)[elements subarrayWithRange:NSMakeRange(1, [elements count] - 1)];
        else
            elements = [NSMutableArray array];
        return [self notifyOrSetPromoFromArray:elements withAlreadySetAmount:amount];
        /* Next iteration */
    }
    /* IF - it is geolocalized element */
    
    /* ELSE IF - it is date tracked */
    else {
        /* Get time elapsed */
        NSString *start = [elem valueForKey:@"start_date"];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        NSDate *startdate = [dateFormat dateFromString:start];
        
        float elapsed = [startdate timeIntervalSinceNow] / 3600;
        /* Get time elapsed */
        
        /* IF - it the start date has passed, notify inmediately */
        if(elapsed < 0){
            NSLog(@"Showing ORDERED DATE ELEMENT: %@", [elem valueForKey:@"description_title"]);
            
            /*Notify about that specific element*/
            NSString *nid = [elem valueForKey:@"_nid"];
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            NSMutableString *str = (NSMutableString *)[([[[elem valueForKey:@"notification_text"] isEqualToString:@""] ? ([[elem valueForKey:@"type"] isEqualToString:@"promos"] ? [elem valueForKey:@"description_title"] : [elem valueForKey:@"description"]) : [elem valueForKey:@"notification_text"] stringByAppendingString:@" "]) stringByReplacingOccurrencesOfString:@"%" withString:@"%%"];
            localNotification.alertBody = str;
            localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            localNotification.userInfo = [[NSDictionary alloc] initWithObjectsAndKeys: [elem valueForKey:@"_id"], @"id", nid, @"nid", nil];
            //localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            /*Notify about that specific element*/
            
            /*Tell db when element should not be notified anymore*/
            if(PREVENT_DUPLICATES){
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *date = [dateFormatter stringFromDate:[NSDate date]];
                int success = [self.dbManager executeQuery:[NSString stringWithFormat:@"INSERT OR IGNORE INTO notificaciones (fecha,notificacionId, elementId, geofence) VALUES ('%@','%@','%@', 'none')", date, nid, [elem valueForKey:@"_id"]]];
                
                if(success == 1){
                    NSLog(@"ERROR - Could not prevent element from being duplicated in the future; could not insert into 'notificaciones' table.");
                    return [self notifyOrSetPromoFromArray:elements withAlreadySetAmount:amount];
                } else {
                    if([elements count] > 1)
                        elements = (NSMutableArray *)[elements subarrayWithRange:NSMakeRange(1, [elements count] - 1)];
                    else
                        elements = [NSMutableArray array];
                    return [self notifyOrSetPromoFromArray:elements withAlreadySetAmount:amount];
                }
            }
            /*Tell db when element should not be notified anymore*/
        }
        /* IF - it the start date has passed, notify inmediately */
        
        /* ELSE - Schedule for future notification */
        else {
            NSLog(@"Scheduling ORDERED DATE ELEMENT: %@", ([[elem valueForKey:@"type"] isEqualToString:@"promos"] ? [elem valueForKey:@"description_title"] : [elem valueForKey:@"description"]));
            
            /* Set variables */
            UIApplication *app = [UIApplication sharedApplication];
            Boolean wasAlreadyScheduled = false;
            NSArray *eventArray = [app scheduledLocalNotifications];
            /* Set variables */
            
            /* Find out whether the element was already scheduled */
            for(int i = 0; i < [eventArray count]; i++){
                UILocalNotification *oneEvent = [eventArray objectAtIndex:i];
                NSDictionary *userInfoCurrent = oneEvent.userInfo;
                NSString *someNid = [[userInfoCurrent valueForKey:@"nid"] stringValue];
                
                if([someNid isEqualToString:[[elem valueForKey:@"_nid"] stringValue]]){
                    wasAlreadyScheduled = true;
                    break;
                }
            }
            /* Find out whether the element was already scheduled */
            
            /* IF - it wasn´t scheduled yet */
            if(!wasAlreadyScheduled){
                /* Notify about that specific element */
                NSString *nid = [elem valueForKey:@"_nid"];
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = startdate;
                NSMutableString *str = (NSMutableString *)[([[[elem valueForKey:@"notification_text"] isEqualToString:@""] ? ([[elem valueForKey:@"type"] isEqualToString:@"promos"] ? [elem valueForKey:@"description_title"] : [elem valueForKey:@"description"]) : [elem valueForKey:@"notification_text"] stringByAppendingString:@" "]) stringByReplacingOccurrencesOfString:@"%" withString:@"%%"];
                localNotification.alertBody = str;
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.userInfo = [[NSDictionary alloc] initWithObjectsAndKeys: [elem valueForKey:@"_id"], @"id", nid, @"nid", @"true", @"wasScheduled", nil];
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                /* Notify about that specific element */
                
                NSLog(@"The scheduled element´s nid is: %@", [elem valueForKey:@"_nid"]);
                eventArray = [app scheduledLocalNotifications];
                for(int i = 0; i < [eventArray count]; i++){
                    UILocalNotification *oneEvent = [eventArray objectAtIndex:i];
                    NSDictionary *userInfoCurrent = oneEvent.userInfo;
                    NSString *someNid = [userInfoCurrent valueForKey:@"nid"];
                }
                
                /* Tell db when element should not be notified anymore */
                if(PREVENT_DUPLICATES){
                    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSString *date = [dateFormatter stringFromDate:[NSDate date]];
                    int success = [self.dbManager executeQuery:[NSString stringWithFormat:@"INSERT OR IGNORE INTO notificaciones (fecha,notificacionId, elementId, geofence) VALUES ('%@','%@','%@', 'scheduledDate')", date, nid, [elem valueForKey:@"_id"]]];
                    
                    if(success == 1){
                        NSLog(@"ERROR - Could not prevent element %@ from being duplicated in the future; could not insert into 'notificaciones' table.");
                        return [self notifyOrSetPromoFromArray:elements withAlreadySetAmount:amount];
                    } else {
                        if([elements count] > 1)
                            elements = (NSMutableArray *)[elements subarrayWithRange:NSMakeRange(1, [elements count] - 1)];
                        else
                            elements = [NSMutableArray array];
                        return [self notifyOrSetPromoFromArray:elements withAlreadySetAmount:amount];
                    }
                }
                /* Tell db when element should not be notified anymore */
            }
            /* IF - it wasn´t scheduled yet */
            
            /* ELSE */
            else {
                NSLog(@"Cancelled scheduling because it already was. ");
            }
            /* ELSE */
        }
        /* ELSE - Schedule for future notification */
    }
    /* ELSE IF - it is date tracked */
    
    return amount;
}

-(void) updateDB
{
    
    /*Get userID*/
    NSArray *json = [self.dbManager loadDataFromDB:@"SELECT * FROM usuario"];
    NSError *e = nil;
    
    if(json == nil || [json count] == 0 || [json[0] count] == 0 || [json[0][1] dataUsingEncoding:NSUTF8StringEncoding] == nil){
        NSLog(@"USER DB IS EMPTY");
        return;
    }
    
    NSArray *user = [NSJSONSerialization JSONObjectWithData:[json[0][1] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &e];
    NSString *userId = [user valueForKey:@"_id"] ? [user valueForKey:@"_id"] : [user valueForKey:@"id"];
    NSLog(@"USER ID: %@", userId);
    NSString *token = [user valueForKey:@"token"];
    NSLog(@"TOKEN %@", token);
    /*Get userID*/
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: [[NSString stringWithFormat:@"data=eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJwcm9tb3MiLCJhY3Rpb24iOiJfbGlzdCIsInB1YmxpY2tleSI6Im5rZnZ0N2dwZ2RrYWt5Yjc4Z3I5In0=&hash=2dac8a861c17ddc05408de5e4d932bc4&userId=%@&publickey=nkfvt7gpgdkakyb78gr9",userId] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
    
    //[request addValue:[@"Bearer " stringByAppendingString:token] forHTTPHeaderField:@"Authorization"];
    
    /*Setting queried URL*/
    NSURLComponents *url = [[NSURLComponents alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat: @"http://%@.reachout.global/api?", SERVER_NAME_LOC]] resolvingAgainstBaseURL:YES];
    NSMutableArray *queryItems = NSMutableArray.new;
    
    NSDictionary *params = [[NSDictionary alloc]initWithObjectsAndKeys:@"nkfvt7gpgdkakyb78gr9",@"publickey",@"eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJwcm9tb3MiLCJhY3Rpb24iOiJfbGlzdCIsInB1YmxpY2tleSI6Im5rZnZ0N2dwZ2RrYWt5Yjc4Z3I5In0=",@"data",@"2dac8a861c17ddc05408de5e4d932bc4",@"hash",userId,@"userId", nil];
    
    [params enumerateKeysAndObjectsUsingBlock:^(NSString *name, NSString *value, BOOL *stop){
        [queryItems addObject:[NSURLQueryItem queryItemWithName:name value: value]];//[value stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]]];
    }];
    url.queryItems = queryItems;
    NSLog(@"URL TO BE USED: %@", url.URL);
    [request setURL:url.URL];
    /*Setting queried URL*/
    
    NSError *error = nil;
    NSHTTPURLResponse *responseCode = nil;
    
    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
    
    if([responseCode statusCode] != 200){
        
        /*Debug logging of geofences amount*/
        if(LOGS){
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        localNotification.alertBody = [NSString stringWithFormat:@"SLC Update ended with error. Found error: %@", responseCode];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        }
        /*Debug logging of geofences amount*/
        
        [self addLogWithAction:@"056" andId:NULL];
        
        NSLog(@"Error getting updated contents, HTTP status code %@",responseCode);
        
    } else if(oResponseData != nil){
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:oResponseData options:kNilOptions error:&error];
        
        /*Debug logging of geofences amount*/
        if(LOGS){
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        localNotification.alertBody = [NSString stringWithFormat:@"SLC Update ended succesfully. Found promos: %lu", [[[result objectForKey:@"promos"] objectForKey:@"promos"] count]];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        }
        /*Debug logging of geofences amount*/
        
        /*Save new data in db*/
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *date = [dateFormatter stringFromDate:[NSDate date]];
        
        NSMutableArray *promosStrs = [[NSMutableArray alloc] init];
        for(NSDictionary *d in [[result objectForKey:@"promos"] objectForKey:@"promos"]){
            if(d != nil)
            [promosStrs addObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:d options:kNilOptions error:&error] encoding:NSUTF8StringEncoding]];
        }
        NSString *promos = [[[@"[" stringByAppendingString:[promosStrs componentsJoinedByString:@","]] stringByAppendingString:@"]"] stringByReplacingOccurrencesOfString:@"\'" withString:@"\\\""];
        
        ///////////////////////////////////////////////////////////////////////////////////////////
        NSArray *json = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM promos"]]];
        if(json == nil || [json count] == 0 || [json[0] count] == 0 || [json[0][2] dataUsingEncoding:NSUTF8StringEncoding] == nil)
            return;
        
        NSArray *allPromos = [NSJSONSerialization JSONObjectWithData:  [json[0][2] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &e];
        NSLog(@"Amount of promos previously in db: %lu", (unsigned long)[allPromos count]);
        
        [self.dbManager executeQuery:[NSString stringWithFormat:@"UPDATE promos SET content=('%@')",promos]];
        
        json = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM promos"]]];
        if(json == nil || [json count] == 0 || [json[0] count] == 0 || [json[0][2] dataUsingEncoding:NSUTF8StringEncoding] == nil)
            return;
        allPromos = [NSJSONSerialization JSONObjectWithData:  [json[0][2] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &e];
        NSLog(@"Current amount of promos in db, after insertion or deletion: %lu", (unsigned long)[allPromos count]);
    } else {
        [self addLogWithAction:@"055" andId:NULL];
    }
    
}

-(NSString *) getLat {
    return [NSString stringWithFormat:@"%@",[[NSNumber alloc] initWithDouble:(double)self.myLocation.latitude]];
}

-(NSString *) getLng {
    return [NSString stringWithFormat:@"%@",[[NSNumber alloc] initWithDouble:(double)self.myLocation.longitude]];
}

-(void) addLogWithAction:(NSString *)action andId:(NSString *)elementId {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSString *lat = [self getLat];
    NSString *lng = [self getLng];
    
    if(lat && lng && ![lat isEqualToString:@"0"] && ![lng isEqualToString:@"0"]){
        //NSLog(@"LAT: %@, LNG: %@", lat, lng);
    } else {
        //NSLog(@"NO LAT: %@, NO LNG: %@", lat, lng);
        return;
    }
    
    NSInteger category = [[action substringWithRange:NSMakeRange(0, 1)] integerValue];
    NSInteger act = [[action substringWithRange:NSMakeRange(1, 2)] integerValue];
    NSString * _category;
    NSString * _act;
    
    NSLog(@"%ld %ld", (long)category, (long)act);
    
    switch(category){
        case 0:
            _category = @"Errors";
            
            switch(act){
                case 50:
                    _act = @"Points Request: Server Error";
                    break;
                case 51:
                    _act = @"Points Request: Server Not Responding";
                    break;
                case 52:
                    _act = @"Promotions Request: Server Error";
                    break;
                case 53:
                    _act = @"Promotions Request: Server Not Responding";
                    break;
                case 54:
                    _act = @"Promotions Request: Promos Not Saved to local DB";
                    break;
                case 55:
                    _act = @"Promotions Request: Server Not Response From Service";
                    break;
                case 56:
                    _act = @"Promotions Request: Server Success False from Service";
                    break;
                case 57:
                    _act = @"Promotions Request: Promotion Array Empty";
                    break;
                case 58:
                    _act = @"Contents Request: Server Error";
                    break;
                case 59:
                    _act = @"Contents Request: Server Not Responding";
                    break;
                case 60:
                    _act = @"Contents Request: Contents Not Saved to Local DB";
                    break;
                case 61:
                    _act = @"Contents Request: Server Not Response From Service";
                    break;
                case 62:
                    _act = @"Contents Request: Server Success False from Service";
                    break;
                case 63:
                    _act = @"Contents Request: Contents Array Empty";
                    break;
                case 64:
                    _act = @"Shops Request: Server Error";
                    break;
                case 65:
                    _act = @"Shops Request: Server Not Responding";
                    break;
                case 66:
                    _act = @"Shops Request: Not Response from Service";
                    break;
                case 67:
                    _act = @"Shops Request: Shops Not Saved to Local DB";
                    break;
                case 68:
                    _act = @"Shops Request: Server Success False from Service";
                    break;
                case 69:
                    _act = @"Profile Request: Server Error";
                    break;
                case 70:
                    _act = @"Profile Request: Server Not Responding";
                    break;
                case 71:
                    _act = @"Promotion Redemption Error: Server Error";
                    break;
                case 72:
                    _act = @"Promotion Redemption Failed: Server Not Responding";
                    break;
                case 73:
                    _act = @"Logs Request: Server Not Response from Service";
                    break;
                case 74:
                    _act = @"Categories Request: Server Not Response from Service";
                    break;
                case 75:
                    _act = @"Categories Request: Categories Not Saved to Local DB";
                    break;
                case 76:
                    _act = @"Categories Request: Categories Not Saved to Local DB Service";
                    break;
            }
            
            break;
        case 1:
            _category = @"App";

            switch(act){
                case 0:
                    _act = @"Session started";
                    break;
            }
            
            break;
        case 2:
            _category = @"Menu";

            switch(act){
                case 1:
                    _act = @"Feature tapped";
                    break;
                case 2:
                    _act = @"Promotions tapped";
                    break;
                case 3:
                    _act = @"News tapped";
                    break;
                case 4:
                    _act = @"Shops tapped";
                    break;
                case 5:
                    _act = @"E-shop tapped";
                    break;
                case 6:
                    _act = @"Help Us tapped";
                    break;
                case 7:
                    _act = @"Contact tapped";
                    break;
                case 8:
                    _act = @"Profile tapped";
                    break;
                case 9:
                    _act = @"Notifications tapped";
                    break;
                case 10:
                    _act = @"FAQ tapped";
                    break;
                case 11:
                    _act = @"Category tapped";
                    break;
                case 12:
                    _act = @"Log Out tapped";
                    break;
            }
            
            break;
        case 3:
            _category = @"Promotions";
            
            switch(act){
                case 0:
                    _act = @"Promotion Preview";
                    break;
                case 1:
                    _act = @"Promotion Opened";
                    break;
                case 2:
                    _act = @"No Code Promotion Redeemed";
                    break;
                case 3:
                    _act = @"E-shop Promotion Redeemed";
                    break;
                case 4:
                    _act = @"Code Promotion Redeemed Success";
                    break;
                case 5:
                    _act = @"Code Promotion Redeemed Step 1";
                    break;
                case 6:
                    _act = @"Code Promotion Redeemed Step 2 Accepted";
                    break;
                case 7:
                    _act = @"Code Promotion Redeemed Step 2 Cancelled";
                    break;
                case 8:
                    _act = @"Code Promotion Redeemed Step 2 Insufficient Points";
                    break;
                case 10:
                    _act = @"Promotion Notification Delivered";
                    break;
                case 11:
                    _act = @"Promotion Notification Opened";
                    break;
                case 12:
                    _act = @"Promotion Notification Closed";
                    break;
                case 21:
                    _act = @"Share: Facebook";
                    break;
                case 22:
                    _act = @"Share: Instagram";
                    break;
                case 23:
                    _act = @"Share: Twitter";
                    break;
            }
            
            break;
        case 4:
            _category = @"Contents";

            switch(act){
                case 0:
                    _act = @"Content Preview";
                    break;
                case 1:
                    _act = @"Content Opened";
                    break;
                case 10:
                    _act = @"Content Notification Delivered";
                    break;
                case 11:
                    _act = @"Content Notification Opened";
                    break;
                case 12:
                    _act = @"Content Notification Closed";
                    break;
                case 21:
                    _act = @"Share: Facebook";
                    break;
                case 22:
                    _act = @"Share: Instagram";
                    break;
                case 23:
                    _act = @"Share: Twitter";
                    break;
                case 2:
                    _act = @"Content Video Played";
                    break;
            }
            
            break;
        case 5:
            _category = @"Shops";
            
            switch(act){
                case 0:
                    _act = @"Shop Preview";
                    break;
                case 1:
                    _act = @"Shop Opened";
                    break;
                case 2:
                    _act = @"Shop How to Get There";
                    break;
            }
            
            break;
        case 6:
            _category = @"Permissions";

            switch(act){
                case 0:
                    _act = @"Notifications Enabled";
                    break;
                case 1:
                    _act = @"Notifications Disabled";
                    break;
                case 10:
                    _act = @"GPS Enabled";
                    break;
                case 11:
                    _act = @"GPS Disabled";
                    break;
            }
            
            break;
        case 7:
            _category = @"Beacons";

            switch(act){
                case 0:
                    _act = @"Beacon Battery Percentage";
                    break;
            }
            
            break;
        case 8:
            _category = @"Profile";
            
            switch(act){
                case 0:
                    _act = @"Profile Updated";
                    break;
            }
            
            break;
        case 9:
            _category = @"Brand Specifics";

            switch(act){
                case 0:
                    _act = @"Grisino: Menu Games Tapped";
                    break;
                case 1:
                    _act = @"Grisino: Grisino Fan CTA Tapped";
                    break;
            }
            
            break;
    }
    
    NSLog(@"%@ %@", _category, _act);
    
    // May return nil if a tracker has not already been initialized with a property
    // ID.
    NSLog(@"Google Analytics sending a tracker...");
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    NSLog(@"%@",tracker);
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:_category
                                                          action:_act
                                                           label:@"Label"
                                                           value:nil] build]];
    
    NSString *logSTR;
    if(elementId != NULL){
        logSTR = [NSString stringWithFormat:@"{\"date\": \"%@\", \"IP\": \"%@\", \"lat\": \"%f\", \"lng\": \"%f\", \"logId\": %@, \"elementId\": %@}", [dateFormat stringFromDate:[NSDate date]], [self getIP], [lat floatValue], [lng floatValue], action, elementId];
    } else {
        logSTR = [NSString stringWithFormat:@"{\"date\": \"%@\", \"IP\": \"%@\", \"lat\": \"%f\", \"lng\": \"%f\", \"logId\": %@}", [dateFormat stringFromDate:[NSDate date]], [self getIP], [lat floatValue], [lng floatValue], action];
    }
    
    NSLog(@"ADDING %@", logSTR);
    
    [self.dbManager executeQuery:[NSString stringWithFormat:@"INSERT OR IGNORE INTO logs (log) VALUES ('%@')", logSTR]];
}

-(NSString *)getIP {
    NSString *address = @"IP";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    
    success = getifaddrs(&interfaces);
    if(success == 0){
        while(temp_addr != NULL){
            if(temp_addr->ifa_addr->sa_family == AF_INET){
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]){
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    freeifaddrs(interfaces);
    return address;
}

-(NSString *) concatString:(NSString*)a toString:(NSString *)b {
    return [a stringByAppendingString:b];
}

-(void)sendLogs {

    /*if(self.dbManager == NULL){
        self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"iLoveIt.db"];
        NSLog(@"EPAAA");
     
        if(LOGS){
            /*Debug logging of geofences amount*
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            localNotification.alertBody = [NSString stringWithFormat:@"Did not find dbManager when updating DB on a SLC."];
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            /*Debug logging of geofences amount*
        }
        
        return;
    }*/
    
    /*Get userID*/
    NSArray *userJSON = [self.dbManager loadDataFromDB:@"SELECT * FROM usuario"];
    NSError *userERROR = nil;
    
    NSLog(@"userJSON count: %lu", [userJSON count]);
    //NSLog(@"userJSON: %@", userJSON);
    if([userJSON count] == 0 || [userJSON[0] count] == 0 || [userJSON[0][1] dataUsingEncoding:NSUTF8StringEncoding] == nil){
        NSLog(@"USER DB IS EMPTY");
        return;
    }
    NSArray *user = [NSJSONSerialization JSONObjectWithData:[userJSON[0][1] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &userERROR];
    NSString *userID = [user valueForKey:@"_id"] ? [user valueForKey:@"_id"] : [user valueForKey:@"id"];
    NSString *token = [user valueForKey:@"token"];
    NSLog(@"_id: %@", [user valueForKey:@"_id"]);
    NSLog(@"id: %@", [user valueForKey:@"id"]);
    
    if(![user valueForKey:@"id"] && ![user valueForKey:@"_id"]){
        NSLog(@"NO ID IN USER");
        return;
    }
    NSLog(@"USER ID: %@", userID);
    /*Get userID*/
    
    /*Get logs*/
    NSArray *logsJSON = [self.dbManager loadDataFromDB:@"SELECT * FROM logs"];
    
    if([logsJSON count] == 0){
        NSLog(@"LOGS DB IS EMPTY");
        return;
    } else {
        NSLog(@"%lu",(unsigned long)[logsJSON count]);
        //SHOULD ERASE LOGS TABLE
    }
    
    NSMutableArray *a = [[NSMutableArray alloc] init];
    for(int i = 0; i < (unsigned long)[logsJSON count]; i++)
        [a addObject:logsJSON[i][1]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSString *logsString = [NSString stringWithFormat:@"{\"userID\":\"%@\", \"platform\":\"iOS\",\"brand\":\"%@\",\"positions\":[{\"date\":\"%@\",\"lng\":\"%@\", \"lat\":\"%@\"}], \"logs\":[%@]}",userID, BRAND, [dateFormat stringFromDate:[NSDate date]], [self getLat], [self getLng], [a componentsJoinedByString:@","]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: [[NSString stringWithFormat:@"data=eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJsb2dzIiwiYWN0aW9uIjoicHVzaCIsInB1YmxpY2tleSI6Im5rZnZ0N2dwZ2RrYWt5Yjc4Z3I5IiwiYnJhbmQiOiJHcmlzaW5vIn0=&hash=fd93177a379b1b97c378f4bc5239b3f0&userId=%@&publickey=nkfvt7gpgdkakyb78gr9&log=%@",userID, logsString] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
    
    /*Setting queried URL*/
    NSURLComponents *url = [[NSURLComponents alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@.reachout.global/api?", SERVER_NAME_LOC]] resolvingAgainstBaseURL:YES];
    
    NSMutableArray *queryItems = NSMutableArray.new;
    
    NSDictionary *params = [[NSDictionary alloc]initWithObjectsAndKeys:@"nkfvt7gpgdkakyb78gr9",@"publickey",@"eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJsb2dzIiwiYWN0aW9uIjoicHVzaCIsInB1YmxpY2tleSI6Im5rZnZ0N2dwZ2RrYWt5Yjc4Z3I5IiwiYnJhbmQiOiJHcmlzaW5vIn0=",@"data",@"fd93177a379b1b97c378f4bc5239b3f0",@"hash",userID,@"userId", nil];
    [params enumerateKeysAndObjectsUsingBlock:^(NSString *name, NSString *value, BOOL *stop){
        [queryItems addObject:[NSURLQueryItem queryItemWithName:name value: value]];//[value stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]]];
    }];
    url.queryItems = queryItems;
    
    //NSLog(@"URL TO BE USED: %@", url.URL);
    [request setURL:url.URL];
    /*Setting queried URL*/
    
    NSError *error = nil;
    NSHTTPURLResponse *responseCode = nil;
    
    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
    
    if([responseCode statusCode] != 200){
        
        if(LOGS){
        /*Debug logging of geofences amount*/
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.alertBody = [NSString stringWithFormat:@"SLC Logs sending ended with error. Found error: %ld",(long)[responseCode statusCode]];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        /*Debug logging of geofences amount*/
        }
        
        NSLog(@"Error sending logs, HTTP status code %ld",(long)[responseCode statusCode]);
        
    } else if(oResponseData != nil){
        if(LOGS){
        /*Debug logging of geofences amount*/
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.alertBody = @"SLC Logs sent succesfully.";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        /*Debug logging of geofences amount*/
        }
        //ERASE NO LONGER NEEDED LOGS
        NSLog(@"ERASING LOGS");
        [self.dbManager executeQuery:@"DROP TABLE IF EXISTS logs"];
        [self.dbManager executeQuery:@"CREATE TABLE IF NOT EXISTS logs (_id INTEGER NOT NULL PRIMARY KEY, log TEXT)"];
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:oResponseData options:kNilOptions error:&error];
        
        NSLog(@"RESULT %@", result);
    } else {
        [self addLogWithAction:@"073" andId:NULL];
    }
}


@end
