/*Globals*/
    var
    SERVER = "prod",
    SERVER_DEFAULT = SERVER,
    BRAND = 'Grisino',
    USEPLUGINS = false,
    IN_APP_BROWSER_OPTIONS = {
        location: 'no',
        clearcache: 'no',
        toolbarposition: 'top',
        toolbar: 'yes',
        closebuttoncaption: 'X'
    },
    IN_APP_THEMEABLE_BROWSER_OPTIONS = {
        statusbar: {
            color: '#ffffff'
        },

        toolbar: {
            height: 70,
            color: '#d60036'
        },

        title: {
            showPageTitle: false
        },
        backButton: {
            image: 'back',
            imagePressed: 'back_pressed',
            align: 'left'
        },
        forwardButton: {
            image: 'forward',
            imagePressed: 'forward_pressed'
        },
        closeButton: {
            image: 'close',
            imagePressed: 'close_pressed',
            align: 'right',
            event: 'closePressed'
        },
        backButtonCanClose: false
    },
    DB_NAME = "iLoveit.db",
    VERSION = 0,
    TOKEN = '',
    TOKEN_DEBUG = true,
    SCROLL_THRESHOLD = 200,
    IOSVERSION,
    EULA_URL = 'http://www.grisino.com/terminosycondicionesfan',
    HARDCODED_GUEST = {
        mail: 'invitado@grisino.com',
        pass: 'fellowguest'
    };

    IN_APP_BROWSER_OPTIONS = IN_APP_THEMEABLE_BROWSER_OPTIONS;
    var interval_ConnectionCheck = undefined;
    var interval_NotificationCheck = undefined;
    var detailed_info;
/*Globals*/

/*First tweaks*/

    $(".annoyingMessage").slideUp();
/*First tweaks*/

var app = angular.module('Grisino', [
    // Angular plugins
    'mgcrea.pullToRefresh',
    'ngCordova',
    'ngAnimate',
    'ng-fastclick',
    'hmTouchEvents',
    'ngCookies',
    'ngCookies',
    'pascalprecht.translate',
    'ui.router',
    'ngMaterial',
    'ngMessages',
    'ngRoute',
    'ngResource',
    'ui.bootstrap',
    'yaru22.angular-timeago',
    // Application controllers  
    'Login',
    'Eula',
    'notifications',
    'PromosTypes',
    'Detail',
    'Juegos',
    'Join',
    'userDNI',
    'Main',
    'Camera',
    'Home',
    'Profile',
    'MainPanel',
    'NewsTypes',
    'ShopsTypes',
    'datosContacto'
]);
app.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('main.home', {
            url: '/home',
            templateUrl: 'screens/home.html',
            controller: 'homeCtrl'
        })
        .state('login', {
            url: '/login',
            templateUrl: 'screens/login.html',
            controller: 'loginCtrl'
        })
        .state('eula', {
            url: '/eula',
            templateUrl: 'screens/eula.html',
            controller: 'eula'
        })
        .state('join', {
            url: '/join',
            templateUrl: 'screens/join.html',
            controller: 'joinCtrl'
        })
        .state('userDNI', {
            url: '/userDNI',
            templateUrl: 'screens/userDNI.html',
            controller: 'userDNICtrl'
        })
        .state('main', {
            url: '/main',
            templateUrl: 'screens/main.html',
            controller: 'mainCtrl'
        })
        .state('main.newsTypes', {
            url: '/newsTypes',
            templateUrl: 'screens/newsTypes.html',
            controller: 'newsTypesCtrl'
        })
        .state('main.profile', {
            url: '/profile',
            templateUrl: 'screens/profile.html',
            controller: 'profileCtrl'
        })
        .state('main.camera', {
            url: '/camera',
            templateUrl: 'screens/camera.html',
            controller: 'cameraCtrl'
        })
        .state('main.promosTypes', {
            url: '/promosTypes',
            templateUrl: 'screens/promosTypes.html',
            controller: 'promosTypesCtrl'
        })
        .state('main.detail', {
            url: '/detail',
            templateUrl: 'screens/detail.html',
            controller: 'detailCtrl'
        })
        .state('main.juegos', {
            url: '/juegos',
            templateUrl: 'screens/juegos.html',
            controller: 'juegosCtrl'
        })
        .state('main.shopsTypes', {
            url: '/shopsTypes',
            templateUrl: 'screens/shopsTypes.html',
            controller: 'shopsTypesCtrl'
        })
        .state('mainPanel', {
            url: '/mainPanel',
            templateUrl: 'screens/mainPanel.html',
            controller: 'mainPanelCtrl'
        })
        .state('main.datosContacto', {
            url: '/datosContacto',
            templateUrl: 'screens/datosContacto.html',
            controller: 'datosContactoCtrl'
        })
        .state('main.notifications', {
            url: '/notifications',
            templateUrl: 'screens/notifications.html',
            controller: 'notificationsCtrl'
        });
});
app.config(function($translateProvider) {

    $translateProvider.translations('en_EN', {
        'GREETING': 'Welcome',
        'START': 'START',
        'EXIT': 'Exit',
    });


    $translateProvider.translations('es_ES', {
        'GREETING': 'Bienvenido',
        'START': 'INICIO',
        'EXIT': 'Salir',
    });


    $translateProvider.translations('pt_PT', {
        'GREETING': 'Bem-vindo',
        'START': 'COMEÇO',
        'EXIT': 'Saída',
    });

    $translateProvider.preferredLanguage('en_EN');
});
app.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('blue')
        .accentPalette('red');
});

app.run(function($rootScope, $cordovaGeolocation, $cordovaDevice, $cordovaSQLite, $cordovaNetwork, $location, $cordovaSocialSharing, $cordovaFacebook, $mdDialog, $mdSidenav, $cordovaLocalNotification, $timeout, $cordovaInAppBrowser, $http, $compile, $mdToast) {

    /*-------------------------VARIABLES-------------------------*/

    /*Initializing rootScope*/
        $rootScope.btnIsMenu = true;
        $rootScope.cards = [];
        $rootScope.user;
        $rootScope.showCamera = false;

        /*Should be an object, but currently represents the only config available in profile*/
        $rootScope.config = 'on';
        /*Should be an object, but currently represents the only config available in profile*/

        $rootScope.isPopUp = false;
        $rootScope.isInAppBrowserOpen = false;
        
        // Shows subcategory icon
        $rootScope.shouldShowIcon = true;

        // Whether state name or logo shows
        $rootScope.stateToggle = false;

        //Initializes hidden loading screen
        $rootScope.loading = false;
        var _loadingTimeout = "showing";
    /*Initializing rootScope*/

        $rootScope.$watch('showCamera', function(newValue, oldValue){
            if(newValue === true)
                $(".view").addClass('bg-transparent');
            else if (newValue === false)
                $(".view").removeClass('bg-transparent');
        });

    /*Initialize loading listener*/
        $rootScope.$watch('loading', function(newValue, oldValue) {
            if(!newValue){
                if(_loadingTimeout == "showing"){
                    console.log("No loading timeout yet it´s showing; hiding now.");
                    $('.calculated.loading-content').fadeOut(300);
                    $('.calculated.loading-back').fadeOut(300);

                    if($(".content").hasClass("noscroll"))
                        $(".content").removeClass("noscroll");

                    clearTimeout($rootScope.loadingTimeout);

                    _loadingTimeout = undefined;
                } else if(_loadingTimeout){
                    console.log("Cancelling loadingTimeout");
                    clearTimeout(_loadingTimeout);
                    _loadingTimeout = undefined;
                }
            } else if(!_loadingTimeout){
                console.log("Starting loadingTimeout");
                _loadingTimeout = setTimeout(function(){
                    $('.calculated.loading-content').fadeIn(300);
                    $('.calculated.loading-back').fadeIn(300);

                    if(!$(".content").hasClass("noscroll"))
                    $(".content").addClass("noscroll");

                    $rootScope.loadingTimeout = setTimeout(function() {
                        console.log("Error de datos.");
                        $rootScope.loading = false;
                    }, 28000);

                    _loadingTimeout = "showing";
                },2000);
            }
        }); 

    /*Initializing states object*/
        var path = $location.$$path.split("/");
        $rootScope.states = path.length > 2 ? [path[1] + "." + path[2]] : [path[1]];
    /*Initializing states object*/

    /*Initializing index*/
        $rootScope.navIndex = {
            category: 0,
            subCategory: []
        };
    /*Initializing index*/

    /*--------------------------METHODS--------------------------*/

    /*Scroll whatever list is on to top*/
        window.scrollToTop = function(){
            console.log("HOLA");
            $(".content").animate({scrollTop:0},500);
        }
    /*Scroll whatever list is on to top*/

    /*Update db on location significant change*/
        window.updateDB = function() {
            /*Update dbs*/
            setTimeout(function() {
                if ($rootScope.restartBuffer)
                    $rootScope.restartBuffer();

                $rootScope.actualizamosCache();
            }, 1000);
            /*Update dbs*/
        };
    /*Update db on location significant change*/

    /*Update notifications*/
        //setTimeout(function(){$rootScope.updateNotis();},7000);
        $rootScope.updateNotis = function() {
            $rootScope.notis = [];
            var notified = [];

            var query = "SELECT * FROM notificaciones";

            /*Check if notification was already notified*/
            $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {

                if (res.rows.length != 0) {
                    console.log("Hay notificaciones array: ");
                    for (var i = 0; i < res.rows.length; i++) {
                        console.log(res.rows.item(i).content);
                        if (!res.rows.item(i).content)
                            continue;
                        notified = JSON.parse(res.rows.item(i).content);
                    }

                    //Look for promos matching de nids of the ones found to be already notified

                    for (var i = 0; i < $rootScope.promosCache.length; i++) {
                        var e = $rootScope.promosCache[i];
                        var found = false;

                        for (var j = 0; j < notified.length; j++)
                            found = notified[j].notificacionId == e._nid;

                        console.log($rootScope.promosCache[i]);
                        console.log("notified number #" + j + " nid: " + notified[j].notificacionId + " == promo number #" + i + " nid: " + e._nid + " ¿?");

                        if (e.triggers.hotspot_data == null || !e.triggers.hotspot_data) {
                            //$rootScope.notis.push($rootScope.scheduleSingleNotification(e.id, e.start_date, e.name, e.description_title));
                        }

                    }
                    console.log($rootScope.notis);

                } else {
                    console.log("Update Notis: " + JSON.stringify(res));
                }
            }, function(err) {});
            /*Check if notification was already notified*/
        }
    /*Update notifications*/

    /*Schedule notification*/
        $rootScope.scheduleSingleNotification = function(id, date, name, description) {

            console.log(id);
            var noti = {
                id: id,
                title: name,
                text: description,
                at: new Date(date),
                data: {
                    id: id
                }
            };

            $cordovaLocalNotification.update(noti).then(function(result) {});

            return noti;
        };
    /*Schedule notification*/

    /*Upon having clicked a local notification*/
        $rootScope.hide_NotificationError = function(){

            $(".content").removeClass("blur");

            $(".modal.notification-error.popup").fadeOut("slow", function(){
                $(".modal.notification-error.popup").css("top", "100%");
                $rootScope.toggleAnnoyingMessage(!$rootScope.isGrisinoFan(), true);
            });
        }

        window.notifyPromo = function(json){
            console.log("Notified a promo. Checking for user...");
            console.log($rootScope.user);

            if(!$rootScope.user || !json)
                return;

            var promo = JSON.parse(json);
            var id = promo.id;//(json.split(':')[0]).slice(2).slice(0,-1);
            
            if(!promo){
                console.log("Tried to notify internally an element without any userInfo configured in the localNotification object.");
                return;
            }

            console.log("detailed promo:");
            console.log(promo);

            var dataObj = {
                data: "eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJwcm9tb3MiLCJhY3Rpb24iOiJfbGlzdCIsInB1YmxpY2tleSI6Ijdlcmc1bXV6bG53NXRoYmZwNGYxIn0=",
                hash: "b10dbf5f24d04462eef212254702bb0c",
                userId: $rootScope.user.id || $rootScope.user._id
            };

            var res = $http.post('http://' + SERVER + '.reachout.global/api?publickey=nkfvt7gpgdkakyb78gr9', dataObj);

            var serverNotResponding = setTimeout(function(){
                var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "053"};
                $rootScope.addLog(_thelog);
            }, 10000);

            res.success(function(data, status, headers, config) {

                console.log("Succesfully brought the newest promos possible to check if notification is still valid.");
                clearTimeout(serverNotResponding);
                /*
                // Saving promos
                //BROKEN SHOPS WORKAROUND
                    data.promos.promos = data.promos.promos.filter(function(promo){
                        if (promo.audience.shops.length > 0) {
                            var hasAtLeastOne = false;
                            for (var j = 0; j < promo.audience.shops.length; j++) {
                                if(promo.audience.shops[j].map.lat && promo.audience.shops[j].map.lat != null){
                                    hasAtLeastOne = true;
                                    break;
                                }
                            }

                            return hasAtLeastOne;
                        }
                    });
                    $rootScope.promosCache = data.promos.promos;
                //BROKEN SHOPS WORKAROUND
                */
                

                if(data.promos.promos.length == 0){
                    var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "057"};
                    $rootScope.addLog(_thelog);
                }

                if (data.promos) {
                    var query = "UPDATE promos SET content=(?)";
                    console.log("got here");
                    $cordovaSQLite.execute($rootScope.db, query, [JSON.stringify(data.promos.promos, null, 4)]).then(function(res) {
                        $rootScope.promosCache = data.promos.promos;

                        var element;
                        for (var i = 0; i < $rootScope.promosCache.length; i++)
                            if ($rootScope.promosCache[i]._id == id || $rootScope.promosCache[i].id == id)
                                element = $rootScope.promosCache[i];

                        if(element){
                            
                            /*$mdToast.show(
                                $mdToast.simple()
                                .content(element.notification_text)
                                .position('top')
                                .hideDelay(3000)
                            );*/

                            $rootScope.toggleAnnoyingMessage(false, true);
                            setTimeout(function(){
                                $mdToast.show(
                                    $mdToast.simple()
                                    .content(element.notification_text)
                                    .position('top')
                                    .hideDelay(3000)
                                );

                                setTimeout(function(){
                                    $rootScope.toggleAnnoyingMessage(!$rootScope.isGrisinoFan(), true);
                                }, 3000);
                            }, 600);


                            console.log("HEREE " + element.notification_text);

                        } else {
                            console.log("Promo wasnt found. Trying with news...");

                            var element;
                            for (var i = 0; i < $rootScope.contentCache.length; i++)
                                if ($rootScope.contentCache[i]._id == id || $rootScope.contentCache[i].id == id)
                                    element = $rootScope.contentCache[i];

                            if(element){
                                
                                /*$mdToast.show(
                                    $mdToast.simple()
                                    .content(element.notification_text)
                                    .position('top')
                                    .hideDelay(3000)
                                );*/

                                $rootScope.toggleAnnoyingMessage(false, true);
                                setTimeout(function(){
                                    $mdToast.show(
                                        $mdToast.simple()
                                        .content(element.notification_text || element.title)
                                        .position('top')
                                        .hideDelay(3000)
                                    );

                                    setTimeout(function(){
                                        $rootScope.toggleAnnoyingMessage(!$rootScope.isGrisinoFan(), true);
                                    }, 3000);
                                }, 600);


                                console.log("HEREE " + element.notification_text);

                            } else {
                                console.log("Element wasnt found");
                            }
                        }

                    }, function(err) {
                        var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "054"};
                        $rootScope.addLog(_thelog);
                    });
                }
            });

            res.error(function(data, status, headers, config) {
                clearTimeout(serverNotResponding);

                var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "052"};
                $rootScope.addLog(_thelog);
            });
        }

        window.detailNotifiedPromo = function(json){
            if(!$rootScope.user)
                return;

            if(!JSON.parse(json)){
                console.log("Tried to detail a userInfo-less notification.");
                return;
            }

            if(!$rootScope.detailingNotification)
                $rootScope.detailingNotification = true;
            else 
                return;

            var id = JSON.parse(json).id;//(json.split(':')[0]).slice(2).slice(0,-1);
            
            console.log("Detailed a promo. Checking for user...");
            console.log($rootScope.user);

            var dataObj = {
                data: "eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJwcm9tb3MiLCJhY3Rpb24iOiJfbGlzdCIsInB1YmxpY2tleSI6Ijdlcmc1bXV6bG53NXRoYmZwNGYxIn0=",
                hash: "b10dbf5f24d04462eef212254702bb0c",
                userId: $rootScope.user.id || $rootScope.user._id
            };

            /*var res = $http.post('http://' + SERVER + '.reachout.global/api?publickey=nkfvt7gpgdkakyb78gr9', dataObj);

            res.success(function(data, status, headers, config) {

                console.log("Succesfully brought the newest promos possible to check if notification is still valid.");

                // Saving promos
                //BROKEN SHOPS WORKAROUND
                data.promos.promos = data.promos.promos.filter(function(promo){
                    if (promo.audience.shops.length > 0) {
                        var hasAtLeastOne = false;
                        for (var j = 0; j < promo.audience.shops.length; j++) {
                            if(promo.audience.shops[j].map.lat && promo.audience.shops[j].map.lat != null){
                                hasAtLeastOne = true;
                                break;
                            }
                        }

                        return hasAtLeastOne;
                    }
                });
                $rootScope.promosCache = data.promos.promos;
                //BROKEN SHOPS WORKAROUND

                if (data.promos) {
                    var query = "UPDATE promos SET content=(?)";

                    $cordovaSQLite.execute($rootScope.db, query, [JSON.stringify(data.promos.promos, null, 4)]).then(function(res) {*/

                        var element;
                        for (var i = 0; i < $rootScope.promosCache.length; i++)
                            if ($rootScope.promosCache[i]._id == id)
                                element = $rootScope.promosCache[i];

                        if(element){
                            
                            if($rootScope.detailing)
                                $rootScope.detail();

                            $rootScope.detail(id + '','promo');
                            $rootScope.detailingNotification = false;
                        } else {

                            var element;
                            for (var i = 0; i < $rootScope.contentCache.length; i++)
                                if ($rootScope.contentCache[i]._id == id)
                                    element = $rootScope.contentCache[i];

                            if(element){
                                
                                if($rootScope.detailing)
                                    $rootScope.detail();

                                $rootScope.detail(id + '','news');
                                $rootScope.detailingNotification = false;
                            } else {
                                $rootScope.toggleAnnoyingMessage(false,true);

                                $(".content").addClass("blur");

                                $(".modal.notification-error.popup").fadeOut("fast", function(){
                                    $(".modal.notification-error.popup").css("top", 20 + $(".header.master-header").height() + "px");

                                    $(".modal.notification-error.popup").fadeIn("slow");
                                });
                            }
                        }

                        /*$rootScope.checkNotification(function(){detailNotifiedPromo(json)},"Reintentando verificar que la promoción elegida todavía exista...", element);

                    }, function(err) {});
                }
            });

            res.error(function(data, status, headers, config) {
                //$rootScope.checkNotification(function(){detailNotifiedPromo(json)},"Reintentando verificar que la promoción elegida todavía exista. Es posible que no haya conexión.");
                $rootScope.detailingNotification = false;
            });*/
        }
    /*Upon having clicked a local notification*/

    /*Upon wanting to change state*/
        $rootScope.$on('$stateChangeStart', function(event, next, nextParams, current, currentParams) {

            if (current.name == "main.detail" && $rootScope.savedNavIndex) {
                $rootScope.navIndex = $rootScope.savedNavIndex;
                $rootScope.savedNavIndex = undefined;
            } else {
                $rootScope.navIndex = { category: 0, subCategory: [] };
            }

            $rootScope.btnIsMenu = next.name != "main.detail";
            var last = $rootScope.states[$rootScope.states.length - 1];

            if (last != next.name) { // Check not to want the same state as before

                //Detener animación de logo
                if ($rootScope.logoAnim) {
                    clearInterval($rootScope.logoAnim);

                    $rootScope.logoAnim = undefined;

                    $rootScope.stateToggle = false;
                }

                /*UPDATE PROMOS*/
                $cordovaSQLite.execute($rootScope.db, "SELECT * FROM promos", []).then(function(res) {
                    if (res.rows.length != 0) 
                        for (var i = 0; i < res.rows.length; i++)         
                            $rootScope.promosCache = JSON.parse(res.rows.item(i).content);               
                }, function(err) {});
                /*UPDATE PROMOS*/


                //Actualizar estados
                if ($rootScope.states.length > 1) {

                    if ($rootScope.states[$rootScope.states.length - 2] == next.name) { // Going back
                        $rootScope.states.splice($rootScope.states.length - 1, 1);
                    } else {
                        $rootScope.states.push(next.name); // Going forward
                    }

                } else { // Only has mainPanel              
                    $rootScope.states.push(next.name);
                }

            }

            function onSuccess(position){
                console.log("Updated location");
                $rootScope.lat = position.coords.latitude;
                $rootScope.lng = position.coords.longitude;
            }

            function onError(error){
                console.log('Location error code: ' + error.code + '\nLocation error msg: ' + error.message);
                $rootScope.lat = undefined;
                $rootScope.lng = undefined;
            }

            $cordovaGeolocation.getCurrentPosition({timeout: 30000, enableHighAccuracy: false}).then(onSuccess,onError);

            /*$("#view-content").animate({
                left: "100%"
            }, 400, function(){
                $("#view-content").animate({
                    left: "0"
                }, 400);
            });*/
        });
    /*Upon wanting to change state*/

    /*Upon having successfully changed state*/
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            $timeout(function() {

                if (toState.name == "main.home" || toState.name == "main.camera" || toState.name == "main.promosTypes" || toState.name == "main.shopsTypes" || toState.name == "main.newsTypes" || toState.name == "main.detail" || toState.name == "main.datosContacto" || toState.name == "main.profile" || toState.name == "mainPanel"  || toState.name == "main.juegos") {

                    if (toState.name != "main.promosTypes" && toState.name != "main.shopsTypes" && toState.name != "main.newsTypes" && toState.name != "main.profile") {
                        $rootScope.updateButtonShape();
                    }

                    $rootScope.loading = false;

                    $rootScope.isShops = toState.name == "main.shopsTypes";
                    $rootScope.isHome = toState.name == "main.home";
                    $rootScope.isNews = toState.name == "main.newsTypes";
                    $rootScope.isDetail = toState.name == "main.detail";
                    $rootScope.isPromos = toState.name == "main.promosTypes";

                    //Constructor de nombre de estado
                    switch (toState.name) {
                        case "main.promosTypes":
                            $rootScope.state = "Promociones";
                            $rootScope.getStars(function(){
                                $rootScope.toggleAnnoyingMessage(!$rootScope.isGrisinoFan(), true);    
                            });
                            break;
                        case "main.home":
                            $rootScope.state = "Destacado";
                            $rootScope.getStars(function(){
                                $rootScope.toggleAnnoyingMessage(!$rootScope.isGrisinoFan(), true);    
                            });
                            break;
                        case "main.detail":
                            $rootScope.state = "Detalle";
                            $rootScope.toggleAnnoyingMessage(false, true);    

                            break;
                        case "main.camera":
                            $rootScope.state = "Compartí tu foto!";
                            $rootScope.toggleAnnoyingMessage(false, true);    

                            break;
                        case "main.datosContacto":
                            $rootScope.state = "Contacto";
                            $rootScope.toggleAnnoyingMessage(false, true);    

                            break;
                        case "main.shopsTypes":
                            $rootScope.state = "Locales";
                            $rootScope.getStars(function(){
                                $rootScope.toggleAnnoyingMessage(!$rootScope.isGrisinoFan(), true);    
                            });
                            break;
                        case "main.newsTypes":
                            $rootScope.state = "Lo nuevo!";
                            $rootScope.getStars(function(){
                                $rootScope.toggleAnnoyingMessage(!$rootScope.isGrisinoFan(), true);    
                            });
                            break;
                        case "main.juegos":
                            $rootScope.state = "¡Juegos!";
                            $rootScope.getStars(function(){
                                $rootScope.toggleAnnoyingMessage(false, true);    
                            });
                            break;
                        case "main.profile":
                            $rootScope.state = "Perfil";
                            $rootScope.toggleAnnoyingMessage(false, true);    

                            break;
                        case "main.notifications":
                            $rootScope.state = "Notificaciones";
                            $rootScope.toggleAnnoyingMessage(false, true);    

                            break;
                        case "mainPanel":
                            $rootScope.toggleAnnoyingMessage(false, true);        
                            break;
                    }

                    $rootScope.stateToggle = true;
                    $rootScope.logoAnim = setInterval(function(){
                        $rootScope.stateToggle = !$rootScope.stateToggle;
                    }, 5000);
                }
            }, 1000);
        });
    /*Upon having successfully changed state*/

    /*Details specific content*/
        $rootScope.detail = function(id, type){

            if($rootScope.isGuest){
                $rootScope.guestBlock();
                return;
            }

            if(!id && !type)
                $rootScope.isRedeeming = false;

            console.log(id + ", " + type + " is being detailed.");

            var time = 750;
            var easing = 'easeOutExpo';
            if(!$rootScope.detailTransitionInProcess){
                $rootScope.detailTransitionInProcess = true;                  
                
                if(!$rootScope.detailing){

                    $rootScope.detailedElement = {id: id, type: type};

                    // Start appending the divs so as to show them already appended
                    showDetail();

                    // Loading ng-show for detail modal
                    $rootScope.loadingDetail = true;

                    //setTimeout(function(){
                        // Reset any remaining scroll for some reason
                        $(".detail-wrapper").scrollTop(0);
                        $("#detail-toolbar").animate({top:20, opacity: 1},time/2,"easeInOutExpo");
                        $(".detail-wrapper").animate({
                            top: 20,
                            opacity: 1
                        },{duration: time, queue: true, easing:easing, complete:function(){

                            $rootScope.detailTransitionInProcess = false;

                            $rootScope.loading = false;
                            $rootScope.detailing = true;
                        }}); 

                    //},100);

                } else {
                    $rootScope.loading = true;
                    $rootScope.detailingShop = false; 

                    $("#detail-toolbar").animate({top:$(document).height() + "px", opacity: 0}, time, easing);
                    $(".detail-wrapper").animate({
                        top: $(document).height() + "px",
                        opacity: 0
                    },time, easing,function(){

                        // Erase previous detail and re append loading
                        $(".detail-content").empty().fadeOut(100,function(){
                            $(this).fadeIn(1);
                        });
                         
                        $("#detail-toolbar").css("top", "-" + $(document).height() + "px");
                        $(".detail-wrapper").css("top", "-" + $(document).height() + "px");

                        $rootScope.detailing = false;
                        $rootScope.loading = false; 

                        $rootScope.detailTransitionInProcess = false; 
                    });             
                }
            }
        }

        function showDetail(){

            $rootScope.hbutton = "Ver información";
            $rootScope.isOpen = false;
            $rootScope.finalizada = false;

            $rootScope.loading = false;

            var info = $rootScope.detailedElement;
            var index;
            var _log;
            var logType;
            var element, type = info.type; //FILL WITH ELEMENT FROM

            window.ga.trackView("Details " + type);

            /*Type-dependant options*/
            /*If it´s a promo*/
            if (type == "promo") {
                _log = 301;
                logType = 3; 
                for (var i = 0; i < $rootScope.promosCache.length; i++)
                    if ($rootScope.promosCache[i].id == info.id){
                        index = i;
                        element = $rootScope.promosCache[i];
                    }

            /*If it´s a shop*/
            } else if (type == "shop") {
                _log = 501;
                logType = 5;
                for (var i = 0; i < $rootScope.venuesCache.length; i++)
                    if ($rootScope.venuesCache[i].id == info.id){
                        index = i;
                        element = $rootScope.venuesCache[i];
                    }

            /*If it´s news or a video*/
            } else {
                _log = 401;
                logType = 4;
                for (var i = 0; i < $rootScope.contentCache.length; i++)
                    if ($rootScope.contentCache[i]._id == info.id || $rootScope.contentCache[i].id == info.id){
                        index = i;
                        element = $rootScope.contentCache[i];
                    }
            }

            console.log(info.id);
            console.log(element);
            console.log(type);

            if(element)
                $rootScope.detailedElement.element = element;

            var source,
                html,
                media,
                _nombre = '',
                _description = '',
                share = '',
                description = '',
                name = '',
                buttons = '',
                map = '',
                link = element.ecommerce_url,
                menubutton = '',
                promoType = 'fecha';

            switch (type) {
                case "promo":
                    source = element.images && element.images.length ? element.images[0].url : "https://s3-sa-east-1.amazonaws.com/iloveit/promo/" + element._id + ".jpg";

                    var s = element.start_date.split("T")[0];
                    var startDate = s.split("-")[2] + "/" + s.split("-")[1] + "/" + s.split("-")[0];
                    s = element.end_date.split("T")[0];
                    var endDate = s.split("-")[2] + "/" + s.split("-")[1] + "/" + s.split("-")[0];

                    if ($rootScope.NOW() > new Date(element.end_date).toISOString())
                        $rootScope.finalizada = true;

                    if ($rootScope.isGrisinoFan()) {
                        // Not e-shop
                        if (link == "") {
                            // Not redeemed
                            if (element.code == false && element.redeemed == undefined) {
                                var points = element.points > 0 ? element.points : (element.points * -1);

                                // Not substracting
                                if (element.points == "") {
                                    buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="width: 90%!important; bottom: 0; left: 5%" hm-tap="showConfirm($event, ' + index + ')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' flex flex-md="100">Quiero mi beneficio</md-button></div>';
                                } else {
                                    if ($rootScope.stars) {
                                        console.log("STARS");
                                        console.log($rootScope.stars);

                                        // Make always positive -> SOLO RESTA DE PUNTOS
                                        var points = parseInt(element.points) > 0 ? parseInt(element.points) : (parseInt(element.points) * -1);

                                        if (parseInt($rootScope.stars.stars) >= points) {
                                            buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="width: 90%!important; bottom: 0; left: 5%" hm-tap="showConfirm($event, ' + index + ')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' flex flex-md="100">Quiero mi beneficio</md-button></div>';
                                        } else {
                                            buttons = '<div style="margin: 20px 25px;" layout-align="center center" layout="row"><div flex="20" style="background-size: 30px;background-repeat: no-repeat;background-position: center center;width: 40px;height: 40px;background-image: url(\'assets/images/icono_alerta.png\')"></div><div flex> Te faltan <strong>' + (points - $rootScope.stars.stars) + ' estrellas para acceder a este beneficio</strong></div></div>';
                                        }
                                    } else {
                                        buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="width: 90%!important; bottom: 0; left: 5%" hm-tap="showConfirm($event, ' + index + ')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' flex flex-md="100">Quiero mi beneficio</md-button></div>';
                                    }
                                }
                            } else {

                                /*Has been redeemed in this very session*/
                                if (element.redeemed != undefined) {
                                    console.log("Promo redeemed -> " + element.redeemed);
                                    if (element.redeemed == "") { // Redeemed without any code being returned
                                        buttons = '<div style="width: 100%;margin-top: 20px;" layout-align="center center" layout="row">Presentá la app a quien te atiende</div>'
                                    } else { // Redeemed with code
                                        buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="font-size: 4vw;width: 90%!important; bottom: 0; left: 5%" flex flex-md="100">¡Felicitaciones! Tu código es <strong>' + element.redeemed + '</strong></md-button></div>';
                                    }

                                } else { // Redeemed in an older session
                                    if (element.code == "") { // Redeemed without any code being returned
                                        buttons = '<div style="width: 100%;margin-top: 20px;" layout-align="center center" layout="row">Presentá la app a quien te atiende</div>'
                                    } else { // Redeemed with code
                                        buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="font-size: 4vw;width: 90%!important; bottom: 0; left: 5%" flex flex-md="100">¡Felicitaciones!. Tu código es <strong>' + element.code + '</strong></md-button></div>';
                                    }
                                }
                            }
                        } else {
                            buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" hm-tap="addLog()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn" style="width: 90%!important; bottom: 0; left: 5%" href="' + link + '?utm_source=fanapp&utm_medium=app&utm_campaign=grisino%20fan" flex flex-md="100">Comprar</md-button></div>';
                        }
                    } else {
                        buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="width: 90%!important; bottom: 0; left: 5%" hm-tap="showConfirm($event, ' + index + ')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' flex flex-md="100">Quiero mi beneficio</md-button></div>';
                    }

                    _nombre = element.description_title;
                    _description = element.description_text;

                    description = '<p style="margin: 5px 25px;">' + element.description_text

                    if (!element.permanent) {
                        description += '<br><br><span class="tiny">Promoción válida desde <b>' +
                            startDate +
                            '</b> hasta <b>' +
                            endDate +
                            '</b></span>';
                    }

                    name = '<p style="margin: 10px 25px;">' + element.description_title + '</p>';

                    if (!element.triggers.hotspot_data) {

                        if (element.audience.shops.length != 0) {
                            var closestShop = undefined;

                            for (var i = 0; i < element.audience.shops.length; i++) {

                                var h = haversine($rootScope.lat, $rootScope.lng, element.audience.shops[i].map.lat, element.audience.shops[i].map.lng);

                                if (!closestShop || h.number < closestShop.number)
                                    closestShop = h;
                            }

                            var haver = $rootScope.lat ? "(" + closestShop.str + ")" : "";
                            promoType = "shop";
                        } else {
                            var haver = "";
                        }

                    } else {
                        var haver = $rootScope.lat ? "(" + haversine(element.triggers.hotspot_data.lat, element.triggers.hotspot_data.lng, $rootScope.lat, $rootScope.lng).str + ")" : "";
                        promoType = "hotSpot";
                    }

                    if (promoType != "fecha") {
                        name = '<p style="margin: 10px 25px;">' + element.description_title + ' ' + haver + '</p>';

                        map = '<div class="scrollableMap"><div id="detailedMap"></div></div>';

                        menubutton = '<md-button id="flipDIV" hm-tap="flipDIV();" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-fab allTabsIcon" style="border: 1px solid white; opacity: 1!important; background-image: url(\'assets/images/icon-mapa3_30x30px-white.png\'); position: fixed !important;top: 100px !important;right: 10px !important;height: 50px !important;width: 50px!important; background-color: #d60036!important;" aria-label="Use Android"></md-button>';
                    }

                    break;
                case "news":
                    source = element.images && element.images.length ? element.images[0].url : "https://s3-sa-east-1.amazonaws.com/iloveit/content/" + element._id + ".jpg";

                    _nombre = element.title;
                    _description = element.description;

                    description = '<p style="margin: 0px 25px;">' + element.description;

                    name = '<p style="margin: 10px 25px;">' + element.title + '</p>';

                    break;
                case "shop":

                    source = element.images && element.images.length ? element.images[0].url : "https://s3-sa-east-1.amazonaws.com/iloveit/shop/" + element._id + ".jpg";

                    _nombre = element.name;
                    _description = element.info;

                    description = '<p style="margin: 10px 25px;">' + element.info;

                    map = '<div class="scrollableMap"><div id="detailedMap"></div></div>';

                    name = '<p style="margin: 10px 25px;">' + element.name + '</p><p style="margin: 10px 25px;"><a href="maps://maps.apple.com?daddr=' + element.map.lat + ',' + element.map.lng + '&dirflg=d">Abrir en mapas</a></p>';

                    menubutton = '<md-button id="flipDIV" hm-tap="flipDIV();" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-fab allTabsIcon" style="border: 1px solid white; opacity: 1!important; background-image: url(\'assets/images/icon-mapa3_30x30px-white.png\'); backgorund-size: 30%!important; position: fixed !important;top: 100px !important;right: 10px !important;height: 50px !important;width: 50px!important; background-color: #d60036!important;" aria-label="Use Android"></md-button>';

                    $rootScope.isShops = true;

                    break;
                case "video":
                    var sampleUrl = obj.elem.fields.youtubeid;
                    var video_id = sampleUrl;

                    source = "https://www.youtube.com/embed/" + video_id;
                    type = "video";

                    media = '<iframe src="' + source + '" frameBorder="0" style="height: 100%; width: 100%;" ></iframe>'
                    break;
            }

            /*Last tweaks*/
            /*If it is not a video*/
            if (type != "video") {

                if(element.images){
                    var images = '';
                    for(var i = 0; i < element.images.length; i++){

                        images += '<div class="swiper-slide" style="padding-bottom: 15px; background-color:white;"><img src="' + element.images[i].url + '" id="' + element._id + '" onload="this.style.opacity=1;">' + (element.images[i].description ? ('<div style="position:absolute; bottom: 0; width: 100%;background: rgba(256,256,256,0.6);padding: 20 0;color:black; left: 0;">' + element.images[i].description + '</div>') : '') + '</div>';
                    }

                    media = '<div style="'+ (element.images.length > 1 ? 'padding-bottom: 70px;' : '') +' height:' + $(".detail-content").width() + 'px!important;" class="swiper-container">'+
                                '<div class="swiper-wrapper">'+                
                                    images+
                                '</div>'+
                                '<div style="bottom: 20;" class="swiper-pagination"></div>'+
                                (element.images.length > 1 ? '<md-button class="md-fab" hm-tap="sliderLeft()" style=" border-width: 0!important;background: transparent;position:absolute; top:' + ($(".detail-content").width() + 30) + ';height: 30px;width:30px; left: 20"><svg fill="gray" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129">  <g><path d="m88.6,121.3c0.8,0.8 1.8,1.2 2.9,1.2s2.1-0.4 2.9-1.2c1.6-1.6 1.6-4.2 0-5.8l-51-51 51-51c1.6-1.6 1.6-4.2 0-5.8s-4.2-1.6-5.8,0l-54,53.9c-1.6,1.6-1.6,4.2 0,5.8l54,53.9z"/></g></svg></md-button><md-button class="md-fab" hm-tap="sliderRight()" style=" border-width: 0!important;background: transparent;position:absolute; top:' + ($(".detail-content").width() + 30) + ';height: 30px;width:30px; right: 20"><svg fill="gray" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></g></svg></md-button>' : '') +
                            '</div>';                   
                        
                } else {

                    media = '<div style="height:' + $(".detail-content").width() + 'px!important;" class="swiper-container">'+
                                '<div class="swiper-wrapper">'+                
                                    '<div class="swiper-slide" style="padding-bottom: 15px; background-color:white;"><img src="' + source + '" id="' + element._id + '" onload="this.style.opacity=1;"></div>'+
                                '</div>'+
                                '<div style="bottom: 20;" class="swiper-pagination"></div>'+
                            '</div>'; 

                }

                element.img = source;

                $rootScope.alert = '';
            }

            console.log(element);

            description += '</p>';

            /*Template construction*/
            html = '<div id="detailedWrapper" style="z-index:34; border-radius:0 0 0 0!important;border: 0px!important;border-width: 0px!important; height: auto!important;" class="imageWrapper square detail"><div class="ribbon" ng-show="finalizada"></div>' +
                    media +
                '</div>' +

                //If is shops will append a flippable map
                map +

                '<div id="detailedInfoWrapper" style="overflow-y: scroll; position: relative; background-color: white; width: 100%;">' +

                    '<div id="detailedInfo" class="square2 no-padding-bottom" style="background: white;padding: 0px!important;">' +

                        name +
                        description +
                        buttons +

                        '<div ng-hide="finalizada" style="width: 90%;margin-left: 5%" layout="row" layout-align="space-around center">' +

                            '<div style="width:100%;box-shadow: 1px 0px 0px rgba(0, 0, 0, 0.2);" flex="30"><div aria-label="compartir" style="display: inline-block;box-sizing: border-box;height: 48px;width: 48px;margin: 0;background-position: center;background-size:50%;background-repeat: no-repeat; background-image: url(assets/images/icono-compartir.png)" class="md-fab md-raised"></div></div>' +

                            '<div flex="70" style="padding: 30px 0px;width: 100%;" layout="row" layout-align="end center">' +
                                '<md-button hm-tap="shareFacebook(' + logType + '21)" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' aria-label="facebook" style="box-shadow:none!important; border: 1px solid #314A7E; background-color:transparent;background-position: center;background-size:50%;margin-left:12px!important;background-repeat: no-repeat; background-image: url(assets/images/icono-facebook.png)" class="md-fab md-raised md-mini"></md-button>' +
                                '<md-button hm-tap="shareTwitter(' + logType + '23)" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' aria-label="Twitter" style="box-shadow:none!important; border: 1px solid #339DC3;background-color:transparent; background-repeat: no-repeat; background-position: center;background-size:50%;margin-left:12px!important; background-image: url(assets/images/icono-twitter.png)" class="md-fab md-raised md-mini"></md-button>' +

                                '<md-button hm-tap="shareInstagram(' + logType + '22)" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' aria-label="instagram" style="box-shadow:none!important; border: 1px solid #125688; background-position: center;background-size:50%;margin-left:12px!important;background-color:transparent;background-repeat: no-repeat; background-image: url(assets/images/icono-instagram.png)" class="md-fab md-raised md-mini"></md-button>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';

            /*Make info available for share purposes*/
            detailed_info = {
                id: element._id,
                source: source,
                description: _description,
                name: _nombre
            };

            /*Template wrapper appending*/
            $(".detail-content").append($compile(html)($rootScope));

            /*Unveil all content detailed and set the swiper if any was required*/
            setTimeout(function() {
                $rootScope.loadingDetail = false;
                if(element.images && element.images.length){
                    var swiper = new Swiper('.swiper-container', {
                        pagination: element.images.length > 1 ? '.swiper-pagination' : '',
                        effect: 'cube',
                        grabCursor: true,
                        centeredSlides: true,
                        slidesPerView: 'auto',
                        cube: {
                            shadow: false
                        }
                    });

                    setTimeout(function(){
                        $rootScope.sliderLeft = function(){
                            $(".swiper-container")[0].swiper.slidePrev();
                        }

                        $rootScope.sliderRight = function(){
                            $(".swiper-container")[0].swiper.slideNext();
                        }
                    },200);
                }
            }, 500);
            /*Unveil all content detailed and set the swiper if any was required*/

            //Set up appended map
            if (type == "promo") {

                if (promoType == "fecha") {} else if (promoType == "hotSpot") {

                    $rootScope.detailingShop = true;

                    $(".scrollableMap").css("height", $("#detailedWrapper").css("height"));

                    var _userLat;
                    var _userLng;

                    if ($rootScope.lat == null || $rootScope.lat == "null") {
                        _userLat = element.triggers.hotspot_data.lat;
                        _userLng = element.triggers.hotspot_data.lng;
                    } else {
                        _userLat = $rootScope.lat;
                        _userLng = $rootScope.lng;
                    }

                    //Map setup
                    L.mapbox.accessToken = 'pk.eyJ1IjoiZmVkZW11bmkiLCJhIjoieWRPazdSRSJ9.aDK-HSUPTYxgahE8BzOtoQ';

                    var map = L.mapbox.map('detailedMap', 'mapbox.streets', { zoomControl: false }).setView([_userLat, _userLng], 16);

                    var geojson = [{
                        "type": "FeatureCollection",
                        "features": [],
                        "id": 'mapbox.checkin'
                    }];

                    if (element.triggers.hotspot_data.lat != undefined) {
                        var newMarker =

                            L.marker([element.triggers.hotspot_data.lat, element.triggers.hotspot_data.lng], {
                                icon: L.mapbox.marker.icon({
                                    'marker-size': 'large',
                                    'marker-color': '#000',
                                    'marker-symbol': 'shop'
                                })
                            })

                        .bindPopup('<div><strong>' + element.description_title + '</strong><br>' + element.triggers.hotspot_data.address + '</div>')
                            .addTo(map);
                    }

                    if($rootScope.lat){

                        map.setView([$rootScope.lat, $rootScope.lng], 10);

                        var marker = L.marker([$rootScope.lat, $rootScope.lng], {
                            icon: L.mapbox.marker.icon({
                                'marker-size': 'large',
                                'marker-color': '#d60036'
                            })
                        }).bindPopup('Mi ubicación').addTo(map);
                        marker.openPopup();
                        map.fitBounds([[$rootScope.lat, $rootScope.lng],[element.triggers.hotspot_data.lat, element.triggers.hotspot_data.lng]]);
                    }

                } else {

                    $rootScope.detailingShop = true;

                    $(".scrollableMap").css("height", $("#detailedWrapper").css("height"));

                    var _userLat;
                    var _userLng;

                    if ($rootScope.lat == null || $rootScope.lat == "null") {
                        _userLat = element.audience.shops[0].map.lat;
                        _userLng = element.audience.shops[0].map.lng;
                    } else {
                        _userLat = $rootScope.lat;
                        _userLng = $rootScope.lng;
                    }

                    //Map setup
                    L.mapbox.accessToken = 'pk.eyJ1IjoiZmVkZW11bmkiLCJhIjoieWRPazdSRSJ9.aDK-HSUPTYxgahE8BzOtoQ';

                    var map = L.mapbox.map('detailedMap', 'mapbox.streets', { zoomControl: false }).setView([_userLat, _userLng], 15);

                    var geojson = [{
                        "type": "FeatureCollection",
                        "features": [],
                        "id": 'mapbox.checkin'
                    }];

                    for (var x = 0; x < element.audience.shops.length; x++) {

                        if (element.audience.shops[x].map.lat != undefined) {
                            var newMarker = L.marker([element.audience.shops[x].map.lat, element.audience.shops[x].map.lng], {
                                icon: L.mapbox.marker.icon({
                                    'marker-size': 'large',
                                    'marker-color': '#000',
                                    'marker-symbol': 'shop'
                                })
                            }).bindPopup('<div><strong>' + element.audience.shops[x].name + '</strong><br>' + element.audience.shops[x].map.address + '</div>').addTo(map);
                        }
                    }

                    if($rootScope.lat){

                        map.setView([$rootScope.lat, $rootScope.lng], 10);

                        var marker = L.marker([$rootScope.lat, $rootScope.lng], {
                            icon: L.mapbox.marker.icon({
                                'marker-size': 'large',
                                'marker-color': '#d60036'
                            })
                        }).bindPopup('Mi ubicación').addTo(map);
                        marker.openPopup();
                    }
                }
            } else if (type == "shop") {

                $rootScope.detailingShop = true;

                $(".scrollableMap").css("height", $("#detailedWrapper").css("height"));

                //Map setup
                L.mapbox.accessToken = 'pk.eyJ1IjoiZmVkZW11bmkiLCJhIjoieWRPazdSRSJ9.aDK-HSUPTYxgahE8BzOtoQ';
                var map = L.mapbox.map('detailedMap', 'mapbox.streets', { zoomControl: false }).setView([element.map.lat + 0.0018, element.map.lng], 15);

                var geojson = [{
                    "type": "FeatureCollection",
                    "features": [],
                    "id": 'mapbox.checkin'
                }];

                if (element.map.lat != undefined) {
                    var newMarker = L.marker([element.map.lat, element.map.lng], {
                        icon: L.mapbox.marker.icon({
                            'marker-size': 'large',
                            'marker-color': '#000'
                        })
                    }).bindPopup('<div><strong>' + element.name + '</strong><br>' + element.address + '<br><u>Tel:</u> <a href="tel:' + element.phone + '">' + element.phone + '</a><br><u>Email:</u> ' + element.info + '<br><br></div>').addTo(map);
                }

                if($rootScope.lat){

                    map.setView([$rootScope.lat, $rootScope.lng], 10);

                    var marker = L.marker([$rootScope.lat, $rootScope.lng], {
                        icon: L.mapbox.marker.icon({
                            'marker-size': 'large',
                            'marker-color': '#d60036'
                        })
                    }).bindPopup('Mi ubicación').addTo(map);
                    marker.openPopup();

                    map.fitBounds([[$rootScope.lat, $rootScope.lng],[element.map.lat, element.map.lng]]);
                }
            }
            if (type == "shop" || (type == "promo" && promoType != "fecha")) {
                $rootScope.isImage = true;
                $rootScope.flipDIV();
            }

            var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: _log, elementId: element.id}
            $rootScope.addLog(_thelog);

            $rootScope.loading = false;
        }

        $rootScope.flipDIV = function() {
            var animDuration = 650;
            //Shows map
            if (!$rootScope.isImage) {

                $("#mapswitch").css("background-image", "url('assets/images/photo-camera.svg')");
                $("#mapswitch").css("background-size", "20%");

                $("#detailedWrapper").animate({
                    left: "100%"
                }, {duration:animDuration, easing:'easeOutQuint', queue:false});

                $(".scrollableMap").animate({
                    left: 0
                }, {duration:animDuration, easing:'easeOutQuint', queue:false});

                $rootScope.isImage = true;

                //Shows image
            } else {
                $("#mapswitch").css("background-image", "url('assets/images/icon-mapa3_30x30px-white.png')");
                $("#mapswitch").css("background-size", "23%");

                $("#detailedWrapper").animate({
                    left: "0px"
                }, {duration:animDuration, easing:'easeOutQuint', queue:false});

                $(".scrollableMap").animate({
                    left: "-100%"
                }, {duration:animDuration, easing:'easeOutQuint', queue:false});

                $rootScope.isImage = undefined;
            }
        }     

        /*In-App Browser catch*/
        $(document).on('click', 'a[href^=http], a[href^=https]', function(e) {

            if (!$rootScope.isInAppBrowserOpen) {

                e.preventDefault();

                var $this = $(this);

                console.log($(this).attr('href'));

                var ref = cordova.ThemeableBrowser.open($this.attr('href'), '_blank', IN_APP_THEMEABLE_BROWSER_OPTIONS);

                console.log(ref);

                ref.addEventListener('closePressed', function(event) {
                    $rootScope.loading = false;
                    $rootScope.isInAppBrowserOpen = false;
                }).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function(event) {
                    $rootScope.loading = false;
                    $rootScope.isInAppBrowserOpen = false;
                });

                $rootScope.isInAppBrowserOpen = true;
            }
        });

        $rootScope.showConfirm = function(ev, index) {
            //ev.preventDefault();

            if($rootScope.isRedeeming)
                return;

            var element = $rootScope.detailedElement.element;

            if(!element)
                return;

            console.log("SHOWCONFIRM");

            // Prevent non-GrisinoFanUsers to redeem at all
            $rootScope.toggleAnnoyingMessage(!$rootScope.isGrisinoFan(), true);
            if (!$rootScope.isGrisinoFan())
                return;

            if (element.code_type == "no_code") {

                $mdDialog.show({
                    controller: DialogController,
                    clickOutsideToClose: true,
                    template: '<md-dialog><md-button class="md-icon-button" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'><md-icon md-svg-src="assets/images/ic_close_24px.svg" style="color: black!important" aria-label="Close dialog"></md-icon></md-button><form style="padding: 15px"><md-dialog-content class="noPadding"><div><center><p style="font-size: 1.1em;">Aprovechá este beneficio acercándote a cualquier local Grisino y mostrando la APP, o visitando nuestro E-shop.</p></center></div></md-dialog-content></form></md-dialog>',
                    targetEvent: ev
                });

                var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP,lat: parseFloat($rootScope.lat) ,lng: parseFloat($rootScope.lng) , id:302, elementId:element.id};
                $rootScope.addLog(_thelog);
                /*$(".redeem-actions").fadeOut("slow", function() {
                    $(".redeem-actions").html('<div style="width: 100%;margin-top: 20px;" layout-align="center center" layout="row">Presentá la app a quien te atiende</div>').fadeIn("slow");
                    $rootScope.promosCache[index].reedemed = "";
                });*/

            } else {
                if (element.points == "") {
                    var textoPuntos = '';
                } else {
                    if (!$rootScope.stars) {
                        $mdDialog.show({
                            controller: DialogController,
                            clickOutsideToClose: true,
                            template: '<md-dialog><md-button class="md-icon-button" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'><md-icon style="color:black!important;" md-svg-src="assets/images/ic_close_24px.svg" aria-label="Close dialog"></md-icon></md-button><form style="padding: 15px"><md-dialog-content class="noPadding"><div><center><p style="font-size: 1em;">No se puede canjear esta promo porque no sos Grisino Fan aún. Para empezar a acumular estrellas, hazte Grisino Fan.</p><md-button style="width: 100%!important; background-color: green!important; margin: 5px 0px; padding: 0px" hm-tap="hide()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn">Hacerme Grisino Fan</md-button></center></div></md-dialog-content></form></md-dialog>',
                            targetEvent: ev
                        }).then(function() {

                            cordova.ThemeableBrowser.open('http://www.grisino.com/customer/account/create/', '_blank', IN_APP_BROWSER_OPTIONS)
                                            .addEventListener('closePressed', function(e) {
                                $rootScope.loading = false;
                            });

                            var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP, lat:parseFloat($rootScope.lat), lng:parseFloat($rootScope.lng), id:308, elementId:element.id};
                            $rootScope.addLog(_thelog);

                        }, function() {});
                        return;

                    } else {

                        // Make always positive -> SOLO RESTA DE PUNTOS
                        var points = parseInt(element.points) > 0 ? parseInt(element.points) : (parseInt(element.points) * -1);

                        if (parseInt($rootScope.stars.stars) >= points) {
                            var textoPuntos = ' y te descontaremos <b>' + points + '</b> estrellas.';
                        } else {
                            $mdDialog.show({
                                controller: DialogController,
                                template: '<md-dialog><md-button class="md-icon-button" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'><md-icon style="color:black!important;" md-svg-src="assets/images/ic_close_24px.svg" aria-label="Close dialog"></md-icon></md-button><md-dialog-content><form style="padding: 15px"><md-dialog-content class="noPadding"><div><center><p style="font-size: 1.5em;">No te alcanzan las estrellas para canjear esta promoción.</p></center></div></md-dialog-content></form></md-dialog>',
                                targetEvent: ev,
                                clickOutsideToClose: true
                            }).then(function() {
                                var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP, lat:parseFloat($rootScope.lat), lng:parseFloat($rootScope.lng), id:308, elementId:element.id};
                                $rootScope.addLog(_thelog);
                            }, function() {});
                            return;
                        }
                    }
                }

                $mdDialog.show({
                    controller: DialogController,
                    template: '<md-dialog style="padding: 15px;"><form style="margin: 0px;"><md-button class="md-icon-button" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'><md-icon style="color:black!important;" md-svg-src="assets/images/ic_close_24px.svg" aria-label="Close dialog"></md-icon></md-button><md-dialog-content class="noPadding"><div><center><p> Vas a hacer uso de este beneficio. Si continuas se mostrará tu código promocional' + textoPuntos + ' ¡Aprovechá este beneficio!</p></center></div></md-dialog-content></form><div style="padding: 0px 24px 10px 24px;"><center><md-button style="width: 100%!important; background-color: green!important; margin: 5px 0px; padding: 0px" hm-tap="answer(\'canjear\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn">Si, quiero mi beneficio</md-button><md-button style="width: 100%!important; margin: 5px 0px; padding: 0px" hm-tap="answer(\'cancelar\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn md-warn">No, en otro momento</md-button></center></div></md-dialog>',
                    targetEvent: ev
                }).then(function(answer) {
                    $rootScope.loading = true;

                    if (answer != 'cancelar'){
                        redeem(ev, index);

                    } else {
                        $rootScope.loading = false;
                        var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP, lat:parseFloat($rootScope.lat), lng:parseFloat($rootScope.lng), id:307, elementId:element.id};
                        $rootScope.addLog(_thelog);
                    }

                }, function() {});
            }
        };
        function DialogController($scope, $mdDialog, $cordovaInAppBrowser) {
            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }
        function redeem(ev, index) {
            var element = $rootScope.detailedElement.element;

            if(!element)
                return;

            var dataObj = {
                data: "eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJwcm9tb3MiLCJhY3Rpb24iOiJyZWRlZW0iLCJwdWJsaWNrZXkiOiIzNXdreWUwZWh2aWFlMTRueHQ0NCJ9",
                hash: "c227732be0e4b52f6458aa5c6de5cea6",
                userId: $rootScope.user.id || $rootScope.user._id,
                id: element.id
            };

            var res = $http.post('http://'+SERVER+'.reachout.global/api?publickey=nkfvt7gpgdkakyb78gr9&id=' + element.id, dataObj, {headers: {'Authorization':'Bearer ' + TOKEN}});

            $rootScope.isRedeeming = true;

            $(".redeem-actions").children("button").css("pointer-events", "none");

            var serverNotResponding = setTimeout(function(){
                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:"072"};
                $rootScope.addLog(_thelog);
                $rootScope.isRedeeming = false;
            }, 10000);

            res.success(function(data, status, headers, config) {

                clearTimeout(serverNotResponding);
            
                if (data.success) {

                    $rootScope.loading = false;

                    if (data.promo_code == "undefined" || data.promo_code == '' || data.promo_code == undefined) {

                        if (element.points == "") {
                            var textoCodigo = 'Presentá la aplicación a quien te atiende, para obtener el beneficio.';
                        } else {
                            var textoCodigo = 'Presentale esta aplicación a quien te atiende, para obtener el beneficio, se descontarán <b>' + (element.points > 0 ? element.points : (element.points * -1)) + '</b> estrellas, de tu cuenta.';
                        }

                        var redeemActions = '<div style="width: 100%;margin-top: 20px;" layout-align="center center" layout="row">Presentá la app a quien te atiende</div>';

                    } else {
                        var textoCodigo = 'Felicitaciones!</p><p><br>Tu código promocional es:</p><p style="font-size: 2em;">' + data.promo_code + '</p><p>Se han descontado <b>' + (element.points > 0 ? element.points : (element.points * -1)) + '</b> estrellas de tu cuenta.</p><p style="color: gray;"></p>';

                        var redeemActions = '<md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="width: 90%!important; font-size: 4vw; bottom: 0; left: 25px; position:relative;" flex flex-md="100">¡Felicitaciones! Tu código es <strong>' + data.promo_code + '</strong></md-button>';

                            //'<md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="font-size: 4vw;width: 90%!important; bottom: 0; left: 5%;" flex flex-md="100">Beneficio ya utilizado. Tu código es <strong>' + data.promo_code + '</strong></md-button>';
                    }

                    if (element.ecommerce_url == "") {

                        $rootScope.loading = false;

                        $mdDialog.show({
                            controller: DialogController,
                            clickOutsideToClose: true,
                            template: '<md-dialog><md-button class="md-icon-button" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'><md-icon style="color:black!important;" md-svg-src="assets/images/ic_close_24px.svg" aria-label="Close dialog"></md-icon></md-button><form style="padding: 15px"><md-dialog-content class="noPadding"><div><center><p style="font-size: 1.5em;">' + textoCodigo + '</p></center></div></md-dialog-content></form></md-dialog>',
                            targetEvent: ev
                        }).then(function(){},function(){

                            $(".redeem-actions").children("button").css("pointer-events", "all");

                            $(".redeem-actions").fadeOut("slow", function() {
                                $(".redeem-actions").html(redeemActions).fadeIn("slow");
                            });

                            $rootScope.promosCache[index].redeemed = data.promo_code;
                            $rootScope.isRedeeming = false;

                            var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP, lat:parseFloat($rootScope.lat), lng:parseFloat($rootScope.lng), id: 306, elementId: element.id};
                            $rootScope.addLog(_thelog);
                        });
                        
                    } else {

                        $mdDialog.show({
                            controller: DialogController,
                            clickOutsideToClose: true,
                            template: '<md-dialog><md-button class="md-icon-button" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'><md-icon style="color:black!important;" md-svg-src="assets/images/ic_close_24px.svg" aria-label="Close dialog"></md-icon></md-button><form style="padding: 15px"><md-dialog-content class="noPadding"><div><center><p><span style="color: black!important;font-size: 1.5em;">Felicitaciones!</span><br>Has obtenido el beneficio<br>Tu código personal es:<br><span style="font-size: 2em;">' + data.promo_code + '</span></p><md-button hm-tap="clipboard(\'' + data.promo_code + '\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' style="width: 100%!important; margin: 5px 0px; padding:1px" class="md-primary md-raised loginBtn"> Copiar al portapapeles </md-button><a href="' + element.ecommerce_url + '"><md-button style="width: 100%!important; background-color: green!important; margin: 5px 0px; padding:2px" hm-tap="answer(\'canjear\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn"> Ir al E-Shop </md-button></a><md-button style="width: 100%!important; margin: 5px 0px; padding:2px" hm-tap="answer(\'cancelar\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn md-warn"> Cancelar </md-button><p>Se han deducido <b>' + (element.points > 0 ? element.points : (element.points * -1)) + '</b> puntos de tu cuenta.</p></center></div></md-dialog-content></form></md-dialog>',
                            targetEvent: ev
                        }).then(function(){},function(){

                            $(".redeem-actions").children("button").css("pointer-events", "all");

                            $(".redeem-actions").fadeOut("slow", function() {
                                $(".redeem-actions").html(redeemActions).fadeIn("slow");
                            });

                            $rootScope.isRedeeming = false;

                            $rootScope.promosCache[index].redeemed = "";

                            var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP, lat:parseFloat($rootScope.lat), lng:parseFloat($rootScope.lng), id:306, elementId:element.id};
                            $rootScope.addLog(_thelog);
                        });

                    }

                } else {
                    $(".redeem-actions").children("button").css("pointer-events", "all");

                    $rootScope.loading = false;

                    var error_text = data.exceptionmessage == "You don't have enough points to redeem this promo: (api.promos.redeem)" ? "No te alcanzan los puntos para conseguir esta promo." : "Puede que no haya conexión. Intentá más tarde";

                    error_text = data.exceptionmessage == "Promotion has no codes left: (api.promos.redeem)" ? "Ups! Beneficio agotado." : error_text;

                    $mdDialog.show({
                        controller: DialogController,
                        template: '<md-dialog><md-button class="md-icon-button" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'><md-icon style="color:black!important;" md-svg-src="assets/images/ic_close_24px.svg" aria-label="Close dialog"></md-icon></md-button><form style="padding: 15px"> <md-dialog-content class="noPadding"> <div> <center><p>' + error_text + '</p></center></div></md-dialog-content> </form></md-dialog>',
                        targetEvent: ev,
                        clickOutsideToClose: true
                    });

                    $rootScope.isRedeeming = false;

                    var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP, lat:parseFloat($rootScope.lat), lng:parseFloat($rootScope.lng), id:308, elementId:element.id};
                    $rootScope.addLog(_thelog);
                }
            });

            res.error(function(data, status, headers, config) {
                $rootScope.isRedeeming = false;
                clearTimeout(serverNotResponding);
                $rootScope.loading = false;
                $rootScope.checkConnection(function() { setTimeout(redeem, 3000) }, "Reintentando conseguir beneficio...");

                var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP, lat:parseFloat($rootScope.lat), lng:parseFloat($rootScope.lng), id:"071"};
                    $rootScope.addLog(_thelog);
            });
        }
    /*Details specific content*/

    /*Toggles back/menu icon*/
        $rootScope.updateButtonShape = function() {
            var a = angular.element(".iconWrapper");

            if ($rootScope.btnIsMenu) {
                if (a.hasClass('back'))
                    a.removeClass('back');

            } else if (!a.hasClass('back')) {
                a.addClass('back');
            }
        }  
    /*Toggles back/menu icon*/

    /*startVideo*/
        $rootScope.startVideo = function (event){
            //$(event.target).siblings("iframe").contents().find("#player").trigger('click');
            $(event.target).siblings("iframe").contents().find(".html5-video-player").trigger('click');
            //$(event.target).siblings("iframe").contents().find(".html5-video-container").trigger('click');

            var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP, lat:parseFloat($rootScope.lat), lng:parseFloat($rootScope.lng), id: 402}
            $rootScope.addLog(_thelog);
        }
    /*startVideo*/

    /*LOGIN*/
        $rootScope.login = function(e,p) {
            //console.log('llega');
            $rootScope.loading = true;

            var user = {
                email: e,
                password: p,
                is_admin: false,
                json: true,
                brand: BRAND
            };

            /*var user = {
              email: "test@test.com",
              password: "test",
              is_admin: false,
              json: true
            };*/

            Playtomic.Auth.login(user, function(response) {
                console.log(response);
                if (response.success) {

                    TOKEN = response.token;
                    TOKEN_DEBUG = TOKEN == undefined;

                    response.user.self_user = JSON.stringify(response.user);
                    response.user.fields.app_version = VERSION;
                    response.user.fields.version = IOSVERSION;
                    response.user.fields.os = 'iOS';
                    response.user.token = TOKEN;

                    console.log(response);
                    Playtomic.Users.update(response.user, function(response) {
                        $rootScope.loading = false;

                        if (response.user) {
                            console.log(response);

                            $rootScope.loading = false;

                            var query = "INSERT INTO usuario (content, facebook_id, user_id, rDate) VALUES (?,?,?,?)";

                            $cordovaSQLite.execute($rootScope.db, query, [JSON.stringify(response.user, null, 4), "", response.user.id, response.user.register_date]).then(function(res) {
                                console.log('Populado Usuario desde Login Usuario ID');
                                $rootScope.user = response.user;
                                $rootScope.user.token = TOKEN;
                                $rootScope.dni = response.user.national_indentity;

                                $rootScope.logged = true;

                                $rootScope.isGuest = $rootScope.user.email == HARDCODED_GUEST.mail;

                                $rootScope.user.facebookId = "";
                                $rootScope.checkTablas();

                            }, function(err) {});
                        } else {
                            $mdToast.show(
                                $mdToast.simple()
                                .content('Error ingresando a tu cuenta. Revisa tu conexión.')
                                .position('top')
                                .hideDelay(5000)
                            );
                            console.log(response);

                            if(TOKEN_DEBUG){
                                $rootScope.dni = undefined;
                                $rootScope.user = undefined;
                                $rootScope.stars = undefined;
                                $rootScope.borraTablas(true);

                                TOKEN = undefined;
                            } else {
                                Playtomic.Auth.logout(function(response) {
                                    $rootScope.dni = undefined;
                                    $rootScope.user = undefined;
                                    $rootScope.stars = undefined;
                                    $rootScope.borraTablas(true);
                                });
                            }

                            $rootScope.loading = false;
                        }
                    });
                } else {
                    $rootScope.loading = false;
                    $mdToast.show(
                        $mdToast.simple()
                        .content('Error en el ingreso')
                        .position('top')
                        .hideDelay(3000)
                    );
                }
            });

            return false;
        };
    /*LOGIN*/

    /*guestBlock*/
        $rootScope.guestBlock = function(){
            $mdDialog.show({
                controller: function ($rootScope, $mdDialog, $scope, $cordovaInAppBrowser) {

                    $scope.goJoin = function(){
                        $mdDialog.hide();
                        $rootScope.goJoin();
                    }

                    $scope.cancel = function() {
                        $mdDialog.hide();
                    };

                    $scope.joinFacebook = function(){
                        $mdDialog.hide();
                        $rootScope.loading = false;
                        $rootScope.takeGuestToFacebook();
                    }

                },
                controllerAs: 'ctrl',
                template: '<md-dialog style="background-color: #d60036; color:white;padding: 15px;"><p style="margin: 0px;">Este contenido está disponible sólo para usuarios registrados. Creá tu cuenta ahora para acceder a los beneficios, ¡es muy fácil! Registrate acá: </p><div layout="column"><div layout="row" style="margin-top: 20px;"><md-button flex class="md-raised" style="border-top-right-radius: 0px;border-bottom-right-radius: 0px;border: 1px solid white;margin-top:10px;background-color: transparent; color:white;box-shadow:none;" hm-tap="goJoin()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'><strong>con tu email</strong></md-button><md-button hm-tap="joinFacebook()" hm-recognizer-options=\'{"threshold":10,"time":4000,"preventGhosts":true}\' flex class="md-raised" style="border-top-left-radius: 0px;border-bottom-left-radius: 0px;border: 1px solid white;margin-top:10px;background-color: transparent; color:white;box-shadow:none;">con Facebook</md-button></div><md-button class="md-raised" style="border: 0px solid white;margin-top:10px;background-color: transparent; color:white;box-shadow:none;" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'>Continuar como invitado</md-button></div></md-dialog>',

                clickOutsideToClose: true
            }).then(function(card) {}, function() {});
        }
    /*guestBlock*/

    /*Guest to facebook*/
        $rootScope.takeGuestToFacebook = function(){
            if(TOKEN_DEBUG){
                $rootScope.dni = undefined;
                $rootScope.user = undefined;
                $rootScope.stars = undefined;
                $rootScope.borraTablas(true, function(){
                    $rootScope.joinFacebook();
                });
                TOKEN = undefined;
            } else {
                Playtomic.Auth.logout(function(response) {
                    $rootScope.dni = undefined;
                    $rootScope.user = undefined;
                    $rootScope.stars = undefined;
                    $rootScope.borraTablas(true, function(){
                        $rootScope.joinFacebook();
                    });
                });
            }   
            //$rootScope.isGuest = $rootScope.user.email == HARDCODED_GUEST.mail;
        }
    /*Guest to facebook*/

    /*JOIN*/
        $rootScope.goJoin = function(){

            $rootScope.toggleAnnoyingMessage(false, true);

            if(TOKEN_DEBUG){
                $rootScope.dni = undefined;
                $rootScope.user = undefined;
                $rootScope.stars = undefined;
                $rootScope.borraTablas(true, function(){console.log("going to join");$location.path("/join");});
                TOKEN = undefined;
            } else {
                Playtomic.Auth.logout(function(response) {
                    $rootScope.dni = undefined;
                    $rootScope.user = undefined;
                    $rootScope.stars = undefined;
                    $rootScope.borraTablas(true, function(){console.log("going to join");$location.path("/join");});
                });
            }    
        }
    /*JOIN*/

    /*Grisino-specific methods*/
        $rootScope.grisinoapi = function(dni, callback) {

            var dataObj = {
                dni: dni || $rootScope.dni
            };

            if ($rootScope.user) {
                $rootScope.user.facebookId = "";
            }

            $rootScope.loading = true;

            var res = $http.post('http://' + SERVER + '.reachout.global:3010/checkExistingDNI', dataObj);

            res.success(function(data, status, headers, config) {
                $rootScope.loading = false;
                console.log(data);
                if (data.exists) {
                    var confirm = $mdDialog.confirm()
                        .title('Aviso')
                        .content('El DNI ingresado ya corresponde a un usuario registrado en la aplicación. Puede salir del registro e ingresar con el e-mail asociado a dicho DNI. De haberse equivocado ingresando el DNI (' + dataObj.dni + '), puede intentar reingresarlo...')
                        .ariaLabel('Lucky day')
                        .ok('Reingresar')
                        .cancel('Salir')
                        .targetEvent();

                    $mdDialog.show(confirm).then(function() {
                        $rootScope.askDNI();
                    }, function() {
                        $location.path("/mainPanel");
                    });
                } else {
                    $rootScope.dni = dataObj.dni;
                    if(callback)
                        callback();
                }

            });

            res.error(function(data, status, headers, config) {
                console.log(data);
                $rootScope.loading = false;
                $location.path("/mainPanel");
                $mdToast.show(
                    $mdToast.simple()
                    .content('Error en registro. Intente más tarde.')
                    .position('top')
                    .hideDelay(3000)
                );
            });
        }
        $rootScope.getStars = function(callback) {

            var dataObj = {
                dni: $rootScope.dni || $rootScope.user.national_identity
            }
            
            if(!dataObj.dni)
                if(callback){
                    callback();
                    return;
                }

            var serverNotResponding = setTimeout(function(){
                var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "051"};
                $rootScope.addLog(_thelog);
            }, 10000);

            var res = $http.post('http://' + SERVER + '.reachout.global:3010/getStars', dataObj);

            res.success(function(data, status, headers, config) {
                $rootScope.stars = {
                    bool: parseInt(data.estrellas) >= 0,
                    stars: data.estrellas
                };

                $rootScope.user.stars = data.estrellas;
                var query = "UPDATE usuario SET content=(?)";
                $cordovaSQLite.execute($rootScope.db, query, [JSON.stringify($rootScope.user)]).then(function(res) {
                    console.log("SAVED STARS TO USER.");
                    console.log(res);
                }, function(err) {
                    console.error(JSON.stringify(err));
                });

                clearTimeout(serverNotResponding);

                if(callback)
                    callback();
            });

            res.error(function(data, status, headers, config) {

                $rootScope.loading = false;

                var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "050"};
                $rootScope.addLog(_thelog);

                $mdToast.show(
                    $mdToast.simple()
                    .content('Error obteniendo cantidad de estrellas.')
                    .position('top')
                    .hideDelay(3000)
                );

                clearTimeout(serverNotResponding);

                if(callback)
                    callback();
            });
        }
        $rootScope.isGrisinoFan = function(){
            return $rootScope.stars && $rootScope.stars.bool && $rootScope.stars.bool != -1;
        }
        $rootScope.toggleAnnoyingMessage = function(force, completely) {

            // Hide completely
            if (completely || $rootScope.state == "Detalle") {
                if (force == undefined) {
                    $(".annoyingMessage").slideToggle("slow");
                } else {
                    if (force) {
                        $(".annoyingMessage").slideDown("slow");
                        $(".annoyingMessage").children(".text").slideDown("slow");
                        $(".annoyingMessage").children(".decoy-text").slideUp("slow");
                        $(".annoyingMessage").find(".bottom").fadeOut("slow");
                        $(".annoyingMessage").find(".top").fadeIn("slow");
                    } else {
                        $(".annoyingMessage").slideUp("slow");
                        $(".annoyingMessage").children(".text").slideUp("slow");
                        $(".annoyingMessage").children(".decoy-text").slideDown("slow");
                    }
                }
            }

            // Toggle the height of the message          
            if (force == undefined) {
                $(".annoyingMessage").children(".text").slideToggle("slow");
                $(".annoyingMessage").children(".decoy-text").slideToggle("slow");

                if($(".annoyingMessage").find(".top").css("display") == "none"){

                    $(".annoyingMessage").find(".bottom").fadeToggle("slow", function(){
                        $(".annoyingMessage").find(".top").fadeToggle("slow");
                    });

                } else {

                    $(".annoyingMessage").find(".top").fadeToggle("slow", function(){
                        $(".annoyingMessage").find(".bottom").fadeToggle("slow");
                    });

                }
            } else {
                if (force && $rootScope.state != "Compartí tu foto!") {
                    $(".annoyingMessage").children(".text").slideDown("slow");
                    $(".annoyingMessage").children(".decoy-text").slideUp("slow");
                    $(".annoyingMessage").find(".top").fadeIn("slow", function() {
                        $(".annoyingMessage").find(".bottom").fadeOut("slow");
                    });
                } else {
                    $(".annoyingMessage").children(".text").slideUp("slow");
                    $(".annoyingMessage").children(".decoy-text").slideDown("slow");
                    $(".annoyingMessage").find(".bottom").fadeIn("slow", function() {
                        $(".annoyingMessage").find(".top").fadeOut("slow");
                    });
                }
            }
        }
    /*Grisino-specific methods*/

    /*Get TODAY´s date*/
        $rootScope.NOW = function() {
            var _now = new Date().toISOString();
            return _now;
        }
    /*Get TODAY´s date*/

    /*Sharing*/
        $rootScope.shareTwitter = function(logId) {
            var id = detailed_info.id,
                name = detailed_info.name,
                description = detailed_info.description;
            
            $cordovaSocialSharing.shareViaTwitter(name + ' - ' + 'Grisino Fan App.', getBase64Image(document.getElementById(id)), "www.grisino.com")
                .then(function(result) {

                    console.log(result);

                    $rootScope.loading = false;

                    var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:logId,elementId:id};
                    $rootScope.addLog(_thelog);
                    /*var _log = JSON.stringify(_thelog) + ",";
                    var query = "INSERT INTO logs (log) VALUES ('" + _log + "')";
                    $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {}, function(err) {
                        console.error(err);
                    });*/

                }, function(err) {
                    $rootScope.loading = false;
                    console.log(err);
                    $mdToast.show(
                        $mdToast.simple()
                        .content('No se compartió en Twitter')
                        .position('top')
                        .hideDelay(5000)
                    );
                });
        }

        function getBase64Image(img) {
            var canvas = document.createElement("canvas");
            canvas.width = img.naturalWidth;
            canvas.height = img.naturalHeight;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);
            var dataURL = canvas.toDataURL("image/png");
            //return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
            return dataURL;
        }

        $rootScope.shareInstagram = function(logId) {
            var id = detailed_info.id,
                name = detailed_info.name;

            $rootScope.loading = true;

            /*TODO: element.img*/
            Instagram.share(getBase64Image(document.getElementById(id)), name, function(err) {
                if (err) {
                    //console.log("not shared");
                    $rootScope.loading = false;

                    $mdToast.show(
                        $mdToast.simple()
                        .content('No se pudo compartir en Instagram')
                        .position('top')
                        .hideDelay(5000)
                    );

                } else {

                    $rootScope.loading = false;
                    //console.log("shared");

                    var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:logId,elementId:sharingElement.id};
                    $rootScope.addLog(_thelog);
                    /*
                    var _log = JSON.stringify(_thelog) + ",";
                    var query = "INSERT INTO logs (log) VALUES ('" + _log + "')";
                    $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {}, function(err) {
                        console.error(err);
                    });*/
                }
            });
        }

        $rootScope.shareFacebook = function(logId) {

            console.log(detailed_info);

            var source = detailed_info.source,
                name = detailed_info.name,
                description = detailed_info.description,
                id = detailed_info.id;

            $rootScope.loading = true;

            $cordovaFacebook.getLoginStatus()
                .then(function(success) {

                    comparteFace(source,name,description, id);

                    console.log(success);

                }, function(error) {
                    // error facebookStatusLogin
                    console.log(error);
                    $cordovaFacebook.login(["public_profile", "email", "user_friends"])
                        .then(function(success) {
                            comparteFace(source,name, description, id, logId);
                        }, function(error) {
                            // error
                            $rootScope.loading = false;
                        });
                });
        }


        function comparteFace(source,name, description, id, logId) {

            var options = {
                method: "feed",
                picture: source,
                name: name,
                link: "http://www.grisino.com/",
                description: 'Lo vi primero en Grisino Fan App. Descargala ahora para iOS y Android. ' + description.replace(/<(?:.|\n)*?>/gm, '')
            };

            $cordovaFacebook.showDialog(options).then(function(success) {
                $rootScope.loading = false;

                var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:logId,elementId:id};
                $rootScope.addLog(_thelog);
                /*
                var _log = JSON.stringify(_thelog) + ",";
                var query = "INSERT INTO logs (log) VALUES ('" + _log + "')";
                $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {}, function(err) {
                    console.error(err);
                });*/

            }, function(error) {
                $rootScope.loading = false;

                if (error == "User cancelled dialog") {
                    $mdToast.show(
                        $mdToast.simple()
                        .content('No se compartió en Facebook')
                        .position('top')
                        .hideDelay(5000)
                    );
                } else if (error == "No active session") {
                    $cordovaFacebook.login(["public_profile", "email", "user_friends"])
                        .then(function(success) {
                            comparteFace();
                        }, function(error) {
                            $rootScope.loading = false;
                        });
                }

                console.error(error);

            });
        }
    /*Sharing*/

    var s = setInterval(function() {
        if(!USEPLUGINS){
            //alert("ANGULAR STILL NOT READY");
            $rootScope.loading = true;  
        } else {
           //alert("ANGULAR READY");
           setTimeout(start,700); 
           clearInterval(s);
        }
    }, 500);

    function start() {

        //alert("STARTING ANGULAR USE");

        /*--------------PLUGIN LOAD DEPENDANT METHODS--------------*/
        /*Get OS version*/
        IOSVERSION = $cordovaDevice.getVersion();
        /*Get OS version*/

        $rootScope.getOSVersion = function(){
            return IOSVERSION;
        }

        /*Initialize IP*/
            if (networkinterface) {
                networkinterface.getIPAddress(function(ip) {
                    $rootScope.IP = ip;
                });
            }
        /*Initialize IP*/

        /*Get version*/
            $rootScope.getVersion = function() {
                var itsfine = true;
                //if ($rootScope.isOnLine) {
                    var dataObj = {
                        data: "eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJicmFuZHMiLCJhY3Rpb24iOiJnZXRWZXJzaW9uIiwicHVibGlja2V5IjoibmtmdnQ3Z3BnZGtha3liNzhncjkiLCJicmFuZCI6IkdyaXNpbm8ifQ==",
                        hash: "0c5f3d46f0f0dbf1b842e9bb72179a55"
                    };

                    var resUpdate = $http.post('http://' + SERVER + '.reachout.global/api?publickey=nkfvt7gpgdkakyb78gr9', dataObj, {headers: {'Authorization':'Bearer ' + TOKEN}});

                    resUpdate.success(function(data, status, headers, config) {

                        if (data.success) {
                            console.log(VERSION + " - " + data.version.version_number);
                            console.log(VERSION < data.version.version_number);

                            if (VERSION < data.version.version_number) {
                                if (data.version.version_required == true) {

                                    var confirm = $mdDialog.confirm()
                                        .title('Nueva versión')
                                        .content('Hay una nueva versión de Grisino Fan. Es necesario que actualices la aplicación para continuar accediendo. Gracias.')
                                        .ariaLabel('Lucky day')
                                        .ok('Actualizar')
                                        .cancel('Salir')
                                        .targetEvent();
                                    $mdDialog.show(confirm).then(function() {

                                        if(TOKEN_DEBUG){
                                            $rootScope.dni = undefined;
                                            $rootScope.user = undefined;
                                            $rootScope.stars = undefined;
                                            $rootScope.borraTablas(true);
                                            window.open('itms-apps://itunes.apple.com/app/idcom.adselerate.grisino');
                                            TOKEN = undefined;
                                        } else {
                                            Playtomic.Auth.logout(function(response) {
                                                $rootScope.dni = undefined;
                                                $rootScope.user = undefined;
                                                $rootScope.stars = undefined;
                                                $rootScope.borraTablas(true);
                                                window.open('itms-apps://itunes.apple.com/app/idcom.adselerate.grisino');
                                            });
                                        }
                                    }, function() {
                                        if(TOKEN_DEBUG){
                                            $rootScope.dni = undefined;
                                            $rootScope.user = undefined;
                                            $rootScope.stars = undefined;
                                            $rootScope.borraTablas(true);
                                            TOKEN = undefined;
                                        } else {
                                            Playtomic.Auth.logout(function(response) {
                                                $rootScope.dni = undefined;
                                                $rootScope.user = undefined;
                                                $rootScope.stars = undefined;
                                                $rootScope.borraTablas(true);
                                            });
                                        }
                                        itsfine = false;
                                    });

                                } else {

                                    var confirm = $mdDialog.confirm()
                                        .title('Nueva versión')
                                        .content('Hay una nueva versión de Grisino Fan. Descargala para obtener una mejor experiencia.')
                                        .ariaLabel('Lucky day')
                                        .ok('Actualizar')
                                        .cancel('¡Ahora no!')
                                        .targetEvent();
                                    $mdDialog.show(confirm).then(function() {
                                        window.open('itms-apps://itunes.apple.com/app/idcom.adselerate.grisino');
                                    }, function() {
                                        
                                    });
                                }
                            }
                        }
                    });

                    resUpdate.error(function(data, status, headers, config) {
                        
                    });
                //}
                return itsfine;
            }
        /*Get version*/

        /*Check location*/
            setTimeout(function() {
                function onSuccess(position){
                    console.log("Updated location");
                    $rootScope.lat = position.coords.latitude;
                    $rootScope.lng = position.coords.longitude;
                }

                function onError(error){

                    var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP,id:611};
                    $rootScope.addLog(_thelog);

                    var confirm = $mdDialog.confirm()
                        .title('No se encontro tu posición')
                        .content('¡A los mejores beneficios accederás solo con GPS activado!')
                        .ariaLabel('Lucky day')
                        .ok('Ok')
                        .cancel('Cancelar')
                        .targetEvent();
                    $mdDialog.show(confirm).then(function() {}, function() {});
                    $rootScope.lat = undefined;
                    $rootScope.lng = undefined;
                }

                $cordovaGeolocation.getCurrentPosition({timeout: 30000, enableHighAccuracy: false}).then(onSuccess,onError);
            }, 30000);
        /*Check location*/

        /*Check connection*/
            var type = $cordovaNetwork.getNetwork();
            var isOnline = $cordovaNetwork.isOnline(); /// stguelve TRUE
            var isOffline = $cordovaNetwork.isOffline(); /// stguelve FALSE

            $rootScope.isOnLine = isOnline;

            if ($rootScope.isOnLine) {
                //console.log('si está');
            } else {
                var confirm = $mdDialog.confirm()
                    .title('Sin conexión')
                    .content('No se encontró ninguna conexión disponible')
                    .ok('Ok');

                $mdDialog.show(confirm).then(function() {}, function() {
                    _exitApp = false;
                });
            }

            $rootScope._isOnline = function(){
                return $cordovaNetwork.isOnline();
            }
        /*Check connection*/

        /*addLog*/
            $rootScope.addLog = function(json){

                var log;
                if(json){
                    if(typeof json.lat === 'string')
                        json.lat = parseFloat(json.lat);

                    if(typeof json.lng === 'string')
                        json.lng = parseFloat(json.lng);

                    if(json.elementID)
                        json.elementId = json.elementID;

                    if(json.id)
                        json.logId = json.id;

                    log = json;
                } else {
                    log = {date:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),logId: 303};
                }

                var category = (json ? (json.logId + "").substring(0,1) : "3");
                var action;
                switch(category){
                    case "0":
                        category = "Errors";

                        action = (json.logId + "").substring(1,3);
                        switch(action){
                            case "50":
                                action = "Points Request: Server Error";
                                break;
                            case "51":
                                action = "Points Request: Server Not Responding";
                                break;
                            case "52":
                                action = "Promotions Request: Server Error";
                                break;
                            case "53":
                                action = "Promotions Request: Server Not Responding";
                                break;
                            case "54":
                                action = "Promotions Request: Promos Not Saved to local DB";
                                break;
                            case "55":
                                action = "Promotions Request: Server Not Response From Service";
                                break;
                            case "56":
                                action = "Promotions Request: Server Success False from Service";
                                break;
                            case "57":
                                action = "Promotions Request: Promotion Array Empty";
                                break;
                            case "58":
                                action = "Contents Request: Server Error";
                                break;
                            case "59":
                                action = "Contents Request: Server Not Responding";
                                break;
                            case "60":
                                action = "Contents Request: Contents Not Saved to Local DB";
                                break;
                            case "61":
                                action = "Contents Request: Server Not Response From Service";
                                break;
                            case "62":
                                action = "Contents Request: Server Success False from Service";
                                break;
                            case "63":
                                action = "Contents Request: Contents Array Empty";
                                break;
                            case "64":
                                action = "Shops Request: Server Error";
                                break;
                            case "65":
                                action = "Shops Request: Server Not Responding";
                                break;
                            case "66":
                                action = "Shops Request: Not Response from Service";
                                break;
                            case "67":
                                action = "Shops Request: Shops Not Saved to Local DB";
                                break;
                            case "68":
                                action = "Shops Request: Server Success False from Service";
                                break;
                            case "69":
                                action = "Profile Request: Server Error";
                                break;
                            case "70":
                                action = "Profile Request: Server Not Responding";
                                break;
                            case "71":
                                action = "Promotion Redemption Error: Server Error";
                                break;
                            case "72":
                                action = "Promotion Redemption Failed: Server Not Responding";
                                break;
                            case "73":
                                action = "Logs Request: Server Not Response from Service";
                                break;
                            case "74":
                                action = "Categories Request: Server Not Response from Service";
                                break;
                            case "75":
                                action = "Categories Request: Categories Not Saved to Local DB";
                                break;
                            case "76":
                                action = "Categories Request: Categories Not Saved to Local DB Service";
                                break;
                        }

                        break;
                    case "1":
                        category = "App";

                        action = (json.logId + "").substring(1,3);
                        switch(action){
                            case "00":
                                action = "Session started";
                                break;
                        }

                        break;
                    case "2":
                        category = "Menu";

                        action = (json.logId + "").substring(1,3);
                        switch(action){
                            case "01":
                                action = "Feature tapped";
                                break;
                            case "02":
                                action = "Promotions tapped";
                                break;
                            case "03":
                                action = "News tapped";
                                break;
                            case "04":
                                action = "Shops tapped";
                                break;
                            case "05":
                                action = "E-shop tapped";
                                break;
                            case "06":
                                action = "Help Us tapped";
                                break;
                            case "07":
                                action = "Contact tapped";
                                break;
                            case "08":
                                action = "Profile tapped";
                                break;
                            case "09":
                                action = "Notifications tapped";
                                break;
                            case "10":
                                action = "FAQ tapped";
                                break;
                            case "11":
                                action = "Category tapped";
                                break;
                            case "12":
                                action = "Log Out tapped";
                                break;
                        }

                        break;
                    case "3":
                        category = "Promotions";

                        action = (json.logId + "").substring(1,3);
                        switch(action){
                            case "00":
                                action = "Promotion Preview";
                                break;
                            case "01":
                                action = "Promotion Opened";
                                break;
                            case "02":
                                action = "No Code Promotion Redeemed";
                                break;
                            case "03":
                                action = "E-shop Promotion Redeemed";
                                break;
                            case "04":
                                action = "Code Promotion Redeemed Success";
                                break;
                            case "05":
                                action = "Code Promotion Redeemed Step 1";
                                break;
                            case "06":
                                action = "Code Promotion Redeemed Step 2 Accepted";
                                break;
                            case "07":
                                action = "Code Promotion Redeemed Step 2 Cancelled";
                                break;
                            case "08":
                                action = "Code Promotion Redeemed Step 2 Insufficient Points";
                                break;
                            case "10":
                                action = "Promotion Notifiaction Delivered";
                                break;
                            case "11":
                                action = "Promotion Notifiaction Opened";
                                break;
                            case "12":
                                action = "Promotion Notifiaction Closed";
                                break;
                            case "21":
                                action = "Share: Facebook";
                                break;
                            case "22":
                                action = "Share: Instagram";
                                break;
                            case "23":
                                action = "Share: Twitter";
                                break;
                        }

                        break;
                    case "4":
                        category = "Contents";

                        action = (json.logId + "").substring(1,3);
                        switch(action){
                            case "00":
                                action = "Content Preview";
                                break;
                            case "01":
                                action = "Content Opened";
                                break;
                            case "10":
                                action = "Content Notification Delivered";
                                break;
                            case "11":
                                action = "Content Notification Opened";
                                break;
                            case "12":
                                action = "Content Notification Closed";
                                break;
                            case "21":
                                action = "Share: Facebook";
                                break;
                            case "22":
                                action = "Share: Instagram";
                                break;
                            case "23":
                                action = "Share: Twitter";
                                break;
                            case "02":
                                action = "Content Video Played";
                                break;
                        }

                        break;
                    case "5":
                        category = "Shops";

                        action = (json.logId + "").substring(1,3);
                        switch(action){
                            case "00":
                                action = "Shop Preview";
                                break;
                            case "01":
                                action = "Shop Opened";
                                break;
                            case "02":
                                action = "Shop How to Get There";
                                break;
                        }

                        break;
                    case "6":
                        category = "Permissions";

                        action = (json.logId + "").substring(1,3);
                        switch(action){
                            case "00":
                                action = "Notifications Enabled";
                                break;
                            case "01":
                                action = "Notifications Disabled";
                                break;
                            case "10":
                                action = "GPS Enabled";
                                break;
                            case "11":
                                action = "GPS Disabled";
                                break;
                        }

                        break;
                    case "7":
                        category = "Beacons";

                        action = (json.logId + "").substring(1,3);
                        switch(action){
                            case "00":
                                action = "Beacon Battery Percentage";
                                break;
                        }

                        break;
                    case "8":
                        category = "Profile";

                        action = (json.logId + "").substring(1,3);
                        switch(action){
                            case "00":
                                action = "Profile Updated";
                                break;
                        }

                        break;
                    case "9":
                        category = "Brand Specifics";

                        action = (json.logId + "").substring(1,3);
                        switch(action){
                            case "00":
                                action = "Grisino: Menu Games Tapped";
                                break;
                            case "01":
                                action = "Grisino: Grisino Fan CTA Tapped";
                                break;
                        }

                        break;
                }

                window.ga.trackEvent(category, action, 'Label', json.logId + "")

                $cordovaSQLite.execute($rootScope.db, "INSERT INTO logs (log) VALUES ('" + JSON.stringify(log) + "')", []).then(function(res) {}, function(err) {
                    console.error(err);
                });
            }
        /*addLog*/

        /*DB Tables create & delete*/
            $rootScope.borraTablas = function(alsoCreate, callback, dontLogout) {

                var query = "DROP TABLE IF EXISTS logs";
                $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                    //console.log("borrada logs");
                    var query = "DROP TABLE IF EXISTS usuario";
                    $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                        //console.log("borrada usuario");
                        var query = "DROP TABLE IF EXISTS contenidos";
                        $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                            //console.log("borrada contenidos");
                            var query = "DROP TABLE IF EXISTS promos";
                            $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                                //console.log("borrada promos");
                                var query = "DROP TABLE IF EXISTS venues";
                                $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                                    //console.log("borrada venues");
                                    var query = "DROP TABLE IF EXISTS categorias";
                                    $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                                        //console.log("borrada categorias");
                                        var query = "DROP TABLE IF EXISTS notificaciones";
                                        $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                                            //console.log("borrada notificaciones");
                                            var query = "DROP TABLE IF EXISTS config";
                                            $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                                                //console.log("borrada notificaciones");

                                                if(TOKEN_DEBUG){
                                                    if (alsoCreate)
                                                        $rootScope.creaTablas();
                                                    $location.path("/mainPanel");
                                                    TOKEN = undefined;
                                                } else if(!dontLogout){
                                                    Playtomic.Auth.logout(function(response) {
                                                            if (response.success) {
                                                                $location.path("/mainPanel");
                                                                
                                                                if (alsoCreate)
                                                                    $rootScope.creaTablas(callback);
                                                                else if(callback)
                                                                    callback();                                                                
                                                            }
                                                        }
                                                    );
                                                } else {
                                                    if (alsoCreate)
                                                        $rootScope.creaTablas(callback);
                                                    else if(callback)
                                                        callback(); 
                                                }
                                            }, function(err) {
                                                console.error(err);
                                            });
                                        }, function(err) {
                                            console.error(err);
                                        });
                                    }, function(err) {
                                        console.error(err);
                                    });
                                }, function(err) {
                                    console.error(err);
                                });
                            }, function(err) {
                                console.error(err);
                            });
                        }, function(err) {
                            console.error(err);
                        });
                    }, function(err) {
                        console.error(err);
                    });
                }, function(err) {
                    console.error(err);
                });
            }
            $rootScope.creaTablas = function(callback) {

                var query = "CREATE TABLE IF NOT EXISTS logs (_id INTEGER NOT NULL PRIMARY KEY, log TEXT)";
                $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                    var query = "CREATE TABLE IF NOT EXISTS usuario (_id INTEGER NOT NULL PRIMARY KEY, content TEXT, facebook_id TEXT, user_id TEXT, rDate TEXT)";
                    $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                        var query = "CREATE TABLE IF NOT EXISTS contenidos (_id INTEGER NOT NULL PRIMARY KEY, fecha TEXT , content TEXT )";
                        $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                            var query = "CREATE TABLE IF NOT EXISTS promos (_id INTEGER NOT NULL PRIMARY KEY, fecha TEXT , content TEXT )";
                            $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                                var query = "CREATE TABLE IF NOT EXISTS venues (_id INTEGER NOT NULL PRIMARY KEY, fecha TEXT , content TEXT )";
                                $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                                    var query = "CREATE TABLE IF NOT EXISTS categorias (_id INTEGER NOT NULL PRIMARY KEY, fecha TEXT , content TEXT )";
                                    $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                                        var query = "CREATE TABLE IF NOT EXISTS notificaciones (_id INTEGER NOT NULL PRIMARY KEY, fecha TEXT , notificacionId TEXT, elementId TEXT, geofence TEXT)";
                                        $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                                            var query = "CREATE TABLE IF NOT EXISTS config (_id INTEGER NOT NULL PRIMARY KEY, content TEXT)";
                                            $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
                                                if(callback)
                                                    callback();
                                            }, function(err) {
                                                console.error(err);
                                            });
                                        }, function(err) {
                                            console.error(err);
                                        });
                                    }, function(err) {
                                        console.error(err);
                                    });
                                }, function(err) {
                                    console.error(err);
                                });
                            }, function(err) {
                                console.error(err);
                            });
                        }, function(err) {
                            console.error(err);
                        });
                    }, function(err) {
                        console.error(err);
                    });
                }, function(err) {
                    console.error(err);
                });
            }
        /*DB Tables create & delete*/

        /*DB Checkers*/
            checkUserExist = function() {

                $cordovaSQLite.execute($rootScope.db, "SELECT * FROM usuario", []).then(function(res) {
                    console.log(res);
                    if (res.rows.length != 0) {
                        for (var i = 0; i < res.rows.length; i++) {
                            $rootScope.user = JSON.parse(res.rows.item(i).content);
                            $rootScope.dni = $rootScope.user.national_identity;

                            $rootScope.isGuest = $rootScope.user.email == HARDCODED_GUEST.mail;

                            $rootScope.stars = {
                                bool: parseInt($rootScope.stars) >= 0,
                                stars: $rootScope.stars
                            };
                            console.log("FOUND STARS IN SAVED USER...");
                            console.log($rootScope.stars);

                            TOKEN = $rootScope.user.token;
                            TOKEN_DEBUG = TOKEN == undefined;

                            window.ga.setUserId($rootScope.user.id);
                        }

                        if($rootScope.dni && $rootScope.dni != ""){
                            cacheContent();
                        } else {
                            $rootScope.getVersion();
                            $location.path("/mainPanel");
                            $rootScope.states = ["mainPanel"];    
                        }
                                                  
                    } else {
                        $location.path("/mainPanel");
                        $rootScope.states = ["mainPanel"];
                    }

                }, function(err) {});
            }
            $rootScope.checkTablas = function() {

                $cordovaSQLite.execute($rootScope.db, "SELECT * FROM contenidos", []).then(function(res) {
                    if (res.rows.length != 0) {

                        for (var i = 0; i < res.rows.length; i++)
                            $rootScope.contentCache = JSON.parse(res.rows.item(i).content);
                        console.log("cacheContent");
                        cacheContent();
                    } else {
                        console.log("creamosCache");
                        creamosCache();
                    }
                }, function(err) {});
            }
        /*DB Checkers*/

        /*SQLITE Content getters*/
            cacheContent = function() {

                $cordovaSQLite.execute($rootScope.db, "SELECT * FROM contenidos", []).then(function(res) {
                    if (res.rows.length != 0) {

                        for (var i = 0; i < res.rows.length; i++) {
                            $rootScope.contentCache = JSON.parse(res.rows.item(i).content);
                        }
                        console.log("Cache content");

                        cachePromos();

                    } else {
                        creamosCache();
                    }
                }, function(err) {});
            }

            cachePromos = function() {

                $cordovaSQLite.execute($rootScope.db, "SELECT * FROM promos", []).then(function(res) {
                    if (res.rows.length != 0) {
                        for (var i = 0; i < res.rows.length; i++) {
                               
                            $rootScope.promosCache = JSON.parse(res.rows.item(i).content);
                        }
                        console.log("Cache promos");

                        cacheVenues();
                    } else {
                        promoList();
                    }
                }, function(err) {});
            }

            cacheVenues = function() {

                $cordovaSQLite.execute($rootScope.db, "SELECT * FROM venues", []).then(function(res) {
                    if (res.rows.length != 0) {
                        for (var i = 0; i < res.rows.length; i++) {
                            //console.log(res.rows.item(i).content);
                            $rootScope.venuesCache = JSON.parse(res.rows.item(i).content);
                        }
                        console.log("Cache venues");

                        cacheCategorias();
                    } else {
                        shopList();
                    }
                }, function(err) {});
            }

            cacheCategorias = function() {

                $cordovaSQLite.execute($rootScope.db, "SELECT * FROM categorias", []).then(function(res) {
                    if (res.rows.length != 0) {

                        for (var i = 0; i < res.rows.length; i++) {
                            $rootScope.categoriasCache = JSON.parse(res.rows.item(i).content);
                        }

                        console.log("Cache categorias");

                        //if (notificationId == "" || !notificationId) {
                            $location.path("/main/home");
                            //$rootScope.states = ["main.home"];

                        /*} else {
                            for (var a = 0; a < $rootScope.promosCache.length; a++) {
            
                                $rootScope.user.facebookId = "";

                                if ($rootScope.promosCache[a]._nid == notificationId) {

                                    str = '<div class="newsPiece" layout="column"> <div id="content' + $rootScope.promosCache[a]._id + '" class="imageWrapper defaultImage" style=" background-size: 80px; background-image: url(\'assets/images/IconoPerfilEstrella.png\')"> <table class="imgDeadLine"> <tr> <td colspan="2" style="text-align: center"> <div class="imgMidText">' + $rootScope.promosCache[a].line1 + '</div></td></tr><tr> <td colspan="2" style="text-align: center"> <div class="imgMidText imgSubText">' + $rootScope.promosCache[a].line2 + '</div></td></tr><tr> <td style="width: 35%"></td></tr></table> <div class="imageDarkerer"></div><div class="imageDarkerer imageClickHandler"></div><img src="https://s3-sa-east-1.amazonaws.com/stg.reachout.global/promo/' + $rootScope.promosCache[a]._id + '.jpg" id="' + $rootScope.promosCache[a]._id + '" onload="this.style.opacity=1;" class="newsElement"></img></div> <div class="newsElementBottomWrapper"><span class="newsElementTitle">' + $rootScope.promosCache[a].description_text + '...</span></div></div>';

                                    var tab = angular.element(str);

                                    $rootScope.detail($rootScope.promosCache[a].id, 'promo');
                                }
                            }
                        }*/
                        navigator.splashscreen.hide();
                    } else {
                        categoriasList();
                    }
                }, function(err) {});
            }
        /*SQLITE Content getters*/

        /*DNI Handlers*/
            $rootScope.checkDNI = function() {
                //Diálogo para DNI
                console.log($rootScope.user);

                // Already has DNI
                if ($rootScope.user && $rootScope.user.national_identity && $rootScope.states[$rootScope.states.length - 1] != "mainPanel" && $rootScope.states[$rootScope.states.length - 1] != "join") {
                    $rootScope.grisinoapi($rootScope.user.national_identity);
                } else {
                    $rootScope.askDNI();
                }
            }
            
            $rootScope.askDNI = function(callback, dontExit, dniText, errorText) {
 
                $mdDialog.show({
                    locals: {

                    },
                    controller: function($scope, $mdDialog) {

                        function isNumeric(s){
                            var ret = true;
                            for(var i = 0; i < s.length; i++){
                                if(s[i] > "9"){
                                    ret = false;
                                    break;
                                }
                            }
                            return ret;
                        }

                        $scope.changeDNI = function(d){
                            if(d && d != ""){
                                if($scope.dni_msg == "Debe de llenarse el DNI para continuar."){
                                    $scope.dni_error = false;
                                    $scope.dni_msg = undefined;
                                }

                                if(isNumeric(d)){
                                    $scope.dni_error = false;
                                    $scope.dni_msg = undefined;
                                } else {
                                    $scope.dni_msg = "El DNI solo puede contener números.";
                                    $scope.dni_error = true;
                                }
                            }
                        }

                        $scope.hide = function() {
                            $mdDialog.hide();
                        };

                        $scope.cancel = function() {
                            $mdDialog.cancel();
                        };

                        $scope.back = function() {
                            $mdDialog.cancel();
                        };

                        $scope.joinDNI = function() {
                            console.log("GETS HERE");
                            if($scope.dni && $scope.dni != ""){
                                console.log("DNI EXISTS");
                                if(isNumeric($scope.dni)){
                                    console.log("DNI IS NUMERIC");
                                    var dataObj = {dni: $scope.dni};

                                    $rootScope.loading = true;
                                    var res = $http.post('http://'+ SERVER + '.reachout.global:3010/checkExistingDNI', dataObj);

                                    res.success(function(data, status, headers, config) {
                                        $rootScope.loading = false;
                                        console.log(data);
                                        if (data.exists) {
                                            var confirm = $mdDialog.confirm()
                                                .title('Aviso')
                                                .content('El DNI ingresado ya corresponde a un usuario registrado en la aplicación. Puede salir del registro e ingresar con el e-mail asociado a dicho DNI. De haberse equivocado ingresando el DNI (' + dataObj.dni + '), puede intentar reingresarlo...')
                                                .ariaLabel('Lucky day')
                                                .ok('Reingresar')
                                                .cancel('Salir')
                                                .targetEvent();

                                            $mdDialog.show(confirm).then(function() {
                                                $rootScope.askDNI(callback);
                                            }, function() {
                                                if(!dontExit){
                                                    $location.path("/mainPanel");
                                                } else {
                                                    dontExit();
                                                    $mdDialog.hide($rootScope.dni);
                                                }
                                            });
                                        } else {
                                            $rootScope.dni = dataObj.dni;
                                            $mdDialog.hide($scope.dni);
                                        }

                                    });

                                    res.error(function(data, status, headers, config) {
                                        console.log(data);
                                        $rootScope.loading = false;
                                        if(!dontExit){
                                                    $location.path("/mainPanel");
                                                } else {
                                                    dontExit();
                                                    $mdDialog.hide($rootScope.dni);
                                                }
                                        $mdToast.show(
                                            $mdToast.simple()
                                            .content(errorText ? errorText : 'Error en registro. Intente más tarde.')
                                            .position('top')
                                            .hideDelay(3000)
                                        );
                                    });
                                    
                                } else {
                                    console.log("EL DNI " + $scope.dni + " NO ES NUMERICO");
                                }
                            } else {
                                $scope.dni_msg = "Debe de llenarse el DNI para continuar.";
                                $scope.dni_error = true;
                            }
                        }
                    },
                    controllerAs: 'ctrl',
                    template: '<md-dialog style="padding: 20px;"><h1 style="font-size: 18px;">' + (dniText ? dniText : 'Para convertirte en Grisino Fan o acceder como tal, necesitamos tu DNI') + '</h1><div style="overflow-y: scroll" class=""><md-input-container><label>DNI</label><input ng-change="changeDNI(dni)" ng-model="dni"></input></md-input-container></div><h4 style="color:red;" ng-show="dni_error">{{dni_msg}}</h4><div style="width: 100%;" layout="row"><md-button style="width: 100%!important; height: 52px; background-color: green!important; margin: 0px 5px 0px 0px!important; font-size: 4vw;padding: 0px" ng-click="cancel()" class="md-primary md-raised loginBtn">Cancelar</md-button><md-button style="width: 100%!important; height: 52px; background-color: green!important; margin: 0px 0px 0px 5px!important; padding: 0px" ng-click="joinDNI()" class="md-primary md-raised loginBtn"> Aceptar </md-button></div></md-dialog>',
                    clickOutsideToClose: false
                }).then(function(dni) {
                    console.log("Ended AskDNI, executing callback with " + dni);
                    if(dni)
                    $rootScope.dni = dni;

                    if (callback)
                        callback(dni);

                }, function() {});
            }
        /*DNI Handlers*/

        /*Handle connection/notification issues*/
            $rootScope.stop_ConnectionCheck = function(){

                clearInterval(interval_ConnectionCheck);
                interval_ConnectionCheck = undefined;

                $rootScope.retry_ConnectionCheck = function(){}

                $(".content").removeClass("blur");
                if($(".content").hasClass("noscroll"))
                    $(".content").removeClass("noscroll"); 

                $(".modal.connection-error.popup").fadeOut("slow", function(){
                    $(".modal.connection-error.popup").css("top", "100%");
                });
            }
            $rootScope.retry_ConnectionCheck = function(){}
            $rootScope.checkConnection = function(actionMethod, actionMessage) {

                if(actionMethod)
                    $rootScope.retry_ConnectionCheck = actionMethod;

                if($cordovaNetwork.isOffline()){

                    $rootScope.loading = false;

                    $(".content").addClass("blur");
                    if(!$(".content").hasClass("noscroll"))
                        $(".content").addClass("noscroll"); 

                    $(".modal.connection-error.popup").children(".text").html(actionMessage || "Reintentando...");

                    $(".modal.connection-error.popup").fadeOut("fast", function(){
                        $(".modal.connection-error.popup").css("top", 20 + $(".header.master-header").height() + "px");

                        $(".modal.connection-error.popup").fadeIn("slow",function(){
                            // Modal is now showing
                            if(interval_ConnectionCheck != undefined){
                                clearInterval(interval_ConnectionCheck);
                                interval_ConnectionCheck = undefined;
                            }
                            interval_ConnectionCheck = setInterval(function(){
                                $rootScope.retry_ConnectionCheck();
                            }, 5000);
                        });
                    });

                } else {
                   $rootScope.stop_ConnectionCheck();
                }
            }
            $rootScope.stop_NotificationCheck = function(){

                clearInterval(interval_NotificationCheck);
                interval_NotificationCheck = undefined;

                $rootScope.retry_NotificationCheck = function(){}

                $(".content").removeClass("blur");
                if($(".content").hasClass("noscroll"))
                    $(".content").removeClass("noscroll"); 

                $(".modal.notification-error.popup").fadeOut("slow", function(){
                    $(".modal.notification-error.popup").css("top", "100%");
                });
            }
            $rootScope.retry_NotificationCheck = function(){}
            $rootScope.checkNotification = function(actionMethod, actionMessage, wasFound) {

                if(actionMethod)
                    $rootScope.retry_NotificationCheck = actionMethod;

                if(!wasFound){
                    $rootScope.loading = false;
                    $(".content").addClass("blur");
                    if(!$(".content").hasClass("noscroll"))
                        $(".content").addClass("noscroll"); 

                    $(".modal.notification-error.popup").children(".text").html(actionMessage || "Reintentando...");

                    $(".modal.notification-error.popup").fadeOut("fast", function(){
                        $(".modal.notification-error.popup").css("top", 20 + $(".header.master-header").height() + "px");

                        $(".modal.notification-error.popup").fadeIn("slow",function(){
                            // Modal is now showing
                            if(interval_NotificationCheck != undefined){
                                clearInterval(interval_NotificationCheck);
                                interval_NotificationCheck = undefined;
                            }
                            interval_NotificationCheck = setInterval(function(){
                                $rootScope.retry_NotificationCheck();
                            }, 5000);
                        });
                    });

                } else {
                   $rootScope.stop_NotificationCheck();
                }
            }
        /*Handle connection issues*/

        /*Update methods chain*/
            $rootScope.actualizamosCache = function() {
                console.log("Updating...");
                console.log($rootScope.user.id || $rootScope.user._id);

                //$rootScope.borraTablas(true, function(){
                    var dataObj = {
                        data: "eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJjb250ZW50cyIsImFjdGlvbiI6Il9saXN0IiwicHVibGlja2V5IjoibmtmdnQ3Z3BnZGtha3liNzhncjkifQ==",
                        hash: "87ad4d606c5e68ea7044e3e846d69211",
                        userId: $rootScope.user.id || $rootScope.user._id
                    };

                    var res = $http.post('http://' + SERVER + '.reachout.global/api?publickey=nkfvt7gpgdkakyb78gr9', dataObj);

                    var serverNotResponding = setTimeout(function(){
                        var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "059"};
                        $rootScope.addLog(_thelog);
                    }, 10000);

                    res.success(function(data, status, headers, config) {

                        clearTimeout(serverNotResponding);

                        if (data.contents) {

                            if(data.contents.contents.length == 0){
                                var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "063"};
                                $rootScope.addLog(_thelog);
                            }

                            $cordovaSQLite.execute($rootScope.db, "UPDATE contenidos SET content=(?)", [JSON.stringify(data.contents.contents, null, 4)]).then(function(res) {

                                $rootScope.contentCache = data.contents.contents;

                                $rootScope.checkConnection(function(){$rootScope.actualizamosCache()},"Reintentando traer contenidos...");

                                $rootScope.actualizarCategorias();

                            }, function(err) {
                                var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "060"};
                                $rootScope.addLog(_thelog);
                            });

                        } else {
                            $rootScope.checkConnection(function(){$rootScope.actualizamosCache()},"Reintentando traer contenidos...");
                        }
                    });

                    res.error(function(data, status, headers, config) {
                        clearTimeout(serverNotResponding);

                        $rootScope.checkConnection(function(){$rootScope.actualizamosCache()}, "Reintentando traer contenidos...");

                        var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "058"};
                        $rootScope.addLog(_thelog);
                    });  
                //}, true);       

                $rootScope.actualizarCategorias = function() {

                    var dataObj = {
                        data: "eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJicmFuZHMiLCJhY3Rpb24iOiJsaXN0Q2F0ZWdvcmllcyIsInB1YmxpY2tleSI6Im5rZnZ0N2dwZ2RrYWt5Yjc4Z3I5IiwiYnJhbmQiOiJHcmlzaW5vIn0=",
                        hash: "334a3ec80f4542a9657f76983224d144",
                        userId: $rootScope.user.id || $rootScope.user._id
                    };

                    var serverNotResponding = setTimeout(function(){
                        var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP, lat:parseFloat($rootScope.lat), lng:parseFloat($rootScope.lng), id:"074"};
                        $rootScope.addLog(_thelog);
                    }, 10000);

                    var res = $http.post('http://' + SERVER + '.reachout.global/api?publickey=nkfvt7gpgdkakyb78gr9', dataObj);

                    res.success(function(data, status, headers, config) {

                        clearTimeout(serverNotResponding);

                        // Saving categories
                        $rootScope.categoriasCache = data;

                        $cordovaSQLite.execute($rootScope.db, "UPDATE categorias SET content=(?)", [JSON.stringify(data, null, 4)]).then(function(res) {
                              
                            $rootScope.actualizarPromos();

                        }, function(err) {
                            var _thelog = {date:$rootScope.NOW(),IP:$rootScope.IP, lat:parseFloat($rootScope.lat), lng:parseFloat($rootScope.lng), id:"075"};
                            $rootScope.addLog(_thelog);
                        });
                    });

                    res.error(function(data, status, headers, config) {

                        clearTimeout(serverNotResponding);

                        $rootScope.checkConnection(function(){$rootScope.actualizarCategorias()},"Reintentando traer categorías...");
                    });
                }

                $rootScope.actualizarPromos = function() {

                    console.log("Gets to update promos");

                    //GRISINO
                    var dataObj = {
                        data: "eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJwcm9tb3MiLCJhY3Rpb24iOiJfbGlzdCIsInB1YmxpY2tleSI6Im5rZnZ0N2dwZ2RrYWt5Yjc4Z3I5In0=",
                        hash: "2dac8a861c17ddc05408de5e4d932bc4",
                        
                        userId: $rootScope.user.id || $rootScope.user._id
                    };

                    //SARKANY
                    /*var dataObj = {
                        data: "eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJwcm9tb3MiLCJhY3Rpb24iOiJfbGlzdCIsInB1YmxpY2tleSI6Ijdlcmc1bXV6bG53NXRoYmZwNGYxIn0=",
                        hash: "b10dbf5f24d04462eef212254702bb0c",
                        userId: $rootScope.user.id || $rootScope.user._id
                    };*/

                    var res = $http.post('http://' + SERVER + '.reachout.global/api?publickey=nkfvt7gpgdkakyb78gr9', dataObj);

                    var serverNotResponding = setTimeout(function(){
                        var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "053"};
                        $rootScope.addLog(_thelog);
                    }, 10000);

                    res.success(function(data, status, headers, config) {

                        clearTimeout(serverNotResponding);

                        // Saving promos
                        if(data.promos.promos.length == 0){
                            var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "057"};
                            $rootScope.addLog(_thelog);
                        }

                        //BROKEN SHOPS WORKAROUND
                        data.promos.promos = data.promos.promos.filter(function(promo){
                            if (promo.audience.shops.length > 0) {
                                var hasAtLeastOne = false;
                                for (var j = 0; j < promo.audience.shops.length; j++) {
                                    if(promo.audience.shops[j].map.lat && promo.audience.shops[j].map.lat != null){
                                        hasAtLeastOne = true;
                                        break;
                                    }
                                }

                                return hasAtLeastOne;
                            } else {
                                return true;
                            }
                        });
                        $rootScope.promosCache = data.promos.promos;
                        //BROKEN SHOPS WORKAROUND

                        if (data.promos) {

                            console.log("Found promos");
                            var query = "UPDATE promos SET content=(?)";

                            console.log(JSON.stringify(data.promos.promos, null, 4));

                            $cordovaSQLite.execute($rootScope.db, query, [JSON.stringify(data.promos.promos, null, 4)]).then(function(res) {

                                $rootScope.actualizarShops();

                            }, function(err) {
                                var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "054"};
                                $rootScope.addLog(_thelog);
                            });
                        } else {
                            console.log("Didn´t find promos");
                        }

                    });

                    res.error(function(data, status, headers, config) {
                        clearTimeout(serverNotResponding);
                        $rootScope.checkConnection(function(){$rootScope.actualizarPromos()},"Reintentando traer promociones...");
                    });
                }

                $rootScope.actualizarShops = function() {
                    var dataObj = {
                        data: "eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJzaG9wcyIsImFjdGlvbiI6Il9saXN0IiwicHVibGlja2V5IjoibmtmdnQ3Z3BnZGtha3liNzhncjkifQ==",
                        hash: "6ca38d16e8c78052745b5cb1f13ac0f8",
                        userId: $rootScope.user.id || $rootScope.user._id
                    };

                    var res = $http.post('http://' + SERVER + '.reachout.global/api?publickey=nkfvt7gpgdkakyb78gr9', dataObj);

                    var serverNotResponding = setTimeout(function(){
                        var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "065"};
                        $rootScope.addLog(_thelog);
                    }, 10000);

                    res.success(function(data, status, headers, config) {

                        clearTimeout(serverNotResponding);

                        // Saving shops
                        $rootScope.venuesCache = data.shops.shops;

                        if (data.shops) {

                            $cordovaSQLite.execute($rootScope.db, "UPDATE venues SET content=(?)", [JSON.stringify(data.shops.shops, null, 4)]).then(function(res) {

                                // Final update callback
                                if ($rootScope.updateCallback)
                                    $rootScope.updateCallback();
                                else
                                    console.log("No update callback...");

                            }, function(err) {
                                var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "067"};
                                $rootScope.addLog(_thelog);
                            });
                        }
                    });

                    res.error(function(data, status, headers, config) {

                        clearTimeout(serverNotResponding);
                        $rootScope.checkConnection(function(){$rootScope.actualizarShops()}, "Reintentando traer locales...");

                        var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "064"};
                        $rootScope.addLog(_thelog);
                    });
                }          
            }
        /*Update methods chain*/

        /*Playtomic content getters*/
            creamosCache = function() {

                var serverNotResponding = setTimeout(function(){
                    var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "059"};
                    $rootScope.addLog(_thelog);
                }, 10000);

                Playtomic.Contents.list({type:'',category:'', is_admin: false}, function(response) {

                    clearTimeout(serverNotResponding);

                    $rootScope.contentCache = response.contents;

                    console.log(response);
                    if (response.contents && response.contents.length) {

                        var query = "INSERT INTO contenidos (fecha, content) VALUES (?,?)";

                        $cordovaSQLite.execute($rootScope.db, query, [response.contents[0].register_date, JSON.stringify(response.contents, null, 4)]).then(function(res) {

                            promoList();

                        }, function(err) {
                            var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "060"};
                            $rootScope.addLog(_thelog);
                
                        });

                    } else {

                        var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "063"};
                        $rootScope.addLog(_thelog);

                        $rootScope.loading = false;
                        $mdToast.show(
                            $mdToast.simple()
                            .content('Error, no se han encontrado contenidos. ¡Intenta más tarde!')
                            .position('top')
                            .hideDelay(3000)
                        );
                        $rootScope.borraTablas(true);
                        $location.path("/mainPanel");
                    }

                });
            }

            promoList = function() {

                var serverNotResponding = setTimeout(function(){
                    var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "053"};
                    $rootScope.addLog(_thelog);
                }, 10000);

                Playtomic.Promos.list({is_admin: false}, function(response) {

                    clearTimeout(serverNotResponding);

                    console.log(response);

                    //BROKEN SHOPS WORKAROUND
                    response.promos = response.promos.filter(function(promo){
                        if (promo.audience.shops.length > 0) {
                            var hasAtLeastOne = false;
                            for (var j = 0; j < promo.audience.shops.length; j++) {
                                if(promo.audience.shops[j].map.lat && promo.audience.shops[j].map.lat != null){
                                    hasAtLeastOne = true;
                                    break;
                                }
                            }
                            console.log(hasAtLeastOne);

                            return hasAtLeastOne;
                        } else {
                            return true;
                        }
                    });
                    $rootScope.promosCache = response.promos;
                    //BROKEN SHOPS WORKAROUND

                    console.log(response);

                    if (response.promos && response.promos.length) {
                        $cordovaSQLite.execute($rootScope.db, "INSERT INTO promos (fecha, content) VALUES (?,?)", [response.promos[0].register_date, JSON.stringify(response.promos, null, 4)]).then(function(res) {
                        
                            categoriasList();

                        }, function(err) {
                            var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "054"};
                            $rootScope.addLog(_thelog);
                
                        });
                    } else {

                        var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "057"};
                        $rootScope.addLog(_thelog);

                        $rootScope.loading = false;
                        $mdToast.show(
                            $mdToast.simple()
                            .content('Error, no se han encontrado promociones. ¡Intenta más tarde!')
                            .position('top')
                            .hideDelay(3000)
                        );
                        $rootScope.borraTablas(true);
                        $location.path("/mainPanel");
                    }
                });
            }

            categoriasList = function() {

                console.log($rootScope.user);

                var dataObj = {
                    data: "eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJicmFuZHMiLCJhY3Rpb24iOiJsaXN0Q2F0ZWdvcmllcyIsInB1YmxpY2tleSI6Im5rZnZ0N2dwZ2RrYWt5Yjc4Z3I5IiwiYnJhbmQiOiJHcmlzaW5vIn0=",
                    hash: "334a3ec80f4542a9657f76983224d144",
                    userId: ($rootScope.user._id ? $rootScope.user._id : $rootScope.user.id)
                };

                var res = $http.post('http://' + SERVER + '.reachout.global/api?publickey=nkfvt7gpgdkakyb78gr9', dataObj, {headers: {'Authorization':'Bearer ' + TOKEN}});

                res.success(function(data, status, headers, config) {

                    console.log(data);

                    $rootScope.categoriasCache = data;

                    $cordovaSQLite.execute($rootScope.db, "INSERT INTO categorias (fecha, content) VALUES (?,?)", ["", JSON.stringify(data, null, 4)]).then(function(res) {
                        
                        shopList();

                    }, function(err) {});
                });

                res.error(function(data, status, headers, config) {
                    $location.path("/main/home");
                    $rootScope.checkConnection(categoriasList, "Reintentando traer categorías...");   
                });
            }

            shopList = function() {

                var serverNotResponding = setTimeout(function(){
                    var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "065"};
                    $rootScope.addLog(_thelog);
                }, 10000);

                Playtomic.Shops.list({}, function(response) {

                    clearTimeout(serverNotResponding);

                    $rootScope.venuesCache = response.shops.shops;
     
                    if (response.shops && response.shops.shops && response.shops.shops.length) {

                        $cordovaSQLite.execute($rootScope.db, "INSERT INTO venues (fecha, content) VALUES (?,?)", [response.shops.shops[0].register_date, JSON.stringify(response.shops.shops, null, 4)]).then(function(res) {


                            $location.path("/main/home");

                            if (!serviceRunning) {
                                setTimeout(function() {
                                    initService();
                                }, 3000);
                            }

                        }, function(err) {
                            var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "067"};
                            $rootScope.addLog(_thelog);
                
                        });
                    }
                });
            }
        /*Playtomic content getters*/

        /*--------------FINAL READYSTATE FIRST METHODS--------------*/

        /*INIT*/
            // Playtomic initialize
            Playtomic.initialize("nkfvt7gpgdkakyb78gr9", "gd341ui4r67d51srfwvs", "http://" + SERVER + ".reachout.global");

            // DB initialize
            $rootScope.db = $cordovaSQLite.openDB(DB_NAME);

            // Service initialize
            //if (serviceRunning)
                //setConfig("open");

            // Db main methods
            $rootScope.creaTablas(checkUserExist);

            $(".annoyingMessage").css("visibility", "visible");
            $(".loading-back").css("visibility", "visible");
            $(".loading-back").css("visibility", "visible");

            // Blank page trap interval start
            /*$rootScope.checkForBlank = setInterval(function() {
                if ($("#mainView").children().length == 0) {
                    $rootScope.borraTablas(true);
                    checkUserExist();
                } else {
                    clearInterval($rootScope.checkForBlank);
                }
            }, 3000);*/
        /*INIT*/
    }//, 1500);
});

function myDeviceReady(){
    USEPLUGINS = true;

    setTimeout(function(){
        window.ga.startTrackerWithId('UA-99232687-1');
    }, 5000);
    
}
$(document).ready(myDeviceReady);
