function List(container, templateFunction, elementGetter) {

    var elements = elementGetter();
    var index = 0,
        appendMargin = 1000,
        divs = [],
        appending = false;

    function isInbound(element) {
        var r = element[0].getBoundingClientRect();

        return (r.top > 0 && r.bottom < ((window.innerHeight || document.documentElement.clientHeight) + appendMargin));
    }

    function deleteAll(callback) {
        if ($(container).children("li.newsPiece").length) {
            $(container).children("li.newsPiece").last().hide(1, function() {
                $(container).children("li.newsPiece").last().remove();
                deleteAll(callback);
            });
        } else if (callback) {
            callback();
        }
    }

    function append() {
        if (!appending && index < elements.length && (divs.length < 5 || isInbound(divs[index - 1]))) {

            /*Barrier*/
            appending = true;
            /*Barrier*/

            /*Construction*/
            var item = templateFunction(elements[index]);
            var j = $(item);
            j.hide();
            $(container).append(j);
            //j.show();
            /*Construction*/

            /*Advance*/
            index++;
            /*Advance*/

            /*Finish appending*/
            divs.push(item);

            //item.fadeIn((isInbound(item) ? 1000 : 500), "easeOutElastic", function(){
            appending = false;
            append();
            //});
            /*Finish appending*/
        }
    }

    function _start() {
        append();
    }

    this.getElements = function() {
        return elementGetter();
    }

    this.start = function() {
        $(container).parent().on("scroll", function(event) {
            append();
        });
        _start();
    }

    this.restart = function(callback) {
        deleteAll(function() {
            index = 0;
            elements = elementGetter();
            divs = [];
            appending = false;

            if (callback)
                callback();

            _start();
        });
    }
}

function getSize(text, container) {
    var ruler = document.getElementById("measureText");
    ruler.innerHTML = text;

    var currentWidth = ruler.clientWidth + 1;
    var desiredWidth = container.width() - 80;

    return (10 * desiredWidth) / currentWidth;
}

function haversine(lat1, lng1, lat2, lng2) {
    var R = 6371000; // km radio de la tierra
    var dLat = toRad(lat2 - lat1);
    var dLon = toRad(lng2 - lng1);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    var distance = Math.floor(d);

    return { number: ((distance - (distance % 50)) + 50), str: distance >= 1000 ? Math.floor(distance / 1000) + "km" : ((distance - (distance % 50)) + 50) + "m" };
}

function toRad(Value) {

    return Value * Math.PI / 180;
}

function areJSONArraysEqual(a, b) {
    var JSONSa = [],
        JSONSb = [];
    console.log(a);
    console.log(b);

    //Undefined
    if (!a || !b)
        return false;

    for (var i = 0; i < a.length; i++)
        JSONSa.push(a[i]);

    for (var i = 0; i < b.length; i++)
        JSONSb.push(b[i]);

    if (JSONSa.length != JSONSb.length || JSONSa.length == 0)
        return false;

    var copy = { a: JSONSa, b: JSONSb };

    for (var i = 0; i < copy.a.length; i++) {
        console.log(JSON.stringify(copy.a[i]));
    }
    for (var i = 0; i < copy.b.length; i++) {
        console.log(JSON.stringify(copy.b[i]));
    }

    for (var i = 0; i < JSONSa.length; i++) {
        var a = [];

        for (prop in JSONSa[i]) {
            if (!JSONSa[i].hasOwnProperty(prop) || prop.indexOf("$$") != -1) {
                //The current property is not a direct property of p
                continue;
            }
            //Do your logic with the property here
            console.log(prop + ": " + JSONSa[i][prop]);
            a.push({ property: prop, value: JSONSa[i][prop] });
        }

        for (var j = 0; j < JSONSb.length; j++) {
            var b = [],
                found = true;

            //If it wasn´t matched already
            if (JSONSb[j]) {
                for (prop in JSONSb[j]) {
                    if (!JSONSb[j].hasOwnProperty(prop) || prop.indexOf("$$") != -1) {
                        //The current property is not a direct property of p
                        continue;
                    }
                    //Do your logic with the property here
                    console.log(prop + ": " + JSONSb[j][prop]);
                    b.push({ property: prop, value: JSONSb[j][prop] });
                }

                //If has the same amount of properties
                if (a.length == b.length) {
                    for (var k = 0; k < a.length; k++) {
                        if (!JSONSa[i][a[k].property] || JSONSa[i][a[k].property] != JSONSb[j][a[k].property]) {
                            found = false;
                            break;
                        }
                    }
                    //JSONSa[i] == JSONSb[j], debe marcarse el JSONSb[j] para no escudriñarlo nuevamente
                    if (found) {
                        JSONSb[j] = undefined;
                        break;
                    }
                } else {
                    found = false;
                }
            }
        }

        if (!found)
            break;

        JSONSb = copy.b;

    }
    console.log(found);

    return found;
}

function unscope(o) {
    var d = [];

    for (var i = 0; i < o.length; i++) {
        d.push(o[i]);
        console.log(o[i]);
    }

    return d;
}

function clog(a) {

    console.log(JSON.stringify(a));
}

function getIndexByVal(array, value) {
    for (var i = 0; i < array.length; i++)
        if (array[i].text == value)
            return { array: array, index: i };

    return { array: array, index: -1 };
}

function ISCROLL(_wrapper, templateFunction, elementGetter, update, compile) {
    var wrapper = _wrapper;
    var items_per_page = 20;
    var scroll_in_progress = false;
    var myScroll;
    var index = 0;
    var lastScroll = 0;
    var elements = elementGetter();
    var appending;

    _scrolling = false;

    this.isScrolling = function(){
        return _scrolling;
    }

    this.getElements = function(){
        return elementGetter();
    }

    function append() {
        var returnDirectly = false;
        var piece = '';
        for(var i = 0; i < items_per_page; i++){
            if (index < elements.length) {

                piece += templateFunction(elements[index]);

                index++;

            } else {
                $( wrapper + ' > #scroller > ul').append(compile(piece));
                returnDirectly = true;
                break;
            }
        }

        if(!returnDirectly)
            $( wrapper + ' > #scroller > ul').append(compile(piece));
        
    }

    function reset(){
        $( wrapper + ' > #scroller > ul').html('');
        index = 0;
    }

    function _reload(){
        console.log("Finished updating");
        
        elements = elementGetter();

        reset();
        append();

        myScroll.refresh();
        pullActionCallback();
    }

    load_content = function(refresh, next_page) {

        if (!refresh) { // Loading the initial content
            append();

            if (myScroll) {
                myScroll.destroy();
                $(myScroll.scroller).attr('style', '');
                myScroll = null;
            }
            trigger_myScroll();

        } else if (refresh && !next_page) { // Refreshing the content
            update(_reload);

        } else if (refresh && next_page) { // Loading the next-page content and refreshing

            append();

            myScroll.refresh();
            pullActionCallback();

        }
    };

    this.reload_content = function(){
        _reload();
    }

    function pullDownAction() {
        load_content('refresh');
        $( wrapper + ' > #scroller > ul').data('page', 1);
        $( wrapper + ' > .scroller').css({ top: 0 });

    }

    function pullUpAction(callback) {
        if ($( wrapper + ' > #scroller > ul').data('page')) {
            var next_page = parseInt($( wrapper + ' > #scroller > ul').data('page'), 10) + 1;
        } else {
            var next_page = 2;
        }
        load_content('refresh', next_page);
        $( wrapper + ' > #scroller > ul').data('page', next_page);

        if (callback) {
            callback();
        }
    }

    function pullActionCallback() {
        if (pullDownEl && pullDownEl.className.match('loading')) {

            pullDownEl.className = 'pullDown';

            $(".pullDownIcon").fadeOut(100, function(){
                pullDownEl.querySelector('.pullDownIcon').innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="40px" version="1.1" height="40px" viewBox="0 0 64 64" enable-background="new 0 0 64 64"><g><path fill="#FFFFFF" d="M3.352,48.296l28.56-28.328l28.58,28.347c0.397,0.394,0.917,0.59,1.436,0.59c0.52,0,1.04-0.196,1.436-0.59   c0.793-0.787,0.793-2.062,0-2.849l-29.98-29.735c-0.2-0.2-0.494-0.375-0.757-0.475c-0.75-0.282-1.597-0.107-2.166,0.456   L0.479,45.447c-0.793,0.787-0.793,2.062,0,2.849C1.273,49.082,2.558,49.082,3.352,48.296z"/></g></svg>';
                $(".pullDownIcon").fadeIn(100);
            });

            myScroll.scrollTo(0, parseInt(pullUpOffset) * (-1), 200);

        } else if (pullUpEl && pullUpEl.className.match('loading')) {

            $('.pullUp').removeClass('loading').html('');

        }
    }

    var pullActionDetect = {
        count: 0,
        limit: 10,
        check: function(count) {
            if (count) {
                pullActionDetect.count = 0;
            }
            // Detects whether the momentum has stopped, and if it has reached the end - 200px of the scroller - it trigger the pullUpAction
            setTimeout(function() {
                if (myScroll.y <= (myScroll.maxScrollY + 200) && pullUpEl && !pullUpEl.className.match('loading')) {
                    $('.pullUp').addClass('loading').fadeOut(1).html('<span class="pullUpIcon">&nbsp;</span><span class="pullUpLabel">Loading...</span>').fadeIn(200);
                    pullUpAction();
                } else if (pullActionDetect.count < pullActionDetect.limit) {
                    pullActionDetect.check();
                    pullActionDetect.count++;
                }
            }, 100);
        }
    }

    function trigger_myScroll(offset) {
        pullDownEl = document.querySelector( wrapper + ' .pullDown');
        if (pullDownEl) {
            pullDownOffset = pullDownEl.offsetHeight;
        } else {
            pullDownOffset = 0;
        }
        pullUpEl = document.querySelector( wrapper + ' .pullUp');
        if (pullUpEl) {
            pullUpOffset = pullUpEl.offsetHeight;
        } else {
            pullUpOffset = 0;
        }

        if (!offset) {
            // If offset is not manually defined - we set it to be the pullUpOffset.
            offset = pullUpOffset;
        }

        myScroll = new IScroll( wrapper , {
            probeType: 1,
            tap: true,
            click: false,
            preventDefaultException: { tagName: /.*/ },
            keyBindings: false,
            deceleration: 0.0006,
            startY: (parseInt(offset) * (-1))
        });

        lastScroll = (parseInt(offset) * (-1));

        myScroll.on('scrollStart', function() {
            scroll_in_progress = true;
            _scrolling = true;
        });
        myScroll.on('scroll', function() {

            var isGoingDown = this.y < lastScroll;

            scroll_in_progress = true;

            if (this.y >= 5 && !isGoingDown && pullDownEl && !pullDownEl.className.match('flip')) {

                $( wrapper + ' .pullDown').fadeOut(75, function(){
                    pullDownEl.className = 'pullDown flip';
                    $( wrapper + ' .pullDown').fadeIn(75);
                });
                
                this.minScrollY = 0;

            } else if (this.y <= 100 && isGoingDown && pullDownEl && pullDownEl.className.match('flip')) {

                $( wrapper + ' .pullDown').fadeOut(75, function(){
                    pullDownEl.className = 'pullDown';
                    $( wrapper + ' .pullDown').fadeIn(75);
                });

                this.minScrollY = -pullDownOffset;

            }

            lastScroll = this.y;

            pullActionDetect.check(0);

        });
        myScroll.on('scrollEnd', function() {
            
            setTimeout(function(){
                _scrolling = false;
            },1000);

            setTimeout(function() {
                scroll_in_progress = false;
            }, 100);

            if (pullDownEl && pullDownEl.className.match('flip')) {
                pullDownEl.querySelector('.pullDownIcon').innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 516.727 516.727" style="enable-background:new 0 0 516.727 516.727;" xml:space="preserve"><g><path d="M516.727,266.696c-0.665-34.825-8.221-69.54-22.175-101.283c-13.908-31.771-34.094-60.551-58.876-84.333   c-24.767-23.8-54.139-42.615-85.929-55.027c-31.773-12.46-65.937-18.412-99.687-17.69c-33.755,0.668-67.36,8.007-98.091,21.539   c-30.754,13.488-58.615,33.058-81.632,57.071c-23.033,24.001-41.229,52.452-53.222,83.229C5.077,200.962-0.66,234.013,0.06,266.696   c0.67,32.688,7.793,65.182,20.903,94.899c13.067,29.738,32.019,56.681,55.266,78.931c23.234,22.268,50.766,39.846,80.528,51.417   c29.749,11.616,61.69,17.136,93.303,16.419c31.62-0.671,63.001-7.58,91.707-20.268c28.724-12.646,54.747-30.979,76.231-53.461   c21.503-22.469,38.461-49.08,49.611-77.827c6.79-17.427,11.396-35.624,13.824-54.062c0.649,0.037,1.302,0.063,1.96,0.063   c18.409,0,33.333-14.923,33.333-33.333c0-0.936-0.049-1.861-0.124-2.777L516.727,266.696L516.727,266.696z M463.762,355.21   c-12.226,27.71-29.94,52.812-51.655,73.532c-21.703,20.732-47.396,37.076-75.127,47.807c-27.724,10.77-57.443,15.859-86.919,15.146   c-29.481-0.677-58.644-7.154-85.323-18.997c-26.692-11.806-50.877-28.901-70.83-49.849c-19.968-20.938-35.691-45.711-46.001-72.427   c-10.349-26.712-15.223-55.321-14.512-83.728c0.678-28.413,6.941-56.465,18.361-82.131c11.384-25.677,27.863-48.943,48.045-68.13   c20.172-19.202,44.026-34.307,69.726-44.195c25.697-9.928,53.195-14.587,80.534-13.877c27.343,0.68,54.286,6.728,78.939,17.726   c24.662,10.963,47.008,26.824,65.429,46.241c18.436,19.405,32.922,42.341,42.391,67.025c9.504,24.684,13.948,51.072,13.241,77.342   h0.125c-0.076,0.916-0.125,1.841-0.125,2.777c0,17.193,13.018,31.34,29.732,33.137C476.551,320.747,471.184,338.453,463.762,355.21   z" fill="#FFFFFF"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>';
                pullDownEl.className = 'pullDown loading';

                pullDownAction();
            }
            // We let the momentum scroll finish, and if reached the end - loading the next page
            pullActionDetect.check(0);

        });

        // In order to prevent seeing the "pull down to refresh" before the iScoll is trigger - the wrapper is located at left:-9999px and returned to left:0 after the iScoll is initiated
        setTimeout(function() {
            $( wrapper + '').css({ left: 0 });
        }, 100);
    }

    load_content();

    //document.addEventListener('touchmove', function(e) { e.preventDefault(); }, false);
}
