'use strict';

var Home = angular.module('Home', []);

Home.controller('homeCtrl', ['$scope', '$location', '$rootScope', '$compile', '$timeout', '$cordovaFacebook', function($scope, $location, $rootScope, $compile, $timeout, $cordovaFacebook) {

    //En join y en main panel, falta en join
    $rootScope.getStars();

    $scope.categories = [
        {
            name: "Todo",
            id: 1,
            subcategories: []
        },

        {
            name: "Beneficios",
            id: 2,
            subcategories: []
        },

        {
            name: "Novedades",
            id: 3,
            subcategories: []
        }
    ];

    //Elementos filtrados según subcategoría, o cual fuere el criterio considerado por $scope.filterElements
    $scope.filteredElements;

    //Inicializar array de queues
    $scope.queues = [];

    //Declarar función de filtro -> retorna array filtrado
    $scope.filterElements = function() {

        var filtered = [],
            aux = [];
        var coords = { lat: $rootScope.lat, lng: $rootScope.lng };

        filtered = $rootScope.promosCache.filter(function(elem) {
            //Los que tienen geo
            return elem.triggers.hotspot_data;
        });

        filtered.sort(function(a, b) {
            return haversine(coords.lat, coords.lng, a.triggers.hotspot_data.lat, a.triggers.hotspot_data.lng).number - haversine(coords.lat, coords.lng, b.triggers.hotspot_data.lat, b.triggers.hotspot_data.lng).number;
        });

        filtered = filtered.concat($rootScope.promosCache.filter(function(elem) {
            //Los que no tienen geo
            return !elem.triggers.hotspot_data;
        }).sort(function(a, b) {
            //Por fecha
            return new Date(a.start_date) > new Date(b.start_date);
        }));

        filtered = filtered.concat($rootScope.contentCache.sort(function(a, b) {
            //Por fecha
            return new Date(a.start_date) > new Date(b.start_date);
        }));

        $scope.filteredElements = filtered;

        return filtered;
    }

    //Inicializar buffer
    $scope.buffer = new _Buffer($scope, $rootScope, function(obj, minIsOn) {

        var tab = angular.element(".home-content");
        var piecesWrapper = tab.children(".tabsGeneric");

        var currentChildren = piecesWrapper[0].children;
        var lastChild = currentChildren[currentChildren.length - 1];

        if(!obj){
            var item = $('<h1 style="font-weight: 200;margin-top: 25px; width:100%; text-align: center; color: #666;">En este momento no podemos acceder a los contenidos. Por favor, intenta más tarde.</h1>');
            piecesWrapper.append(item);         
            item.fadeIn("slow");
            return;
        }

        var elem = obj.elem;
        var id = elem._id;

        if (obj.elem.type == "promos" || obj.elem.type == "mobile") {
            var source = "https://s3-sa-east-1.amazonaws.com/iloveit/promo/" + id + ".jpg";
        } else if (obj.elem.type == "video") {
            var sampleUrl = obj.elem.fields.youtubeid;
            var video_id = sampleUrl; //.split("v=")[1].substring(0, 11);
            var source = "https://www.youtube.com/embed/" + video_id;
        } else if (obj.elem.type == undefined) {
            var source = "https://s3-sa-east-1.amazonaws.com/iloveit/shop/" + id + ".jpg";
        } else {
            var source = "https://s3-sa-east-1.amazonaws.com/iloveit/content/" + id + ".jpg";
        }

        if (elem.type == "news" || elem.type == "trivia" || elem.type == "rss" || elem.type == "lookbook" || elem.type == "image" || elem.type == "product") {

            var _laDes = elem.description.replace(/'/g, "\\'");

            var str = '<div class="newsPiece" layout="column"> <div class="imageWrapper defaultImage" style="height:' + ($(".tabsGeneric").width() - 16) + 'px!important;background-image: url(\'assets/images/novedades.png\')"><img src="' + source + '" hm-tap="detail(\'' + id + '\', \'news\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' id="' + elem._id + '" onload="this.style.opacity=1;" class="newsElement"></div><div class="newsElementBottomWrapper"><span class="newsElementTitle">' + elem.title + '</span><div class="newsElementDescription">' + _laDes + '</div> </div></div>';

            var piece = str;

        } else if (elem.type == "promos" || elem.type == "mobile" || elem.type == "promos") {

            var _laDes = elem.description_text.replace(/'/g, "\\'");

            var haver = $rootScope.lat && elem.triggers.hotspot_data ? "(" + haversine($rootScope.lat, $rootScope.lng, elem.triggers.hotspot_data.lat, elem.triggers.hotspot_data.lng).str + ")" : "";

            var lineSize = getSize(elem.description_title, piecesWrapper);

            var _ribbon = "";

            if ($rootScope.NOW() > new Date(elem.end_date).toISOString()) 
                _ribbon = "ribbon";
            
            var str = '<div hm-tap="detail(\'' + elem.id + '\', \'promo\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="newsPiece" layout="column"> <div id="content' + elem.id + '" class="imageWrapper defaultImage" style=" height:' + ($(".tabsGeneric").width() - 16) + 'px!important;background-size: 80px; background-image: url(\'assets/images/beneficios.png\')"> <table class="imgDeadLine"> <tr> <td colspan="2" style="text-align: center"> <div style="font-size:' + lineSize + '!important;"class="imgMidText">' + elem.description_title + '</div></td></tr></table><div class="' + _ribbon + '"></div> <div class="imageDarkerer"></div><div class="imageDarkerer imageClickHandler"></div><img src="' + source + '" id="' + elem._id + '" onload="this.style.opacity=1;" class="newsElement"></img></div> <div class="newsElementBottomWrapper"><span class="newsElementTitle">' + elem.description_title + " " + haver + '</span><div class="newsElementDescription">' + _laDes + '</div></div></div>';

            var piece = str;

        } else if (elem.type == undefined || elem.type == "shops") {

            var _laDes = elem.info.replace(/'/g, "\\'");

            var str = '<div class="newsPiece" layout="column"><div hm-tap="detail(\'' + elem.id + '\', \'shop\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' style="height:' + ($(".tabsGeneric").width() - 16) + 'px!important;background-image: url(\'assets/images/LinkMapa.png\')" class="imageWrapper defaultImage"><img src="' + source + '" id="' + elem._id + '" onload="this.style.opacity=1;" class="newsElement"></img></div><div class="newsElementBottomWrapper"> <span class="newsElementTitle">' + elem.name + '</span><div class="newsElementDescription">' + _laDes + '</div></div></div>'
            var piece = str;

        } else if (elem.type == "video") {

            var _laDes = elem.description.replace(/'/g, "\\'");

            var str = '<div class="newsPiece" layout="column"><div style="height:' + ($(".tabsGeneric").width() - 16) + 'px!important;background-image: url(\'assets/images/IconoMapa.jpeg\')" class="imageWrapper defaultImage"><iframe src="' + source + '" frameBorder="0" style="height: 100%; width: 100%;" ></iframe></div><div class="newsElementBottomWrapper no-padding"> <span class="newsElementTitle">' + elem.title + '</span><div class="newsElementDescription">' + _laDes + '</div></div></div>';

            var piece = str;

        } else {
            console.log("El tipo de elemento " + elem.type + " no tiene un template designado.");
            var piece = ('<div class="newsPiece">Desconocido</div>');
        }

        var lastBottom = piecesWrapper.children().length == 0 ? 0 : (piecesWrapper.children().last().position().top + piecesWrapper.children().last().outerHeight());
        if(lastBottom < tab.height() || (lastBottom - tab.scrollTop()) < (tab.height() + 500)){        
            var item = $compile(piece)($scope).fadeOut();
            piecesWrapper.append(item);
            item.fadeIn(1000);

            return false;
        } else { 
            return true;
        }

    }, function() {
        $(".home-content").children(".tabsGeneric").append($compile('<div class="newsPiece" layout="column"><img ng-hide="toggleRetryButton" src="assets/images/LogoNauticappFull_b.png" class="footerLogo"/><md-button ng-show="toggleRetryButton" hm-tap="retryConnection()" style="background-color: white; width: 90%; margin: 10px 5%!important; border: 2px solid black;" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'>Reintentar conexión</md-button></div>')($scope));
    });

    //Inicializar queues por cada subcategoría
    for(var i = 0; i < $scope.categories.length; i++){
        $scope.queues.push({queueIndex:0, isRunning:false, elements:[], reset: function(){this.queueIndex = 0; this.isRunning = false; this.elements = [];}});

    }

    angular.element(document).ready(setTimeout(function() {

        //Sigue en la screen del presente controlador
        if ($rootScope.states && $rootScope.states[$rootScope.states.length - 1] == "main.home") {

            $rootScope.loading = true;

            //First buffer run
            _startBuffer($scope.buffer, 10);

            //Set scroll buffering
            _initScrollBuffering($(".home-content"), $scope, $rootScope, $compile);

            $rootScope.updateCallback = function() {

                //Buffering
                $scope.buffer.filterElements();
                $scope.buffer.bufferElems(undefined);

                $rootScope.loading = false;

            }

            $(".home-content").xpull({
                'callback': function() {
                    if($rootScope._isOnline())
                        _restartBuffer($scope.buffer, $(".home-content"));
                    $rootScope.actualizamosCache();
                }
            });
        }

    }, 500));

}]);
