'use strict';
	
var datosContacto = angular.module('datosContacto', []);
	
	
datosContacto.controller('datosContactoCtrl',  ['$scope','$mdDialog', '$rootScope', '$cordovaInAppBrowser', '$cordovaContacts','$mdToast', function ($scope, $mdDialog, $rootScope, $cordovaInAppBrowser,$cordovaContacts,$mdToast) {
	
	/***************************** VARS DECLARATION **********************************/
	window.ga.trackView("Contact");

	$rootScope.loading = false;

	var _data = {
		displayName:  "Sarkany",
		phoneNumbers: [{"work": "1140826666"}]
	};

	$scope.addContact = function(_data) {
		console.log("llama");
	    $cordovaContacts.save(_data).then(function(result) {
	      // Contact saved
	      $mdToast.show(
		      $mdToast.simple()
		        .content('Se guardo en tus contactos')
		        .position('top')
		        .hideDelay(3000)
		    );
	    }, function(err) {
	      $mdToast.show(
		      $mdToast.simple()
		        .content('ERROR, no se guardo en tus contactos')
		        .position('top')
		        .hideDelay(3000)
		    );
	    });
	  };
	
	
}]);