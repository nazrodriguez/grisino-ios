/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  AppDelegate.m
//  Sarkany
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import "LocationManager.h"
#import "AppDelegate.h"
#import "MainViewController.h"
#import "Reachability.h"
#import <Cordova/CDVPlugin.h>
#import "TestFairy.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import <AVFoundation/AVFoundation.h>

#import <netinet/in.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import <SystemConfiguration/SCNetworkReachability.h>

@import UIKit;

@implementation AppDelegate

@synthesize window, viewController;

NSString *SERVER_NAME_APP = @"prod";
Boolean DEBUG2 = true;
Boolean isActive = false;
Boolean LOGS2 = false;

- (id)init
{
    /** If you need to do any extra app-specific initialization, you can do it here
     *  -jm
     **/
    DEBUG2 = ([SERVER_NAME_APP isEqualToString:@"staging"] || [SERVER_NAME_APP isEqualToString:@"dev"]) ? true : false;
    
    NSHTTPCookieStorage* cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];

    [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];

    int cacheSizeMemory = 8 * 1024 * 1024; // 8MB
    int cacheSizeDisk = 32 * 1024 * 1024; // 32MB
#if __has_feature(objc_arc)
        NSURLCache* sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
#else
        NSURLCache* sharedCache = [[[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"] autorelease];
#endif
    [NSURLCache setSharedURLCache:sharedCache];
    
    
    //Call function that handles haversines with gelocalizable promos existing in sqlite db
    

    self = [super init];
    return self;
}

#pragma mark UIApplicationDelegate implementation

/**
 * This is main kick off after the app inits, the views and Settings are setup here. (preferred - iOS4 and up)
 */
NSString *shouldDetail = @"false";

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    
    [TestFairy begin:@"a3a0a59a305201adae260cb4f23ae77222bab453"];
    
    Reachability *reachability = [Reachability reachabilityForLocalWiFi];
    
    reachability.reachableBlock = ^(Reachability *reachability){
        NSLog(@"Reaching");
        NSLog(@"%@", [reachability currentReachabilityFlags]);
        NSLog(@"%li", (long)[reachability currentReachabilityStatus]);
        NSLog(@"%@", [reachability currentReachabilityString]);
    };
    
    reachability.unreachableBlock = ^(Reachability *reachability){
        NSLog(@"Not reaching");
    };
    
    //[reachability startNotifier];
    
    GAI *gai = [GAI sharedInstance];
    [gai trackerWithTrackingId:@"UA-99232687-1"];
    
    gai.trackUncaughtExceptions = YES;
    
    if(DEBUG2)
        gai.logger.logLevel = kGAILogLevelVerbose;
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(status == AVAuthorizationStatusAuthorized) { // authorized
        
    }
    else if(status == AVAuthorizationStatusDenied){ // denied
        
    }
    else if(status == AVAuthorizationStatusRestricted){ // restricted
        
    }
    else if(status == AVAuthorizationStatusNotDetermined){ // not determined
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){ // Access has been granted ..do something
                
            } else { // Access denied ..do something
                
            }
        }];
    }
    
    /*TEST*/
    self.shareModel = [LocationManager sharedManager];
    self.shareModel.afterResume = NO;
    
    isActive = true;
    
    [self.shareModel addApplicationStatusToPList:@"didFinishLaunchingWithOptions"];

    /*Set background fetch*/
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    UIAlertView * alert;
    
    //We have to make sure that the Background App Refresh is enable for the Location updates to work in the background.
    if ([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied) {
        
        alert = [[UIAlertView alloc]initWithTitle:@""
                                          message:@"The app doesn't work without the Background App Refresh enabled. To turn it on, go to Settings > General > Background App Refresh"
                                         delegate:nil
                                cancelButtonTitle:@"Ok"
                                otherButtonTitles:nil, nil];
        [alert show];
        
    } else if ([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted) {
        
        alert = [[UIAlertView alloc]initWithTitle:@""
                                          message:@"The functions of this app are limited because the Background App Refresh is disabled."
                                         delegate:nil
                                cancelButtonTitle:@"Ok"
                                otherButtonTitles:nil, nil];
        [alert show];
        
    } else {
        
        // When there is a significant changes of the location,
        // The key UIApplicationLaunchOptionsLocationKey will be returned from didFinishLaunchingWithOptions
        // When the app is receiving the key, it must reinitiate the locationManager and get
        // the latest location updates
        
        // This UIApplicationLaunchOptionsLocationKey key enables the location update even when
        // the app has been killed/terminated (Not in th background) by iOS or the user.
        
        if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
            [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
        }
        
        NSLog(@"UIApplicationLaunchOptionsLocationKey : %@" , [launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey]);
        if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey]) {
            /*Debug logging of geofences amount*
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
            localNotification.alertBody = @"Re" monitoring location thanks to significant location event";
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            /*Debug logging of geofences amount*/
            // This "afterResume" flag is just to show that he receiving location updates
            // are actually from the key "UIApplicationLaunchOptionsLocationKey"
            self.shareModel.afterResume = YES;
            NSLog(@"WAKING FROM SLC");

            [self.shareModel startSLC];
            [self.shareModel addResumeLocationToPList];
        } else {
            /*Debug logging of geofences amount*
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
            localNotification.alertBody = @"Can´t start monitoring Location because opened app manually";
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            //[[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            /*Debug logging of geofences amount*/
            NSLog(@"WAKING MANUALLY");

            //NSLog(@"Can´t start monitoring Location because opened app manually");
        }
    }
    
    /*TEST*/
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];

#if __has_feature(objc_arc)
        self.window = [[UIWindow alloc] initWithFrame:screenBounds];
#else
        self.window = [[[UIWindow alloc] initWithFrame:screenBounds] autorelease];
#endif
    self.window.autoresizesSubviews = YES;

#if __has_feature(objc_arc)
        self.viewController = [[MainViewController alloc] init];
#else
        self.viewController = [[[MainViewController alloc] init] autorelease];
#endif

    // Set your app's start page by setting the <content src='foo.html' /> tag in config.xml.
    // If necessary, uncomment the line below to override it.
    // self.viewController.startPage = @"index.html";

    // NOTE: To customize the view's frame size (which defaults to full screen), override
    // [self.viewController viewWillAppear:] in your view controller.

    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
    //TEST
    /*if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }*/

    return YES;
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler {
    
    /*Debug logging of geofences amount*
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
    localNotification.alertBody = @"Started BF";
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    /*Debug logging of geofences amount*/
    
    if(self.shareModel.dbManager == NULL)
    self.shareModel.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"iLoveit.db"];
    
    /*Get userID*/
    NSArray *json = [self.shareModel.dbManager loadDataFromDB:@"SELECT * FROM usuario"];
    NSError *e = nil;
    
    if(json == nil || [json count] == 0 || [json[0] count] == 0 || [json[0][1] dataUsingEncoding:NSUTF8StringEncoding] == nil){
        NSLog(@"User db is empty");
        completionHandler(UIBackgroundFetchResultFailed);
        return;
    }
    NSLog(@"Found user db");
    
    NSArray *user = [NSJSONSerialization JSONObjectWithData:[json[0][1] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &e];
    NSString *userId = [user valueForKey:@"_id"] ? [user valueForKey:@"_id"] : [user valueForKey:@"id"];
    /*Get userID*/
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: [[NSString stringWithFormat:@"data=eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJwcm9tb3MiLCJhY3Rpb24iOiJfbGlzdCIsInB1YmxpY2tleSI6Im5rZnZ0N2dwZ2RrYWt5Yjc4Z3I5In0=&hash=2dac8a861c17ddc05408de5e4d932bc4&userId=%@&publickey=nkfvt7gpgdkakyb78gr9",userId] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
    
    /*Setting queried URL*/
    NSURLComponents *url = [[NSURLComponents alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat: @"http://%@.reachout.global/api?", SERVER_NAME_APP]] resolvingAgainstBaseURL:YES];
    NSMutableArray *queryItems = NSMutableArray.new;
    
    NSDictionary *params = [[NSDictionary alloc]initWithObjectsAndKeys:@"nkfvt7gpgdkakyb78gr9",@"publickey",@"eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJwcm9tb3MiLCJhY3Rpb24iOiJfbGlzdCIsInB1YmxpY2tleSI6Im5rZnZ0N2dwZ2RrYWt5Yjc4Z3I5In0=",@"data",@"2dac8a861c17ddc05408de5e4d932bc4",@"hash",userId,@"userId", nil];
    
    [params enumerateKeysAndObjectsUsingBlock:^(NSString *name, NSString *value, BOOL *stop){
        [queryItems addObject:[NSURLQueryItem queryItemWithName:name value: value]];//[value stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]]];
    }];
    url.queryItems = queryItems;
    NSLog(@"URL TO BE USED: %@", url.URL);
    [request setURL:url.URL];
    /*Setting queried URL*/
    
    NSError *error = nil;
    NSHTTPURLResponse *responseCode = nil;
    
    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];
    
    if([responseCode statusCode] != 200){
        /*Debug logging of geofences amount*
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        localNotification.alertBody = [NSString stringWithFormat:@"BFEnded with error. Found error: %@", responseCode];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        /*Debug logging of geofences amount*/
        [self.shareModel addLogWithAction:@"056" andId:NULL];
        
        NSLog(@"Error getting updated contents, HTTP status code %@",responseCode);
        completionHandler(UIBackgroundFetchResultFailed);
    } else if(oResponseData != nil){
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:oResponseData options:kNilOptions error:&error];
        
        /*Debug logging of geofences amount*
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        localNotification.alertBody = [NSString stringWithFormat:@"BF Ended succesfully. Found promos: %lu", [[[result objectForKey:@"promos"] objectForKey:@"promos"] count]];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        /*Debug logging of geofences amount*/
        
        /*Save new data in db*/
        
        NSMutableArray *promosStrs = [[NSMutableArray alloc] init];
        for(NSDictionary *d in [[result objectForKey:@"promos"] objectForKey:@"promos"]){
            if(d != nil)
            [promosStrs addObject:[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:d options:kNilOptions error:&error] encoding:NSUTF8StringEncoding]];
            
            //NSLog(@"If there was an error, it was %@.", error);
        }
        NSLog(@"Amount of promos downloaded: %lu", (unsigned long)[promosStrs count]);
        
        NSString *promos = [[[@"[" stringByAppendingString:[promosStrs componentsJoinedByString:@","]] stringByAppendingString:@"]"] stringByReplacingOccurrencesOfString:@"\'" withString:@"\\\""];
        //NSString *promos = [[[[[@"[" stringByAppendingString:[promosStrs componentsJoinedByString:@","]] stringByAppendingString:@"]"] stringByReplacingOccurrencesOfString:@"\'" withString:@"\\\""] stringByReplacingOccurrencesOfString:@"href=\\\"" withString:@"href=\\\'"] stringByReplacingOccurrencesOfString:@"\\\">" withString:@"\\\'>"];
        //NSLog(@"%@", promos);
        
        NSArray *json = [[NSArray alloc] initWithArray:[self.shareModel.dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM promos"]]];
        if(json && json != nil && [json count] != 0 && [json[0] count] != 0 && [json[0][2] dataUsingEncoding:NSUTF8StringEncoding] != nil){
            NSArray *allPromos = [NSJSONSerialization JSONObjectWithData:  [json[0][2] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &e];
            NSLog(@"Amount of promos previously in db: %lu", (unsigned long)[allPromos count]);
            
            [self.shareModel.dbManager executeQuery:[NSString stringWithFormat:@"UPDATE promos SET content=('%@')",promos]];

            json = [[NSArray alloc] initWithArray:[self.shareModel.dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM promos"]]];
            if(json && json != nil && [json count] != 0 && [json[0] count] != 0 && [json[0][2] dataUsingEncoding:NSUTF8StringEncoding] != nil){
                allPromos = [NSJSONSerialization JSONObjectWithData:  [json[0][2] dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error: &e];
                NSLog(@"Current amount of promos in db, after insertion or deletion: %lu", (unsigned long)[allPromos count]);
                
                completionHandler(UIBackgroundFetchResultNewData);
            }
        }
    } else {
        NSLog(@"Background fetch brought nil in oRespondeData");
        [self.shareModel addLogWithAction:@"055" andId:NULL];
    }
    /*Second try*/
    
    
     //completionHandler(UIBackgroundFetchResultNewData);
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    [self.shareModel restartSLC];
    
    isActive = false;
    
    NSLog(@"restart the slc mofo");
    
    //[self.shareModel NotifyNearPromos];
    /*__block UIBackgroundTaskIdentifier bgTask = [application beginBackgroundTaskWithName:@"NNP" expirationHandler:^{
        [application endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        if(LOGS2){
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.alertBody = @"NNP BGTASK";
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        }
            
        [self.shareModel NotifyNearPromos];
        
        [application endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    });*/
    
    
    [self.shareModel addApplicationStatusToPList:@"applicationDidEnterBackground"];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    NSLog(@"applicationDidBecomeActive");
    
    [self.shareModel addApplicationStatusToPList:@"applicationDidBecomeActive"];

    //Remove the "afterResume" Flag after the app is active again.
    self.shareModel.afterResume = NO;
    
    //if(!isActive){
        [self.shareModel startSLC];
        //isActive = true;
    //}
    
    //[self.shareModel.anotherLocationManager startMonitoringSignificantLocationChanges];
    
    /*TEST*
    NSLog(@"monitored: %lu", (unsigned long)[[self.shareModel.anotherLocationManager monitoredRegions] count]);
    for (CLRegion *monitored in [self.shareModel.anotherLocationManager monitoredRegions])
        [self.shareModel.anotherLocationManager stopMonitoringForRegion:monitored];
    NSString *gfid = [[NSUUID UUID] UUIDString];
    CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:([[[CLLocation alloc]  initWithLatitude:-34.604594 longitude:-58.405483] coordinate]) radius:400 identifier:gfid];
    [self.shareModel.anotherLocationManager startMonitoringForRegion:region];
    /*TEST*/
    
    //[self.shareModel searchForNearbyShop:@""];
    
    [self.shareModel addLogWithAction:@"100" andId:NULL];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    NSLog(@"applicationWillTerminate");
    [self.shareModel addApplicationStatusToPList:@"applicationWillTerminate"];
    
    isActive = false;
    
    //[self.shareModel startMonitoringSignificantLocationChanges];

    /*REQUEST LOCATION BEFORE SINCE THIS EVENT MAY OCCUR LONG AFTER AN UPDATED LOCATION HAS BEEN STORED*/
    //[self.shareModel.anotherLocationManager requestLocation];

    //SHOULD BE WRAPPED IN A BACKGROUND EXECUTION TASK BLOCK
    /*Look for nearby promos and notify them*/
    //[self.shareModel NotifyNearPromos];
    
}

// this happens while we are running ( in the background, or from within our own app )
// only valid if Sarkany-Info.plist specifies a protocol to handle
- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
    if (!url) {
        return NO;
    }
    
    // all plugins will get the notification, and their handlers will be called
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:CDVPluginHandleOpenURLNotification object:url]];

    return YES;
}

-(void) shouldNotDetail {
    shouldDetail = @"false";
    NSLog(@"SHOULD NOT DETAIL");
}

// repost all remote and local notification using the default NSNotificationCenter so multiple plugins may respond
- (void)            application:(UIApplication*)application
    didReceiveLocalNotification:(UILocalNotification*)notification
{
    if(notification.userInfo != nil){
        if([[notification.userInfo valueForKey:@"type"] isEqualToString:@"news"]){
            [self.shareModel addLogWithAction:@"410" andId:[notification.userInfo valueForKey:@"id"]];
        } else {
            [self.shareModel addLogWithAction:@"310" andId:[notification.userInfo valueForKey:@"id"]];
        }
    } else {
        [self.shareModel addLogWithAction:@"310" andId:NULL];
    }
        
    if([notification.userInfo objectForKey:@"wasScheduled"]){
        /*Execute query*/
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *date = [dateFormatter stringFromDate:[NSDate date]];
        [self.shareModel.dbManager executeQuery:[NSString stringWithFormat:@"INSERT OR IGNORE INTO notificaciones (fecha,notificacionId, elementId, geofence) VALUES ('%@','%@','%@', 'none')", date, [notification.userInfo valueForKey:@"nid"], [notification.userInfo valueForKey:@"id"]]];
        /*Execute query*/
    }
    
    NSLog(@"Notification type: %@", [notification.userInfo valueForKey:@"type"]);

    if([[notification.userInfo valueForKey:@"type"] isEqualToString:@"news"])
        NSLog(@"News notification was received: %@", notification.userInfo);
    else
        NSLog(@"Some notification was received.");
    
    if(application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground){
        
        if(notification.userInfo != nil){
            
            if([[notification.userInfo valueForKey:@"type"] isEqualToString:@"news"])
                [self.shareModel addLogWithAction:@"411" andId:[notification.userInfo valueForKey:@"id"]];
            else
                [self.shareModel addLogWithAction:@"311" andId:[notification.userInfo valueForKey:@"id"]];
            
            /*Detail in WebApp*/
            NSError *err;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject: notification.userInfo options:0 error:&err];
            NSString *data = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"detailNotifiedPromo('%@')",data]];
            /*Detail in WebApp*/
        } else {
            NSLog(@"Received notification had nil userInfo");
        }
        
    } else {
        if(notification.userInfo != nil){
            
            if([[notification.userInfo valueForKey:@"type"] isEqualToString:@"news"])
                [self.shareModel addLogWithAction:@"410" andId:[notification.userInfo valueForKey:@"id"]];
            else
                [self.shareModel addLogWithAction:@"310" andId:[notification.userInfo valueForKey:@"id"]];
            
            /*Detail in WebApp*/
            NSError *err;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject: notification.userInfo options:0 error:&err];
            NSString *data = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"notifyPromo('%@')",data]];
            /*Detail in WebApp*/
        }
    }
    /*TEST*/
    [[NSNotificationCenter defaultCenter] postNotificationName:CDVLocalNotification object:notification];
}

#ifndef DISABLE_PUSH_NOTIFICATIONS

    - (void)                                 application:(UIApplication*)application
        didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
    {
        // re-post ( broadcast )
        NSString* token = [[[[deviceToken description]
            stringByReplacingOccurrencesOfString:@"<" withString:@""]
            stringByReplacingOccurrencesOfString:@">" withString:@""]
            stringByReplacingOccurrencesOfString:@" " withString:@""];

        [[NSNotificationCenter defaultCenter] postNotificationName:CDVRemoteNotification object:token];
    }

    - (void)                                 application:(UIApplication*)application
        didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
    {
        // re-post ( broadcast )
        [[NSNotificationCenter defaultCenter] postNotificationName:CDVRemoteNotificationError object:error];
    }
#endif

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window
#else
- (UIInterfaceOrientationMask)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window
#endif
{
    // iPhone doesn't support upside down by default, while the iPad does.  Override to allow all orientations always, and let the root view controller decide what's allowed (the supported orientations mask gets intersected).
    NSUInteger supportedInterfaceOrientations = (1 << UIInterfaceOrientationPortrait) | (1 << UIInterfaceOrientationLandscapeLeft) | (1 << UIInterfaceOrientationLandscapeRight) | (1 << UIInterfaceOrientationPortraitUpsideDown);

    return supportedInterfaceOrientations;
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication*)application
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}



@end
