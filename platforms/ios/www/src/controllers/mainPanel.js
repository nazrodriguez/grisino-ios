'use strict';

var facebookId = "";
var MainPanel = angular.module('MainPanel', []);


MainPanel.controller('mainPanelCtrl', ['$scope', '$rootScope', '$cordovaFacebook', '$cordovaSQLite', '$location', '$mdToast', '$mdDialog', function($scope, $rootScope, $cordovaFacebook, $cordovaSQLite, $location, $mdToast, $mdDialog) {

    // Reset devInfo because user has logged out
    $rootScope.devInfo = undefined;
    SERVER = SERVER_DEFAULT;

    setTimeout(function() {
        var bottom = parseInt($("#regBtn").css("left"));
        var margin = 25;
        var height = $("#regBtn").height() + margin;

        $("#regBtn").animate({ bottom: bottom + "px", opacity: 1 }, 500);
        $("#emailBtn").animate({ bottom: bottom + height + "px", opacity: 1 }, 500);
        $("#fbBtn").animate({ bottom: bottom + (height * 2) + "px", opacity: 1 }, 500);

    }, 300);

    $scope.loginGuest = function(){
        $rootScope.isGuest = true;
        $rootScope.login(HARDCODED_GUEST.mail, HARDCODED_GUEST.pass);
    }

    $rootScope.joinFacebook = function() {

        $rootScope.loading = true;
        $cordovaFacebook.login(["public_profile", "user_birthday", "user_friends", "email"]).then(function(success) {

            console.log(success);
            var _tokken = success.authResponse.accessToken;
            var facebookId = success.authResponse.userID;
            var _userEmail = "";

            console.log("GETS HERE");

            var user = {
                //email: success.email,
                //gender: success.gender,
                facebookId: facebookId,
                accessToken: _tokken,
                brand: BRAND,
                fields: {
                    wantsNews: true,
                    os: 'iOS',
                    version: IOSVERSION,
                    app_version: VERSION
                }
            };

            /*????????? WHY ¿¿¿¿¿¿¿¿¿¿*/
            user.fields = JSON.stringify(user.fields);

            Playtomic.Auth.loginFacebook(user, function(response) {
                console.log(response);
                if (response.success) {

                    $rootScope.isGuest = false;

                    if (response.user.national_identity != '') {

                        $rootScope.loading = false;
                        console.log("DNI");

                        TOKEN = response.token;
                        TOKEN_DEBUG = TOKEN == undefined;
                        $rootScope.user = response.user;
                        $rootScope.user.token = TOKEN;

                        var query = "INSERT INTO usuario (content, facebook_id, user_id, rDate) VALUES (?,?,?,?)";
                        $cordovaSQLite.execute($rootScope.db, query, [JSON.stringify(response.user, null, 4), facebookId, response.user.id, response.user.register_date]).then(function(res) {}, function(err) {
                            console.log('error en cache de usuario' + JSON.stringify(err, null, 4));
                        });

                        //ask for stars
                        $rootScope.getStars(function(){
                            $rootScope.toggleAnnoyingMessage(!$rootScope.isGrisinoFan(), true);    
                        });

                        $rootScope.user.facebookId = facebookId;
                        $rootScope.user.facebookUserImage = "https://graph.facebook.com/" + $rootScope.user.facebookId + "/picture?width=100&height=100";
                        $rootScope.user.facebookUserImageBig = "https://graph.facebook.com/" + $rootScope.user.facebookId + "/picture?width=" + $("#mainView").width();
                        $rootScope.checkTablas();

                    } else {

                        console.log("NO DNI");
                        $rootScope.loading = false;

                        // ask for dni and for stars later, then save it in sqlite
                        $rootScope.askDNI(function() {
                            $rootScope.loading = true;

                            $rootScope.getStars(function(){
                                $rootScope.toggleAnnoyingMessage(!$rootScope.isGrisinoFan(), true);    
                            });

                            var _user = {};

                            TOKEN = response.token;
                            _user = response.user;

                            _user.self_user = JSON.stringify(response.user);
                            _user.national_identity = $rootScope.dni;

                            _user.token = TOKEN;

                            _user.facebookId = facebookId;
                            _user.facebookUserImage = "https://graph.facebook.com/" + facebookId + "/picture?width=100&height=100";
                            _user.facebookUserImageBig = "https://graph.facebook.com/" + facebookId + "/picture?width=" + $("#mainView").width();

                            //Update user in our db
                            Playtomic.Users.update(_user, function(response) {
                                //Update success     
                                if (response.user) {
                                    $rootScope.user = _user;

                                    //Successfully updated user to save his DNI
                                    var query = "INSERT INTO usuario (content, facebook_id, user_id, rDate) VALUES (?,?,?,?)";
                                    $cordovaSQLite.execute($rootScope.db, query, [JSON.stringify(response.user, null, 4), facebookId, response.user.id, response.user.register_date]).then(function(res) {
                                        $rootScope.checkTablas();
                                        //$rootScope.loading = false;
                                    }, function(err) {
                                        console.log('error en cache de usuario' + JSON.stringify(err, null, 4));
                                        $rootScope.loading = false;
                                    });
                                } else {
                                    if(TOKEN_DEBUG){
                                        TOKEN = undefined;
                                        $rootScope.dni = undefined;
                                        $rootScope.user = undefined;
                                        $rootScope.stars = undefined;
                                        $rootScope.borraTablas(true);
                                        $rootScope.loading = false;
                                        $location.path("/mainPanel");
                                        $mdToast.show(
                                            $mdToast.simple()
                                            .content('No se pudo guardar el DNI. Es posible que hubiera un fallo de conexión. Reintentalo.')
                                            .position('top')
                                            .hideDelay(3000)
                                        );
                                    } else {
                                        Playtomic.Auth.logout(function(response) {
                                            $rootScope.dni = undefined;
                                            $rootScope.user = undefined;
                                            $rootScope.stars = undefined;
                                            $rootScope.borraTablas(true);
                                            $rootScope.loading = false;
                                            $location.path("/mainPanel");
                                            $mdToast.show(
                                                $mdToast.simple()
                                                .content('No se pudo guardar el DNI. Es posible que hubiera un fallo de conexión. Reintentalo.')
                                                .position('top')
                                                .hideDelay(3000)
                                            );
                                        });
                                    }
                                }
                            });
                        });
                    }
                } else {
                    console.log(response);
                    $rootScope.loading = false;

                    $mdDialog.show($mdDialog.confirm().title('Aviso').content('Tu cuenta de facebook no tiene un email válido o no es visible. Por favor, utiliza la opción "Registrate", para registrarte por email.').ariaLabel('Lucky day').ok('Aceptar').targetEvent()).then(function() {}, function() {});

                    $('#alertField').css("display", "inherit");
                }
            });

        }, function(error) {
            console.log(JSON.stringify(error));
        });

    }

    $scope.goTo = function(scr) {
        $location.path('/' + scr);
    }

    $scope.Passport = function(user) {

    }

}]);


function confirmEmail($scope, $mdDialog, $cordovaSQLite) {

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.confirm = function() {
        var user = {
            email: $scope.email,
            gender: success.gender,
            accessToken: _tokken,
            brand: BRAND
        };
        $scope.Passport(user);
        $mdDialog.hide();
    };

}
