'use strict';

var Join = angular.module('Join', []);

Join.controller('joinCtrl', ['$scope', '$rootScope', '$mdDialog', '$location', '$cordovaSQLite', '$mdToast', '$http', function($scope, $rootScope, $mdDialog, $location, $cordovaSQLite, $mdToast, $http) {

    /******************************** VARS DECLARATION ********************************/
    $rootScope.loading = false;
    $rootScope.user;

    /***************************** PASSWORD FIELD FOCUS ******************************/

    $scope.focus = function() {
        $('#alertField').hide();
    }

    $rootScope.dni = undefined;

    $scope.wantsNews = true;

    $rootScope.showEULA = function() {

        $mdDialog.show({
            controller: EULAController,
            controllerAs: 'ctrl',
            template: '<md-dialog style="padding: 0px 25px 25px 25px; max-width: 100%;"><div class="EULAWindow"><br>Gracias por ayudarnos a testear esta nueva experiencia <strong style="color:#d11339">Grisino</strong><md-divider style="margin:12.5px 0px"></md-divider><p><strong>T&eacute;rminos y Condiciones</strong></p><p><strong><br/> Contrato de Licencia de Usuario Final</strong></p><p>Este Contrato de Licencia de Usuario Final ("CLUF") es un contrato legal vinculante entre usted, como individuo o entidad, y CRESKOTEC S.A. &nbsp;(de ahora en m&aacute;s "GRISINO" ).&nbsp; Al descargar, instalar o utilizar esta aplicaci&oacute;n para Android, iOS u otra plataforma m&oacute;vil, seg&uacute;n el caso (de ahora en m&aacute;s el "Software"), usted acepta quedar obligado por los t&eacute;rminos de &eacute;ste Contrato de Licencia de Usuario Final, de ahora en adelante CLUF. Si no est&aacute; de acuerdo con los t&eacute;rminos de la licencia, no active la casilla "Acepto las condiciones" y no podr&aacute; utilizar el Software.</p><p>Usted est&aacute; de acuerdo que la instalaci&oacute;n o el uso del Software significa que ha le&iacute;do, entendido y acepta estar obligado por el CONTRATO DE LICENCIA DE USUARIO FINAL.</p><p>El Software se proporciona en virtud de este CLUF &uacute;nicamente para su uso privado y no comercial.</p><ol><li><strong> Descripci&oacute;n del Software</strong></li></ol><p>El Software es una aplicaci&oacute;n de software descargable que permite acceder a funcionalidad, servicios y beneficios propuestos por GRISINO directamente desde tu&nbsp;Android, iPhone, iPad o cualquier otro dispositivo m&oacute;vil compatible ( "Dispositivo" ). Puede descargar el Software en forma gratuita desde la Tienda Online de Aplicaciones de su sistema m&oacute;vil, ya sea que Ud. sea un miembro registrado anteriormente en el Programa de Beneficios Grisino FAN o no, pero debe asociarlo a una cuenta existente o registrarse creando una nueva cuenta, siguiendo las indicaciones del Software, para permitir que su funcionalidad sea completa. Sin ser una enunciaci&oacute;n limitante, el uso del Software est&aacute; destinado a, visualizar contenidos, promociones, participar en actividades l&uacute;dicas individuales o sociales, canjear promociones y/o beneficios, consultar puntos/saldos disponibles, sumar puntos o cualquier otro indicador de participaci&oacute;n y/o transacci&oacute;n, localizar locales de inter&eacute;s y/o ubicaci&oacute;n geogr&aacute;fica, publicar y/o compartir contenidos generados por Ud., acceder a sitios web propios o de terceros, registrar compras realizadas, y cualquier otra Funcionalidad que GRISINO desee incluir en &eacute;sta o futuras versiones del Software.</p><p>&nbsp;</p><ol start="2"><li><strong> Licencia</strong></li></ol><p>GRISINO le otorga, sujeto a los t&eacute;rminos y condiciones de este Acuerdo, una licencia no exclusiva, no transferible que le permite:</p><ul><li>Utilizar el software para su uso personal.</li><li>Instalar el Software en un solo dispositivo.</li><li>Hacer una copia del Software en un formato legible por m&aacute;quina exclusivamente para fines de copia de seguridad, siempre que se reproduzcan el Software en su forma original y con todos los avisos de la copia de seguridad.</li></ul><p>Para mayor claridad, lo anterior no tiene por finalidad prohibir instalaci&oacute;n y copia de seguridad del software en&nbsp; otros dispositivos en los que se acord&oacute; tambi&eacute;n el CONTRATO DE LICENCIA DE USUARIO FINAL. Cada una de las instancias de este CLUF que usted est&aacute; de acuerdo le otorga a los derechos antes mencionados en relaci&oacute;n con la instalaci&oacute;n, el uso y el back-up de una copia del Software en un dispositivo.</p><ol start="3"><li><strong> T&iacute;tulo de Propiedad</strong></li></ol><p>La propiedad y todos los derechos (incluyendo sin limitaci&oacute;n derechos de propiedad intelectual) del Software permanecen con GRISINO. Con excepci&oacute;n de los derechos expresamente reconocidos en la presente Licencia, no se conceder&aacute;n otros derechos, ya sea expresa o impl&iacute;cita.</p><ol start="4"><li><strong> Restricciones</strong></li></ol><p>Usted entiende y acepta que usted s&oacute;lo puede usar el Software de tal manera que cumpla con todas las leyes aplicables en las jurisdicciones en que se utilice el Software. Su uso se har&aacute; de conformidad con las restricciones del caso acerca de la privacidad y los derechos de propiedad intelectual.</p><p>&nbsp;</p><p>Usted no puede:</p><ul><li>Crear versiones de otros Software derivados del Software.</li><li>Utilizar el software para cualquier prop&oacute;sito que no sea el descrito en este documento.</li><li>Copiar o reproducir el Software, excepto como se describe en la presente licencia.</li><li>Vender, asignar, licenciar, divulgar, distribuir o transferir de cualquier otra forma poner a disposici&oacute;n del Software o de cualquier copia del Software de ninguna forma a terceras partes.</li><li>Modificar, traducir, descompilar, desensamblar ni realizar ingenier&iacute;a inversa en el Software, o la tentativa de lo anterior, salvo en la medida en que esta prohibici&oacute;n no est&aacute; permitido en la legislaci&oacute;n aplicable.</li><li>Quitar o modificar ning&uacute;n aviso de propiedad o marcas en el Software.</li><li>Subir, compartir y/o publicar&nbsp; textos que no sean de su exclusiva autor&iacute;a y para los cuales posea todos los derechos de publicaci&oacute;n necesarios.</li><li>Subir, compartir y/o publicar&nbsp; fotograf&iacute;as y/o videos&nbsp; que no sean de su exclusiva autor&iacute;a y para los cuales posea todos los derechos de publicaci&oacute;n necesarios.</li><li>Subir, compartir y/o publicar&nbsp; textos, fotos, audios y/o videos que sean difamatorios, ofensivos para otros miembros usuarios del Software, que sean contrarios a principios &eacute;ticos generalmente aceptados y/o a las normas legales.<br/> </li></ul><p>El incumplimiento de estas restricciones producir&aacute; la baja inmediata de Ud. como usuario del Software impidiendo al infractor a continuar utilizando el mismo. Sin perjuicio de lo expuesto, GRISINO se reserva el derecho de iniciar las acciones legales necesarias a efectos de lograr un resarcimiento para s&iacute; y/o para los terceros damnificados por la acci&oacute;n indebida desarrollada.<br/> </p><ol start="5"><li><strong> Informaci&oacute;n personal y privacidad</strong></li></ol><p>Puede que le solicitemos que nos proporcione cierta informaci&oacute;n acerca de usted durante el proceso de descarga de Software y/o durante el uso del mismo. El software podr&iacute;a registrar la actividad que Ud. desarrolla con &eacute;l a efectos de mejorar su experiencia de uso y proveerle contenidos y servicios m&aacute;s relevantes acorde a sus intereses. Al optar por utilizar el Software y/o el, usted indica su comprensi&oacute;n y aceptaci&oacute;n del CLUF. Nunca compartiremos su informaci&oacute;n personal con otras empresas. Sin embargo Usted entiende y acepta que GRISINO puede revelar informaci&oacute;n cuando as&iacute; lo requiera la ley o en la creencia de buena fe que dicha revelaci&oacute;n es razonablemente necesaria para cumplir con procesos legales, hacer cumplir los t&eacute;rminos de este CLUF, o para proteger los derechos, propiedad o seguridad de GRISINO, sus usuarios o el p&uacute;blico.</p><p>&nbsp;</p><ol start="6"><li><strong> Sin garant&iacute;a</strong></li></ol><p>GRISINO no garantiza que las funciones contenidas en el software vaya a cumplir con las necesidades o requerimientos que se pueda tener, o que el software funcionar&aacute; libre de errores, o de forma ininterrumpida, o que cualquier defecto o error ser&aacute; corregido, o que el software es totalmente compatible con cualquier plataforma en particular. El software se ofrece en el estado "tal cual es" y sin garant&iacute;a, ya sea expresa o impl&iacute;cita. GRISINO expresamente niega todas las garant&iacute;as de cualquier tipo, ya sea expresa o impl&iacute;cita, incluyendo, pero no limitado a, las garant&iacute;as impl&iacute;citas de comerciabilidad y/o idoneidad para un prop&oacute;sito particular. Usted acepta que la informaci&oacute;n que se presenta en el software no posee garant&iacute;a alguna de veracidad o certeza por lo que no podr&aacute; ejercer reclamo alguno por decisiones que usted tomara basado en la informaci&oacute;n presentada. Es de su exclusiva responsabilidad realizar las verificaciones correspondientes antes de tomar acci&oacute;n alguna. Sin ser una enunciaci&oacute;n abarcativa queda bajo su responsabilidad verificar direcciones, horarios, ubicaciones indicadas mediante GPS, promociones vigentes, actividades sugeridas, datos de contacto, entre otras.&nbsp;&nbsp; Algunas jurisdicciones no permiten la renuncia o la exclusi&oacute;n de garant&iacute;as impl&iacute;citas, por lo que es posible que no se apliquen a usted.</p><p>&nbsp;</p><ol start="7"><li><strong> Derecho a rescindir o modificar el Software</strong></li></ol><p>GRISINO podr&aacute; modificar el Software y este CLUF con aviso, ya sea por correo electr&oacute;nico o publicando un aviso en el sitio Web, incluyendo pero no limitado al cobro de tarifas por el Software, o cambiar la funcionalidad o la apariencia del Software. En el caso que GRISINO modifique el Software o los t&eacute;rminos de la licencia, usted puede terminar este CLUF y dejar de utilizar el Software. GRISINO podr&aacute; poner fin a su uso del Software y dar por finalizado el presente CLUF en cualquier momento, con o sin previo aviso.</p><p>&nbsp;</p><ol start="8"><li><strong> Open Source</strong></li></ol><p>El Software puede contener porciones de software de fuente abierta. Cada elemento del software de fuente abierta est&aacute; sujeto a sus propias condiciones de licencia aplicables. Los Derechos de Autor del software de c&oacute;digo abierto est&aacute;n en manos de los respectivos titulares de los derechos de autor de dicho c&oacute;digo.</p><p>&nbsp;</p><ol start="9"><li><strong> Indemnizaci&oacute;n</strong></li></ol><p>Al aceptar el CLUF, usted se compromete a indemnizar y mantener indemne a GRISINO, sus filiales, empresas asociadas y otros socios, de cualquier da&ntilde;o directo o indirecto que surja como consecuencia de la utilizaci&oacute;n indebida que Ud. realice del Software o de cualquier otro asunto relacionado con el Software.</p><p>&nbsp;</p><ol start="10"><li><strong> Limitaci&oacute;n de responsabilidad</strong></li></ol><p>Usted reconoce y acepta expresamente que GRISINO no ser&aacute; responsable de ning&uacute;n da&ntilde;o indirecto, incidental, consecuente del uso que usted haga del software, incluyendo pero no limitado a, da&ntilde;os y perjuicios,&nbsp; da&ntilde;os por p&eacute;rdida de datos, de dinero, de ingresos, de puntos, de beneficios de cualquier &iacute;ndole, u otras p&eacute;rdidas intangibles (incluso si GRISINO ha sido advertido de la posibilidad de tales da&ntilde;os). Algunas jurisdicciones no permiten la limitaci&oacute;n de los da&ntilde;os y perjuicios y/o exclusiones de responsabilidad por da&ntilde;os incidentales o consecuentes. En este sentido, algunas de las limitaciones anteriores pueden no aplicarse a usted.</p><p>&nbsp;</p><ol start="11"><li><strong> General</strong></li></ol><p>El acuerdo de licencia entre usted y GRISINO fija jurisdicci&oacute;n en la Ciudad Aut&oacute;noma de Buenos Aires, siendo ese el foro exclusivo para cualquier disputa derivada o relacionada con este CONTRATO. El contrato de licencia constituye el acuerdo completo entre usted y GRISINO con respecto al Software. Si alguna disposici&oacute;n de este CLUF es considerada por un tribunal de jurisdicci&oacute;n competente para ser contraria a la ley, tal disposici&oacute;n ser&aacute; cambiada e interpretada con el fin de cumplir mejor los objetivos de la disposici&oacute;n original hasta el m&aacute;ximo permitido por la ley y el resto de las disposiciones de este acuerdo permanecer&aacute; en pleno vigor y efecto. Usted no puede ceder este Contrato y la cesi&oacute;n de este CLUF por usted ser&aacute; nula y sin valor.&nbsp; GRISINO, otros logotipos y nombres son marcas registradas de GRISINO. Usted se compromete a no utilizar estas marcas en cualquier forma sin permiso previo y por escrito. Los t&iacute;tulos de secci&oacute;n y la numeraci&oacute;n de este CLUF se muestran para su comodidad y no producen efectos jur&iacute;dicos.</p><p>&nbsp;</p></div><table class="fixed EULATable"><tr><td><center><md-button hm-tap="cancel();" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-raised md-primary loginBtn EULABtn">Cancelar</md-button></center></td><td><center><md-button hm-tap="accept();" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-raised md-primary loginBtn EULABtn">Aceptar</md-button></center></td></tr></table></md-dialog>',

            clickOutsideToClose: false
        }).then(function(card) {

            $rootScope.checkDNI();

        }, function() {
            var confirm = $mdDialog.confirm()
                .title('Aviso')
                .content('Es necesario aceptar los términos para utilizar la aplicación. ¿Deseas salir o volver a ver los términos?.')
                .ariaLabel('Lucky day')
                .ok('Salir')
                .cancel('Volver')
                .targetEvent();

            $mdDialog.show(confirm).then(function() {
                $location.path("/mainPanel");
            }, $rootScope.showEULA);

        });
    }

    //TODO: VER SI NECESITA EULA
    $rootScope.showEULA();

    /******************************** SIGN UP FUNCTION ********************************/

    $scope._checkDNI = function(){
        $(".dni.login-input").blur();
        setTimeout(function(){
            $rootScope.checkDNI();
        },100);
    }

    $scope.join = function(ev) {

        if ($scope.firstName == undefined) {
            $mdToast.show(
                $mdToast.simple()
                .content('Nombre incompleto')
                .position('top')
                .hideDelay(3000)
            );
        } else if ($scope.lastName == undefined) {
            $mdToast.show(
                $mdToast.simple()
                .content('Apellido incompleto')
                .position('top')
                .hideDelay(3000)
            );
        } else if ($scope.email == undefined || $scope.email == "" || $scope.email.indexOf("@") == -1 || $scope.email.indexOf(".") == -1) {
            $mdToast.show(
                $mdToast.simple()
                .content('E-mail incorrecto/no especificado')
                .position('top')
                .hideDelay(3000)
            );
        } else if ($scope.password == undefined) {
            $mdToast.show(
                $mdToast.simple()
                .content('Contraseña incompleta')
                .position('top')
                .hideDelay(3000)
            );
        } else if ($scope.password != $scope.repeatPassword) {
            $mdToast.show(
                $mdToast.simple()
                .content('Las contraseñas no coinciden')
                .position('top')
                .hideDelay(3000)
            );
        } else if (!$scope.dni || $scope.dni == "") {
            $mdToast.show(
                $mdToast.simple()
                .content('Por favor, ingresa tu DNI')
                .position('top')
                .hideDelay(3000)
            );
        } else {

            var user = {
                name: $scope.firstName + " " + $scope.lastName,
                first_name: $scope.firstName,
                last_name: $scope.lastName,
                email: $scope.email,
                password: $scope.password,
                brand: BRAND,
                fields: {
                    wantsNews: $scope.wantsNews,
                    os: 'iOS',
                    version: IOSVERSION,
                    app_version: VERSION
                },
                national_identity: $scope.dni
            };

            $rootScope.loading = true;

            Playtomic.Users.signup(user, function(response) {

                if (response.success) {
                    console.log(response);
                    
                    TOKEN = response.token;
                    TOKEN_DEBUG = TOKEN == undefined;

                    $rootScope.user = response.user.user;
                    response.user.user.token = TOKEN;
                    $rootScope.user.token = TOKEN;

                    $rootScope.isGuest = $rootScope.user.email == HARDCODED_GUEST.mail;

                    var query = "INSERT INTO usuario (content, facebook_id, user_id) VALUES (?,?,?)";

                    $cordovaSQLite.execute($rootScope.db, query, [JSON.stringify(response.user.user, null, 4), "", response.user.user.id]).then(function(res) {

                        $rootScope.logged = true;

                        $rootScope.checkTablas();

                    }, function(err) {
                        console.log('error en cache de usuario' + JSON.stringify(err, null, 4));
                        $rootScope.loading = false;    
                    });

                } else {
                    console.log(response);
                    // ERROR
                    $rootScope.loading = false;
                    if (response.errorcode == "507") {
                        $mdToast.show(
                            $mdToast.simple()
                            .content('Ya existe un usuario con este mail en nuestra base de datos.')
                            .position('top')
                            .hideDelay(3000)
                        );
                    } else if(response.exceptionmessage.indexOf("is being used already") != -1){
                        $mdToast.show(
                            $mdToast.simple()
                            .content('El ' + ((response.exceptionmessage.indexOf("dni") != -1) ? 'dni' : 'e-mail') + ' ingresado ya corresponde a otro usuario registrado en la aplicación.')
                            .position('top')
                            .hideDelay(3000)
                        );
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                            .content('Error en el registro.')
                            .position('top')
                            .hideDelay(3000)
                        );
                    }
                }
            });
        }
    };

}]);

function passController($scope, $mdDialog, $rootScope, $http, $mdToast) {
    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.accept = function(ev) {

        $rootScope.loading = true;

        var dataObj = {
            email: $rootScope.user.email,
            brand: BRAND
        };

        var resUpdate = $http.post('http://' + SERVER + '.reachout.global/update-password', dataObj);

        $mdDialog.hide();

        resUpdate.success(function(data, status, headers, config) {

            console.log(data);

            if (data.success) {

                $mdDialog.hide($scope.email);

                $mdToast.show(
                    $mdToast.simple()
                    .content('Te enviamos un email con los pasos necesarios para cambiar tu contraseña.')
                    .position('top')
                    .hideDelay(6000)
                );

                $scope.email = "";

            } else {

                $mdToast.show(
                    $mdToast.simple()
                    .content('Ocurrió un error, inténtalo más tarde.')
                    .position('top')
                    .hideDelay(6000)
                );

                $scope.email = "";

            }
            
            $rootScope.loading = false;
        });

        resUpdate.error(function(data, status, headers, config) {
            console.log(JSON.stringify(data));
            $rootScope.loading = false;
            $rootScope.checkConnection(function(){
                $scope.accept(ev);
            });
        });
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
        $rootScope.loading = false;
    };
}


function EULAController($scope, $mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.accept = function(answer) {
        $mdDialog.hide(answer);
    };
}
