'use strict';

var Profile = angular.module('Profile', []);


Profile.controller('profileCtrl', ['$scope', '$location', '$mdDialog', '$rootScope', '$interval', '$cordovaSQLite', '$cordovaInAppBrowser', '$mdToast', function($scope, $location, $mdDialog, $rootScope, $interval, $cordovaSQLite, $cordovaInAppBrowser, $mdToast) {

    window.ga.trackView("Profile");

    /*Vars*/
    $rootScope.backupData;
    $rootScope.thereAreChanges = false;
    var freshStart = false;

    if($rootScope.lastDataKnown == undefined){
        $cordovaSQLite.execute($rootScope.db, "SELECT * FROM config", []).then(function(res) {
            if (res.rows.length != 0){
                //for (var b = 0; b < res.rows.length; b++)
                if (res.rows.item(0).content == "on")
                    $rootScope.lastDataKnown = "1";
                else
                    $rootScope.lastDataKnown = "0";
            } else {
                $rootScope.lastDataKnown = "1";
                freshStart = true;
            }

            console.log($rootScope.lastDataKnown);
        }, function(err) {
            console.log(err);

            $rootScope.lastDataKnown = "1";
            freshStart = true;
            console.log($rootScope.lastDataKnown);
        });
    }

    $scope.data = $rootScope.lastDataKnown ? $rootScope.lastDataKnown : "1";

    $scope.bankinfo = [{
        "bankID": 15,
        "nombre": "Banco de Comercio",
        "tarjetas": [{ "id": 1, "nombre": "MasterCard Grisino" }]
    },{
        "bankID": 0,
        "nombre": "BBVA Francés",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "Mastercard" }
        ]
    }, {
        "bankID": 1,
        "nombre": "Galicia",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "Mastercard" },
            { "id": 2, "nombre": "American Express" }
        ]
    }, {
        "bankID": 2,
        "nombre": "Macro",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "Mastercard" },
            { "id": 2, "nombre": "American Express" }
        ]
    }, {
        "bankID": 3,
        "nombre": "Santander Río",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "American Express" }
        ]
    }, {
        "bankID": 4,
        "nombre": "Citibank",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "Diners" },
            { "id": 2, "nombre": "Mastercard" }
        ]
    }, {
        "bankID": 5,
        "nombre": "ICBC",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "Mastercard" }
        ]
    }, {
        "bankID": 6,
        "nombre": "Provincia",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "Mastercard" },
            { "id": 2, "nombre": "American Express" }
        ]
    }, {
        "bankID": 7,
        "nombre": "Hipotecario",
        "tarjetas": [{ "id": 1, "nombre": "Visa" }]
    }, {
        "bankID": 8,
        "nombre": "Supervielle",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "Mastercard" }
        ]
    }, {
        "bankID": 9,
        "nombre": "HSBC",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "Mastercard" },
            { "id": 2, "nombre": "American Express" }
        ]
    }, {
        "bankID": 10,
        "nombre": "Nación",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "Mastercard" },
            { "id": 2, "nombre": "Nativa Visa" },
            { "id": 3, "nombre": "Nativa Mastercard" }
        ]
    }, {
        "bankID": 11,
        "nombre": "Itaú",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "Mastercard" }
        ]
    }, {
        "id": 12,
        "nombre": "Patagonia",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "Mastercard" },
            { "id": 2, "nombre": "American Express" }
        ]
    }, {
        "bankID": 13,
        "nombre": "Comafi",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "Mastercard" },
            { "id": 2, "nombre": "American Express" }
        ]
    }, {
        "bankID": 14,
        "nombre": "Otros bancos",
        "tarjetas": [{ "id": 0, "nombre": "Visa" },
            { "id": 1, "nombre": "Naranja" },
            { "id": 2, "nombre": "Mastercard" },
            { "id": 3, "nombre": "American Express" },
            { "id": 4, "nombre": "Diners" },
            { "id": 5, "nombre": "Cabal" },
            { "id": 6, "nombre": "Argencard" },
            { "id": 7, "nombre": "Nativa MC" },
            { "id": 8, "nombre": "Nativa Visa" },
            { "id": 9, "nombre": "Italcred" }
        ]
    }].sort(function(a, b) {
        a.tarjetas.sort(function(_a, _b) {
            return _a.nombre > _b.nombre ? 1 : -1;
        });

        b.tarjetas.sort(function(_a, _b) {
            return _a.nombre > _b.nombre ? 1 : -1;
        });

        return a.nombre == "Otros bancos" ? -1 : (b.nombre == "Otros bancos" ? 1 : (a.nombre > b.nombre ? -1 : 1));
    });
    /*Vars*/

    /*Initialize cards, birthday, profile image, facebook boolean and profile data*/
    $rootScope.loading = false;
    if ($rootScope.user.fields.cards != undefined && $rootScope.user.fields.cards != [] && $rootScope.user.fields.cards != [null] && $rootScope.user.fields.cards != null) {
        $scope.banks = unscope($rootScope.user.fields.cards);
    } else {
        $scope.banks = [];
    }

    if (($rootScope.user.facebookId == "" || $rootScope.user.facebookId == undefined) && ($rootScope.user.facebook.id == "" || $rootScope.user.facebook.id == undefined)) {
        $scope.profileBackImage = "";
        $scope.profileImage = "assets/images/empty_user.png"
        $scope.isFacebook = false;
    } else {
        $scope.isFacebook = true;
        $scope.profileBackImage = $rootScope.user.facebookUserImageBig;
    }

    $scope.profile = {
        name: $rootScope.user.name,
        email: $rootScope.user.email,
        gender: ($rootScope.user.gender && $rootScope.user.gender != "") ? ($rootScope.user.gender == "male" ? "hombre" : "mujer") : "",
        dni: $rootScope.user.national_identity,
        wantsNews: $rootScope.user.fields.wantsNews
    };

    console.log($rootScope.user);

    // Prepare birthdate fields
    if ($rootScope.user.birthday)
        if($rootScope.user.birthday.indexOf("T") != -1)
            var birthdate = ($rootScope.user.birthday.split("T")[0]).split("-");
        else
            var birthdate = $rootScope.user.birthday.split("/");

    if (birthdate && birthdate != '' && birthdate.length > 0) {
        $scope.day = birthdate[2] && birthdate[2] != "undefined" ? birthdate[2] : undefined;
        $scope.month = birthdate[1] && birthdate[1] != "undefined" ? birthdate[1] : undefined;
        $scope.year = birthdate[0] && birthdate[0] != "undefined" ? birthdate[0] : undefined;
    } else {
        $scope.day = undefined;
        $scope.month = undefined;
        $scope.year = undefined;
    }

    // Prepare & store backup data
    var data = {
        email: $scope.profile.email,
        name: $scope.profile.name,
        fields: {
            cards: unscope($scope.banks),
            wantsNews: $scope.profile.wantsNews ? true : false
        },
        dni: $rootScope.user.national_identity,
        birthday: $scope.day + '/' + $scope.month + '/' + $scope.year,
        gender: ($rootScope.user.gender && $rootScope.user.gender != "") ? ($rootScope.user.gender == "male" ? "hombre" : "mujer") : ""
    };
    $rootScope.backupData = data;

    /*
    //Initialize config notifications
    $cordovaSQLite.execute($rootScope.db, "SELECT * FROM config", []).then(function(res) {
        if (res.rows.length != 0){
            for (var b = 0; b < res.rows.length; b++){
                console.log(res.rows.item(b));
                if (res.rows.item(b).content == "on")
                    $scope.data = "1";
                else
                    $scope.data = "0";
            }
        } else {
            $scope.data = "1";
        }

    }, function(err) {
        $scope.data = "1";
    });*/

    /*Push 80 years into combo*/
    var year = new Date().getFullYear();
    $scope.years = [];
    for (var i = year - 80; i < year; i++)
        $scope.years.push('' + i);

    /*Show existent cards*/
    setTimeout(function() {
        $(".shipAnimate").each(function() {
            $(this).animate({
                opacity: 1
            }, 500, function() {
                console.log("Finished showing cards.");
            });
        });
    }, 500);

    $rootScope.$watch('thereAreChanges', function(newValue, oldValue) {
        if (newValue) {
            $("#saveChanges").removeClass("disabledButton");
        } else {
            $("#saveChanges").addClass("disabledButton");
        }
    });

    $scope.$watch('data', function(newValue, oldValue) {
        if(freshStart){
            freshStart = false;
        } else {
            if(oldValue != newValue)
            $rootScope.thereAreChanges = $rootScope.lastDataKnown != newValue;
        }
    });

    $scope.$watch('profile.name', function(newValue, oldValue) {
        if (oldValue != newValue)
            $rootScope.thereAreChanges = true;
    });

    $scope.$watch('profile.email', function(newValue, oldValue) {
        if (oldValue != newValue)
            $rootScope.thereAreChanges = true;
    });
    $scope.$watch('profile.gender', function(newValue, oldValue) {
        if (oldValue != newValue)
            $rootScope.thereAreChanges = true;
    });
    $scope.$watch('profile.password', function(newValue, oldValue) {
        if (oldValue != newValue)
            $rootScope.thereAreChanges = true;
    });
    $scope.$watch('profile.dni', function(newValue, oldValue) {
        if (oldValue != newValue)
            $rootScope.thereAreChanges = true;
    });
    $scope.$watch('n', function(newValue, oldValue) {
        if (oldValue != newValue)
            $rootScope.thereAreChanges = true;
    });
    $scope.$watch('m', function(newValue, oldValue) {
        if (oldValue != newValue)
            $rootScope.thereAreChanges = true;
    });

    $scope.$watch('w', function(newValue, oldValue) {
        if (oldValue != newValue)
            $rootScope.thereAreChanges = true;
    });

    $scope.$watch('profile.wantsNews', function(newValue, oldValue) {
        if (oldValue != newValue)
            $rootScope.thereAreChanges = true;
    });

    $scope.unSuscribe = function() {
        $rootScope.loading = true;

        Playtomic.Users.remove({ email: $rootScope.user.email }, function(r) {
            if (r.success) {
                $rootScope.loading = false;
                $rootScope.user = null;
                $location.path("/back/mainPanel");
            } else {
                console.log("Error Code: " + r.errorcode);
                $rootScope.loading = false;
            }
        });
    };

    $rootScope.restoreData = function() {

        $scope.profile.name = $rootScope.backupData.name;
        $scope.profile.email = $rootScope.backupData.email;
        $scope.profile.gender = $rootScope.backupData.gender;
        $scope.profile.dni = $rootScope.backupData.dni;
        $scope.profile.wantsNews = $rootScope.backupData.wantsNews;

        $scope.data = $rootScope.lastDataKnown;

        var strbday = $rootScope.backupData.birthday.split("/");
        var birthdate = {
            day: strbday[0],
            month: strbday[1],
            year: strbday[2]
        }

        $scope.day = birthdate.day;
        $scope.month = birthdate.month;
        $scope.year = birthdate.year;

        $scope.banks = $rootScope.backupData.fields.cards;

        $rootScope.thereAreChanges = false;
    }

    $scope.resetPass = function(ev) {
        $mdDialog.show({
            controller: passController,
            controllerAs: 'ctrl',
            template: '<md-dialog style="padding: 20px;"><h1 style="font-size: 18px;">Te enviaremos un email con las instrucciones para el cambio de contraseña</h1><md-button style="width: 100%!important; height: 52px; background-color: black!important; margin: 14px 0px 0px 0px; padding: 0px" hm-tap="accept()" class="md-primary md-raised loginBtn"> Aceptar </md-button></md-dialog>',
            targetEvent: ev,
            clickOutsideToClose: true
        }).then(function(email) {}, function() {});
    }

    $rootScope.updateUser = function(callback) {

        $rootScope.loading = "inmediate";

        if($scope.profile.email.length < 5 || $scope.profile.email.length > 100 || $scope.profile.email.indexOf("@") == -1){
            $mdToast.show(
                $mdToast.simple()
                .content("Tu email debe ser una dirección válida y ser compuesta por entre 5 y 100 caracteres.")
                .position('top')
                .hideDelay(5000)
            );
            return;
        }

        /*Update notification setting*/
            if ($scope.data == "1")
                var _state = 'on';
            else
                var _state = 'off';

            var query = "UPDATE config SET content=(?)";
            $cordovaSQLite.execute($rootScope.db, query, [_state]).then(function(res) {
                console.log("UPDATED CONFIG NOTIFICATION VAR TO " + $scope.data);

                if ($scope.data == "1")
                    var _acc = 600;
                else
                    var _acc = 601;

                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:_acc};
                $rootScope.addLog(_thelog);
                $rootScope.lastDataKnown = $scope.data;

            }, function(err) {
                console.error(JSON.stringify(err));
                $mdToast.show(
                    $mdToast.simple()
                    .content('Hubo un error guardando los cambios. Inténtalo más tarde.')
                    .position('top')
                    .hideDelay(5000)
                );
                $scope.data = $rootScope.lastDataKnown;
                return;
            });
        /*Update notification setting*/

        //If there´s more than an undefined component of the birthdate, warn and cancel all
        if ((($scope.day ? 0 : 1) + ($scope.month ? 0 : 1) + ($scope.year ? 0 : 1)) < 3 && (($scope.day ? 0 : 1) + ($scope.month ? 0 : 1) + ($scope.year ? 0 : 1)) > 0) {
            var confirm = $mdDialog.confirm().title('Aviso').content('Porfavor, completá toda tu fecha de nacimiento para poder guardar los cambios correctamente. También podés omitirlo y llenar tu fecha de nacimiento en otro momento; se guardará cualquier otro cambio que hayas hecho de todas maneras.').ariaLabel('Lucky day').ok('Completar').cancel('Omitir').targetEvent();

            $rootScope.loading = false;

            $mdDialog.show(confirm).then(function() {}, function() {
                $scope.day = undefined;
                $scope.month = undefined;
                $scope.year = undefined;
                $rootScope.updateUser();
            });

            return;

        }

        var data = {};
        data.email = $scope.profile.email;
        data.brand = "Grisino";
        data.name = $scope.profile.name;
        data.id = $rootScope.user.id ? $rootScope.user.id : ($rootScope.user._id ? $rootScope.user._id : undefined);
        
        if(data.id == undefined){
            $mdToast.show(
                    $mdToast.simple()
                    .content('Hubo un error guardando los cambios. Inténtalo más tarde.')
                    .position('top')
                    .hideDelay(5000)
                );
            return;
        }

        data.fields = {};
        data.fields.cards = angular.copy($scope.banks);
        data.fields.wantsNews = $scope.profile.wantsNews;

        data.birthday = (!$scope.year && !$scope.day && !$scope.month) ? '' : ($scope.year + '/' + $scope.month + '/' + $scope.day);
        data.gender = $scope.profile.gender != '' ? ($scope.profile.gender == "hombre" ? "male" : "female") : '';
        data.national_identity = $scope.profile.dni;

        data.self_user = JSON.stringify($rootScope.user);

        data.is_active = data.is_active === "true" ? true : false;

        console.log("");
        console.log("About to Playtomic.Users.update. Here´s current User:");
        console.log(data);

        var serverNotResponding = setTimeout(function(){
            var _thelog = {date:$rootScope.NOW(), IP:$rootScope.IP, lat: parseFloat($rootScope.lat),lng: parseFloat($rootScope.lng), id: "070"};
            $rootScope.addLog(_thelog);
        }, 10000);

        Playtomic.Users.update(data, function(response) {
            $rootScope.loading = false;

            clearTimeout(serverNotResponding);

            console.log("");
            console.log("And here is new User:")
            console.log(response);

            if (response.user) {

                $rootScope.user.email = data.email;
                $rootScope.user.name = data.name;
                $rootScope.user.gender = data.gender;
                $rootScope.user.fields = data.fields;
                $rootScope.user.national_identity = data.national_identity;
                $rootScope.user.birthday = data.birthday;

                $rootScope.loading = false;
                $rootScope.thereAreChanges = false;
                $rootScope.backupData = data;

                $mdToast.show(
                    $mdToast.simple()
                    .content('Los cambios se guardaron exitosamente')
                    .position('top')
                    .hideDelay(5000)
                );

                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:800};
                $rootScope.addLog(_thelog);

                var query = "UPDATE usuario SET content=(?)";
                $cordovaSQLite.execute($rootScope.db, query, [JSON.stringify($rootScope.user)]).then(function(res) {

                    if (callback)
                        callback();
                }, function(err) {
                    console.error(JSON.stringify(err));
                });
                
            } else {
                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:"069"};
                $rootScope.addLog(_thelog);

                console.log(response);
                if(response.exceptionmessage){
                    var text = 'Error guardando los cambios. Inténtalo más tarde.';
                    switch (response.exceptionmessage){
                        case 'The ID: ' + $scope.profile.dni + ' is not valid or is being used already! (api.users.update:284)':
                            text = 'El DNI ingresado ya corresponde a otro usuario registrado en la aplicación.'
                            break;
                        case "The E-Mail: " + $scope.profile.email + " is not valid or is being used already! (api.users.update:278)":
                            text = 'El e-mail ingresado ya corresponde a otro usuario registrado en la aplicación.';
                            break;
                    }

                } else {
                    text = 'El e-mail ingresado ya corresponde a otro usuario registrado en la aplicación.';
                }

                $mdToast.show(
                    $mdToast.simple()
                    .content(text)
                    .position('top')
                    .hideDelay(5000)
                );

                $rootScope.loading = false;
            }
        });
    }

    $scope._logout = function() {
        if ($rootScope.thereAreChanges) {
            var confirm = $mdDialog.confirm()
                .title('Cambios en perfil')
                .content('Hay cambios sin guardar. ¿Qué desea hacer?')
                .ariaLabel('Lucky day')
                .ok('Guardar')
                .cancel('Descartar cambios')
                .targetEvent();

            $mdDialog.show(confirm).then(function() {
                $rootScope.updateUser(function() {
                    $rootScope.logout();
                });

            }, function() {
                $rootScope.logout();
                $rootScope.restoreData();
            });
        } else {
            $rootScope.logout();
        }
    };

    $scope.changeDNI = function() {
        $("#dniinput").blur();
        $rootScope.askDNI(function(dni) {
            console.log("NEW DNI " + dni);
            $scope.profile.dni = dni;
        }, function() {
            $scope.profile.dni = $rootScope.dni;
        }, "Si querés, podés cambiar tu dni. Procurá ingresar solo números.",
        "Error comprobando que el dni no sea preexistente");
    }

    $scope.addCard = function() {
        setTimeout(function() {
            $mdDialog.show({
                locals: {
                    binfo: $scope.bankinfo
                },
                controller: CardDialogController,
                controllerAs: 'ctrl',
                template: '<md-dialog style="padding: 20px;"><md-button class="md-icon-button" style="background-image: url(assets/images/cerrar2.png)" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'></md-button><h1 style="font-size: 18px;">Quiero recibir información de promociones de:</h1><div style="overflow-y: scroll" class=""><md-input-container style="width:100%"> <md-select style="padding: 0;" placeholder="Banco" ng-model="selected_bank_id" ng-change="changedBank(selected_bank_id)"> <md-option ng-repeat="bank in bankinfo" value="{{bank.bankID}}">{{bank.nombre}}</md-option></md-select></md-input-container><md-input-container style="width:100%"> <md-select style="padding:0;" placeholder="Tarjeta" ng-model="selected_card_id" ng-change="changedCard(selected_card_id)"> <md-option ng-repeat="tarjeta in selected_bank.tarjetas" value="{{tarjeta.id}}">{{tarjeta.nombre}}</md-option></md-select></md-input-container></div><h4">Te llegará info sólo de las tarjetas que especifiques</h4><h4 style="color:red;" ng-show="add_card_error">{{add_card_msg}}</h4><md-button style="width: 100%!important; height: 52px; background-color: green!important; margin: 14px 0px 0px 0px; padding: 0px" hm-tap="accept()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn"> Aceptar </md-button></md-dialog>',
                clickOutsideToClose: true
            }).then(function(options) {
                var bank = options.bank,
                    card = options.card;

                /*Check if already existed*/
                var wasFound = false;
                for (var i = 0; i < $scope.banks.length; i++)
                    if ($scope.banks[i].bankID == bank.bankID){
                        wasFound = true;

                        for(var j = 0; j < $scope.banks[i].tarjetas.length; j++){
                            if($scope.banks[i].tarjetas[j].id == card.id){
                                return;
                            }
                        }

                        $scope.banks[i].tarjetas.push(card);
                    }
                if(!wasFound){
                    console.log("Pushing new bank " + JSON.stringify(bank) + " with new card " + JSON.stringify(card));

                    bank.tarjetas = [card];
                    $scope.banks.push(bank);
                } else {
                    console.log("Bank " + JSON.stringify(bank) + " was found already.");
                }


                /*Check if already existed*/

                $scope.hasCardsLoaded = true;
                var copy = { cards: card, fields: $rootScope.user.fields.cards };
                $rootScope.thereAreChanges = !areJSONArraysEqual($scope.banks, $rootScope.user.fields.cards);

                setTimeout(function() {
                    $(".shipAnimate").eq($scope.banks.length - 1).animate({
                        opacity: 1
                    }, 500);
                }, 300);

            }, function() {});

            setTimeout(function() {
                $(".md-dialog-container").css("height", "calc(100% - 50px)");
            }, 100);
        }, 100);
    }

    $scope.editCard = function(bank, card) {
        setTimeout(function() {
            var info = {
                bank: angular.toJson(bank),
                card: angular.toJson(card)
            };
            $mdDialog.show({
                locals: {
                    bank: bank,
                    card: card,
                    binfo: $scope.bankinfo      
                },
                controllerAs: 'ctrl',
                controller: CardEditDialogController,
                template: '<md-dialog style="padding: 20px;"><md-button class="md-icon-button" style="background-image: url(assets/images/cerrar2.png)" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'></md-button><h1 style="font-size: 18px; padding-top: 0px;">Editar información para:</h1><div style="overflow-y: scroll" class=""><md-input-container style="width:100%"> <md-select style="padding: 0;" placeholder="Banco" ng-model="selected_bank_id" ng-change="changedBankEdit(selected_bank_id)"> <md-option ng-repeat="bank in bankinfo" value="{{bank.bankID}}">{{bank.nombre}}</md-option></md-select></md-input-container><md-input-container style="width:100%"> <md-select style="padding:0;" placeholder="Tarjeta" ng-model="selected_card_id" ng-change="changedCardEdit(selected_card_id)"> <md-option ng-repeat="tarjeta in selected_bank.tarjetas" value="{{tarjeta.id}}">{{tarjeta.nombre}}</md-option></md-select></md-input-container></div><md-button style="width: 100%!important; height: 52px; background-color: green!important; margin: 14px 0px 0px 0px; padding: 0px" hm-tap="edit(\'' + btoa(JSON.stringify(info)) + '\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn"> Aceptar </md-button></md-dialog>',
                clickOutsideToClose: true
            })
            .then(function(data) {

                console.log(data);

                if (data != undefined) {
                    var index = {
                        bank: -1,
                        card: -1
                    }



                    for(var i = 0; i < $scope.banks.length; i++){

                        if($scope.banks[i].bankID == data.old.bank.bankID){
                            index.bank = i;

                            for(var j = 0; j < $scope.banks[i].tarjetas.length; j++){
                                if($scope.banks[i].tarjetas[j].id == data.old.card.id){
                                    index.card = j;
                                    break;
                                }
                            }

                            break;
                        }
                    }

                    if(data.old.bank.bankID != data.new.bank.bankID){ // Different bank, push to banks.
                        data.new.bank.tarjetas = [data.new.card];
                        $scope.banks[index.bank] = data.new.bank;
                    } else { // Different card, replace with new card inside previous card index.      
                        $scope.banks[index.bank].tarjetas[index.card] = data.new.card;
                    }

                    $scope.hasCardsLoaded = true;
                    $rootScope.thereAreChanges = !areJSONArraysEqual($scope.banks, $rootScope.user.fields.cards);
                }

            }, function() {
                console.log('You cancelled the dialog.');
            });
            setTimeout(function() {
                $(".md-dialog-container").css("height", "calc(100% - 50px)");
            }, 100);
        }, 100);
    }

    $scope.deleteCard = function(bank, card) {
        console.log(bank);
        console.log(card);
        var index = {
            bank: -1,
            card: -1
        }
        for(var i = 0; i < $scope.banks.length; i++){
            if($scope.banks[i].bankID == bank.bankID){
                index.bank = i;

                for(var j = 0; j < $scope.banks[i].tarjetas.length; j++){
                    if($scope.banks[i].tarjetas[j].id == card.id){
                        index.card = j;
                        break;
                    }
                }

                $scope.banks[i].tarjetas.splice(index.card, 1);
                break;
            }
        }

        if($scope.banks[index.bank].tarjetas.length == 0)
            $scope.banks.splice(index.bank, 1);
        
        if ($scope.banks.length == 0)
            $scope.hasCardsLoaded = false;

        $rootScope.thereAreChanges = !areJSONArraysEqual($scope.banks, $rootScope.user.fields.cards);
    }

    $scope.goEULA = function() {
        //$location.path("eula");
        $rootScope.loading = true;

        var options = IN_APP_THEMEABLE_BROWSER_OPTIONS;

        var ref = cordova.ThemeableBrowser.open('http://www.grisino.com/terminosycondicionesapp/', '_blank', options)

        ref.addEventListener('closePressed', function(event) {
            $rootScope.loading = false;
            $rootScope.isInAppBrowserOpen = false;
        }).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function(event) {
            $rootScope.loading = false;
            $rootScope.isInAppBrowserOpen = false;
        });

    }

}]);

function passController($scope, $mdDialog, $rootScope, $http, $mdToast) {
    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.accept = function(ev) {

        $rootScope.loading = true;

        var dataObj = {
            email: $rootScope.user.email,
            brand: "Grisino"
        };

        var resUpdate = $http.post('http://' + SERVER + '.reachout.global/update-password', dataObj);

        resUpdate.success(function(data, status, headers, config) {

            if (data.success) {

                $mdDialog.hide($scope.email);

                $mdToast.show(
                    $mdToast.simple()
                    .content('Te enviamos un email con los pasos necesarios para cambiar tu contraseña')
                    .position('top')
                    .hideDelay(6000)
                );

                $scope.email = "";

            } else {

                $mdToast.show(
                    $mdToast.simple()
                    .content('Ocurrio un error, intentalo más tarde')
                    .position('top')
                    .hideDelay(6000)
                );

                $scope.email = "";

            }

            $rootScope.loading = false;
        });

        resUpdate.error(function(data, status, headers, config) {
            console.log(JSON.stringify(data));
        });
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
        $rootScope.loading = false;
    };
}


function CardDialogController($scope, $mdDialog, binfo) {

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.bankinfo = binfo;
    $scope.bankinfo.sort(function(a, b) {
        return a > b ? 1 : -1;
    });

    $scope.changedBank = function(id) {
        for (var a = 0; a < $scope.bankinfo.length; a++)
        if ($scope.bankinfo[a].bankID == id) {
            $scope.selected_bank = angular.copy($scope.bankinfo[a]);
            $scope.selected_card = undefined;
        }

        if ($scope.add_card_error) 
            $scope.add_card_msg = "Porfavor elegí una tarjeta para continuar";
    }

    $scope.changedCard = function(id) {
        for (var a = 0; a < $scope.selected_bank.tarjetas.length; a++)
        if ($scope.selected_bank.tarjetas[a].id == id)
            $scope.selected_card = $scope.selected_bank.tarjetas[a];

        if ($scope.add_card_error) 
            $scope.add_card_error = false;
    }

    $scope.accept = function(ev) {
        if ($scope.selected_bank == undefined || $scope.selected_card == undefined) {
            $scope.add_card_error = true;

            if (!$scope.selected_bank && !$scope.selected_card)
                $scope.add_card_msg = "Porfavor elegí un banco y  una tarjeta para continuar";
            else
                $scope.add_card_msg = "Porfavor elegí una tarjeta para continuar";
        } else {
            $mdDialog.hide({ "bank": angular.copy($scope.selected_bank), "card": angular.copy($scope.selected_card) });
        }
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };
}

function CardEditDialogController($scope, $mdDialog, bank, card, binfo) {

    $scope.bankinfo = binfo;
    $scope.bankinfo.sort(function(a, b) {
        return a.name > b.name ? 1 : -1;
    })
    //$scope.selected_bank = bank;
    //$scope.selected_card = card;

    $scope.changedBankEdit = function(id) {
        for (var a = 0; a < $scope.bankinfo.length; a++)
        if ($scope.bankinfo[a].bankID == id) {
            $scope.selected_bank = angular.copy($scope.bankinfo[a]);
            $scope.selected_card = undefined;
        }

        $scope.changedCardEdit($scope.selected_card_id);

        console.log("changedBank");
        console.log($scope.selected_bank);

        if ($scope.add_card_error) 
            $scope.add_card_msg = "Porfavor elegí una tarjeta para continuar";
    }

    $scope.changedCardEdit = function(id) {
        for (var a = 0; a < $scope.selected_bank.tarjetas.length; a++)
        if ($scope.selected_bank.tarjetas[a].id == id)
            $scope.selected_card = $scope.selected_bank.tarjetas[a];

        console.log("changedCard");
        console.log($scope.selected_card);

        if ($scope.add_card_error) 
            $scope.add_card_error = false;
    }

    $scope.changedBankEdit(bank.bankID);
    $scope.changedCardEdit(card.id);

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    setTimeout(function() {
        $scope.selected_bank_id = bank.bankID;
        $scope.selected_card_id = card.id;
    }, 100);

    $scope.edit = function(data) {
        data = JSON.parse(atob(data));
        var old_bank = JSON.parse(data.bank),
            old_card = JSON.parse(data.card),
            new_bank = JSON.parse(angular.toJson($scope.selected_bank)),
            new_card = JSON.parse(angular.toJson($scope.selected_card));
        
        $mdDialog.hide((old_bank.bankID == new_bank.bankID && old_card.id == new_card.id) ? undefined : { "new": {"bank": $scope.selected_bank, "card": $scope.selected_card}, "old": {"bank": old_bank, "card": old_card} });
    }
}