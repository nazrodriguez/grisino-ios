'use strict';

var ShopsTypes = angular.module('ShopsTypes', []);


ShopsTypes.controller('shopsTypesCtrl',  ['$scope', '$rootScope', '$location', '$route', '$compile', '$timeout', function ($scope, $rootScope, $location, $route, $compile, $timeout) {
    

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Declaración de vars ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    //$scope.categories = $rootScope.categoriasCache.custom_categories.shops;
    $scope.categories = [{
        name: "Todo",
        id: 1,
        subcategories: []
    }];

    $scope.filtersAreDown = false;
    $("#toggleFilters").css({ backgroundColor: '#d60036' });
    $("#toggleFilters").css({ backgroundImage: 'url(assets/images/icon-zoom_30x30px-white.png)' });
    $scope.mapIsUp = false;
    $("#toggleMap").css({ backgroundColor: '#d60036' });
    $("#toggleMap").css({ backgroundImage: 'url(assets/images/icon-mapa3_30x30px-white.png)' }); 

    //console.log(JSON.stringify($scope.categories));
    //console.log(JSON.stringify($rootScope.categoriasCache.custom_categories));

    //Array de subcategorías actuales
    $scope.filteredCategories = $scope.categories[$rootScope.navIndex.category].subcategories;

    /*$rootScope.navIndex.subCategory = [];
    for(var i = 0; i < $scope.filteredCategories.length; i++){
        $rootScope.navIndex.subCategory.push(false);
    }*/

    for(var i = 0; i < $scope.categories.length; i++){
        $rootScope.navIndex.subCategory.push([]);
        for(var j = 0; j < $scope.categories[i].subcategories.length; j++){
            $rootScope.navIndex.subCategory[i].push(false);
        }
    }

    $scope.shopArr = $rootScope.venuesCache;

    //console.log(JSON.stringify($scope.shopArr));

    //Array de filtro ~> llenado programático / Comienza con todas las promos
    $scope.filteredElements;

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Declaración de vars ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    $rootScope.filteredShopsMarks = $scope.filterElements;
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Configuración de buffering~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~// 
    //Inicializar array de queues
    $scope.queues = [];

    //Declarar función de filtro -> retorna array filtrado
    $scope.filterElements = function(){
        var category = $scope.categories[$rootScope.navIndex.category];
             
        var filtered = [];

        //console.log(category);
        filtered = $scope.shopArr

        //Filtering
        .filter(function(mark){                 
            return ($scope.someVal1 ? mark.map.administrative_area_level_1 == $scope.someVal1 : true) && ($scope.someVal2 ? mark.map.locality == $scope.someVal2.name : true) && ($scope.someVal3 ? mark.name == $scope.someVal3.name : true);
        });

        if($rootScope.lat && $rootScope.lng){
            var coords = {lat: $rootScope.lat, lng: $rootScope.lng};
            var aux = filtered;

            filtered.sort(function(a,b){
                return haversine(coords.lat, coords.lng, a.map.lat, a.map.lng).number - haversine(coords.lat, coords.lng, b.map.lat, b.map.lng).number;
            });
        }

        $scope.filteredElements = filtered;
        return filtered;
    }   

    //Inicializar buffer
    $scope.buffer = new _Buffer($scope, $rootScope, function(obj, minIsOn){

        var tab = angular.element("#shops-tab-wrapper-0");
        var piecesWrapper = tab.children(".tabsGeneric");

        var currentChildren = piecesWrapper[0].children;
        var lastChild = currentChildren[currentChildren.length - 1];

        if(!obj){
            var item = $('<h1 style="font-weight: 200;margin-top: 25px; width:100%; text-align: center; color: #666;">En este momento no podemos acceder a los locales. Por favor, intenta más tarde.</h1>');
            piecesWrapper.append(item);         
            item.fadeIn("slow");
            return;
        }

        var id = obj.elem._id;
        var source = "https://s3-sa-east-1.amazonaws.com/iloveit/shop/" + id + ".jpg";

        var elem = obj.elem;

        var _laDes=elem.info.replace(/'/g, "\\'");

        var haver = $rootScope.lat ? "(" + haversine(elem.map.lat, elem.map.lng, $rootScope.lat, $rootScope.lng).str + ")" : "";

        var piece = '<div class="newsPiece" layout="column"> <div hm-tap="detail(\'' + elem.id + '\', \'shop\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' style="height:' + ($(".tabsGeneric").width() - 16) + 'px!important;background-image: url(assets/images/LinkMapa.png)" class="imageWrapper defaultImage"> <img src="' + source + '" id="'+ elem.id + '" onload="this.style.opacity=1;" class="newsElement"></img> </div><div class="newsElementBottomWrapper"> <div layout="row"> <div class="shopsMapIcon"></div><span class="newsElementTitle">'+elem.name+' ' + haver +'</span> </div><div class="newsElementDescription">'+_laDes+'</div> </div></div>';

        if(obj.isLast)
            $rootScope.loading = false;

        //NO NEED FOR MIN IS ON
        //Bottom: lastChild.position().top + lastChild.outerHeight() - tab.scrollTop()
        
        var lastBottom = piecesWrapper.children().length == 0 ? 0 : (piecesWrapper.children().last().position().top + piecesWrapper.children().last().outerHeight());
        if(lastBottom < tab.height() || (lastBottom - tab.scrollTop()) < (tab.height() + 500)){        
            var item = $compile(piece)($scope).fadeOut();
            piecesWrapper.append(item);
            item.fadeIn(1000);

            return false;
        } else { 

            $rootScope.loading = false;
            return true;
        }
    });   

    //Inicializar queues por cada subcategoría
    for(var i = 0; i < $scope.categories.length; i++){
            $scope.queues.push({queueIndex:0, isRunning:false, elements:[], reset: function(){this.queueIndex = 0; this.isRunning = false; this.elements = [];}});
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Configuración de buffering~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    $rootScope.showInfo = function(){
        $scope.isMapUp = !$scope.isMapUp;

        if($scope.isMapUp){
            $(".tickets-content").css('overflow', 'scroll');

            $("#detailedInfo").css('height', $scope.filtersAreDown ? (($(".tickets-content").height() - $("#shopSelectWrapper").height()) + 'px') : ($(".tickets-content").height() + 'px'));

            $("#map").css('height', $("#detailedInfo").css('height'));

            $("#detailedInfo").animate({
                top: $scope.filtersAreDown ? $("#shopSelectWrapper").height() + "px" : "0px"
            }, 700);

            $("#toggleMap").css({ backgroundColor: '#fff' });
            $("#toggleMap").css({ backgroundImage: 'url(assets/images/icon-mapa3_30x30px-gris.png)' }); 
            
        } else {

            $(".tickets-content").animate({
                scrollTop: 0
            }, 700);
            $(".tickets-content").css('overflow', 'hidden'); 

            
            $("#detailedInfo").animate({
                top: $(".tickets-content").css('height')
            }, 700);

            $("#toggleMap").css({ backgroundColor: '#d60036' });
            $("#toggleMap").css({ backgroundImage: 'url(assets/images/icon-mapa3_30x30px-white.png)' }); 
     
        }   
    }

    $rootScope.toggleFilters = function(){
        $scope.filtersAreDown = !$scope.filtersAreDown;

        $("#shopSelectWrapper").animate({
            top: $scope.filtersAreDown ? "0px" : ("-" + $("#shopSelectWrapper").height() + "px")
        }, 1000);


        if($scope.isMapUp){
            if($scope.filtersAreDown){    
                $("#detailedInfo").animate({
                    top: $("#shopSelectWrapper").height() + "px",
                    opacity: 0
                }, 700, function(){
                    $("#detailedInfo").css('height', (($(".tickets-content").height() - $("#shopSelectWrapper").height()) + 'px'));

                    console.log("Current height is " + (($(".tickets-content").height() - $("#shopSelectWrapper").height()) + 'px'));

                    $("#map").css('height', $("#detailedInfo").css('height'));

                    $("#detailedInfo").animate({
                        opacity: 1
                    }, 700);
                });

                $("#toggleFilters").css({ backgroundColor: '#fff' });
                $("#toggleFilters").css({ backgroundImage: 'url(assets/images/icon-zoom_30x30px-gris.png)' });  

            } else {
                $("#detailedInfo").animate({
                    top: "0px",
                    opacity: 0
                }, 700, function(){
                    $("#detailedInfo").css('height', ($(".tickets-content").height() + 'px'));

                    console.log("Current height is: " + ($(".tickets-content").height() + 'px'));

                    $("#map").css('height', $("#detailedInfo").css('height'));

                    $("#detailedInfo").animate({
                        opacity: 1
                    }, 700);
                });

                
                $("#toggleFilters").css({ backgroundColor: '#d60036' });
                $("#toggleFilters").css({ backgroundImage: 'url(assets/images/icon-zoom_30x30px-white.png)' });
            }
        } else {
            if(!$scope.filtersAreDown){    

                $("#toggleFilters").css({ backgroundColor: '#d60036' });
                $("#toggleFilters").css({ backgroundImage: 'url(assets/images/icon-zoom_30x30px-white.png)' }); 

            } else {

                $("#toggleFilters").css({ backgroundColor: '#fff' });
                $("#toggleFilters").css({ backgroundImage: 'url(assets/images/icon-zoom_30x30px-gris.png)' }); 
            }
        }
    }

    $scope.provincias = [];

    function getCombos(){
    
        var mark, shop, localidad, provincia, exists = {p: false, l: false, s: false};

        for(var i = 0; i < $scope.marks.length; i++){

            /*if(!$scope.marks[i].map || !$scope.marks[i].map.administrative_area_level_1 || !$scope.marks[i].map.locality){
                continue();
            }*/

            mark = $scope.marks[i];

            shop = {
                coordenadas: {
                    latitud: mark.map.lat, 
                    longitud: mark.map.lng
                },

                id: mark.id,
                name: mark.name
            };

            localidad = {
                name: mark.map.locality,
                shops: []
            };

            provincia = {
                name: mark.map.administrative_area_level_1,
                localidades: []
            };

            exists = {p: undefined, l: undefined, s: undefined};

            for(var j = 0; j < $scope.provincias.length; j++)
                if($scope.provincias[j].name == provincia.name)
                    exists.p = j;

            if(exists.p != undefined){

                for(var k = 0; k < $scope.provincias[exists.p].localidades.length; k++)
                    if($scope.provincias[exists.p].localidades[k].name == localidad.name)
                        exists.l = k;   

                if(exists.l != undefined){

                    for(var l = 0; l < $scope.provincias[exists.p].localidades[exists.l].shops.length; l++){

                        if($scope.provincias[exists.p].localidades[exists.l].shops[l].name == shop.name){
                            exists.s = l;
                            break;
                        }
                    }

                    if(exists.s == undefined)
                        $scope.provincias[exists.p].localidades[exists.l].shops.push(shop);                             

                } else {

                    for(var l = 0; l < localidad.shops.length; l++){

                        if(localidad.shops[l].name == shop.name){
                            exists.s = l;
                            break;
                        }                                   
                    
                    }

                    if(exists.s == undefined)
                        localidad.shops.push(shop);

                    $scope.provincias[exists.p].localidades.push(localidad);
                }

            } else {

                for(var k = 0; k < provincia.localidades.length; k++)
                    if(provincia.localidades[k].name == localidad.name)
                        exists.l = k;   

                if(exists.l != undefined){

                    for(var l = 0; l < provincia.localidades[exists.l].shops.length; l++){

                        if(provincia.localidades[exists.l].shops[l].name == shop.name){
                            exists.s = l;
                            break;
                        }
                    }

                    if(exists.s == undefined)
                        provincia.localidades[exists.l].shops.push(shop);                               

                } else {

                    for(var l = 0; l < localidad.shops.length; l++){

                        if(localidad.shops[l].name == shop.name){
                            exists.s = l;
                            break;
                        }                                   
                    
                    }

                    if(exists.s == undefined)
                        localidad.shops.push(shop);

                    provincia.localidades.push(localidad);
                }

                $scope.provincias.push(provincia);
            }

        }
        //console.log(JSON.stringify($scope.provincias));
    }

    $scope.indexes = {prov: 0, localidad: 0};


    function sortSelect(id){
        if(id == "provincias"){
            $scope.provincias.sort(function(a,b){
                return a.name > b.name ? 1 : -1;
            })
        } else if(id == "localidades"){
            $scope.provincias[$scope.indexes.prov].localidades.sort(function(a,b){
                return a.name > b.name ? 1 : -1;
            })
        } else {
            $scope.provincias[$scope.indexes.prov].localidades[$scope.indexes.localidad].shops.sort(function(a,b){
                return a.name > b.name ? 1 : -1;
            })
        }
    }

    $scope.inicio = function(){

        for(var x = 0; x < $scope.marks.length; x++){
            
            if($scope.marks[x].map){                

                if($scope.marks[x].map.lat != "undefined" && $scope.marks[x].map.lat != undefined && $scope.marks[x].map.lat != null && $scope.marks[x].map.lat != "null"){
                    var newMarker = 

                    L.marker([$scope.marks[x].map.lat, $scope.marks[x].map.lng], {
                      icon: L.mapbox.marker.icon({
                        'marker-size': 'large',
                        'marker-color': '#d60036'
                      })
                    })

                    .bindPopup('<div><strong>'+ $scope.marks[x].name +'</strong><br>'+$scope.marks[x].info.substring(0, 100)+'...<br><br></div>' + '<button class="trigger" onclick="" id="markerPopupButton-'+ x +'" style="width: 100%">Ver Detalle</button>')
                    .addTo($rootScope.map)

                    .on('click', function(event) {

                        event.target._popup._container.children[1].children[0].children[1].onclick= function(event){
                            //console.log("hace algo");
                            var id = event.target.id;
                            var index = id.split("-")[1];

                            $rootScope.mark = $scope.marks[index];
                            $rootScope.state = $scope.marks[index].name;
                            $rootScope.detail($scope.marks[index]._id, $rootScope.filteredPromosMarks != undefined ? "promo" : "shop");
                        };                  
                    });

                    if($scope.marks.length == 1)
                    newMarker.openPopup();
                }
            }
        }

        if($rootScope.lat && $rootScope.lat != "undefined"){

            $rootScope.map.setView([$rootScope.lat, $rootScope.lng], 10);

            L.marker([$rootScope.lat, $rootScope.lng], {
                icon: L.mapbox.marker.icon({
                    'marker-size': 'large',
                    'marker-color': '#d60036'
                })
            }).addTo($rootScope.map);
        }
        
        $rootScope.loading = false;
    }


    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Uso de buffer y subcategorías~~~~~~~~~~~~~~~~~~~~~~~~~//
    //angular.element(document).ready(setTimeout(function(){

        
        //Sigue en la screen del presente controlador
        if($rootScope.states && $rootScope.states[$rootScope.states.length-1] == "main.shopsTypes"){

            $("#detailedInfo").css("padding", parseInt($("#showInfo").css('height')) + "px 0px 0px 0px");
            $("#detailedInfo").css("z-index", "1000");
            $("#detailedInfo").css("position", "absolute");

            $(".tickets-content").css('overflow', 'hidden'); 

            $("#detailedInfo").css("height", (parseInt($(".tickets-content").css('height')) - parseInt($("#shopSelectWrapper").css('height')) - parseInt($("#showInfo").css('height'))) + "px");

            $(".scrollableMap").css("height", (parseInt($(".tickets-content").css('height')) - parseInt($("#shopSelectWrapper").css('height'))) + "px");

            $("#detailedInfo").css("width", "100%");

            $("#shopSelectWrapper").css("top", $scope.filtersAreDown ? "0px" : ("-" + $("#shopSelectWrapper").height() + "px"));

            $("#detailedInfo").css("top", (parseInt($(".tickets-content").css('height')) - parseInt($("#showInfo").css('height'))) + "px");

            $("#allButton").animate({
                opacity: 1
            }, 300);    

            $rootScope.loading = true;

            //First buffer run
            _startBuffer($scope.buffer, 10); 

            $rootScope.filteredShopsMarks = $scope.filteredElements;

            var isPromos = false;
            $scope.marks = $rootScope.filteredShopsMarks;

            if($rootScope.lat){
                var lat = $rootScope.lat;
                var lng = $rootScope.lng;
            } else {
                var lat = $scope.marks[0].map.lat;
                var lng = $scope.marks[0].map.lng;
            }

            L.mapbox.accessToken = 'pk.eyJ1IjoiZmVkZW11bmkiLCJhIjoieWRPazdSRSJ9.aDK-HSUPTYxgahE8BzOtoQ';

            $rootScope.map = L.mapbox.map('map', 'mapbox.streets', { zoomControl: false }).setView([lat, lng], 10);

            //new L.Control.Zoom({ position: 'bottomright' }).addTo(map);
            var myLayer = L.mapbox.featureLayer().addTo($rootScope.map);

            var geojson = [
                {
                    "type": "FeatureCollection",
                    "features": [],
                    "id":'mapbox.checkin',
                    "properties": {
                        "icon": {
                            "iconUrl": "/assets/images/icon-gastronomia-30x40px_1.png",
                            "iconSize": [30, 40], // size of the icon
                            "iconAnchor": [15, 40], // point of the icon which will correspond to marker's location
                        }
                    }
                }
            ];

            $scope.inicio();
            getCombos();
            sortSelect("provincias");

            $("#shopSelectWrapper").animate({
                top: "-" + $("#shopSelectWrapper").height() + "px"
            }, 1000);

            $rootScope.updateCallback = function(){

                console.log("Buffering for reload. Check out the new filtered elements: ");

                //Buffering
                console.log($scope.buffer.filterElements(true));

                $scope.buffer.bufferElems(undefined);

                $rootScope.filteredShopsMarks = $scope.filteredElements;

                $rootScope.loading = false;

                if($scope.buffer.elements.length == 1)
                    $(".tabsGeneric").css("height", ($("#shops-tab-wrapper-0").height() + 100) + "px");
                else
                    $(".tabsGeneric").css("height", "auto");

            }

            $("#shops-tab-wrapper-0").xpull({
                'callback': function(){
                    if($rootScope._isOnline())
                        _restartBuffer($scope.buffer, $("#shops-tab-wrapper-0"));
                    $rootScope.actualizamosCache();
                }
            }); 

            //Set scroll buffering
            for(var j = 0; j < $scope.categories.length; j++)
            _initScrollBuffering($("#shops-tab-wrapper-" + j), $scope, $rootScope, $compile);

            //Set scroll position saving
            for(var j = 0; j < $scope.categories.length; j++)
            _initScrollAmountWatch($("#shops-tab-wrapper-" + j), $rootScope);  
            
        }


        
   //}, 500));

    /*********************************** MAPBOX CONFIG ********************************/
    $scope.$watch('someVal1', function(newValue, oldValue){
        if(newValue != undefined){
            for(var i = 0; i < $scope.provincias.length; i++){

                if($scope.provincias[i].name == newValue){
                    $scope.indexes.prov = i;
                    $scope.localidades = $scope.provincias[i].localidades;

                    var aproxShop = $scope.localidades[0].shops[0];

                    $rootScope.map.setView([aproxShop.coordenadas.latitud, aproxShop.coordenadas.longitud], 10);

                    break;
                }
            }

            $scope.filters = newValue;
            $rootScope.isfiltering = true;
            $scope.someVal2 = undefined;
            $scope.someVal3 = undefined;

            _restartBuffer($scope.buffer, $("#shops-tab-wrapper-0"), undefined, $compile);
            $rootScope.updateCallback();

            //Ordenar provincias
            sortSelect("localidades");
        }
    });

    $scope.$watch('someVal2', function(newValue, oldValue){
        if(newValue != undefined){
            for(var i = 0; i < $scope.provincias[$scope.indexes.prov].localidades.length; i++){

                if($scope.provincias[$scope.indexes.prov].localidades[i].name == newValue.name){
                    $scope.indexes.localidad = i;
                    $scope.shops = $scope.provincias[$scope.indexes.prov].localidades[i].shops;
                    //console.log("Shops: " + JSON.stringify($scope.shops));
                    break;
                }
            }

            var aproxShop = newValue.shops[0];

            $rootScope.map.setView([aproxShop.coordenadas.latitud, aproxShop.coordenadas.longitud], 13);

            $scope.filters = $scope.someVal1 + " > " + newValue.name;        
            $scope.someVal3 = undefined;

            _restartBuffer($scope.buffer, $("#shops-tab-wrapper-0"), undefined, $compile);
            $rootScope.updateCallback();

            //Ordenar localidades
            sortSelect("locales");
        }
    });

    $scope.$watch('someVal3', function(newValue, oldValue){
        if(newValue != undefined){
            _restartBuffer($scope.buffer, $("#shops-tab-wrapper-0"), undefined, $compile);
            $rootScope.updateCallback();

            $rootScope.toggleFilters();

            var shop = $scope.someVal3;
            map.setView([shop.coordenadas.latitud, shop.coordenadas.longitud], 16);

        }
    });

}]);