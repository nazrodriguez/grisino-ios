//
//  LocationShareModel.h
//  Location
//
//  Created by Rick
//  Copyright (c) 2014 Location. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "Haversine.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@class AppDelegate;

@interface LocationManager : NSObject

@property (nonatomic) CLLocationManager * anotherLocationManager;

@property (nonatomic) CLLocationCoordinate2D myLastLocation;
@property (nonatomic) CLLocationAccuracy myLastLocationAccuracy;

@property (nonatomic) CLLocationCoordinate2D myLocation;
@property (nonatomic) CLLocationAccuracy myLocationAccuracy;

@property (nonatomic) NSMutableDictionary *myLocationDictInPlist;
@property (nonatomic) NSMutableArray *myLocationArrayInPlist;

@property (nonatomic) BOOL afterResume;

//Sqlite test
@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) NSArray *dbArray;
//Sqlite test

//Haversine
@property (nonatomic, strong) Haversine *haversine;
//Haversine

+ (id)sharedManager;

- (void)startMonitoringLocation;
- (void)startSLC;
- (void)restartSLC;
- (void)restartMonitoringLocation;

-(void)NotifyNearPromos;
-(void) updateDB;
-(void) searchForNearbyShop:(NSString *)shop;

-(void) addLogWithAction:(NSString *)action andId:(NSString *)id;
-(NSString *) getLat;
-(NSString *) getLng;

- (void)addResumeLocationToPList;
- (void)addLocationToPList:(BOOL)fromResume;
- (void)addApplicationStatusToPList:(NSString*)applicationStatus;

@end
