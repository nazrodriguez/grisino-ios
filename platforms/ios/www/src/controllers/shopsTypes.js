﻿'use strict';

var ShopsTypes = angular.module('ShopsTypes', []);


ShopsTypes.controller('shopsTypesCtrl',  ['$scope', '$rootScope', '$location', '$route', '$compile', '$timeout', function ($scope, $rootScope, $location, $route, $compile, $timeout) {
    

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Declaración de vars ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    //$scope.categories = $rootScope.categoriasCache.custom_categories.shops;
    $scope.categories = [{
        name: "Todo",
        id: 1,
        subcategories: []
    }];

    window.ga.trackView("Shops List");

    $scope.filtersAreDown = false;
    $("#toggleFilters").css({ backgroundColor: '#d60036' });
    $("#toggleFilters").css({ backgroundImage: 'url(assets/images/icon-zoom_30x30px-white.png)' });
    $scope.mapIsUp = false;
    $("#toggleMap").css({ backgroundColor: '#d60036' });
    $("#toggleMap").css({ backgroundImage: 'url(assets/images/icon-mapa3_30x30px-white.png)' }); 

    //console.log(JSON.stringify($scope.categories));
    //console.log(JSON.stringify($rootScope.categoriasCache.custom_categories));

    //Array de subcategorías actuales
    $scope.filteredCategories = $scope.categories[$rootScope.navIndex.category].subcategories;

    $scope.filteredElements;

    $rootScope.filteredShopsMarks = $scope.filterElements;
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    $rootScope.showInfo = function(){
        $scope.isMapUp = !$scope.isMapUp;

        if($scope.isMapUp){
            $(".tickets-content").css('overflow', 'scroll');

            $("#detailedInfo").css('height', $scope.filtersAreDown ? (($(".tickets-content").height() - $("#shopSelectWrapper").height()) + 'px') : ($(".tickets-content").height() + 'px'));

            $("#map").css('height', $("#detailedInfo").css('height'));

            $("#detailedInfo").animate({
                top: $scope.filtersAreDown ? $("#shopSelectWrapper").height() + "px" : "0px"
            }, 700);

            $("#toggleMap").css({ backgroundColor: '#fff' });
            $("#toggleMap").css({ backgroundImage: 'url(assets/images/icon-mapa3_30x30px-gris.png)' }); 
            
        } else {

            $(".tickets-content").animate({
                scrollTop: 0
            }, 700);
            $(".tickets-content").css('overflow', 'hidden'); 

            
            $("#detailedInfo").animate({
                top: $(".tickets-content").css('height')
            }, 700);

            $("#toggleMap").css({ backgroundColor: '#d60036' });
            $("#toggleMap").css({ backgroundImage: 'url(assets/images/icon-mapa3_30x30px-white.png)' }); 
     
        }   
    }

    $rootScope.toggleFilters = function(){
        $scope.filtersAreDown = !$scope.filtersAreDown;

        $("#shopSelectWrapper").animate({
            top: $scope.filtersAreDown ? "0px" : ("-" + $("#shopSelectWrapper").height() + "px")
        }, 1000);


        if($scope.isMapUp){
            if($scope.filtersAreDown){    
                $("#detailedInfo").animate({
                    top: $("#shopSelectWrapper").height() + "px",
                    opacity: 0
                }, 700, function(){
                    $("#detailedInfo").css('height', (($(".tickets-content").height() - $("#shopSelectWrapper").height()) + 'px'));

                    console.log("Current height is " + (($(".tickets-content").height() - $("#shopSelectWrapper").height()) + 'px'));

                    $("#map").css('height', $("#detailedInfo").css('height'));

                    $("#detailedInfo").animate({
                        opacity: 1
                    }, 700);
                });

                $("#toggleFilters").css({ backgroundColor: '#fff' });
                $("#toggleFilters").css({ backgroundImage: 'url(assets/images/icon-zoom_30x30px-gris.png)' });  

            } else {
                $("#detailedInfo").animate({
                    top: "0px",
                    opacity: 0
                }, 700, function(){
                    $("#detailedInfo").css('height', ($(".tickets-content").height() + 'px'));

                    console.log("Current height is: " + ($(".tickets-content").height() + 'px'));

                    $("#map").css('height', $("#detailedInfo").css('height'));

                    $("#detailedInfo").animate({
                        opacity: 1
                    }, 700);
                });

                
                $("#toggleFilters").css({ backgroundColor: '#d60036' });
                $("#toggleFilters").css({ backgroundImage: 'url(assets/images/icon-zoom_30x30px-white.png)' });
            }
        } else {
            if(!$scope.filtersAreDown){    

                $("#toggleFilters").css({ backgroundColor: '#d60036' });
                $("#toggleFilters").css({ backgroundImage: 'url(assets/images/icon-zoom_30x30px-white.png)' }); 

            } else {

                $("#toggleFilters").css({ backgroundColor: '#fff' });
                $("#toggleFilters").css({ backgroundImage: 'url(assets/images/icon-zoom_30x30px-gris.png)' }); 
            }
        }
    }

    $scope.provincias = [];

    function getCombos(){
    
        var mark, shop, localidad, provincia, exists = {p: false, l: false, s: false};

        for(var i = 0; i < $scope.marks.length; i++){

            /*if(!$scope.marks[i].map || !$scope.marks[i].map.administrative_area_level_1 || !$scope.marks[i].map.locality){
                continue();
            }*/

            mark = $scope.marks[i];

            shop = {
                coordenadas: {
                    latitud: mark.map.lat, 
                    longitud: mark.map.lng
                },

                id: mark.id,
                name: mark.name
            };

            localidad = {
                name: mark.map.locality,
                shops: []
            };

            provincia = {
                name: mark.map.administrative_area_level_1,
                localidades: []
            };

            exists = {p: undefined, l: undefined, s: undefined};

            for(var j = 0; j < $scope.provincias.length; j++)
                if($scope.provincias[j].name == provincia.name)
                    exists.p = j;

            if(exists.p != undefined){

                for(var k = 0; k < $scope.provincias[exists.p].localidades.length; k++)
                    if($scope.provincias[exists.p].localidades[k].name == localidad.name)
                        exists.l = k;   

                if(exists.l != undefined){

                    for(var l = 0; l < $scope.provincias[exists.p].localidades[exists.l].shops.length; l++){

                        if($scope.provincias[exists.p].localidades[exists.l].shops[l].name == shop.name){
                            exists.s = l;
                            break;
                        }
                    }

                    if(exists.s == undefined)
                        $scope.provincias[exists.p].localidades[exists.l].shops.push(shop);                             

                } else {

                    for(var l = 0; l < localidad.shops.length; l++){

                        if(localidad.shops[l].name == shop.name){
                            exists.s = l;
                            break;
                        }                                   
                    
                    }

                    if(exists.s == undefined)
                        localidad.shops.push(shop);

                    $scope.provincias[exists.p].localidades.push(localidad);
                }

            } else {

                for(var k = 0; k < provincia.localidades.length; k++)
                    if(provincia.localidades[k].name == localidad.name)
                        exists.l = k;   

                if(exists.l != undefined){

                    for(var l = 0; l < provincia.localidades[exists.l].shops.length; l++){

                        if(provincia.localidades[exists.l].shops[l].name == shop.name){
                            exists.s = l;
                            break;
                        }
                    }

                    if(exists.s == undefined)
                        provincia.localidades[exists.l].shops.push(shop);                               

                } else {

                    for(var l = 0; l < localidad.shops.length; l++){

                        if(localidad.shops[l].name == shop.name){
                            exists.s = l;
                            break;
                        }                                   
                    
                    }

                    if(exists.s == undefined)
                        localidad.shops.push(shop);

                    provincia.localidades.push(localidad);
                }

                $scope.provincias.push(provincia);
            }
        }
    }

    $scope.indexes = {prov: 0, localidad: 0};

    function sortSelect(id){
        if(id == "provincias"){
            $scope.provincias.sort(function(a,b){
                return a.name > b.name ? 1 : -1;
            })
        } else if(id == "localidades"){
            $scope.provincias[$scope.indexes.prov].localidades.sort(function(a,b){
                return a.name > b.name ? 1 : -1;
            })
        } else {
            $scope.provincias[$scope.indexes.prov].localidades[$scope.indexes.localidad].shops.sort(function(a,b){
                return a.name > b.name ? 1 : -1;
            })
        }
    }

    

    $scope.inicio = function(){

        if($rootScope.lat){
            var lat = $rootScope.lat;
            var lng = $rootScope.lng;
        } else {
            var lat = $scope.marks[0].map.lat;
            var lng = $scope.marks[0].map.lng;
        }

        if($rootScope.map)
            $rootScope.map.remove();

        L.mapbox.accessToken = 'pk.eyJ1IjoiZmVkZW11bmkiLCJhIjoieWRPazdSRSJ9.aDK-HSUPTYxgahE8BzOtoQ';

        $rootScope.map = L.mapbox.map('map', 'mapbox.streets', { zoomControl: false }).setView([lat, lng], 10);

        //new L.Control.Zoom({ position: 'bottomright' }).addTo(map);
        var myLayer = L.mapbox.featureLayer().addTo($rootScope.map);

        var geojson = [
            {
                "type": "FeatureCollection",
                "features": [],
                "id":'mapbox.checkin',
                "properties": {
                    "icon": {
                        "iconUrl": "/assets/images/icon-gastronomia-30x40px_1.png",
                        "iconSize": [30, 40], // size of the icon
                        "iconAnchor": [15, 40], // point of the icon which will correspond to marker's location
                    }
                }
            }
        ];

        for(var x = 0; x < $scope.marks.length; x++){
            
            if($scope.marks[x].map){                

                if($scope.marks[x].map.lat != "undefined" && $scope.marks[x].map.lat != undefined && $scope.marks[x].map.lat != null && $scope.marks[x].map.lat != "null"){
                    var newMarker = 

                    L.marker([$scope.marks[x].map.lat, $scope.marks[x].map.lng], {
                      icon: L.mapbox.marker.icon({
                        'marker-size': 'large',
                        'marker-color': '#000',
                        'marker-symbol': 'shop'
                      })
                    })

                    .bindPopup('<div><strong>'+ $scope.marks[x].name +'</strong><br>'+$scope.marks[x].info.substring(0, 100)+'...<br><br></div>' + '<button class="trigger" onclick="" id="markerPopupButton-'+ x +'" style="width: 100%">Ver Detalle</button>')
                    .addTo($rootScope.map)

                    .on('click', function(event) {

                        event.target._popup._container.children[1].children[0].children[1].onclick= function(event){
                            //console.log("hace algo");
                            var id = event.target.id;
                            var index = id.split("-")[1];
                            console.log(id);

                            $rootScope.mark = $scope.marks[index];
                            $rootScope.state = $scope.marks[index].name;
                            $rootScope.detail($scope.marks[index]._id, $rootScope.filteredPromosMarks != undefined ? "promo" : "shop");
                        };                  
                    });

                    if($scope.marks.length == 1)
                    newMarker.openPopup();
                }
            }
        }

        if($rootScope.lat && $rootScope.lat != "undefined"){

            $rootScope.map.setView([$rootScope.lat, $rootScope.lng], 10);

            L.marker([$rootScope.lat, $rootScope.lng], {
                icon: L.mapbox.marker.icon({
                    'marker-size': 'large',
                    'marker-color': '#d60036'
                })
            }).bindPopup('Mi ubicación').addTo($rootScope.map).openPopup();
        }
        
        $rootScope.loading = false;
    }

    angular.element(document).ready(setTimeout(function(){
 
        //Sigue en la screen del presente controlador
        if($rootScope.states && $rootScope.states[$rootScope.states.length-1] == "main.shopsTypes"){

            $("#detailedInfo").css("padding", parseInt($("#showInfo").css('height')) + "px 0px 0px 0px");
            $("#detailedInfo").css("z-index", "1000");
            $("#detailedInfo").css("position", "absolute");

            $(".tickets-content").css('overflow', 'hidden'); 

            $("#detailedInfo").css("height", (parseInt($(".tickets-content").css('height')) - parseInt($("#shopSelectWrapper").css('height')) - parseInt($("#showInfo").css('height'))) + "px");

            $(".scrollableMap").css("height", (parseInt($(".tickets-content").css('height')) - parseInt($("#shopSelectWrapper").css('height'))) + "px");

            $("#detailedInfo").css("width", "100%");

            $("#shopSelectWrapper").css("top", $scope.filtersAreDown ? "0px" : ("-" + $("#shopSelectWrapper").height() + "px"));

            $("#detailedInfo").css("top", (parseInt($(".tickets-content").css('height')) - parseInt($("#showInfo").css('height'))) + "px");

            $("#allButton").animate({
                opacity: 1
            }, 300);    

            setTimeout(function(){
                $("#shopSelectWrapper").removeClass("noshow");
                $("#detailedInfo").removeClass("noshow");   
            },5000);

            /*l.start();

            $rootScope.updateCallback = function(){
                $rootScope.filteredShopsMarks = l.getElements();
                $scope.marks = $rootScope.filteredShopsMarks;

                /*TODO: reset map*
                //$scope.inicio();

                l.restart(function(){
                    $rootScope.loading = false;
                    $(".content.shops").xpull({
                        'pullThreshold': 150,
                        'callback': function() {
                            $rootScope.actualizamosCache();
                        }
                    });
                });
            }*/
            
            /* List set up */
                var l = new ISCROLL("#listWrapper",function(elem){

                    var piece;
                    var id = elem._id;
                    var source = elem.images && elem.images.length ? elem.images[0].url : "https://s3-sa-east-1.amazonaws.com/iloveit/shop/" + id + ".jpg";

                    var _laDes = elem.info.replace(/'/g, "\\'");

                    piece = '<li hm-tap="_detail(\'' + elem.id + '\', \'shop\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="newsPiece" layout="column"><div style="height:' + ($(document).width() - 16) + 'px!important;background-image: url(\'assets/images/LinkMapa.png\')" class="imageWrapper defaultImage"><img src="' + source + '" id="' + elem._id + '" onload="this.style.opacity=1;" class="newsElement"></img></div><div class="newsElementBottomWrapper"> <span class="newsElementTitle">' + elem.name + '</span><div class="newsElementDescription">' + _laDes + '</div></div></li>'

                    return piece;
                },function(){
                    var filtered = [];

                    filtered = $rootScope.venuesCache

                    //Filtering
                    .filter(function(mark){                 
                        return ($scope.someVal1 ? mark.map.administrative_area_level_1 == $scope.someVal1 : true) && ($scope.someVal2 ? mark.map.locality == $scope.someVal2.name : true) && ($scope.someVal3 ? mark.name == $scope.someVal3.name : true);
                    });

                    if($rootScope.lat){
                        var coords = {lat: $rootScope.lat, lng: $rootScope.lng};
                        var aux = filtered;

                        filtered.sort(function(a,b){
                            return haversine(coords.lat, coords.lng, a.map.lat, a.map.lng).number - haversine(coords.lat, coords.lng, b.map.lat, b.map.lng).number;
                        });
                    }

                    return filtered;
                }, function(c){
                    c();
                    $rootScope.updateCallback();
                    $rootScope.actualizamosCache();
                }, function(s){
                    return $compile(s)($scope);
                });
            /* List set up */

            /* Map setup */
                $rootScope.filteredShopsMarks = l.getElements();
                $scope.marks = $rootScope.filteredShopsMarks;

                $scope.inicio();
                getCombos();
                sortSelect("provincias");
            /* Map setup */  

            $scope._detail = function(id, type) {
                if (!l.isScrolling())
                    $rootScope.detail(id, type);
            }

            $rootScope.updateCallback = function(){
                l.reload_content();
                $rootScope.filteredShopsMarks = l.getElements();
                $scope.marks = $rootScope.filteredShopsMarks;
            };
        }        
    }, 500));

    /*********************************** MAPBOX CONFIG ********************************/
     /*Shops-filters combos listeners*/
        $scope.$watch('someVal1', function(newValue, oldValue){
            if(newValue != undefined){
                for(var i = 0; i < $scope.provincias.length; i++){

                    if($scope.provincias[i].name == newValue){
                        $scope.indexes.prov = i;
                        $scope.localidades = $scope.provincias[i].localidades;

                        var aproxShop = $scope.localidades[0].shops[0];

                        $rootScope.map.setView([aproxShop.coordenadas.latitud, aproxShop.coordenadas.longitud], 10);

                        break;
                    }
                }

                $scope.filters = newValue;
                $rootScope.isfiltering = true;
                $scope.someVal2 = undefined;
                $scope.someVal3 = undefined;

                $rootScope.updateCallback();

                //Ordenar provincias
                sortSelect("localidades");
            }
        });
        $scope.$watch('someVal2', function(newValue, oldValue){
            if(newValue != undefined){
                for(var i = 0; i < $scope.provincias[$scope.indexes.prov].localidades.length; i++){

                    if($scope.provincias[$scope.indexes.prov].localidades[i].name == newValue.name){
                        $scope.indexes.localidad = i;
                        $scope.shops = $scope.provincias[$scope.indexes.prov].localidades[i].shops;
                        break;
                    }
                }

                var aproxShop = newValue.shops[0];

                $rootScope.map.setView([aproxShop.coordenadas.latitud, aproxShop.coordenadas.longitud], 13);

                $scope.filters = $scope.someVal1 + " > " + newValue.name;        
                $scope.someVal3 = undefined;

                $rootScope.updateCallback();

                //Ordenar localidades
                sortSelect("locales");
            }
        });
        $scope.$watch('someVal3', function(newValue, oldValue){
            if(newValue != undefined){
                $rootScope.updateCallback();

                $rootScope.toggleFilters();

                var shop = $scope.someVal3;
                $rootScope.map.setView([shop.coordenadas.latitud, shop.coordenadas.longitud], 16);

            }
        });
    /*Shops-filters combos listeners*/

}]);