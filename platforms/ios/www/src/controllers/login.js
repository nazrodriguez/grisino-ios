'use strict';

var Login = angular.module('Login', []);


Login.controller('loginCtrl', ['$scope', '$location', '$rootScope', '$cordovaSQLite', '$http', '$mdToast', '$mdDialog', function($scope, $location, $rootScope, $cordovaSQLite, $http, $mdToast, $mdDialog) {

    /*********************************** VARS DECLARATION ********************************/

    $rootScope.loading = false;

    /******************************** LOGIN  AUTENTICATION ********************************/

    $scope.forgotPassword = function(ev) {
        var m = $scope.email;
        setTimeout(function() {
            $mdDialog.show({
                locals: {
                    mail: m
                },
                controller: EmailController,
                controllerAs: 'ctrl',
                template: '<md-dialog style="padding: 20px;"><md-button class="md-icon-button" style="background-image: url(assets/images/cerrar2.png)" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'></md-button><h1 style="font-size: 18px;">Ingresá tu mail para recuperar la contraseña:</h1><div style="overflow-y: scroll" class=""><md-input-container> <label>Email</label><input value="' + $scope.email + '" required type="email" name="email" ng-model="email" minlength="5" maxlength="100" ng-pattern="/^.+@.+\..+$/" /><div ng-messages="projectForm.email.$error" role="alert"><div ng-message-exp="[\'required\', \'minlength\', \'maxlength\', \'pattern\']">Tu email debe ser una dirección válida y ser compuesta por entre 5 y 100 caracteres.</div></div></md-input-container></div><md-button style="width: 100%!important; height: 52px; background-color: black!important; margin: 14px 0px 0px 0px; padding: 0px" ng-click="accept()" class="md-primary md-raised loginBtn"> Aceptar </md-button></md-dialog>',
                clickOutsideToClose: true
            }).then(function(email) {
                if(!email || email == ""){
                    $mdToast.show(
                        $mdToast.simple()
                        .content('Error en el ingreso')
                        .position('top')
                        .hideDelay(3000)
                    );
                    return;
                }

                $rootScope.loading = true;

                var c = {
                    email: email,
                    brand: BRAND
                }

                var res = $http.post('http://' + SERVER + '.reachout.global/forgot', JSON.stringify(c));

                res.success(function(data, status, headers, config) {
                    $rootScope.loading = false;
                    console.log(data);
                    var confirm = $mdDialog.confirm()
                        .title('Reestablecimiento de contraseña')
                        .content(data.success ? 'Te enviamos un link a tu casilla de mail para reestablecer tu contraseña.' : 'El mail ingresado no existe. Intentá nuevamente.')
                        .ariaLabel('Lucky day')
                        .ok('Ok')
                        .targetEvent();

                    $mdDialog.show(confirm).then(function() {
                        $scope.email = email;
                    });
                });

                res.error(function(data, status, headers, config) {
                    console.log(JSON.stringify(data));
                });
            }, function() {
                console.log("Say something");
            });
        }, 500);
    }

    $scope.login = function(){
        if($scope.email.indexOf("switchdev") != -1){
            $mdToast.show(
                $mdToast.simple()
                .content('Se cambió a DEV')
                .position('top')
                .hideDelay(3000)
            );
            SERVER = "dev";
            $rootScope.devInfo = SERVER + AppVersion.version + "(" + AppVersion.build + ")";
        } else if($scope.email.indexOf("switchstg") != -1){
            $mdToast.show(
                $mdToast.simple()
                .content('Se cambió a STAGING')
                .position('top')
                .hideDelay(3000)
            );
            SERVER = "staging";
            $rootScope.devInfo = SERVER + AppVersion.version + "(" + AppVersion.build + ")";
        } else if($scope.email.indexOf("switchprod") != -1){
            $mdToast.show(
                $mdToast.simple()
                .content('Se cambió a PROD')
                .position('top')
                .hideDelay(3000)
            );
            SERVER = "prod";
            $rootScope.devInfo = SERVER + AppVersion.version + "(" + AppVersion.build + ")";
        } else {
            $rootScope.login($scope.email, $scope.password);
            return;
        }
        Playtomic.initialize("nkfvt7gpgdkakyb78gr9", "gd341ui4r67d51srfwvs", "http://" + SERVER + ".reachout.global");
    }

    $scope.backToMainPanel = function() {
        //console.log('back');
        $location.path("/mainPanel");
    }

}]);


function EmailController($scope, $mdDialog, mail) {
    $scope.email = mail;

    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.accept = function(ev) {
        $mdDialog.hide($scope.email);
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };
}
