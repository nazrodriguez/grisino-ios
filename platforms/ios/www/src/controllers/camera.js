'use strict';

var Camera = angular.module('Camera', []);


Camera.controller('cameraCtrl', ['$scope', '$location', '$rootScope', '$cordovaSQLite', '$http', '$mdToast', '$mdDialog', '$q', '$mdBottomSheet', '$cordovaSocialSharing', function($scope, $location, $rootScope, $cordovaSQLite, $http, $mdToast, $mdDialog, $q, $mdBottomSheet, $cordovaSocialSharing) {

    var MAX_STICKER_SCALE = 5;
    var MAX_TEXT_SCALE = 5;
    var MIN_TEXT_SCALE = 2;
    var TEXT_GRAB_AREA_ADDITION = 40;
    var STARTING_FONT_SIZE = 35;
    var finalResult;
    var inSimulator = false,
        canRotate = true,
        canScale = true,
        clearAfterwards = false,
        photo,
        division = 40, 
        gridDebug = false,
        saveDebug = false,
        useAproxEquation = true;
    var position = 2;
    var photoRes = undefined;

    /* INIT */
        $scope.showStickers = true;
        $scope.finalOptions = false;
        $scope.snapping = false;
        $scope.bounceText = false;
        $scope.stickers;
        $scope.text;

        $scope.CSDK = new CameraSDK();
        if (inSimulator) {
            $rootScope.showCamera = true;
        } else {
            $scope.CSDK.openCamera();
        }
        $scope.CSDK.initStickers();
        $scope.CSDK.initText();

        $mdToast.show(
            $mdToast.simple()
            .content("¡Tomá tu foto para ponerle los stickers más divertidos!")
            .position('top')
            .hideDelay(6000)
        );
    /* INIT */

    function CameraSDK() {
        /* Declare */
            var canvasses = {
                camera: {
                    ref: document.getElementById('camera'),
                    ctx: document.getElementById('camera').getContext('2d')
                },
                overlay: {
                    ref: document.getElementById("overlay"),
                    ctx: document.getElementById("overlay").getContext('2d', {})
                },
                result: {
                    ref: document.getElementById("result"),
                    ctx: document.getElementById("result").getContext('2d')
                },
                text: {
                    ref: document.getElementById("text"),
                    ctx: document.getElementById("text").getContext('2d')  
                }
            }
        /* Declare */

        /* Init */
            var jobj = $(".cameraModal.stickers");
            canvasses.camera.ref.width = jobj.width();
            canvasses.camera.ref.height = jobj.height();
            canvasses.result.ref.width = jobj.width();
            canvasses.result.ref.height = jobj.height();
            canvasses.overlay.ref.width = jobj.width();
            canvasses.overlay.ref.height = jobj.height();
            canvasses.text.ref.width = jobj.width();
            canvasses.text.ref.height = jobj.height();

            canvasses.camera.ref.style.width = jobj.width();
            canvasses.camera.ref.style.height = jobj.height();
            canvasses.result.ref.style.width = jobj.width();
            canvasses.result.ref.style.height = jobj.height();
            canvasses.overlay.ref.style.width = jobj.width();
            canvasses.overlay.ref.style.height = jobj.height();
            canvasses.text.ref.style.width = jobj.width();
            canvasses.text.ref.style.height = jobj.height();
        /* Init */

        /* Public methods */
            this.openCamera = function() {
                CameraPreview.startCamera({x: 0, y: 20, width:canvasses.camera.ref.width, height: canvasses.camera.ref.height, previewDrag: false, toBack: true});

                $rootScope.showCamera = true;
            }

            this.closeCamera = function() {
                $rootScope.showCamera = false;
                $scope.finalOptions = false;
                $location.path("/main/home");
            }

            this.flipCamera = function(){
                CameraPreview.switchCamera();
                //
            }

            this.bounceTextContainer = function(){
                $scope.bounceText = true;
            }

            this.toggle = function(index) {
                console.log(index);
                $scope.stickers[index].visible = !$scope.stickers[index].visible;
                $mdBottomSheet.hide();
            }

            this.share = function(type){

                var image = toDataURL(finalResult);
                var message = "Ya saqué mi foto con Grisino! Hacelo vos también en www.grisino.com.ar"
                
                if(!type){
                    $cordovaSocialSharing.share(message, "Título", image, 'www.grisino.com').then(function(result) {
                      
                        $scope.showLoader = false;
                        if(clearAfterwards){
                            canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                            addPhotoToCanvas();
                        }
                    }, function(err) {
                      
                        $scope.showLoader = false;
                        if(clearAfterwards){
                            canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                            addPhotoToCanvas();
                        }
                    });
                } else {
                    switch(type){
                        case "facebook":
                            $cordovaSocialSharing. shareViaFacebook(message, image).then(function(result) {
                      
                                $scope.showLoader = false;
                                if(clearAfterwards){
                                    canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                                    addPhotoToCanvas();
                                }
                            }, function(err) {
                              
                                $scope.showLoader = false;
                                if(clearAfterwards){
                                    canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                                    addPhotoToCanvas();
                                }
                            });
                            break;
                        case "messenger":
                            break;
                        case "instagram":
                            /*$cordovaSocialSharing. shareViaInstagram(message, image).then(function(result) {
                      
                                $scope.showLoader = false;
                                if(clearAfterwards){
                                    canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                                    addPhotoToCanvas();
                                }
                            }, function(err) {
                              
                                $scope.showLoader = false;
                                if(clearAfterwards){
                                    canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                                    addPhotoToCanvas();
                                }
                            });*/
                            var confirm = $mdDialog.confirm()
                                .title('Aviso')
                                .content('Para compartir por Instagram, seleccioná la opción "Copiar a Instagram".')
                                .ariaLabel('Lucky day')
                                .ok('¡Okay!')
                                .targetEvent();

                            $mdDialog.show(confirm).then(function() {
                                Instagram.share(getBase64Image(finalResult), message, function(err) {
                                    if (err) {
                                        $mdToast.show(
                                            $mdToast.simple()
                                            .content('No se pudo compartir en Instagram')
                                            .position('top')
                                            .hideDelay(5000)
                                        );
                                        $scope.showLoader = false;
                                        if(clearAfterwards){
                                            canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                                            addPhotoToCanvas();
                                        }
                                    } else {
                                        $scope.showLoader = false;
                                        if(clearAfterwards){
                                            canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                                            addPhotoToCanvas();
                                        }
                                    }
                                });
                            }, function() {});                            
                            break;
                        case "whatsapp":
                            var confirm = $mdDialog.confirm()
                                .title('Aviso')
                                .content('Para compartir por Whatsapp, seleccioná la opción "Copiar a Whatsapp".')
                                .ariaLabel('Lucky day')
                                .ok('¡Okay!')
                                .targetEvent();

                            $mdDialog.show(confirm).then(function() {
                                setTimeout(function(){
                                    $cordovaSocialSharing.shareViaWhatsApp(null, image , null, function() {
                                            scope.showLoader = false;
                                            if(clearAfterwards){
                                                canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                                                addPhotoToCanvas();
                                            }
                                        }, function(errormsg){
                                            scope.showLoader = false;
                                            if(clearAfterwards){
                                                canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                                                addPhotoToCanvas();
                                            }
                                        });
                                }, 0);
                            });
                            break;
                    }
                }
            }

            this.toggleText = function(){
                if(!$scope.editing_text){
                    $scope.editing_text = true;
                    
                    setTimeout(function(){
                        $(".text-adder").find("input").focus();

                        var swiper = new Swiper('.swiper-container', {
                            spaceBetween: 10,
                            centeredSlides: true,
                            slidesPerView: 'auto'
                        });

                        var mc = new Hammer(document.getElementById('textInput'));
                        mc.on("tap", function(ev) {
                            ev.target.classList.add("zoomBounceMe");
                        });

                    }, 100);
                } else {
                    $scope.editing_text = false;

                    var text = $('#textDummy')[0];

                    text.style.transform = 'translate(' + ($scope.text.x - (parseInt(text.style.width)/2)) + 'px, ' + ($scope.text.y - (parseInt(text.style.height)/2)) + 'px) rotate(' + $scope.text.angle + 'deg) scale(' + $scope.text.scale + ')';
                    drawText();
                }
            }        

            this.toggleStickerOptions = function(force){
                if(force != undefined)
                    $scope.hideStickerOptions = force;
                else
                    $scope.hideStickerOptions = $scope.hideStickerOptions ? false : true; 

                if($scope.hideStickerOptions)
                $mdDialog.show({
                    template: '<md-dialog class="md-grid" layout="column">' +
                        '<md-button style="z-index: 1000000; position: absolute; top: 5px; right: 5px;" ng-click="hide()" class="unraised md-fab" aria-label="close button">' + 
                            '<md-icon style="width: 35px; height: 35px;" md-svg-src="assets/svg/stickers/close.svg"></md-icon>' + 
                        '</md-button>' +
                        '<div class="swiper-stickers-container swiper-container-horizontal">' + 
                            '<div style="z-index: 10000;" class="swiper-wrapper">' + 
                                '<div ng-repeat="list in lists" style="-webkit-overflow-scrolling: touch; width: 100%;height: calc(100% - 150px); margin-top: 70px !important;" class="swiper-slide" ng-cloak>' +
                                    '<h3 style="margin-top: -50px; text-align: center; width: 100%; color: white;">{{list.name}}</h3>'+
                                    '<div style="height: 100%; width: 100%; overflow: scroll;">'+
                                        '<div ng-if="sticker" ng-click="CSDK.toggle(sticker.index)" ng-repeat="sticker in list.stickers" class="sticker-options">' +
                                            '<md-button aria-label="sticker-button" ng-class="{bordered: sticker.visible}" class="md-grid-item-content" style="background-image: url(\'{{sticker.path}}\');">' +
                                            '</md-button>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' + 
                            '<div style="top: calc(100% - 50px); bottom: 0px; height: 10px;" class="swiper-pagination"></div>' +
                        '</div>'+

                        '</md-dialog>',
                    controller: function CardDialogController($scope, $mdDialog, stickers, categories, CSDK) {
                        
                        $scope.CSDK = CSDK;
                        //var itemsPerList = 12;
                        var listAmount = categories.length;//Math.ceil(stickers.length / itemsPerList);
                        $scope.lists = [];

                        var list;
                        var j = 0, step = 0;
                        for(var i = 0; i < listAmount; i++){
                            list = {
                                stickers: [],
                                name: categories[i].title
                            };

                            //console.log(itemsPerList * i);
                            //for(var j = itemsPerList * i; j < ((itemsPerList * i) + itemsPerList) && j < stickers.length; j++){
                            console.log(categories[i]);
                            for(j = step; j < step + categories[i].amount; j++){
                                stickers[j].index = j;
                                list.stickers.push(stickers[j]);
                                console.log("Adding " + j + " sticker to list " + i);
                            }
                            step = j;
                            $scope.lists.push(list);
                        }

                        console.log($scope.lists);

                        setTimeout(function(){
                            var swiper = new Swiper('.swiper-stickers-container', {
                                spaceBetween: 10,
                                pagination: '.swiper-pagination',
                                centeredSlides: true,
                                slidesPerView: '1'
                            });
                        },100);

                        $scope.accept = function(ev) {
                            $mdDialog.hide();
                        };

                        $scope.hide = function() {
                            $mdDialog.hide();
                        };

                        $scope.cancel = function() {
                            $mdDialog.cancel();
                        };
                    },
                    controllerAs: 'ctrl',
                    locals: {
                        stickers: $scope.stickers,
                        categories: $scope.categories,
                        CSDK: $scope.CSDK
                    },
                    clickOutsideToClose: true
                });
            }

            this.snap = function() {
                $scope.snapping = true;

                if (inSimulator) {

                    canvasses.result.ctx.fillStyle = "green";
                    canvasses.result.ctx.fillRect(0, 0, canvasses.result.ref.width, canvasses.result.ref.height);

                    canvasses.overlay.ctx.fillStyle = "red";
                    canvasses.overlay.ctx.fillRect(0, 0, canvasses.overlay.ref.width, canvasses.overlay.ref.height);
                    $scope.showPreview = true;

                    $scope.snapping = false;
                } else {
                    
                    if(gridDebug){
                        for(var i = 0; i < (canvasses.overlay.ref.width / division); i++){
                            canvasses.overlay.ctx.beginPath();
                            canvasses.overlay.ctx.strokeStyle = "blue";
                            canvasses.overlay.ctx.lineWidth = 2;
                            canvasses.overlay.ctx.moveTo(i * division, 0);
                            canvasses.overlay.ctx.lineTo(i * division, canvasses.overlay.ref.height);
                            canvasses.overlay.ctx.stroke();  
                        }

                        for(var j = 0; j < (canvasses.overlay.ref.height / division); j++){
                            canvasses.overlay.ctx.beginPath();
                            canvasses.overlay.ctx.strokeStyle = "blue";
                            canvasses.overlay.ctx.lineWidth = 2;
                            canvasses.overlay.ctx.moveTo(0, j * division);
                            canvasses.overlay.ctx.lineTo(canvasses.overlay.ref.width, j * division);
                            canvasses.overlay.ctx.stroke();  
                        }
                    }

                    CameraPreview.takePicture(function(data) {
                        photo = new Image();

                        photo.onload = saveDebug ? function(){$scope.showPreview = true;} : addPhotoToCanvas;
                        photo.src = 'data:image/jpeg;base64,' + data;

                        $scope.snapping = false;
                    });
                }
            }

            this.closePreview = function(){
                canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                $scope.showPreview = false;
                $scope.hideStickerOptions = true;
                $scope.finalOptions = false;
                initStickers();
                initText();
            }

            this.continueEditing = function(){
                canvasses.overlay.ctx.clearRect(0,0,canvasses.overlay.ref.width, canvasses.overlay.ref.height);
                canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                addPhotoToCanvas();
                $scope.finalOptions = false;
            }

            this.savePicture = function() {
                fillResult(function() {
                    CameraRoll.saveToCameraRoll(canvasses.result.ref.toDataURL(), function(success) {
                        if (success)
                            console.log("A " + success);

                        $scope.showLoader = false;
                        if(clearAfterwards){
                            canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                            addPhotoToCanvas();
                        }

                        $scope.finalOptions = true;
                    }, function(err) {
                        console.log("B " + err);

                        $scope.showLoader = false;
                        if(clearAfterwards){
                            canvasses.result.ctx.clearRect(0,0,canvasses.result.ref.width, canvasses.result.ref.height);
                            addPhotoToCanvas();
                        }

                        $scope.finalOptions = true;
                    });
                });
            }

            this.select_color = function(color){
                $scope.text.color = color;
            }
        /* Public methods */ 

        /* Private methods */
            function adjustCanvasses(jobj){
                var jold = $(".cameraModal.stickers"), diff, interactContainer = $(".resize-container")[0];
                console.log(jobj);

                diff = canvasses.camera.ref.width - jobj.width;
                canvasses.camera.ref.width = jobj.width;
                canvasses.camera.ref.style.left = diff / 2;

                diff = canvasses.camera.ref.height - jobj.height;
                canvasses.camera.ref.height = jobj.height;
                canvasses.camera.ref.style.top = diff / 2;


                diff = canvasses.overlay.ref.width - jobj.width;
                canvasses.overlay.ref.width = jobj.width;
                canvasses.overlay.ref.style.left = diff / 2;

                diff = canvasses.overlay.ref.height - jobj.height;
                canvasses.overlay.ref.height = jobj.height;
                canvasses.overlay.ref.style.top = diff / 2;


                diff = canvasses.result.ref.width - jobj.width;
                canvasses.result.ref.width = jobj.width;
                canvasses.result.ref.style.left = diff / 2;

                diff = canvasses.result.ref.height - jobj.height;
                canvasses.result.ref.height = jobj.height;
                canvasses.result.ref.style.top = diff / 2;


                diff = canvasses.text.ref.width - jobj.width;
                canvasses.text.ref.width = jobj.width;
                canvasses.text.ref.style.left = diff / 2;

                diff = canvasses.text.ref.height - jobj.height;
                canvasses.text.ref.height = jobj.height;
                canvasses.text.ref.style.top = diff / 2;


                diff = interactContainer.width - jobj.width;
                interactContainer.width = jobj.width;
                interactContainer.style.left = diff / 2;

                diff = interactContainer.height - jobj.height;
                interactContainer.height = jobj.height;
                interactContainer.style.top = diff / 2;
            }

            function getBase64Image(img) {
                var canvas = document.createElement("canvas");
                canvas.width = img.naturalWidth;
                canvas.height = img.naturalHeight;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);
                var dataURL = canvas.toDataURL("image/png");
                //return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
                return dataURL;
            }

            function fillResult(callback){
                /*Init*/
                    $scope.showLoader = true;
                    if(gridDebug)
                        canvasses.result.ref.style.opacity = "0";
                /*Init*/

                canvasses.result.ctx.rect(0, 0, canvasses.result.ref.width, canvasses.result.ref.height);
                canvasses.result.ctx.fillStyle = "#d60036";
                canvasses.result.ctx.fill();

                /*Place all stickers in overlay canvas, erasing it FIRST*/
                    update(function(){
                        /*With all said and done, include overlay snapshot into result canvas*/
                            function stickerPromise(){
                                let deferred = $q.defer();

                                var overlay = new Image();
                                overlay.onload = function() {

                                    var ratio,
                                        width = canvasses.result.ref.width,
                                        height = canvasses.result.ref.height;

                                    if(overlay.width != width){

                                        if (overlay.width < width){
                                            ratio = width / overlay.width;
                                        } else {
                                            ratio = width * overlay.width;
                                        }

                                    } else if(overlay.height != height){

                                        if (overlay.height < height){
                                            ratio = height / overlay.height;
                                        } else {
                                            ratio = height * overlay.height
                                        }

                                    } else {
                                        ratio = 1;
                                    }

                                    canvasses.result.ctx.drawImage(overlay, 0, 0, canvasses.result.ref.width, canvasses.result.ref.height);
                                    canvasses.overlay.ctx.clearRect(0, 0, canvasses.overlay.ref.width, canvasses.overlay.ref.height);
                                    
                                    console.log("FINISHED STICKERS TO RESULT");
                                    deferred.resolve();
                                }
                                overlay.src = canvasses.overlay.ref.toDataURL();

                                return deferred.promise;
                            }
                        /*With all said and done, include overlay snapshot into result canvas*/

                        /*Afterwards, include text*/
                            function textPromise() {
                                let deferred = $q.defer();

                                var text = new Image();
                                text.onload = function() {

                                    if($scope.text.value != "Ingresa tu texto aquí..."){
                                        var ratio,
                                            width = canvasses.result.ref.width,
                                            height = canvasses.result.ref.height;

                                        if(text.width != width){

                                            if (text.width < width){
                                                ratio = width / text.width;
                                            } else {
                                                ratio = width * text.width;
                                            }

                                        } else if(text.height != height){

                                            if (text.height < height){
                                                ratio = height / text.height;
                                            } else {
                                                ratio = height * text.height
                                            }

                                        } else {
                                            ratio = 1;
                                        }

                                        canvasses.result.ctx.drawImage(text, 0, 0, canvasses.result.ref.width, canvasses.result.ref.height);
                                        canvasses.text.ctx.clearRect(0, 0, canvasses.text.ref.width, canvasses.text.ref.height);
                                    }
                                    console.log("FINISHED TEXT TO RESULT");
                                    deferred.resolve();
                                }
                                text.src = canvasses.text.ref.toDataURL();

                                return deferred.promise;
                            }
                        /*Afterwards, include text*/

                        $q.all([stickerPromise(), textPromise()]).then((values) => {
                            /* Photo was already loaded, loaded text onto result so save canvas result as image */
                                finalResult = new Image();
                                finalResult.onload = callback;
                                finalResult.src = canvasses.result.ref.toDataURL();
                            /* Photo was already loaded, loaded text onto result so save canvas result as image */
                            console.log("FINISHED UPDATING RESULT");
                        });
                    });
                /*Place all stickers in overlay canvas, erasing it FIRST*/
            }

            function addPhotoToCanvas() {

                var ratio,
                    width = canvasses.result.ref.width,
                    height = canvasses.result.ref.height;

                if (photo.width > width)
                    ratio = width / photo.width;
                
                else if (photo.height > height) 
                    ratio = height / photo.height;
                
                else 
                    ratio = 1;

                console.log("Photo", photo.width, photo.height);

                var diff = {
                    height: canvasses.result.ref.height - (photo.height * ratio),
                    width: canvasses.result.ref.width - (photo.width * ratio)
                };

                photoRes = {
                    width: photo.width,
                    height: photo.height
                };

                canvasses.result.ctx.drawImage(photo, diff.width/2, diff.height/2, photo.width * ratio, photo.height * ratio);

                $scope.showPreview = true;
            }

            function update(callback){

                //Clear canvas
                canvasses.overlay.ctx.clearRect(0,0,canvasses.overlay.ref.width, canvasses.overlay.ref.height);

                //Re add photo if existant
                if(photo && !saveDebug)
                    addPhotoToCanvas();

                /*Re add stickers*/
                var sticker, offset, jobj, height, width, x, y, diff;
                for (var i = 0; i < $scope.stickers.length; i++) {
                    sticker = $scope.stickers[i];
                    jobj = $("#resize-drag-" + i),
                    offset = jobj.offset();

                    //diff = (sticker.scale > 1 ? 4.188761589 * Math.pow(Math.E, 1.055958349 * sticker.scale) : 0);
                    diff = (sticker.scale - 1) * 40;

                    height = jobj.height() * sticker.scale,
                    width = jobj.width() * sticker.scale,
                    x = sticker.x - (useAproxEquation ? diff : 0),
                    y = sticker.y - (useAproxEquation ? diff : 0);

                    if(!sticker.visible)
                        continue;

                    if(canRotate){
                        canvasses.overlay.ctx.save();
                        canvasses.overlay.ctx.translate(x + width/2, y + height/2);
                        canvasses.overlay.ctx.rotate(degToRad(sticker.angle));
                    }

                    //canvasses.overlay.ctx.drawImage(jobj[0], x, y, width, height);
                    canvasses.overlay.ctx.drawImage(jobj[0], -width/2, -height/2, width, height);

                    if(gridDebug){
                        canvasses.overlay.ctx.rect(x, y, division, division);

                        canvasses.overlay.ctx.fillStyle = "red";
                        canvasses.overlay.ctx.strokeStyle = "black";
                        canvasses.overlay.ctx.lineWidth = 1;
                        canvasses.overlay.ctx.fill();
                        canvasses.overlay.ctx.stroke();
                    }

                    if(canRotate){
                        canvasses.overlay.ctx.restore();
                        canvasses.overlay.ctx.translate(0, 0);
                    }
                }
                /*Re add stickers*/

                if(callback)
                    callback();
            }   

            function degToRad(deg){

                return deg * (Math.PI / 180);
            }

            function toDataURL(image) {
                var canvas = document.createElement('CANVAS');
                canvas.height = image.naturalHeight;
                canvas.width = image.naturalWidth;
                canvas.getContext('2d').drawImage(image, 0, 0);
                return canvas.toDataURL("image/png");
            }

            function onInteractUpdateWithSticker(event){
                var i = event.target.id.split("-")[2];
                var image = document.getElementById('resize-drag-' + i);

                $scope.stickers[i].x = ($scope.stickers[i].x || 0) + event.dx,
                $scope.stickers[i].y = ($scope.stickers[i].y || 0) + event.dy;

                if(event.da && canRotate)
                    $scope.stickers[i].angle = $scope.stickers[i].angle + event.da;
                if(event.ds && canScale && ($scope.stickers[i].scale * (1 + event.ds)) < MAX_STICKER_SCALE)
                    $scope.stickers[i].scale = $scope.stickers[i].scale * (1 + event.ds); 

                image.style.transform = 'translate(' + $scope.stickers[i].x + 'px, ' + $scope.stickers[i].y + 'px) rotate(' + $scope.stickers[i].angle + 'deg) scale(' + $scope.stickers[i].scale + ')';

                if(saveDebug)
                    $scope.CSDK.update();
            }
            
            function onInteractUpdateWithText(event){
                var text = $("#textDummy")[0];

                $scope.text.x = ($scope.text.x || 0) + event.dx,
                $scope.text.y = ($scope.text.y || 0) + event.dy;

                if(event.da && canRotate)
                    $scope.text.angle = $scope.text.angle + event.da;
                if(event.ds && canScale && ($scope.text.scale * (1 + event.ds)) < MAX_TEXT_SCALE && ($scope.text.scale * (1 + event.ds)) >= MIN_TEXT_SCALE)
                    $scope.text.scale = $scope.text.scale * (1 + event.ds); 

                text.style.width = getTextWidth($scope.text.value, STARTING_FONT_SIZE + "px RobotoBold");
                /*text.style.height = $scope.text.height * $scope.text.scale;*/
                
                text.style.transform = 'translate(' + ($scope.text.x - (parseInt(text.style.width)/2)) + 'px, ' + ($scope.text.y - (parseInt(text.style.height)/2)) + 'px) rotate(' + $scope.text.angle + 'deg) scale(' + ($scope.text.scale + 0.8) + ')';

                drawText();
            }

            function onHammerText(ev){
                /* Updating properties */
                    $scope.text.width = getTextWidth($scope.text.value, STARTING_FONT_SIZE + "px RobotoBold");

                    if(ev.type == "rotate"){
                        if(!ev.isFirst && $scope.text.angle != 0){
                            console.log("Event " + ev.type, "isFirst: " + ev.isFirst, "isFinal: " + ev.isFinal, "rotation: " + ev.rotation, "scale:" + ev.scale);
                            console.log("Text options", "rotation: " + $scope.text.angle, "scale:" + $scope.text.scale);

                            $scope.text.angle = (($scope.text.angle + (ev.rotation/10)) > 360) ? ($scope.text.angle + (ev.rotation/10)) % (ev.rotation/10) : $scope.text.angle + (ev.rotation/10);
                        } else {
                            $scope.text.angle = ev.rotation;
                        }
                    }
                    if(ev.type == "pinch" && (ev.scale/1.5) >= MIN_TEXT_SCALE && (ev.scale/1.5) < MAX_TEXT_SCALE){
                        if(!ev.isFirst && $scope.text.angle != 1)
                            $scope.text.scale += ((ev.scale/1.5) - 1);
                        else
                            $scope.text.scale = ev.scale;
                    }

                    if(ev.type == "pan") {
                        //console.log(ev.deltaX, ev.deltaY, ev.center.x, ev.center.y, $scope.text.x, $scope.text.y);

                        var a = ev.center.x > $scope.text.x - $scope.text.width/2 - TEXT_GRAB_AREA_ADDITION,
                            b = ev.center.x < $scope.text.x + $scope.text.width/2 + TEXT_GRAB_AREA_ADDITION,
                            c = ev.center.y > $scope.text.y - $scope.text.height/2 - TEXT_GRAB_AREA_ADDITION,
                            d = ev.center.y < $scope.text.y + $scope.text.height/2 + TEXT_GRAB_AREA_ADDITION;

                        if(a && b && c && d){  
                            $scope.text.x = ev.pointers[0].clientX;
                            $scope.text.y = ev.pointers[0].clientY;

                            $scope.text.dx = (ev.pointers[0].clientX - $scope.text.x);
                            $scope.text.dy = (ev.pointers[0].clientY - $scope.text.y);
                        }
                    }
                    //console.clear();
                    //console.log(ev.type, "scale: " + ev.scale, "rotation: " + ev.rotation);
                    //console.log("text", "scale: " + $scope.text.scale, "angle: " + $scope.text.angle);
                /* Updating properties */

                /* Draw text with updated properties */
                    //Clear canvas
                    canvasses.text.ctx.clearRect(0, 0, canvasses.text.ref.width, canvasses.text.ref.height); 
               
                    canvasses.text.ctx.save();
                    canvasses.text.ctx.translate($scope.text.x, $scope.text.y - $scope.text.height);
                    if(canRotate)
                        canvasses.text.ctx.rotate(degToRad($scope.text.angle));

                    /* Addition of text to canvas */
                        canvasses.text.ctx.font = (STARTING_FONT_SIZE * $scope.text.scale) + "px RobotoBold";
                        canvasses.text.ctx.fillStyle = $scope.text.color;
                        canvasses.text.ctx.textBaseline = "middle";
                        canvasses.text.ctx.textAlign = "center";
                        canvasses.text.ctx.fillText($scope.text.value, 0, 0 );
                    /* Addition of text to canvas */

                    canvasses.text.ctx.restore();
                /* Draw text with updated properties */   
            }

            function onTextTap(ev){
                console.log(ev);

                /* Recognizing boundaries */
                    var a = ev.center.x > $scope.text.x - $scope.text.width/2 - TEXT_GRAB_AREA_ADDITION,
                        b = ev.center.x < $scope.text.x + $scope.text.width/2 + TEXT_GRAB_AREA_ADDITION,
                        c = ev.center.y > $scope.text.y - $scope.text.height/2 - TEXT_GRAB_AREA_ADDITION,
                        d = ev.center.y < $scope.text.y + $scope.text.height/2 + TEXT_GRAB_AREA_ADDITION;
                    if(a && b && c && d)
                        $scope.CSDK.toggleText();   
                /* Recognizing boundaries */  
            }
                    
            function initStickers(){

                $scope.stickers = [];
                var stickerPaths = [
                    "assets/svg/stickers/carillas varon 3-01.svg",
                    "assets/svg/stickers/carillas varon 3-02.svg",
                    "assets/svg/stickers/carillas varon 3-03.svg",
                    "assets/svg/stickers/carillas varon 3-04.svg",
                    "assets/svg/stickers/carillas varon 3-05.svg",
                    "assets/svg/stickers/carillas varon 3-06.svg",
                    "assets/svg/stickers/carillas varon 3-07.svg",
                    "assets/svg/stickers/carillas varon 3-08.svg",
                    "assets/svg/stickers/carillas varon 3-09.svg",
                    "assets/svg/stickers/carillas varon 3-10.svg",
                    "assets/svg/stickers/carillas varon 3-11.svg",
                    "assets/svg/stickers/carillas varon 3-12.svg",
                    "assets/svg/stickers/carillas varon 3-13.svg",
                    "assets/svg/stickers/carillas varon 3-14.svg",
                    "assets/svg/stickers/carillas varon 3-15.svg"

                    /*Testing extra stickers*/
                    ,"assets/svg/stickers/carillas varon 3-01.svg",
                    "assets/svg/stickers/carillas varon 3-02.svg",
                    "assets/svg/stickers/carillas varon 3-03.svg",
                    "assets/svg/stickers/carillas varon 3-04.svg",
                    "assets/svg/stickers/carillas varon 3-05.svg",
                    "assets/svg/stickers/carillas varon 3-06.svg",
                    "assets/svg/stickers/carillas varon 3-07.svg",
                    "assets/svg/stickers/carillas varon 3-08.svg",
                    "assets/svg/stickers/carillas varon 3-09.svg",
                    "assets/svg/stickers/carillas varon 3-10.svg",
                    "assets/svg/stickers/carillas varon 3-11.svg",
                    "assets/svg/stickers/carillas varon 3-12.svg",
                    "assets/svg/stickers/carillas varon 3-13.svg",
                    "assets/svg/stickers/carillas varon 3-14.svg",
                    /*Testing extra stickers*/
                ];

                $scope.categories = [
                    {
                        title: "acuaticos",
                        amount: 18
                    },
                    {
                        title: "amorosos",
                        amount: 37
                    },
                    {
                        title: "asustadizos",
                        amount: 33
                    },
                    {
                        title: "cosmicos",
                        amount: 38
                    },
                    {
                        title: "parlanchines",
                        amount: 33
                    },
                    {
                        title: "picnic",
                        amount: 34
                    },
                    {
                        title: "recreo",
                        amount: 43
                    }
                ];


                for(var j = 0; j < $scope.categories.length; j++)
                for(var i = 1; i <= $scope.categories[j].amount; i++){
                    $scope.stickers.push({
                        x: canvasses.overlay.ref.width / 2,
                        y: canvasses.overlay.ref.height / 2,
                        w: 100,
                        h: 100,
                        path: "assets/svg/stickers/" + $scope.categories[j].title + "/" + i + ".svg",
                        visible: false,
                        interact: undefined,
                        scale: MIN_TEXT_SCALE,
                        angle: 0,
                        category: j
                    });
                }

                setTimeout(function(){
                    for(var i = 0; i < $scope.stickers.length; i++){
                        $scope.stickers[i].interact = interact('.resize-drag-' + i).draggable({onmove: onInteractUpdateWithSticker}).gesturable({onmove: onInteractUpdateWithSticker});
                        $('#resize-drag-' + i)[0].style.transform = 'translate(' + $scope.stickers[i].x + 'px, ' + $scope.stickers[i].y + 'px) rotate(' + $scope.stickers[i].angle + 'deg) scale(' + $scope.stickers[i].scale + ')';
                    }
                }, 500);

                var listAmount = $scope.categories.length;//Math.ceil(stickers.length / itemsPerList);
                $scope.lists = [];

                var list;
                var j = 0, step = 0;
                for(var i = 0; i < listAmount; i++){
                    list = {
                        stickers: [],
                        name: $scope.categories[i].title
                    };

                    for(j = step; j < step + $scope.categories[i].amount; j++){
                        $scope.stickers[j].index = j;
                        list.stickers.push($scope.stickers[j]);
                        console.log("Adding " + j + " sticker to list " + i);
                    }
                    step = j;
                    $scope.lists.push(list);
                }

                setTimeout(function(){
                    var swiper = new Swiper('.swiper-stickers-container', {
                        spaceBetween: 10,
                        pagination: '.swiper-pagination',
                        centeredSlides: true,
                        slidesPerView: '1'
                    });
                },5000);
            }

            function initText(){
                $scope.text = {
                    value: "Ingresa tu texto aquí...",
                    color: "#ffffff",
                    x: canvasses.text.ref.width / 2,
                    y: canvasses.text.ref.height / 2,
                    dx: 0,
                    dy: 0,
                    scale: MIN_TEXT_SCALE,
                    angle: 0,
                    width: 200,
                    height: STARTING_FONT_SIZE,
                    interact: undefined,
                    hammer: undefined
                };

                $scope.text.interact = interact('#textDummy').draggable({onmove: onInteractUpdateWithText}).gesturable({onmove: onInteractUpdateWithText});
            }

            function drawText(){
                /* Draw text with updated properties */
                    //Clear canvas
                    canvasses.text.ctx.clearRect(0, 0, canvasses.text.ref.width, canvasses.text.ref.height); 
               
                    canvasses.text.ctx.save();
                    canvasses.text.ctx.translate($scope.text.x, $scope.text.y - $scope.text.height);
                    if(canRotate)
                        canvasses.text.ctx.rotate(degToRad($scope.text.angle));

                    /* Addition of text to canvas */
                        canvasses.text.ctx.font = (STARTING_FONT_SIZE * $scope.text.scale) + "px RobotoBold";
                        canvasses.text.ctx.fillStyle = $scope.text.color;
                        canvasses.text.ctx.textBaseline = "middle";
                        canvasses.text.ctx.textAlign = "center";
                        canvasses.text.ctx.fillText($scope.text.value, 0, 0 );
                    /* Addition of text to canvas */

                    canvasses.text.ctx.restore();
                /* Draw text with updated properties */  
            }

            function getTextWidthDOM(text, font) {
                var f = font || '12px arial',
                    o = $('<span>' + text + '</span>')
                        .css({'font': f, 'float': 'left', 'white-space': 'nowrap'})
                        .css({'visibility': 'hidden'})
                        .appendTo($('body')),
                    w = o.width(),
                    h = o.height();

              o.remove();

              return w;
            }

            function getTextWidth(text, font) {
                // if given, use cached canvas for better performance
                // else, create new canvas
                var canvas = document.getElementById("measureCanvas");
                var context = canvas.getContext("2d");
                var oldFont = context.font;
                context.font = font;
                var metrics = context.measureText(text);
                context.font = oldFont;
                return metrics.width;
            };
        /* Private methods */

        /* Exposure of private to public */
            this.update = update;        
            this.initStickers = initStickers;
            this.initText = initText;
        /* Exposure of private to public */
    }

    $scope.$watch('showPreview', function(newValue, oldValue){
        if(newValue === true)
            CameraPreview.hide();
        else if(newValue === false)
            CameraPreview.show();
    });

    $scope.$watch('text.value', function(newValue, oldValue){
        if(newValue !== undefined && newValue != oldValue && oldValue === 'Ingresa tu texto aquí...')
            $scope.text.value = "";
    });
}]);
