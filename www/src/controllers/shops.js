'use strict';

var Shops = angular.module('Shops', []);


Shops.controller('shopsCtrl',  ['$scope', '$rootScope', '$location', '$route', function ($scope, $rootScope, $location, $route) {

	//Comprobará si hay conexión yde paso actualizará
	$rootScope.actualizamosCache();

	$scope.localidades = [];
	$scope.shops = [];

	window.ga.trackView("Shops List");

	$rootScope.updateCallback = function(){
		if($rootScope.toggleRetryButton){
			$location.path("/main/shopsTypes");
			$route.reload();
		}
	}

	if($rootScope.filteredShopsMarks != undefined){

		var detailed = $rootScope.detailedElement;
		var isPromos = false;
		
		$scope.marks = $scope.filteredShopsMarks;
		setTimeout($scope.inicio, 400);

	} else if($rootScope.filteredPromosMarks != undefined){
		var filtered = [];
		var isPromos = true;

		for(var i = 0; i < $rootScope.filteredPromosMarks.length; i++){
            
            console.log(JSON.stringify($rootScope.filteredPromosMarks[i]));

            /*if($rootScope.filteredPromosMarks[i].audience.residence){
            	$rootScope.filteredPromosMarks[i].map = {lat: $rootScope.filteredPromosMarks[i].audience.residence.lat, lng: $rootScope.filteredPromosMarks[i].audience.residence.lng};	
        	}*/

        	if($rootScope.filteredPromosMarks[i].triggers.hotspot_data){
            	$rootScope.filteredPromosMarks[i].map = {lat: $rootScope.filteredPromosMarks[i].triggers.hotspot_data.lat, lng: $rootScope.filteredPromosMarks[i].triggers.hotspot_data.lng};	

            	$rootScope.filteredPromosMarks[i].name = $rootScope.filteredPromosMarks[i].description_title;
    			$rootScope.filteredPromosMarks[i].info = $rootScope.filteredPromosMarks[i].description_text;
    	
    			filtered.push($rootScope.filteredPromosMarks[i]);
        	}
        	
        }

        $scope.marks = $scope.filteredPromosMarks;
		setTimeout(function(){$scope.inicio();}, 400);
	} else {
		
		$scope.marks=$rootScope.venuesCache;
	}		

	//console.log(JSON.stringify($rootScope.venuesCache[0]));

	//console.log($rootScope.lat);

	/*Playtomic.Shops.list({}, function(response) {	
		console.log(response);
		if(response.shops) {			
			_shopRArr=response.shops.shops;			
			
			for(var a=0; a< _shopRArr.length; a++){
				var _shopItem= {'local':response.shops.shops[a].name,'localidad': 'Villa Gesell', 'description': response.shops.shops[a].address,'latitude':'-37.77071473849609','longitude':'-57.9638671875'};
				_shopArr.push(_shopItem);
			}
			
			$scope.marks = _shopArr;
			
		} else {
			//console.log('Error en shops');
		}		
	});*/
		
	/******************************** PROVINCIAS ARRAY ********************************/

	$scope.provincias = [];

	function getCombos(){
	
		var mark, shop, localidad, provincia, exists = {p: false, l: false, s: false};

		for(var i = 0; i < $scope.marks.length; i++){

			/*if(!$scope.marks[i].map || !$scope.marks[i].map.administrative_area_level_1 || !$scope.marks[i].map.locality){
				continue();
			}*/

			mark = $scope.marks[i];

			shop = {
				coordenadas: {
					latitud: mark.map.lat, 
					longitud: mark.map.lng
				},

				id: mark.id,
				name: mark.name
			};

			localidad = {
				name: mark.map.locality,
				shops: []
			};

			provincia = {
				name: mark.map.administrative_area_level_1,
				localidades: []
			};

			exists = {p: undefined, l: undefined, s: undefined};

			for(var j = 0; j < $scope.provincias.length; j++)
				if($scope.provincias[j].name == provincia.name)
					exists.p = j;

			if(exists.p != undefined){

				for(var k = 0; k < $scope.provincias[exists.p].localidades.length; k++)
					if($scope.provincias[exists.p].localidades[k].name == localidad.name)
						exists.l = k;	

				if(exists.l != undefined){

					for(var l = 0; l < $scope.provincias[exists.p].localidades[exists.l].shops.length; l++){

						if($scope.provincias[exists.p].localidades[exists.l].shops[l].name == shop.name){
							exists.s = l;
							break;
						}
					}

					if(exists.s == undefined)
						$scope.provincias[exists.p].localidades[exists.l].shops.push(shop);								

				} else {

					for(var l = 0; l < localidad.shops.length; l++){

						if(localidad.shops[l].name == shop.name){
							exists.s = l;
							break;
						}									
					
					}

					if(exists.s == undefined)
						localidad.shops.push(shop);

					$scope.provincias[exists.p].localidades.push(localidad);
				}

			} else {

				for(var k = 0; k < provincia.localidades.length; k++)
					if(provincia.localidades[k].name == localidad.name)
						exists.l = k;	

				if(exists.l != undefined){

					for(var l = 0; l < provincia.localidades[exists.l].shops.length; l++){

						if(provincia.localidades[exists.l].shops[l].name == shop.name){
							exists.s = l;
							break;
						}
					}

					if(exists.s == undefined)
						provincia.localidades[exists.l].shops.push(shop);								

				} else {

					for(var l = 0; l < localidad.shops.length; l++){

						if(localidad.shops[l].name == shop.name){
							exists.s = l;
							break;
						}									
					
					}

					if(exists.s == undefined)
						localidad.shops.push(shop);

					provincia.localidades.push(localidad);
				}

				$scope.provincias.push(provincia);
			}

		}
		//console.log(JSON.stringify($scope.provincias));

	}

	$scope.indexes = {prov: 0, localidad: 0};
	
	/******************************** LOCALIDADES ARRAY ********************************/

	$scope.locals = [
		{'local':'Grisino','title': 'punto1', 'description': '1718 14th St','latitude':'-34.602409','longitude':' -58.380621'},
	];
	

	/******************************** GETTER PROVINCIA  ********************************/

	$scope.setProvIndex = function(index){
		$scope.indexes.prov = index;
		$scope.someVal2 = $scope.provincias[index].localidades[0];
		$scope.someVal3 = $scope.provincias[index].localidades[0].shops[0];
	}

	$scope.setLocalidadIndex = function(index){
		$scope.indexes.localidad = index;
	}

	$scope.searchLocal = function(index){
		//console.log(shop.coordenadas.latitud+" - "+shop.coordenadas.longitud);

		var shop = $scope.someVal3;
		map.setView([shop.coordenadas.latitud, shop.coordenadas.longitud], 16);
	}

	/*********************************** MAPBOX CONFIG ********************************/
	$scope.$watch('someVal1', function(newValue, oldValue){
		if(newValue != undefined)
		for(var i = 0; i < $scope.provincias.length; i++){
			console.log($scope.provincias[i]);
			console.log(newValue);
			console.log("   ");

			if($scope.provincias[i].name == newValue){
				$scope.indexes.prov = i;
				$scope.localidades = $scope.provincias[i].localidades;
				console.log("Localidades: " + JSON.stringify($scope.localidades));
				break;
			}
		}
	});

	$scope.$watch('someVal2', function(newValue, oldValue){
		if(newValue != undefined)
		for(var i = 0; i < $scope.provincias[$scope.indexes.prov].localidades.length; i++){
			console.log($scope.provincias[$scope.indexes.prov].localidades[i]);
			console.log(newValue);
			console.log("   ");

			if($scope.provincias[$scope.indexes.prov].localidades[i].name == newValue.name){
				$scope.indexes.localidad = i;
				$scope.shops = $scope.provincias[$scope.indexes.prov].localidades[i].shops;
				console.log("Shops: " + JSON.stringify($scope.shops));
				break;
			}
		}
	});

	$scope.$watch('someVal3', function(newValue, oldValue){
		if(newValue != undefined){
			map.setView([newValue.coordenadas.latitud, newValue.coordenadas.longitud], 16);
		}
	});

	L.mapbox.accessToken = 'pk.eyJ1IjoiZmVkZW11bmkiLCJhIjoieWRPazdSRSJ9.aDK-HSUPTYxgahE8BzOtoQ';

	var lat = -37.77071473849609;
	var lng = -57.9638671875;

	var map = L.mapbox.map('map', 'mapbox.streets', { zoomControl: false }).setView([lat, lng], 5);

	new L.Control.Zoom({ position: 'bottomright' }).addTo(map);
	var myLayer = L.mapbox.featureLayer().addTo(map);

	var geojson = [
		{
		  "type": "FeatureCollection",
		  "features": [],
		  "id":'mapbox.checkin',
		  "properties": {
		  	"icon": {
	  			"iconUrl": "/assets/images/icon-gastronomia-30x40px_1.png",
            	"iconSize": [30, 40], // size of the icon
            	"iconAnchor": [15, 40], // point of the icon which will correspond to marker's location
		  	}
		  }

		}
	];

	function getIconPath(markerCategories){
		var allCategories = isPromos ? $rootScope.categoriasCache.custom_categories.promos : $rootScope.categoriasCache.custom_categories.shops;

		for(var i = 0; i < markerCategories.length; i++)
			for(var j = 0; j < allCategories.length; j++)
				if(markerCategories[i] == allCategories[j].id){
					
					if(allCategories[j].name == "Deportes"){
						return "swimming";
					} else if(allCategories[j].name == "Parrilla" || allCategories[j].name == "Gastronomía" || allCategories[j].name == "Variada"){
						return "restaurant";
					} else if(allCategories[j].name == "Cafeteria"){
						return "cafe";
					} else if(allCategories[j].name == "Compras"){
						return "shop";
					} else if(allCategories[j].name == "Casas" || allCategories[j].name == "Cabañas"){
						return "building";
					} else if(allCategories[j].name == "Hoteles" || allCategories[j].name == "Hospedaje"){
						return "lodging";
					} else if(allCategories[j].name == "Campings"){
						return "park";
					} else if(allCategories[j].name == "Turismo"){
						return "camera";
					} else if(allCategories[j].name == "Servicio"){
						return "fuel";
					} else if(allCategories[j].name == "Destacado"){
						return "shop";
					} else if(allCategories[j].name == "Servicios y Urgencias"){
						return "hospital";
					} else { //No hay coincidencias
						console.log("No hay coincidencias, se encuentra categoria, pero el name es: " + allCategories[j].name);
					}					
				} else {
					console.log("No hay coincidencias, no se encuentra categoria que coincida con el id de la categoria especificada por el indice " + j);
				}
	}
	
		
	$scope.inicio = function(){

		for(var x = 0; x < $scope.marks.length; x++){
			
			if($scope.marks[x].map){				

				if($scope.marks[x].map.lat != "undefined" && $scope.marks[x].map.lat != undefined && $scope.marks[x].map.lat != null && $scope.marks[x].map.lat != "null"){
					var newMarker = 

					L.marker([$scope.marks[x].map.lat, $scope.marks[x].map.lng], {
				      icon: L.mapbox.marker.icon({
				        'marker-size': 'large',
				        'marker-symbol': getIconPath($scope.marks[x].category)
				      })
				    })

				    .bindPopup(/*detailed != undefined ? '<div><strong>'+ $scope.marks[x].name +'</strong><br>'+ $scope.marks[x].info.substring(0, 100)+'...<br><br></div>' :*/ '<div><strong>'+ $scope.marks[x].name +'</strong><br>'+$scope.marks[x].info.substring(0, 100)+'...<br><br></div>' + '<button class="trigger" onclick="" id="markerPopupButton-'+ x +'" style="width: 100%">Ver Detalle</button>')
				    .addTo(map)

				    .on('click', function(event) {

				    	event.target._popup._container.children[1].children[0].children[1].onclick= function(event){
				    		//console.log("hace algo");
							var id = event.target.id;
							var index = id.split("-")[1];

							$rootScope.mark = $scope.marks[index];
							$rootScope.state = $scope.marks[index].name;
						    $rootScope.detail($scope.marks[index]._id, "shop");
				    	};					
					});

				    if($scope.marks.length == 1)
					newMarker.openPopup();
				}
			}
		}

		if($rootScope.lat=="undefined" || $rootScope.lat==undefined){

		}else{

			if(detailed != undefined)
				map.setView([$scope.marks[0].map.lat, $scope.marks[0].map.lng], 18);
			else 
				map.setView([$rootScope.lat, $rootScope.lng], 10);

			L.marker([$rootScope.lat, $rootScope.lng], {
				      icon: L.mapbox.marker.icon({
				        'marker-size': 'large',
						'marker-color': '#000'
				      })
				    }).addTo(map)
		}
		
		$rootScope.loading = false;
	}

	$scope.inicio();
	getCombos();

	/*myLayer.on('layeradd', function(e) {
	    var marker = e.layer,
	        feature = marker.feature;

	    marker.setIcon(L.icon(feature.properties.icon));
	});*/

	// Add features to the map.
	//myLayer.setGeoJSON(geoJson);

	//console.log(JSON.stringify($scope.provincias));

}]);