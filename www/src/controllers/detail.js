'use strict';

var Detail = angular.module('Detail', ['ngMaterial']);


Detail.controller('detailCtrl', ['$scope', '$rootScope', '$compile', '$timeout', '$mdDialog', '$cordovaSQLite', '$cordovaFacebook', '$cordovaSocialSharing', '$cordovaInAppBrowser', '$mdBottomSheet', '$http', '$mdToast', function($scope, $rootScope, $compile, $timeout, $mdDialog, $cordovaSQLite, $cordovaFacebook, $cordovaSocialSharing, $cordovaInAppBrowser, $mdBottomSheet, $http, $mdToast) {

    $scope.hbutton = "Ver información";
    $scope.isOpen = false;
    $scope.finalizada = false;

    $rootScope.loading = false;

    var info = $rootScope.detailedElement;
    var index;
    var element, type = info.type; //FILL WITH ELEMENT FROM

    /*Type-dependant options*/
    /*If it´s a promo*/
    if (type == "promo") {

        for (var i = 0; i < $rootScope.promosCache.length; i++)
            if ($rootScope.promosCache[i].id == info.id) {
                element = $rootScope.promosCache[i];
                index = i;
            }

            /*If it´s a shop*/
    } else if (type == "shop") {

        for (var i = 0; i < $rootScope.venuesCache.length; i++)
            if ($rootScope.venuesCache[i].id == info.id)
                element = $rootScope.venuesCache[i];

            /*If it´s news or a video*/
    } else if (type == "news") {

        for (var i = 0; i < $rootScope.contentCache.length; i++)
            if ($rootScope.contentCache[i]._id == info.id)
                element = $rootScope.contentCache[i];
    }

    console.log(element);
    console.log(type);

    var source,
        html,
        media,
        _nombre = '',
        _description = '',
        share = '',
        description = '',
        name = '',
        buttons = '',
        map = '',
        link = element.ecommerce_url,
        menubutton = '',
        promoType = 'fecha';

    switch (type) {
        case "promo":
            source = "https://s3-sa-east-1.amazonaws.com/iloveit/promo/" + element._id + ".jpg";

            var s = element.start_date.split("T")[0];
            var startDate = s.split("-")[2] + "/" + s.split("-")[1] + "/" + s.split("-")[0];
            s = element.end_date.split("T")[0];
            var endDate = s.split("-")[2] + "/" + s.split("-")[1] + "/" + s.split("-")[0];

            if ($rootScope.NOW() > new Date(element.end_date).toISOString())
                $scope.finalizada = true;

            if ($rootScope.isGrisinoFan()) {
                // Not e-shop
                if (link == "") {
                    // Not redeemed
                    if (element.code == false && element.redeemed == undefined) {
                        var points = element.points > 0 ? element.points : (element.points * -1);

                        // Not substracting
                        if (element.points == "") {
                            buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="width: 90%!important; bottom: 0; left: 0" hm-tap="showConfirm($event)" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' flex flex-md="100">Quiero mi beneficio</md-button></div>';
                        } else {
                            if ($rootScope.stars) {
                                // Make always positive -> SOLO RESTA DE PUNTOS
                                var points = element.points > 0 ? element.points : (element.points * -1);

                                if ($rootScope.stars.stars >= points) {
                                    buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="width: 90%!important; bottom: 0; left: 0" hm-tap="showConfirm($event)" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' flex flex-md="100">Quiero mi beneficio</md-button></div>';
                                } else {
                                    buttons = '<div style="width: 100%;margin-top: 20px;" layout-align="center center" layout="row"><div style="background-size: cover;margin-right: 10px;width: 26px;height: 26px;background-image: url(\'assets/images/icono_alerta.png\')"></div><div> Te faltan <strong>' + (points - $rootScope.stars.stars) + ' estrellas</strong></div></div>';
                                }
                            } else {
                                buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="width: 90%!important; bottom: 0; left: 0" hm-tap="showConfirm($event)" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' flex flex-md="100">Quiero mi beneficio</md-button></div>';
                            }
                        }
                    } else {

                        /*Has been redeemed in this very session*/
                        if (element.redeemed != undefined) {
                            console.log("Promo redeemed -> " + element.redeemed);
                            if (element.redeemed == "") { // Redeemed without any code being returned
                                buttons = '<div style="width: 100%;margin-top: 20px;" layout-align="center center" layout="row">Presentá la app a quien te atiende</div>'
                            } else { // Redeemed with code
                                buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="font-size: 4vw;width: 90%!important; bottom: 0; left: 0" flex flex-md="100">Beneficio ya utilizado. Tu código es <strong>' + element.redeemed + '</strong></md-button></div>';
                            }

                        } else { // Redeemed in an older session
                            if (element.code == "") { // Redeemed without any code being returned
                                buttons = '<div style="width: 100%;margin-top: 20px;" layout-align="center center" layout="row">Presentá la app a quien te atiende</div>'
                            } else { // Redeemed with code
                                buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="font-size: 4vw;width: 90%!important; bottom: 0; left: 0" flex flex-md="100">Beneficio ya utilizado. Tu código es <strong>' + element.code + '</strong></md-button></div>';
                            }
                        }
                    }
                } else {
                    buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="width: 90%!important; bottom: 0; left: 0" href="' + link + '?utm_source=fanapp&utm_medium=app&utm_campaign=grisino%20fan" flex flex-md="100">Comprar en e-shop</md-button></div>';
                }
            } else {
                buttons = '<div class="redeem-actions"><md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="width: 90%!important; bottom: 0; left: 0" hm-tap="showConfirm($event)" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' flex flex-md="100">Quiero mi beneficio</md-button></div>';
            }

            _nombre = element.description_title;
            _description = element.description_text;

            description = '<p class="animated fadeInLeft" style="-webkit-animation-delay: 1.5s; margin: 5px 25px;">' + element.description_text

            if (!element.permanent) {
                description += '<br><br><span class="tiny">Promoción válida desde <b>' +
                    startDate +
                    '</b> hasta <b>' +
                    endDate +
                    '</b></span></p>';
            }

            name = '<p class="animated fadeInLeft" style="-webkit-animation-delay: 0.9s; margin: 10px 25px;">' + element.description_title + '</p>';

            if (!element.triggers.hotspot_data) {

                if (element.audience.shops.length != 0) {
                    var closestShop = undefined;

                    for (var i = 0; i < element.audience.shops.length; i++) {

                        var h = haversine($rootScope.lat, $rootScope.lng, element.audience.shops[i].map.lat, element.audience.shops[i].map.lng);

                        if (!closestShop || h.number < closestShop.number)
                            closestShop = h;
                    }

                    var haver = $rootScope.lat ? "(" + closestShop.str + ")" : "";
                    promoType = "shop";
                } else {
                    var haver = "";
                }

            } else {
                var haver = $rootScope.lat ? "(" + haversine(element.triggers.hotspot_data.lat, element.triggers.hotspot_data.lng, $rootScope.lat, $rootScope.lng).str + ")" : "";
                promoType = "hotSpot";
            }

            if (promoType != "fecha") {
                map = '<div class="scrollableMap"><div id="map" class="detailedMap"></div></div>';

                menubutton = '<md-button id="flipDIV" hm-tap="flipDIV();" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-fab allTabsIcon" style="border: 1px solid white; opacity: 1!important; background-image: url(\'assets/images/icon-mapa3_30x30px-white.png\'); position: fixed !important;top: 100px !important;right: 10px !important;height: 50px !important;width: 50px!important; background-color: #d60036!important;" aria-label="Use Android"></md-button>';

                $rootScope.isShops = true;
            }

            break;
        case "news":
            source = "https://s3-sa-east-1.amazonaws.com/iloveit/content/" + element._id + ".jpg";

            _nombre = element.title;
            _description = element.description;

            description = '<p class="animated fadeInLeft" style="-webkit-animation-delay: 1.5s; margin: 0px 25px;">' + element.description;

            name = '<p class="animated fadeInLeft" style="-webkit-animation-delay: 0.9s; margin: 10px 25px;">' + element.title + '</p>';

            break;
        case "shop":

            source = "https://s3-sa-east-1.amazonaws.com/iloveit/shop/" + element._id + ".jpg";

            _nombre = element.name;
            _description = element.info;

            description = '<p class="animated fadeInLeft" style="-webkit-animation-delay: 1.5s; margin: 10px 25px;">' + element.info;

            map = '<div class="scrollableMap"><div id="map" class="detailedMap"></div></div>';

            name = '<p class="animated fadeInLeft" style="-webkit-animation-delay: 0.9s; margin: 10px 25px;">' + element.name + '</p><p class="animated fadeInLeft" style="-webkit-animation-delay: 0.9s; margin: 10px 25px;"><a href="maps://maps.apple.com?daddr=' + element.map.lat + ',' + element.map.lng + '&dirflg=d">Abrir en mapas</a></p>';

            menubutton = '<md-button id="flipDIV" hm-tap="flipDIV();" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-fab allTabsIcon" style="border: 1px solid white; opacity: 1!important; background-image: url(\'assets/images/icon-mapa3_30x30px-white.png\'); backgorund-size: 30%!important; position: fixed !important;top: 100px !important;right: 10px !important;height: 50px !important;width: 50px!important; background-color: #d60036!important;" aria-label="Use Android"></md-button>';

            $rootScope.isShops = true;

            break;
        case "video":
            var sampleUrl = obj.elem.fields.youtubeid;
            var video_id = sampleUrl;

            source = "https://www.youtube.com/embed/" + video_id;
            type = "video";

            media = '<iframe src="' + source + '" frameBorder="0" style="height: 100%; width: 100%;" ></iframe>'
            break;
    }

    /*Last tweaks*/
    /*If it is not a video*/
    if (type != "video") {
        media = '<img src="' + source + '" id="' + element._id + '" onload="this.style.opacity=1;" class="newsElement">';

        element.img = source;

        $scope.alert = '';
    }

    /*If it is not a promo*/
    if (type != "promo") {
        description += '</p>';
    }

    /*Template construction*/
    html = '<div id="detailedWrapper" style="z-index:5; border-radius:0 0 0 0!important;border: 0px!important;border-width: 0px!important; height: auto!important;" class="imageWrapper square detail"><div class="ribbon" ng-show="finalizada"></div>' +

        '<div class="newsPiece flat" layout="column">' +

        '<div class="imageWrapper detail defaultImage" style=" height:' + $(".promos-content").width() + 'px!important;background-image: url(\'assets/images/novedades.png\')">' +

        media +

        '</div>' +
        '</div>' +
        '</div>' +

        //If is shops will append a flippable map
        map +
        menubutton +

        '<div id="detailedInfoWrapper" style="overflow-y: scroll; position: absolute; background: rgba(0,0,0,0); width: 100%;">' +

        '<div id="detailedInfoFill" style="width: 100%; position:relative; background: rgba(0,0,0,0);"></div>' +

        '<div id="detailedInfo" class="square2 animated fadeInRight no-padding-bottom" style="padding: 0px!important; -webkit-animation-delay: 0.2s;">' +

        name +
        description +
        buttons +

        '<div ng-hide="finalizada" style="width: 90%;margin-left: 5%" layout="row" layout-align="space-around center">' +

        '<div style="width:100%;box-shadow: 1px 0px 0px rgba(0, 0, 0, 0.2);" flex="30"><div aria-label="compartir" style="display: inline-block;box-sizing: border-box;height: 48px;width: 48px;margin: 0;background-position: center;background-size:50%;background-repeat: no-repeat; background-image: url(assets/images/icono-compartir.png)" class="md-fab md-raised"></div></div>' +

        '<div flex="70" style="padding: 30px 0px;width: 100%;" layout="row" layout-align="center center">' +
        '<md-button hm-tap="shareFacebook();" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' aria-label="facebook" style="box-shadow:none!important; border: 1px solid #314A7E; background-position: center;background-size:50%;margin-left:12px!important;background-repeat: no-repeat; background-image: url(assets/images/icono-facebook.png)" class="md-fab md-raised md-mini"></md-button>' +
        '<md-button hm-tap="shareTwitter();" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' aria-label="Twitter" style="box-shadow:none!important; border: 1px solid #339DC3; background-repeat: no-repeat; background-position: center;background-size:50%;margin-left:12px!important; background-image: url(assets/images/icono-twitter.png)" class="md-fab md-raised md-mini"></md-button>' +

        '<md-button hm-tap="shareInstagram();" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' aria-label="instagram" style="box-shadow:none!important; border: 1px solid #125688; background-position: center;background-size:50%;margin-left:12px!important;background-repeat: no-repeat; background-image: url(assets/images/icono-instagram.png)" class="md-fab md-raised md-mini"></md-button>' +
        '</div>' +
        '</div>' +

        '</div>' +

        '<div id="detailedInfoFill2" style="width: 100%; position:relative; background: rgba(0,0,0,0);"></div>' +

        '</div>';

    /*Template wrapper appending*/
    $(".promos-content").append($compile(html)($scope));

    //Set up appended map
    if (type == "promo") {

        if (promoType == "fecha") {} else if (promoType == "hotSpot") {

            $(".scrollableMap").css("height", $("#detailedWrapper").css("height"));

            var _userLat;
            var _userLng;

            if ($rootScope.lat == null || $rootScope.lat == "null") {
                _userLat = element.triggers.hotspot_data.lat;
                _userLng = element.triggers.hotspot_data.lng;
            } else {
                _userLat = $rootScope.lat;
                _userLng = $rootScope.lng;
            }

            //Map setup
            L.mapbox.accessToken = 'pk.eyJ1IjoiZmVkZW11bmkiLCJhIjoieWRPazdSRSJ9.aDK-HSUPTYxgahE8BzOtoQ';

            var map = L.mapbox.map('map', 'mapbox.streets', { zoomControl: false }).setView([_userLat, _userLng], 16);

            var geojson = [{
                "type": "FeatureCollection",
                "features": [],
                "id": 'mapbox.checkin'
            }];

            if (element.triggers.hotspot_data.lat != undefined) {
                var newMarker =

                    L.marker([element.triggers.hotspot_data.lat, element.triggers.hotspot_data.lng], {
                        icon: L.mapbox.marker.icon({
                            'marker-size': 'large',
                            'marker-color': '#000',
                            'marker-symbol': 'shop'
                        })
                    })

                .bindPopup('<div><strong>' + element.description_title + '</strong><br>' + element.triggers.hotspot_data.address + '</div>')
                    .addTo(map)
            }

        } else {

            $(".scrollableMap").css("height", $("#detailedWrapper").css("height"));

            var _userLat;
            var _userLng;

            if ($rootScope.lat == null || $rootScope.lat == "null") {
                _userLat = element.audience.shops[0].map.lat;
                _userLng = element.audience.shops[0].map.lng;
            } else {
                _userLat = $rootScope.lat;
                _userLng = $rootScope.lng;
            }

            //Map setup
            L.mapbox.accessToken = 'pk.eyJ1IjoiZmVkZW11bmkiLCJhIjoieWRPazdSRSJ9.aDK-HSUPTYxgahE8BzOtoQ';

            var map = L.mapbox.map('map', 'mapbox.streets', { zoomControl: false }).setView([_userLat, _userLng], 15);

            var geojson = [{
                "type": "FeatureCollection",
                "features": [],
                "id": 'mapbox.checkin'
            }];

            for (var x = 0; x < element.audience.shops.length; x++) {

                if (element.audience.shops[x].map.lat != undefined) {
                    var newMarker = L.marker([element.audience.shops[x].map.lat, element.audience.shops[x].map.lng], {
                        icon: L.mapbox.marker.icon({
                            'marker-size': 'large',
                            'marker-color': '#000',
                            'marker-symbol': 'shop'
                        })
                    }).bindPopup('<div><strong>' + element.audience.shops[x].name + '</strong><br>' + element.audience.shops[x].map.address + '</div>').addTo(map)
                }
            }
        }
    } else if (type == "shop") {

        $(".scrollableMap").css("height", $("#detailedWrapper").css("height"));

        //Map setup
        L.mapbox.accessToken = 'pk.eyJ1IjoiZmVkZW11bmkiLCJhIjoieWRPazdSRSJ9.aDK-HSUPTYxgahE8BzOtoQ';
        var map = L.mapbox.map('map', 'mapbox.streets', { zoomControl: false }).setView([element.map.lat + 0.0018, element.map.lng], 15);

        var geojson = [{
            "type": "FeatureCollection",
            "features": [],
            "id": 'mapbox.checkin'
        }];

        if (element.map.lat != undefined) {
            var newMarker = L.marker([element.map.lat, element.map.lng], {
                icon: L.mapbox.marker.icon({
                    'marker-size': 'large',
                    'marker-color': '#000'
                })
            }).bindPopup('<div><strong>' + element.name + '</strong><br>' + element.address + '<br><u>Tel:</u> <a href="tel:' + element.phone + '">' + element.phone + '</a><br><u>Email:</u> ' + element.info + '<br><br></div>').addTo(map);
        }
    }

    $scope.flipDIV = function() {
        $(".promos-content").animate({ scrollTop: 0 }, 500, "swing");

        //Shows map
        if (!$scope.isImage) {
            $("#detailedWrapper").animate({
                left: "100%"
            }, 500);

            $(".scrollableMap").animate({
                left: 0
            }, 500);


            $("#flipDIV").css("background-image", "url('assets/images/photo-camera.svg')");
            $("#flipDIV").css("background-size", "50%");

            $scope.isImage = true;

            //Shows image
        } else {

            $("#detailedWrapper").animate({
                left: 0
            }, 500);

            $(".scrollableMap").animate({
                left: "-100%"
            }, 500);


            $("#flipDIV").css("background-image", "url('assets/images/icon-mapa3_30x30px-white.png')");
            $("#flipDIV").css("background-size", "50%");


            $scope.isImage = undefined;
        }
    }

    if (type == "shop" || (type == "promo" && promoType != "fecha")) {
        $scope.isImage = true;
        $scope.flipDIV();
    }

    /*In-App Browser catch*/
    $(document).on('click', 'a[href^=http], a[href^=https]', function(e) {

        if (!$rootScope.isInAppBrowserOpen) {

            e.preventDefault();

            var $this = $(this);

            console.log($(this).attr('href'));

            $cordovaInAppBrowser.open($(this).attr('href'), '_blank', IN_APP_BROWSER_OPTIONS)
                .then(function(event) {
                    console.log($(this).attr('href'));
                })
                .catch(function(event) {
                    console.log($(this).attr('href'));
                });

            $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event) {

            });

            $rootScope.$on('$cordovaInAppBrowser:loaderror', function(e, event) {

            });

            $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event) {
                console.log('cierra el inappbrowser');
                $rootScope.loading = false;
                $rootScope.isInAppBrowserOpen = false;
            });

            $rootScope.isInAppBrowserOpen = true;
        }
    });

    $scope.showConfirm = function(ev) {

        // Prevent non-GrisinoFanUsers to redeem at all
        $rootScope.toggleAnnoyingMessage(!$rootScope.isGrisinoFan(), true);
        if (!$rootScope.isGrisinoFan())
            return;

        if (element.code_type == "no_code") {

            $mdDialog.show({
                controller: DialogController,
                clickOutsideToClose: true,
                template: '<md-dialog><md-button class="md-icon-button" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'><md-icon md-svg-src="assets/images/ic_close_24px.svg" style="color: black!important" aria-label="Close dialog"></md-icon></md-button><form style="padding: 15px"><md-dialog-content class="noPadding"><div><center><p style="font-size: 1.1em;">Presentá la aplicación a quien te atiende, para obtener el beneficio.</p></center></div></md-dialog-content></form></md-dialog>',
                targetEvent: ev
            });

            $(".redeem-actions").fadeOut("slow", function() {
                $(".redeem-actions").html('<div style="width: 100%;margin-top: 20px;" layout-align="center center" layout="row">Presentá la app a quien te atiende</div>').fadeIn("slow");
                $rootScope.promosCache[index].reedemed = "";
            });

        } else {
            if (element.points == "") {
                var textoPuntos = '';
            } else {
                if (!$rootScope.stars) {
                    $mdDialog.show({
                        controller: DialogController,
                        clickOutsideToClose: true,
                        template: '<md-dialog><md-button class="md-icon-button" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'><md-icon style="color:black!important;" md-svg-src="assets/images/ic_close_24px.svg" aria-label="Close dialog"></md-icon></md-button><form style="padding: 15px"><md-dialog-content class="noPadding"><div><center><p style="font-size: 1em;">No se puede canjear esta promo porque no sos Grisino Fan aún. Para empezar a acumular estrellas, hazte Grisino Fan.</p><md-button style="width: 100%!important; background-color: green!important; margin: 5px 0px; padding: 0px" hm-tap="hide()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn">Hacerme Grisino Fan</md-button></center></div></md-dialog-content></form></md-dialog>',
                        targetEvent: ev
                    }).then(function() {

                        $cordovaInAppBrowser.open('http://www.grisino.com/customer/account/create/', '_blank', IN_APP_BROWSER_OPTIONS)
                            .then(function(event) {
                                // success
                            })
                            .catch(function(event) {
                                // error
                            });

                        //$cordovaInAppBrowser.close();

                        $rootScope.$on('$cordovaInAppBrowser:loadstart', function(e, event) {});

                        $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event) {});

                        $rootScope.$on('$cordovaInAppBrowser:loaderror', function(e, event) {});

                        $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event) {
                            console.log('cierra el inappbrowser');
                            $rootScope.loading = false;
                        });
                    }, function() {});
                    return;

                } else {

                    // Make always positive -> SOLO RESTA DE PUNTOS
                    var points = element.points > 0 ? element.points : (element.points * -1);

                    if ($rootScope.stars.stars >= points) {
                        var textoPuntos = 'Una vez visualizado el código, se descontarán de tu cuenta <b>' + element.points + '</b> estrellas.';
                    } else {
                        $mdDialog.show({
                            controller: DialogController,
                            template: '<md-dialog><md-button class="md-icon-button" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'><md-icon style="color:black!important;" md-svg-src="assets/images/ic_close_24px.svg" aria-label="Close dialog"></md-icon></md-button><md-dialog-content><form style="padding: 15px"><md-dialog-content class="noPadding"><div><center><p style="font-size: 1.5em;">No te alcanzan las estrellas para canjear esta promoción.</p></center></div></md-dialog-content></form></md-dialog>',
                            targetEvent: ev,
                            clickOutsideToClose: true
                        }).then(function() {}, function() {});
                        return;
                    }
                }
            }

            $mdDialog.show({
                controller: DialogController,
                template: '<md-dialog style="padding: 15px;"><form style="margin: 0px;"><md-button class="md-icon-button" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'><md-icon style="color:black!important;" md-svg-src="assets/images/ic_close_24px.svg" aria-label="Close dialog"></md-icon></md-button><md-dialog-content class="noPadding"><div><center><p> Vas a hacer uso de este beneficio. Si continuás se mostrará el código personal que debes presentar al cajero.<br>' + textoPuntos + '<br><b>Recuerda!</b> la cantidad de estrellas se actualizan semanalmente</p></center></div></md-dialog-content></form><div style="padding: 0px 24px 10px 24px;"><center><md-button style="width: 100%!important; background-color: green!important; margin: 5px 0px; padding: 0px" hm-tap="answer(\'canjear\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn">Si, quiero mi beneficio</md-button><md-button style="width: 100%!important; margin: 5px 0px; padding: 0px" hm-tap="answer(\'cancelar\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn md-warn">No, en otro momento</md-button></center></div></md-dialog>',
                targetEvent: ev
            }).then(function(answer) {
                $rootScope.loading = true;

                if (answer != 'cancelar')
                    redeem(ev);
                else
                    $rootScope.loading = false;

            }, function() {});
        }
    };

    $rootScope.loading = false;


    //Fecha/hora, IP, Lat-Long, ID acción, valor, Descripción
    var _log = '{"fecha":"' + $rootScope.NOW + '","IP":"' + $rootScope.IP + '", "lat:"' + $rootScope.lat + '", "lng":"' + $rootScope.lng + '","accion":"' + element.type + '","valor":"' + element.id + '","titulo":"' + element.name + '"},';

    var query = "INSERT INTO logs (log) VALUES ('" + _log + "')";
    $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {}, function(err) {
        console.error(err);
    });

    function redeem(ev) {

        var dataObj = {
            data: "eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJwcm9tb3MiLCJhY3Rpb24iOiJyZWRlZW0iLCJwdWJsaWNrZXkiOiIzNXdreWUwZWh2aWFlMTRueHQ0NCJ9",
            hash: "c227732be0e4b52f6458aa5c6de5cea6",
            userId: $rootScope.user.id,
            id: element.id
        };

        var res = $http.post('http://'+SERVER+'.reachout.global/api?publickey=nkfvt7gpgdkakyb78gr9&id=' + element.id, dataObj);

        res.success(function(data, status, headers, config) {

            if (data.success) {

                $rootScope.loading = false;

                if (data.promo_code == "undefined" || data.promo_code == '' || data.promo_code == undefined) {

                    if (element.points == "") {
                        var textoCodigo = 'Presentá la aplicación a quien te atiende, para obtener el beneficio.';
                    } else {
                        var textoCodigo = 'Presentale esta aplicación a quien te atiende, para obtener el beneficio, se descontarán <b>' + element.points + '</b> estrellas, de tu cuenta.';
                    }

                    var redeemActions = '<div style="width: 100%;margin-top: 20px;" layout-align="center center" layout="row">Presentá la app a quien te atiende</div>';

                } else {
                    var textoCodigo = 'Tu código personal es:</p><p style="font-size: 2em;">' + data.promo_code + '</p><p style="color: gray;">Muéstraselo al cajero</p><p>Se han descontado <b>' + element.points + '</b> estrellas de tu cuenta.';

                    var redeemActions = '<md-button ng-hide="finalizada" class="md-primary md-raised loginBtn" style="font-size: 4vw;width: 90%!important; bottom: 0; left: 0" flex flex-md="100">Beneficio ya utilizado. Tu código es <strong>' + data.promo_code + '</strong></md-button>';
                }

                if (element.ecommerce_url == "") {

                    $rootScope.loading = false;

                    $mdDialog.show({
                        controller: DialogController,
                        clickOutsideToClose: true,
                        template: '<md-dialog><form style="padding: 15px"><md-dialog-content class="noPadding"><div><center><p style="font-size: 1.5em;">Felicitaciones!</p><p><br>Has obtenido el beneficio.<br>' + textoCodigo + '</p><p>Recuerda! La actualización de estrellas puede demorar un par de días.<p/></center></div></md-dialog-content></form></md-dialog>',
                        targetEvent: ev
                    }).then(function(){},function(){

                        $(".redeem-actions").fadeOut("slow", function() {
                            $(".redeem-actions").html(redeemActions).fadeIn("slow");
                        });

                        $rootScope.promosCache[index].redeemed = data.promo_code;
                    });
                    
                } else {

                    $mdDialog.show({
                        controller: DialogController,
                        clickOutsideToClose: true,
                        template: '<md-dialog><form style="padding: 15px"><md-dialog-content class="noPadding"><div><center><p><span style="color: black!important;font-size: 1.5em;">Felicitaciones!</span><br>Has obtenido el beneficio<br>Tu código personal es:<br><span style="font-size: 2em;">' + data.promo_code + '</span></p><md-button hm-tap="clipboard(\'' + data.promo_code + '\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' style="width: 100%!important; margin: 5px 0px; padding:1px" class="md-primary md-raised loginBtn"> Copiar al portapapeles </md-button><a href="' + element.ecommerce_url + '"><md-button style="width: 100%!important; background-color: green!important; margin: 5px 0px; padding:2px" hm-tap="answer(\'canjear\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn"> Ir al E-Shop </md-button></a><md-button style="width: 100%!important; margin: 5px 0px; padding:2px" hm-tap="answer(\'cancelar\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn md-warn"> Cancelar </md-button><p>Se han deducido <b>' + element.points + '</b> puntos de tu cuenta.</p></center></div></md-dialog-content></form></md-dialog>',
                        targetEvent: ev
                    }).then(function(){},function(){

                        $(".redeem-actions").fadeOut("slow", function() {
                            $(".redeem-actions").html(redeemActions).fadeIn("slow");
                        });

                        $rootScope.promosCache[index].redeemed = "";
                    });

                    $cordovaSQLite.execute($rootScope.db, "INSERT INTO logs (log) VALUES ('" + '{"fecha":"' + $rootScope.NOW + '","IP":"' + $rootScope.IP + '", "lat:"' + $rootScope.lat + '", "lng":"' + $rootScope.lng + '","accion":"canjeo","valor":"' + element.id + '","descripcion":"' + element.name + '","codigo":"' + element.code + '","puntos":"' + element.points + '"},' + "')", []).then(function(res) {}, function(err) {
                        console.error(err);
                    });
                }

            } else {

                $rootScope.loading = false;

                var error_text = data.exceptionmessage == "You don't have enough points to redeem this promo: (api.promos.redeem)" ? "No te alcanzan los puntos para conseguir esta promo." : "Puede que no haya conexión. Intentá más tarde";

                error_text = data.exceptionmessage == "Promotion has no codes left: (api.promos.redeem)" ? "Beneficio agotado." : error_text;

                $mdDialog.show({
                    controller: DialogController,
                    template: '<md-dialog><form style="padding: 15px"> <md-dialog-content class="noPadding"> <div> <center><p style="font-size: 1.5em;">Ups! Ocurrió un error.</p><p>' + error_text + '</p></center></div></md-dialog-content> </form></md-dialog>',
                    targetEvent: ev,
                    clickOutsideToClose: true
                });
            }
        });

        res.error(function(data, status, headers, config) {
            $rootScope.loading = false;
            $rootScope.checkConnection(function() { setTimeout(redeem, 3000) }, "Reintentando conseguir beneficio...");
        });
    }

    $scope.shareTwitter = function() {
        $cordovaSocialSharing.shareViaTwitter(_nombre + '-' + _description.replace(/<(?:.|\n)*?>/gm, '') + 'Contenido compartido desde Grisino Fan App.', _nombre, [], [], [], source)
            .then(function(result) {
                $rootScope.loading = false;
                console.log(result);
                var _thelog = {
                    "fecha": '"' + $rootScope.NOW() + '"',
                    "IP": '"' + $rootScope.IP + '"',
                    "lat": '"' + $rootScope.lat + '"',
                    "lng": '"' + $rootScope.lng + '"',
                    "accion": "compartio en Twitter",
                    "id": '"' + element.id + '"'
                }
                var _log = JSON.stringify(_thelog) + ",";
                var query = "INSERT INTO logs (log) VALUES ('" + _log + "')";
                $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {}, function(err) {
                    console.error(err);
                });

            }, function(err) {
                $rootScope.loading = false;
                console.log(err);
                $mdToast.show(
                    $mdToast.simple()
                    .content('No se encontró la aplicacion Twitter')
                    .position('top')
                    .hideDelay(5000)
                );
            });
    }

    function getBase64Image(img) {
        var canvas = document.createElement("canvas");
        canvas.width = img.naturalWidth;
        canvas.height = img.naturalHeight;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        //return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
        return dataURL;
    }

    $scope.shareInstagram = function() {
        $rootScope.loading = true;

        /*TODO: element.img*/
        Instagram.share(getBase64Image(document.getElementById(element._id)), _nombre, function(err) {
            if (err) {
                //console.log("not shared");
                $rootScope.loading = false;

                $mdToast.show(
                    $mdToast.simple()
                    .content('No se compartió en Instagram')
                    .position('top')
                    .hideDelay(5000)
                );

            } else {

                $rootScope.loading = false;
                //console.log("shared");
                var _thelog = {
                    "fecha": '"' + $rootScope.NOW() + '"',
                    "IP": '"' + $rootScope.IP + '"',
                    "lat": '"' + $rootScope.lat + '"',
                    "lng": '"' + $rootScope.lng + '"',
                    "accion": "compartio en Instagram",
                    "id": '"' + element.id + '"'
                }
                var _log = JSON.stringify(_thelog) + ",";
                var query = "INSERT INTO logs (log) VALUES ('" + _log + "')";
                $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {}, function(err) {
                    console.error(err);
                });
            }
        });
    }

    $scope.shareFacebook = function() {

        $rootScope.loading = true;

        $cordovaFacebook.getLoginStatus()
            .then(function(success) {

                comparteFace();

                console.log(success);

            }, function(error) {
                // error facebookStatusLogin
                console.log(error);
                $cordovaFacebook.login(["public_profile", "email", "user_friends"])
                    .then(function(success) {
                        comparteFace();
                    }, function(error) {
                        // error
                        $rootScope.loading = false;
                    });
            });
    }


    function comparteFace() {
        /*TODO: element.img*/
        var options = {
            method: "feed",
            picture: source,
            name: _nombre,
            link: "http://www.grisino.com/",
            caption: _description.replace(/<(?:.|\n)*?>/gm, '')
        };

        $cordovaFacebook.showDialog(options)
            .then(function(success) {
                $rootScope.loading = false;

                var _thelog = {
                    "fecha": '"' + $rootScope.NOW() + '"',
                    "IP": '"' + $rootScope.IP + '"',
                    "lat": '"' + $rootScope.lat + '"',
                    "lng": '"' + $rootScope.lng + '"',
                    "accion": "compartio en Facebook",
                    "id": '"' + element.id + '"'
                }
                var _log = JSON.stringify(_thelog) + ",";
                var query = "INSERT INTO logs (log) VALUES ('" + _log + "')";
                $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {}, function(err) {
                    console.error(err);
                });

            }, function(error) {
                $rootScope.loading = false;

                if (error == "User cancelled dialog") {
                    $mdToast.show(
                        $mdToast.simple()
                        .content('No se compartió en Facebook')
                        .position('top')
                        .hideDelay(5000)
                    );
                } else if (error == "No active session") {
                    $cordovaFacebook.login(["public_profile", "email", "user_friends"])
                        .then(function(success) {
                            comparteFace();
                        }, function(error) {
                            $rootScope.loading = false;
                        });
                }

                console.error(error);

            });
    }

}]);

function DialogController($scope, $mdDialog, $cordovaInAppBrowser) {
    $scope.hide = function() {
        $mdDialog.hide();
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
}
