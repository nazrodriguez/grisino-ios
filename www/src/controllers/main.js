'use strict';

var Main = angular.module('Main', []);

Main.controller('mainCtrl', ['$scope', '$rootScope', '$location', '$timeout', '$mdSidenav', '$log', 'nowTime', '$mdDialog', '$cordovaFacebook', '$cordovaInAppBrowser', '$cordovaSQLite', function($scope, $rootScope, $location, $timeout, $mdSidenav, $log, nowTime, $mdDialog, $cordovaFacebook, $cordovaInAppBrowser, $cordovaSQLite) {

    $scope.tooltip = false;
    $(".tooltip").slideUp(100);
    $scope.toggleTooltip = function(){

        if($scope.tooltip == "showing")
            return;

        $scope.tooltip = "showing";
        $(".tooltip").slideDown(200);

        setTimeout(function(){
            $scope.tooltip = false;
            $(".tooltip").slideUp(200);
        }, 4000);
    }
    
    $cordovaSQLite.execute($rootScope.db, "INSERT INTO config (content) VALUES (?)", [$rootScope.config]).then(function(res) {}, function(err) {});

    if ($rootScope.user) {
        if (($rootScope.user.facebook.id == '' || $rootScope.user.facebook.id == undefined) && ($rootScope.user.facebookId == "" || $rootScope.user.facebookId == undefined || $rootScope.user.facebookId == "undefined")) {
            $scope.profileImage = "assets/images/empty_user.png";
        } else {
            if (!$rootScope.user.facebookUserImage || !$rootScope.user.facebookUserImageBig) {
                $rootScope.user.facebookUserImage = "https://graph.facebook.com/" + $rootScope.user.facebook.id + "/picture?width=100&height=100";
                $rootScope.user.facebookUserImageBig = "https://graph.facebook.com/" + $rootScope.user.facebook.id + "/picture?width=" + $("#mainView").width();
            }
            $scope.profileImage = $rootScope.user.facebookUserImage;
        }
    }

    $scope.setProfileImage = function(source) {
        $scope.profileImage = source;
    }

    $scope.setBackProfileImage = function(source) {
        $scope.backProfileImage = source;
    }

    $scope.getBackProfileImage = function() {
        return $scope.backProfileImage;
    }

    $scope.getProfileImage = function() {
        return $scope.profileImage;
    }

    $scope.goPath = function(p) {
        //If there are no changes in profile
        if (!$rootScope.thereAreChanges) {
            if ($rootScope.states[$rootScope.states.length - 1] == "main.newsTypes" && p == "/main/newsTypes") {} else if ($rootScope.states[$rootScope.states.length - 1] == "main.promosTypes" && p == "/main/promosTypes") {} else {
                $location.path(p);
            }

        } else if ($rootScope.states[$rootScope.states.length - 1] == "main.profile") {
            var confirm = $mdDialog.confirm()
                .title('Cambios en perfil')
                .content('Hay cambios sin guardar. ¿Qué desea hacer?')
                .ariaLabel('Lucky day')
                .ok('Guardar')
                .cancel('Descartar cambios')
                .targetEvent();

            $mdDialog.show(confirm).then(function() {
                $rootScope.updateUser(function() {
                    if ($rootScope.states[$rootScope.states.length - 1] == "main.newsTypes" && p == "/main/newsTypes") {} else if ($rootScope.states[$rootScope.states.length - 1] == "main.promosTypes" && p == "/main/promosTypes") {} else {
                        $location.path(p);
                    }
                });

            }, function() {
                if ($rootScope.states[$rootScope.states.length - 1] == "main.newsTypes" && p == "/main/newsTypes") {} else if ($rootScope.states[$rootScope.states.length - 1] == "main.promosTypes" && p == "/main/promosTypes") {} else {
                    $location.path(p);
                }
                $rootScope.restoreData();
            });
        }
    }

    $scope.sections = [
        { 'id': 'home', 'label': 'DESTACADO', 'htmlName': 'main.home', 'iconClass': 'IconoHome' },
        { 'id': 'promos', 'label': 'PROMOCIONES', 'htmlName': 'main.promosTypes', 'iconClass': 'IconoPromos' },
        { 'id': 'news', 'label': 'LO NUEVO!', 'htmlName': 'main.newsTypes', 'iconClass': 'IconoNews' },
        { 'id': 'camera', 'label': 'STICKERS', 'htmlName': 'main.camera', 'iconClass': 'IconoCamera' },
        { 'id': 'diversion', 'label': 'DIVERSIÓN', 'htmlName': 'main.diversion', 'iconClass': 'IconoJuegos' },
        { 'id': 'places', 'label': 'LOCALES', 'htmlName': 'main.shopsTypes', 'iconClass': 'IconoShops' },
        { 'id': 'nav', 'label': 'E-SHOP', 'htmlName': 'main.nav', 'iconClass': 'IconoCarta' },
        { 'id': 'pools', 'label': 'AYUDANOS A MEJORAR', 'htmlName': 'main.pools', 'iconClass': 'IconoMisiones' },
        { 'id': 'contacto', 'label': 'CONTACTO', 'htmlName': 'main.datosContacto', 'iconClass': 'IconoMensajes' },
        { 'id': 'profile', 'label': 'MI PERFIL', 'htmlName': 'main.profile', 'iconClass': 'IconoNombre' },
        { 'id': 'notifications', 'label': 'NOTIFICACIONES', 'htmlName': 'main.notifications', 'iconClass': 'IconoNotifications' },
        { 'id': 'faq', 'label': 'PREGUNTAS FRECUENTES', 'htmlName': 'main.faq', 'iconClass': 'IconoFAQ' }
    ];

    $scope.back = function() {

        var location = $rootScope.states[$rootScope.states.length - 2].split(".");
        var lFormatted = "/" + location[0] + "/" + location[1];

        $location.path(lFormatted);
    }

    $rootScope.categoryClick = function(name) {

        if ($mdSidenav('left').isOpen()) {
            $mdSidenav('left').toggle();
        }

        setTimeout(function() {
            //if (name != $rootScope.states[$rootScope.states.length - 1]) {
                //$rootScope.loading = true;
            //}
            if(name.split(".")[1] != 'profile' || !$rootScope.isGuest)
                $location.path("/" + name.replace(".", "/"));

            if (name.split(".")[1] == 'home') {
                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:201};
                $rootScope.addLog(_thelog);

                $rootScope.btnIsMenu = false;
            }

            if (name.split(".")[1] == 'notifications') {
                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:209};
                $rootScope.addLog(_thelog);

                $rootScope.btnIsMenu = false;
            }

            if (name.split(".")[1] == 'nav') {

                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:205};
                $rootScope.addLog(_thelog);

                var ref = cordova.ThemeableBrowser.open('http://www.grisino.com/grisino.html?utm_source=fanapp&utm_medium=app&utm_campaign=grisino%20fan', '_blank', IN_APP_THEMEABLE_BROWSER_OPTIONS);
                ref.addEventListener('closePressed', function(event) {
                    $rootScope.loading = false;
                    $rootScope.isInAppBrowserOpen = false;
                }).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function(event) {
                    $rootScope.loading = false;
                    $rootScope.isInAppBrowserOpen = false;
                });
            }

            // FROM ANNOYING MESSAGE IN SIDENAV
            if (name.split(".")[1] == 'grisinofan') {

                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:901};
                $rootScope.addLog(_thelog);

                var ref = cordova.ThemeableBrowser.open('http://www.grisino.com/quierosergrisinofan', '_blank', IN_APP_THEMEABLE_BROWSER_OPTIONS);
                ref.addEventListener('closePressed', function(event) {
                    $rootScope.loading = false;
                    $rootScope.isInAppBrowserOpen = false;
                }).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function(event) {
                    $rootScope.loading = false;
                    $rootScope.isInAppBrowserOpen = false;
                });
            }

            if (name.split(".")[1] == 'diversion') {

                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:900};
                $rootScope.addLog(_thelog);

                var ref = cordova.ThemeableBrowser.open('http://www.reachout.global/Grisino/juegos/', '_blank', IN_APP_THEMEABLE_BROWSER_OPTIONS);

                ref.addEventListener('closePressed', function(event) {
                    $rootScope.loading = false;
                    $rootScope.isInAppBrowserOpen = false;
                }).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function(event) {
                    $rootScope.loading = false;
                    $rootScope.isInAppBrowserOpen = false;
                });
            }

            if (name.split(".")[1] == 'faq') {

                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:210};
                $rootScope.addLog(_thelog);

                var ref = cordova.ThemeableBrowser.open('http://www.reachout.global/Grisino/faq', '_blank', IN_APP_THEMEABLE_BROWSER_OPTIONS);

                ref.addEventListener('closePressed', function(event) {
                    $rootScope.loading = false;
                    $rootScope.isInAppBrowserOpen = false;
                }).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function(event) {
                    $rootScope.loading = false;
                    $rootScope.isInAppBrowserOpen = false;
                });
            }

            if (name.split(".")[1] == 'pools') {

                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:206};
                $rootScope.addLog(_thelog);

                $rootScope.loading = false;
                $mdDialog.show({
                    controller: PollController,
                    controllerAs: 'ctrl',
                    template: '<md-dialog style="padding: 15px;"><md-button class="md-icon-button" style="background-image: url(assets/images/cerrar2.png)" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\'></md-button><div style="padding:10px;">Valoramos mucho tus comentarios acerca de la app, podés enviarnos tu mensaje, y si tenés un momento, por favor, contestá nuestra Encuesta de Satisfacción. Gracias por tu tiempo.<br><div layout="column"><md-button style="width: 100%!important; margin: 5px 0px; padding: 0px" hm-tap="mensaje()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn md-warn"> Enviar mensaje </md-button><md-button style="width: 100%!important; margin: 5px 0px; padding: 0px" hm-tap="encuesta()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn md-warn"> Encuesta de satisfacción </md-button><md-button style="width: 100%!important; margin: 5px 0px; padding: 0px" hm-tap="cancel()" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="md-primary md-raised loginBtn md-warn">Tal vez más tarde</md-button></div></div></md-dialog>',

                    clickOutsideToClose: true
                }).then(function(card) {}, function() {});
            }

            if (name.split(".")[1] == 'promos') {
                $rootScope.btnIsMenu = false;

                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:202};
                $rootScope.addLog(_thelog);

                $rootScope.updateButtonShape();
            }

            if (name.split(".")[1] == 'profile') {
                //$rootScope.btnIsMenu = false;

                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:208};
                $rootScope.addLog(_thelog);

                if($rootScope.isGuest){
                    console.log("A GUEST IS TRYING TO ACCESS THE PROFILE. FUCKING STOP HIM, MAN.")
                    $rootScope.guestBlock();
                } else {
                    console.log("Isn´t guest.");
                }

                //$rootScope.updateButtonShape();
            }

            if (name.split(".")[1] == 'news') {
                $rootScope.btnIsMenu = false;

                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:203};
                $rootScope.addLog(_thelog);

                $rootScope.updateButtonShape();
            }

            if(name.split(".")[1] == 'contacto'){
                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:207};
                $rootScope.addLog(_thelog);
            }

            if (name.split(".")[1] == 'places') {
                $rootScope.btnIsMenu = false;

                var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:204};
                $rootScope.addLog(_thelog);

                $rootScope.updateButtonShape();
            }
            $rootScope.MenuOpen = false;
        }, 300);
    }


    $scope.buttonClick = function() {
        //console.log('abre');
        var states = $rootScope.states;
        var index = states.length - 1;

        var current = states[index];
        var previous = states[index - 1];

        if (current != 'main.detail') {

            //If there are no changes in profile
            if (!$rootScope.thereAreChanges) {
                setTimeout(function() {
                    if (!$mdSidenav('left').isOpen())
                        $mdSidenav('left').toggle();

                    $rootScope.MenuOpen = true;
                }, 100);
            } else if (current == "main.profile" && !$scope.hasRestored) {
                var confirm = $mdDialog.confirm()
                    .title('Cambios en perfil')
                    .content('Hay cambios sin guardar. ¿Qué desea hacer?')
                    .ariaLabel('Lucky day')
                    .ok('Guardar')
                    .cancel('Descartar cambios')
                    .targetEvent();

                $mdDialog.show(confirm).then(function() {
                    $rootScope.updateUser(function() {
                        setTimeout(function() {
                            if (!$mdSidenav('left').isOpen()) {
                                $mdSidenav('left').toggle();
                            }
                            $rootScope.MenuOpen = true;
                        }, 100);
                        $scope.hasRestored = false;
                    });

                }, function() {
                    setTimeout(function() {
                        if (!$mdSidenav('left').isOpen()) {
                            $mdSidenav('left').toggle();
                        }
                        $rootScope.MenuOpen = true;
                    }, 100);
                    $scope.hasRestored = true;
                    $rootScope.restoreData();
                });
            } else {
                setTimeout(function() {
                    if (!$mdSidenav('left').isOpen()) {
                        $mdSidenav('left').toggle();
                    }
                    $rootScope.MenuOpen = true;
                }, 100);
                $rootScope.thereAreChanges = false;
                $scope.hasRestored = false;
            }
        } else {

            $rootScope.btnIsMenu = previous != "main.detail";
            $scope.back();

            $rootScope.updateButtonShape();

        }
    }

    /********************************** TOGGLE MENU CLOSE ********************************/

    $scope.close = function() {
        $mdSidenav('left').toggle();
        $rootScope.MenuOpen = false;
    };


    $rootScope.logout = function(ev) {
        var _thelog = {fecha:$rootScope.NOW(),IP:$rootScope.IP,lat:parseFloat($rootScope.lat),lng:parseFloat($rootScope.lng),id:212};
        $rootScope.addLog(_thelog);

        $mdSidenav('left').toggle();
        setTimeout(function() {
            var confirm = $mdDialog.confirm()
            .title('Cerrar sesión y salir')
            .content('¿Seguro que desea cerrar la sesión? Se cerraría la aplicación.')
            .ariaLabel('Lucky day')
            .ok('Cerrar sesión')
            .cancel('Volver')
            .targetEvent(ev);
            $mdDialog.show(confirm).then(function() {

                if ($rootScope.user.facebookId != "") 
                    $cordovaFacebook.logout().then(function(success) {}, function(error) {});
                
                if(TOKEN_DEBUG){
                    $rootScope.dni = undefined;
                    $rootScope.user = undefined;
                    $rootScope.stars = undefined;
                    $rootScope.borraTablas(true);
                    TOKEN = undefined;
                } else {
                    Playtomic.Auth.logout(function(response) {
                        $rootScope.dni = undefined;
                        $rootScope.user = undefined;
                        $rootScope.stars = undefined;
                        $rootScope.borraTablas(true);
                    });
                }
            }, function() {});
        }, 500);
    };

}]);

function PollController($rootScope, $mdDialog, $scope, $cordovaInAppBrowser) {

    $scope.hide = function() {
        $mdDialog.hide();
        $rootScope.loading = false;
    };

    $scope.accept = function(ev) {
        $mdDialog.hide();
        $rootScope.loading = false;
    };

    $scope.cancel = function() {
        $mdDialog.hide();
        $rootScope.loading = false;
    };

    $scope.mensaje = function() {
        console.log(IN_APP_THEMEABLE_BROWSER_OPTIONS);

        var ref = cordova.ThemeableBrowser.open(('https://feedback.reachout.global/grisino/?userId=' + $rootScope.user.id + '&email=' + $rootScope.user.email + '&device=iOS' + $rootScope.getOSVersion()), '_blank', IN_APP_THEMEABLE_BROWSER_OPTIONS);

        ref.addEventListener('closePressed', function(event) {
            $rootScope.loading = false;
            $rootScope.isInAppBrowserOpen = false;
        }).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function(event) {
            $rootScope.loading = false;
            $rootScope.isInAppBrowserOpen = false;
        });

        $rootScope.loading = false;
    }

    $scope.encuesta = function() {

        var ref = cordova.ThemeableBrowser.open('https://adminrog.trk.org/GrisinoEncuesta01?email=' + $rootScope.user.email, '_blank', IN_APP_THEMEABLE_BROWSER_OPTIONS);

        ref.addEventListener('closePressed', function(event) {
            $rootScope.loading = false;
            $rootScope.isInAppBrowserOpen = false;
        }).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function(event) {
            $rootScope.loading = false;
            $rootScope.isInAppBrowserOpen = false;
        });
      
        $rootScope.loading = false;
    }

}
