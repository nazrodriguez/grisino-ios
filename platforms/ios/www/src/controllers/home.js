var Home = angular.module('Home', []);

Home.controller('homeCtrl', ['$scope', '$location', '$rootScope', '$compile', '$timeout', '$cordovaFacebook', '$q', '$mdToast', function($scope, $location, $rootScope, $compile, $timeout, $cordovaFacebook, $q, $mdToast) {

    window.ga.trackView("Home-Featured");

    //Check to see if it was indeed updated
    $rootScope.getVersion();

    //En join y en main panel, falta en join
    $rootScope.getStars();

    $("body").removeClass("backgrounded");

    angular.element(document).ready(setTimeout(function() {

        //Sigue en la screen del presente controlador
        if ($rootScope.states && $rootScope.states[$rootScope.states.length - 1] == "main.home") {
            $rootScope.loading = false;
            $rootScope.loadingInmediate = false;

            var iscroll = new ISCROLL("#listWrapper", function(elem) {

                var piece;
                var id = elem._id;

                if (elem.type == "promos" || elem.type == "mobile") {
                    var source = elem.images && elem.images.length ? elem.images[0].url : "https://s3-sa-east-1.amazonaws.com/iloveit/promo/" + id + ".jpg";
                } else if (elem.type == "video") {
                    var sampleUrl = elem.fields.youtubeid;
                    var video_id = sampleUrl;
                    var source = "https://www.youtube.com/embed/" + video_id;
                } else if (elem.type == undefined) {
                    var source = elem.images && elem.images.length ? elem.images[0].url : "https://s3-sa-east-1.amazonaws.com/iloveit/shop/" + id + ".jpg";
                } else {
                    var source = elem.images && elem.images.length ? elem.images[0].url : "https://s3-sa-east-1.amazonaws.com/iloveit/content/" + id + ".jpg";
                }

                if (elem.type == "news" || elem.type == "trivia" || elem.type == "rss" || elem.type == "lookbook" || elem.type == "image" || elem.type == "product") {

                    var _laDes = elem.description.replace(/'/g, "\\'");

                    piece = '<li hm-tap="_detail(\'' + id + '\', \'news\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="newsPiece" layout="column"> <div class="imageWrapper defaultImage" style="height:' + ($(document).width() - 16) + 'px!important;"><img src="' + source + '" id="' + elem._id + '" onload="this.style.opacity=1;" class="newsElement"></div><div class="newsElementBottomWrapper"><span class="newsElementTitle">' + elem.title + '</span><div class="newsElementDescription">' + _laDes + '</div> </div></li>';

                } else if (elem.type == "promos" || elem.type == "mobile" || elem.type == "promos") {

                    var _laDes = elem.description_text.replace(/'/g, "\\'");
                    var haver = '';

                    if ($rootScope.lat) {
                        if (elem.audience.shops.length > 0) {
                            var minDistance;
                            var index;
                            for (var i = 0; i < elem.audience.shops.length; i++) {
                                if (elem.audience.shops[i] == null)
                                    continue;

                                if (!minDistance || haversine(elem.audience.shops[i].map.lat, elem.audience.shops[i].map.lng, $rootScope.lat, $rootScope.lng).number < minDistance) {
                                    minDistance = haversine(elem.audience.shops[i].map.lat, elem.audience.shops[i].map.lng, $rootScope.lat, $rootScope.lng).number;
                                    index = i;
                                }
                            }

                            if(index != undefined){
                                var h = haversine(elem.audience.shops[index].map.lat, elem.audience.shops[index].map.lng, $rootScope.lat, $rootScope.lng);
                                
                                if(h.number > 1000){
                                    haver = '';
                                } else if(h.number > 500){
                                    haver = "¡Estás cerca!";
                                } else {
                                    haver = "(En un local a " + h.str + " de acá!)";
                                }
                            }

                        } else {
                            if(elem.triggers.hotspot_data){
                                var h = haversine(elem.triggers.hotspot_data.lat, elem.triggers.hotspot_data.lng, $rootScope.lat, $rootScope.lng);

                                if(h.number > 1000){
                                    haver = '';
                                } else if(h.number > 500){
                                    haver = "¡Estás cerca!";
                                } else {
                                    haver = "(" + h.str + ")";
                                }
                                
                            } else {
                                haver = "";
                            }
                        }
                    }

                    var _ribbon = "";

                    if ($rootScope.NOW() > new Date(elem.end_date).toISOString())
                        _ribbon = "ribbon";

                    var isRelevant = (elem.category.indexOf('promo-dest') != -1) ? 'relevant' : '';

                    piece = '<li hm-tap="_detail(\'' + elem.id + '\', \'promo\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="newsPiece" layout="column"> <div id="content' + elem.id + '" class="imageWrapper ' + isRelevant + ' defaultImage" style=" height:' + ($(document).width() - 16) + 'px!important;background-size: 80px; background-image: url(\'assets/images/beneficios.png\')"><div class="' + _ribbon + '"></div><div class="imageDarkerer imageClickHandler"></div><img src="' + source + '" id="' + elem._id + '" onload="this.style.opacity=1;" class="newsElement"></img></div> <div class="newsElementBottomWrapper ' + isRelevant + '"><span class="newsElementTitle">' + elem.description_title + " " + haver + '</span><div class="newsElementDescription">' + _laDes + '</div></div></li>';


                } else if (elem.type == undefined || elem.type == "shops") {

                    var _laDes = elem.info.replace(/'/g, "\\'");

                    piece = '<li class="newsPiece" layout="column"><div hm-tap="_detail(\'' + elem.id + '\', \'shop\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' style="height:' + ($(document).width() - 16) + 'px!important;background-image: url(\'assets/images/LinkMapa.png\')" class="imageWrapper defaultImage"><img src="' + source + '" id="' + elem._id + '" onload="this.style.opacity=1;" class="newsElement"></img></div><div class="newsElementBottomWrapper"> <span class="newsElementTitle">' + elem.name + '</span><div class="newsElementDescription">' + _laDes + '</div></div></li>'

                } else if (elem.type == "video") {

                    var _laDes = elem.description.replace(/'/g, "\\'");

                    piece = '<li class="newsPiece" layout="column"><div style="height:' + ($(document).width() - 16) + 'px!important;" class="imageWrapper defaultImage"><iframe src="' + source + '?rel=0" frameBorder="0" style="height: 100%; width: 100%;" ></iframe><div hm-tap="_startVideo($event)" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' style="position:absolute; top:0;left:0;right:0;bottom:0;background:transparent;text-align:center;vertical-align:middle;font-size: 20vw;color:white;"></div></div><div class="newsElementBottomWrapper no-padding"> <span class="newsElementTitle">' + elem.title + '</span><div class="newsElementDescription">' + _laDes + '</div></div></li>';

                } else {
                    console.log("El tipo de elemento " + elem.type + " no tiene un template designado.");
                    piece = ('<li class="newsPiece"></li>');
                }

                //piece = '<li style="height:200px; padding: 100px; width: 100% border: 1px solid red;">TEST LI</li>';

                return piece;
            }, function() {
                var filtered = [],
                    aux = [];
                var coords = { lat: $rootScope.lat, lng: $rootScope.lng };

                //PROMOS DESTACADAS CON GEO
                var geopromos = $rootScope.promosCache.filter(function(elem) {
                    //Los que tienen geo
                    return (elem.triggers.hotspot_data || elem.audience.shops.length > 0) && elem.category.indexOf("promo-dest") != -1;
                }).sort(function(a, b) { //ORDENADAS POR GEO
                    var distanceA = -1;
                    var distanceB = -1;

                    if (a.audience.shops.length > 0) {
                        var minDistanceA;
                        var indexA;

                        for (var i = 0; i < a.audience.shops.length; i++) {
                            if (a.audience.shops[i] == null || !a.audience.shops[i].map.lat || a.audience.shops[i].map.lat == null) {
                                continue;
                            }

                            if (!minDistanceA || haversine(a.audience.shops[i].map.lat, a.audience.shops[i].map.lng, $rootScope.lat, $rootScope.lng).number < minDistanceA) {
                                minDistanceA = haversine(a.audience.shops[i].map.lat, a.audience.shops[i].map.lng, $rootScope.lat, $rootScope.lng).number;
                                indexA = i;
                            }
                        }

                        if (indexA != undefined)
                            distanceA = haversine(coords.lat, coords.lng, a.audience.shops[indexA].map.lat, a.audience.shops[indexA].map.lng).number;

                    } else {
                        distanceA = haversine(coords.lat, coords.lng, a.triggers.hotspot_data.lat, a.triggers.hotspot_data.lng).number;
                    }

                    if (b.audience.shops.length > 0) {
                        var minDistanceB;
                        var indexB;
                        for (var i = 0; i < b.audience.shops.length; i++) {
                            if (b.audience.shops[i] == null || !b.audience.shops[i].map.lat || b.audience.shops[i].map.lat == null) {
                                continue;
                            }

                            if (!minDistanceB || haversine(b.audience.shops[i].map.lat, b.audience.shops[i].map.lng, $rootScope.lat, $rootScope.lng).number < minDistanceB) {
                                minDistanceB = haversine(b.audience.shops[i].map.lat, b.audience.shops[i].map.lng, $rootScope.lat, $rootScope.lng).number;
                                indexB = i;
                            }
                        }

                        if (indexB != undefined) {
                            console.log(indexB);
                            distanceB = haversine(coords.lat, coords.lng, b.audience.shops[indexB].map.lat, b.audience.shops[indexB].map.lng).number;
                        }

                    } else {
                        distanceB = haversine(coords.lat, coords.lng, b.triggers.hotspot_data.lat, b.triggers.hotspot_data.lng).number;
                    }

                    //Mark as irrelevant
                    a.irrelevant = distanceA > 50000 ? true : undefined;
                    b.irrelevant = distanceB > 50000 ? true : undefined;

                    return distanceA - distanceB;
                }).filter(function(promo) {
                    return promo.irrelevant == undefined;
                });

                //PROMOS DESTACADAS POR FECHA
                var nongeopromos = $rootScope.promosCache.filter(function(elem) {
                    //Los que no tienen geo
                    return !elem.triggers.hotspot_data && !elem.audience.shops.length && elem.category.indexOf("promo-dest") != -1;
                }).sort(function(a, b) { //ORDENADAS POR FECHA
                    //Por fecha
                    return new Date(a.start_date) > new Date(b.start_date) ? 1 : -1;
                });

                //NOTICIAS DESTACADAS
                var nongeonews = $rootScope.contentCache.filter(function(elem) {
                    return elem.category.indexOf("content-dest") != -1;
                }).sort(function(a, b) { //ORDENADAS POR FECHA
                    //Por fecha
                    return new Date(a.start_date) > new Date(b.start_date);
                });

                //ORDENAMIENTO DE BLOQUES
                filtered = nongeopromos.concat(nongeonews).concat(geopromos).filter(function(elem){
                    return $rootScope.NOW() > new Date(elem.start_date).toISOString()
                });

                return filtered;
            }, function(c) {
                $rootScope.updateCallback = c;
                $rootScope.actualizamosCache();
            }, function(s) {
                return $compile(s)($scope);
            });
        }

        $scope._detail = function(id, type) {
            if (!iscroll.isScrolling())
                $rootScope.detail(id, type);
        }

        $scope._startVideo = function(event){
            if (!iscroll.isScrolling())
                $rootScope.startVideo(event);
        }

        /*cordova.ThemeableBrowser.open('http://apache.org', '_blank', {
            statusbar: {
                color: '#ffffff'
            },

            toolbar: {
                height: 44,
                color: '#f0f0f0'
            },

            title: {
                color: '#003264',
                showPageTitle: true
            },

            forwardButton: {
                image: 'forward',
                imagePressed: 'forward_pressed',
                align: 'left',
                event: 'forwardPressed'
            },

            closeButton: {
                image: 'close',
                imagePressed: 'close_pressed',
                align: 'left',
                event: 'closePressed'
            },

            menu: {
                image: 'menu',
                imagePressed: 'menu_pressed',
                title: 'Test',
                cancel: 'Cancel',
                align: 'right',
                items: [
                    {
                        event: 'helloPressed',
                        label: 'Hello World!'
                    },

                    {
                        event: 'testPressed',
                        label: 'Test!'
                    }
                ]
            },

            backButtonCanClose: true
        }).addEventListener('backPressed', function(e){
            console.log('Bye!');
        }).addEventListener('helloPressed', function(e){
            console.log('Hello!');
        }).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function(e){
            console.log(e.message);
        }).addEventListener(cordova.ThemeableBrowser.EVT_WRN, function(e){
            console.log(e.message);
        });*/

    }, 500));

}]);
