'use strict';

var userDNI = angular.module('userDNI', []);


userDNI.controller('userDNICtrl',  ['$scope', '$rootScope','$location', '$mdDialog','$cordovaSQLite', '$http', function ($scope, $rootScope, $location, $mdDialog, $cordovaSQLite, $http) {
	$('#alertField').hide();
	
	/******************************** VARS DECLARATION ********************************/	
	
	$rootScope.loading=true;	
	
	/******************************** ADD DNI TO PROFILE ********************************/
	$scope.focus=function(){
		$('#alertField').hide();
	}

  var year = new Date().getFullYear();
  $scope.years = [];
  for(var i = year - 90; i < year; i++)
    $scope.years.push(i);	

	$scope.joinDNI=function(ev){

		//$rootScope.loading=true;
		
		var data = {};
			data.brand = "Grisino";
      data.email = $rootScope.user.email;
			//data.national_identity = $scope.dni;
			//$rootScope.user.national_identity = $scope.dni;

    if($scope.g == undefined && $scope.n != undefined && $scope.m != undefined && $scope.w != undefined){
    var confirm = $mdDialog.confirm()        
        .title('Registro por finalizar')
        .content('No ha definido su género, pero podrá hacerlo cuando quiera desde su perfil. ¿Desea finalizar el registro e ingresar con los datos proporcionados?')
        .ariaLabel('Lucky day')
        .ok('Ingresar')
        .cancel('Volver')
        .targetEvent(ev);
      $mdDialog.show(confirm).then(function() {
         //$location.path('/main/home');
      $rootScope.loading=true;
      Playtomic.Users.update(data, function(response) {
        //console.log(response);  
        if(response.user) {         
          $scope.checkTablas();
        }else {
          $rootScope.loading=false;        
        }
      });
      }, function() {
         $location.path('/main/join');   
      });
		} else if($scope.g != undefined && ($scope.n == undefined || $scope.m == undefined || $scope.w == undefined)){
      var confirm = $mdDialog.confirm()        
        .title('Registro por finalizar')
        .content('No ha definido su fecha de nacimiento ( o no completamente ), pero podrá hacerlo cuando quiera desde su perfil. ¿Desea finalizar el registro e ingresar con los datos proporcionados?')
        .ariaLabel('Lucky day')
        .ok('Ingresar')
        .cancel('Volver')
        .targetEvent(ev);
      $mdDialog.show(confirm).then(function() {

         //$location.path('/main/home');
        $rootScope.loading=true;
      Playtomic.Users.update(data, function(response) {
        //console.log(response);  
        if(response.user) {         
          $scope.checkTablas();
        }else {
          $rootScope.loading=false;        
        }
      });

      }, function() {
        $location.path('/main/join');
      });
    } else if($scope.g == undefined && ($scope.n == undefined || $scope.m == undefined || $scope.w == undefined)){
      var confirm = $mdDialog.confirm()        
        .title('Registro por finalizar')
        .content('No ha definido ni su fecha de nacimiento ni su género, pero podrá hacerlo cuando quiera desde su perfil. ¿Desea finalizar el registro e ingresar con los datos proporcionados?')
        .ariaLabel('Lucky day')
        .ok('Ingresar')
        .cancel('Volver')
        .targetEvent(ev);
      $mdDialog.show(confirm).then(function() {

         $rootScope.loading=true;
      Playtomic.Users.update(data, function(response) {
        //console.log(response);  
        if(response.user) {         
          $scope.checkTablas();
        }else {
          $rootScope.loading=false;        
        }
      });

      }, function() {
        $location.path('/main/join');
      });
    } else {
      //$location.path('/main/home');

      $rootScope.loading=true;
      Playtomic.Users.update(data, function(response) {
        //console.log(response);  
        if(response.user) {         
          $scope.checkTablas();
        }else {
          $rootScope.loading=false;        
        }
      });

    }

    /*

		if($scope.dni == undefined){
			$('#alertField').css("display", "inherit");
			$('#alertField').html('DNI Inválido');
      $rootScope.loading=false;					
		} else {
			$rootScope.loading=true;
			Playtomic.Users.update(data, function(response) {
				//console.log(response);	
				if(response.user) {					
					$scope.checkTablas();
				}else {
					$rootScope.loading=false;
					// ERROR
					$('#alertField').css("display", "inherit");
					$('#alertField').html('Invalid DNI');					
				}
			});
		}*/
	}


	$scope.back=function(ev){
		// Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.confirm()        
        .title('Cancelar y salir')
        .content('¿Seguro desea retroceder? Su cuenta ya fue creada, podrá agregar esta información cuando lo desee. Para ingresar con la cuenta creada, toque Aceptar.')
        .ariaLabel('Lucky day')
        .ok('OK')
        .cancel('Cancelar')
        .targetEvent(ev);
      $mdDialog.show(confirm).then(function() {
        //$scope.alert = 'You decided to get rid of your debt.';
         if(navigator.app){
          navigator.app.exitApp();
         } else if (navigator.device){
          navigator.device.exitApp();
         }
      }, function() {
        //$scope.alert = 'You decided to keep your debt.';
      });
	}


	/****************************** CHECK LAS TABLAS *******************************/

  $scope.checkTablas=function(){

      var query = "SELECT * FROM contenidos";

      $cordovaSQLite.execute($rootScope.db, query,[]).then(function(res) {
            if (res.rows.length != 0) {           
                  
                  console.log('cache contenidos populada');
                  
                  for (var i = 0; i < res.rows.length; i++) {
                    $rootScope.contentCache=JSON.parse(res.rows.item(i).content);
                  } 

                  $scope.cachePromos();                  

            }else{
                  console.log('cache contenidos vacia, populamos sqlLitle');
                  $scope.creamosCache();                      
            }
          }, function (err) {
            //console.error(err);
      });               
  }

  $scope.cachePromos=function(){
    var query = "SELECT * FROM promos";
      $cordovaSQLite.execute($rootScope.db, query,[]).then(function(res) {
            if (res.rows.length != 0) {
                  
                  for (var i = 0; i < res.rows.length; i++) {
                    $rootScope.promosCache=JSON.parse(res.rows.item(i).content);
                  } 

                  console.log('cache promos populada');
                  $scope.cacheVenues();                     
            }
          }, function (err) {
            //console.error(err);
      });     
  }

  $scope.cacheVenues=function(){
    var query = "SELECT * FROM venues";
      $cordovaSQLite.execute($rootScope.db, query,[]).then(function(res) {
            if (res.rows.length != 0) {
                  
                  for (var i = 0; i < res.rows.length; i++) {
                    $rootScope.venuesCache=JSON.parse(res.rows.item(i).content);
                  } 

                  console.log('cache venues populada');

                  /***** INGRESA AL HOME *************/
                 $('#succesField').css("display", "inherit");
                 $location.path("/userDNI");
                 /***** INGRESA AL HOME *************/  
            }
          }, function (err) {
            //console.error(err);
      });        
  }

  /****************************** CREAMOS LAS TABLAS CACHE *******************************/

  $scope.creamosCache=function(){
          
      var data = {};
      data.type = '';
      data.category = '';
      
      /******************************** CONTENT LIST CHARGE ********************************/
      
      Playtomic.Contents.list(data, function(response) {

          //console.log('Contenidos '+JSON.stringify(response, null, 4));

          var _fecha=response.contents[0].register_date;
          $rootScope.contentCache=response.contents;              
          var _contenidos=JSON.stringify(response.contents, null, 4);

          if(response.contents) {

              var query = "INSERT INTO contenidos (fecha, content) VALUES (?,?)";

              $cordovaSQLite.execute($rootScope.db, query,[_fecha,_contenidos]).then(function(res) {
                    console.log('Populada Contenidos');
                    $scope.promoList();
                  }, function (err) {
                    console.log('error en inserción '+ JSON.stringify(err, null, 4));
              });

          } else {

          }       
      });


      /******************************** PROMOS LIST CHARGE ********************************/
      $scope.promoList=function(){
        var data = {};
      
        Playtomic.Promos.list(data, function(response) {
            
            //console.log('Promos '+JSON.stringify(response, null, 4));

            var _fecha=response.promos[0].register_date;
            $rootScope.promosCache=response.promos;                
            var _promociones=JSON.stringify(response.promos, null, 4);
            //console.log($rootScope.promoType);
            if(response.promos) {
                var query = "INSERT INTO promos (fecha, content) VALUES (?,?)";

                $cordovaSQLite.execute($rootScope.db, query,[_fecha,_promociones]).then(function(res) {
                      console.log('Populada Promociones');                      
                      $scope.categoriasList();
                    }, function (err) {
                      console.log('error en inserción '+ JSON.stringify(err, null, 4));
                });
            } else {
                
            }
        });
      }

      /******************************** CATEGORIAS LIST CHARGE ********************************/
      $scope.categoriasList=function(){

        var dataObj = {
            data : "eyJjYWNoZSI6ZmFsc2UsInNlY3Rpb24iOiJicmFuZHMiLCJhY3Rpb24iOiJsaXN0Q2F0ZWdvcmllcyIsInB1YmxpY2tleSI6IjM1d2t5ZTBlaHZpYWUxNG54dDQ0IiwiYnJhbmQiOiJQdW50byBOYXV0aWNvIn0=",
            hash : "c6f6d8787535516427739bbc904934ab"
        };

        var res = $http.post('http://iloveit.club/api?publickey=35wkye0ehviae14nxt44', dataObj);
        res.success(function(data, status, headers, config) {

          console.log(JSON.stringify(data));
          
          var _fecha="";
          $rootScope.categoriasCache=data;                
          var _categorias=JSON.stringify(data, null, 4);

          var query = "INSERT INTO categorias (fecha, content) VALUES (?,?)";

            $cordovaSQLite.execute($rootScope.db, query,[_fecha,_categorias]).then(function(res) {
                  console.log('Populada Categorias');                      
                  $scope.shopList();
                }, function (err) {
                  console.log('error en inserción '+ JSON.stringify(err, null, 4));
          });
        });

        res.error(function(data, status, headers, config) {
          console.log(JSON.stringify(data));
          //alert( "failure message: " + JSON.stringify({data: data}));
        });
        
      }         

      /******************************** VENUES LIST CHARGE ********************************/

      $scope.shopList=function(){
          var data = {};
          
          Playtomic.Shops.list(data, function(response) {
              
              //console.log('Promos '+JSON.stringify(response, null, 4));

              var _fecha=response.shops.shops[0].register_date;
              $rootScope.venuesCache=response.shops.shops;             
              var _venues=JSON.stringify(response.shops.shops, null, 4);
              //console.log($rootScope.promoType);
              if(response.shops) {
                  
                  var query = "INSERT INTO venues (fecha, content) VALUES (?,?)";

                  $cordovaSQLite.execute($rootScope.db, query,[_fecha,_venues]).then(function(res) {
                        console.log('Populada Venues');                        
                        $('#succesField').css("display", "inherit");
                        /********  INIT SERVICE  ***********/
                        /********  INIT SERVICE  ***********/
                        if(serviceRunning){
                        }else{
                          setTimeout( function(){ initService(); }, 20000);
                        }
                        /***** GUARDA CONFIG *************/
                        /***** GUARDA CONFIG *************/

                        var query = "INSERT INTO config (content) VALUES (?)";

                          $cordovaSQLite.execute($rootScope.db, query,[JSON.stringify($rootScope.configOBJ, null, 4)]).then(function(res) {
                            
                            /***** INGRESA AL HOME *************/
                            /***** INGRESA AL HOME *************/

                                $location.path( "/main/home");
                            
                            }, function (err) {
                              console.log('error en guardar config'+ JSON.stringify(err, null, 4));
                        });

                        /***** GUARDA CONFIG *************/
                        /***** GUARDA CONFIG *************/
                        
                      }, function (err) {
                        console.log('error en inserción '+ JSON.stringify(err, null, 4));
                  });
              } else {                  
              }
          });          
      }
    } 

  $rootScope.loading=false;
}]);