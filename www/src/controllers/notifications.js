'use strict';
	
var notifications = angular.module('notifications', []);
	
	
notifications.controller('notificationsCtrl',  ['$scope','$mdDialog', '$rootScope', '$cordovaInAppBrowser', '$cordovaContacts','$mdToast', '$cordovaSQLite', function ($scope, $mdDialog, $rootScope, $cordovaInAppBrowser,$cordovaContacts,$mdToast, $cordovaSQLite) {
	
	$rootScope.loading = false;
	$scope.notificaciones = [];
	var notified;

	window.ga.trackView("Notifications List");

    /*Check if notification was already notified*/
    $cordovaSQLite.execute($rootScope.db, "SELECT * FROM notificaciones", []).then(function(res) {

        if (res.rows.length != 0) {
            var notis = [], promo, content;

            for (var i = 0; i < res.rows.length; i++) {

                if (!res.rows.item(i))
                    continue;
                notified = res.rows.item(i);

                if(notified.geofence != "none")
                	continue;

	            for(var j = 0; j < $rootScope.promosCache.length; j++){
	            	promo = $rootScope.promosCache[j];

	            	if(notified.elementId == promo._id || notified.elementId == promo.id){
		            	if(notis.length < 20 && notis.indexOf(promo._id) == -1){
		            		$scope.notificaciones.push({
		            			img: promo.images && promo.images.length ? promo.images[0].url : "https://s3-sa-east-1.amazonaws.com/iloveit/promo/" + promo.id + ".jpg",
		            			title: promo.description_title,
		            			text: promo.notification_text,
		            			type: "promo",
		            			id: promo.id
		            		})
		            		notis.push(promo._id);
		            	} else if(notis.length > 19){
		            		break;
		            	}
		            } else {
		            	for(var k = 0; k < $rootScope.contentCache.length; k++){
		            		content = $rootScope.contentCache[k];

		            		if(notified.elementId == content._id || notified.elementId == content.id){
		            			if(notis.length < 20 && notis.indexOf(content._id) == -1){
				            		$scope.notificaciones.push({
				            			img: content.images && content.images.length ? content.images[0].url : "https://s3-sa-east-1.amazonaws.com/iloveit/content/" + content._id + ".jpg",
				            			title: content.title,
				            			text: content.notification_text,
				            			type: "news",
				            			id: content._id
				            		})
				            		notis.push(content._id);
				            	}
				            	break;
		            		}
		            	}
		            }
	            }
            }
        } else {
            console.log("Update Notis: " + JSON.stringify(res));
        }

        $scope.notificaciones.reverse();

    }, function(err) {});
}]);