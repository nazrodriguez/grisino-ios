﻿'use strict';

var PromosTypes = angular.module('PromosTypes', []);


PromosTypes.controller('promosTypesCtrl', ['$scope', '$rootScope', '$timeout', '$compile', function($scope, $rootScope, $timeout, $compile) {

    $rootScope.toShow = false;

    $scope.promoArr = $rootScope.promosCache;

    angular.element(document).ready(setTimeout(function() {

        window.ga.trackView("Promo List");

        //Sigue en la screen del presente controlador
        if ($rootScope.states && $rootScope.states[$rootScope.states.length - 1] == "main.promosTypes") {

            $rootScope.loading = false;

            var l = new ISCROLL("#listWrapper",function(elem){

                var id = elem.id;
                var source = elem.images && elem.images.length ? elem.images[0].url : "https://s3-sa-east-1.amazonaws.com/iloveit/promo/" + id + ".jpg";
                var _laDes = elem.description_text.replace(/'/g, "\\'");
                var haver = '';

                if(elem.images && elem.images.length && elem.images[0].url == ""){

                    source = undefined;
                }

                if ($rootScope.lat) {
                    if (elem.audience.shops.length > 0) {
                        var minDistance;
                        var index;
                        for (var i = 0; i < elem.audience.shops.length; i++) {
                            if(elem.audience.shops[i] == null)
                                continue;

                            if (!minDistance || haversine(elem.audience.shops[i].map.lat, elem.audience.shops[i].map.lng, $rootScope.lat, $rootScope.lng).number < minDistance) {
                                minDistance = haversine(elem.audience.shops[i].map.lat, elem.audience.shops[i].map.lng, $rootScope.lat, $rootScope.lng).number;
                                index = i;
                            }
                        }

                        if(index != undefined){
                            var h = haversine(elem.audience.shops[index].map.lat, elem.audience.shops[index].map.lng, $rootScope.lat, $rootScope.lng);
                            
                            if(h.number > 1100){
                                haver = '';
                            } else if(h.number > 500){
                                haver = "¡Estás cerca!";
                            } else {
                                haver = "(" + h.str + ")";
                            }
                        }

                    } else {
                        if(elem.triggers.hotspot_data){
                            var h = haversine(elem.triggers.hotspot_data.lat, elem.triggers.hotspot_data.lng, $rootScope.lat, $rootScope.lng);

                            if(h.number > 1100){
                                haver = '';
                            } else if(h.number > 500){
                                haver = "¡Estás cerca!";
                            } else {
                                haver = "(" + h.str + ")";
                            }
                            
                        } else {
                            haver = "";
                        }
                    }
                }

                var _ribbon = "";
                if ($rootScope.NOW() > new Date(elem.end_date).toISOString()) {
                    _ribbon = "ribbon";
                }

                var isRelevant = (elem.category.indexOf('promo-dest') != -1) ? 'relevant' : '';

                var piece = '<li hm-tap="_detail(\'' + elem.id + '\', \'promo\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="newsPiece" layout="column"> <div hm-tap="_detail(\'' + elem.id + '\', \'promo\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' id="content' + elem.id + '" class="imageWrapper '+ isRelevant +' defaultImage" style="height:' + ($(document).width() - 16) + 'px!important; background-size: 80px; background-image: url(\'assets/images/beneficios.png\')"> <table class="imgDeadLine"> <tr> <td colspan="2" style="text-align: center"> <div class="imgMidText">' + elem.description_title + '</div></td></tr></table><div class="'+_ribbon+'"></div> <div class="imageDarkerer"></div><div class="imageDarkerer imageClickHandler"></div><img ' + (source ? ('src="' + source + '"') : '') + ' id="' + elem._id + '" onload="this.style.opacity=1;" class="newsElement"></img></div> <div class="newsElementBottomWrapper '+ isRelevant +' "><span class="newsElementTitle">' + elem.description_title + " " + haver + '</span><div class="newsElementDescription">' + _laDes + '</div></div></li>';

                return piece;
                
            }, function() {
                var filtered = [],
                    aux = [];
                var coords = { lat: $rootScope.lat, lng: $rootScope.lng };

                console.log($rootScope.promosCache);

                filtered = $rootScope.promosCache.filter(function(elem) {
                    //Los que tienen geo
                    return elem.triggers.hotspot_data || elem.audience.shops.length > 0;
                }).sort(function(a, b) {
                    var distanceA = -1;
                    var distanceB = -1;

                    if (a.audience.shops.length > 0) {
                        var minDistanceA;
                        var indexA;

                        for (var i = 0; i < a.audience.shops.length; i++) {
                            if(a.audience.shops[i] == null || !a.audience.shops[i].map.lat || a.audience.shops[i].map.lat == null){
                                continue;
                            }

                            if (!minDistanceA || haversine(a.audience.shops[i].map.lat, a.audience.shops[i].map.lng, $rootScope.lat, $rootScope.lng).number < minDistanceA) {
                                minDistanceA = haversine(a.audience.shops[i].map.lat, a.audience.shops[i].map.lng, $rootScope.lat, $rootScope.lng).number;
                                indexA = i;
                            }
                        }

                        if(indexA != undefined)
                        distanceA = haversine(coords.lat, coords.lng, a.audience.shops[indexA].map.lat, a.audience.shops[indexA].map.lng).number;

                    } else {
                        distanceA = haversine(coords.lat, coords.lng, a.triggers.hotspot_data.lat, a.triggers.hotspot_data.lng).number;
                    }

                    if (b.audience.shops.length > 0) {
                        var minDistanceB;
                        var indexB;
                        for (var i = 0; i < b.audience.shops.length; i++) {
                            if(b.audience.shops[i] == null || !b.audience.shops[i].map.lat || b.audience.shops[i].map.lat == null){
                                continue;
                            }

                            if (!minDistanceB || haversine(b.audience.shops[i].map.lat, b.audience.shops[i].map.lng, $rootScope.lat, $rootScope.lng).number < minDistanceB) {
                                minDistanceB = haversine(b.audience.shops[i].map.lat, b.audience.shops[i].map.lng, $rootScope.lat, $rootScope.lng).number;
                                indexB = i;
                            }
                        }

                        if(indexB != undefined){
                            distanceB = haversine(coords.lat, coords.lng, b.audience.shops[indexB].map.lat, b.audience.shops[indexB].map.lng).number;
                        }

                    } else {
                        distanceB = haversine(coords.lat, coords.lng, b.triggers.hotspot_data.lat, b.triggers.hotspot_data.lng).number;
                    }

                    /*Mark as irrelevant*/
                    a.irrelevant = distanceA > 50000 ? true : undefined;
                    b.irrelevant = distanceB > 50000 ? true : undefined;
                    /*Mark as irrelevant*/

                    return distanceA - distanceB;
                }).filter(function(promo){
                    return promo.irrelevant == undefined;
                });

                filtered = $rootScope.promosCache.filter(function(elem) {
                    //Los que no tienen geo pero son destacados
                    return !elem.triggers.hotspot_data && !elem.audience.shops.length && elem.category.indexOf("promo-dest") != -1;
                }).sort(function(a, b) {
                    //Por fecha
                    return new Date(a.start_date) > new Date(b.start_date);
                }).concat(filtered).concat($rootScope.promosCache.filter(function(elem) {
                    //Los que no tienen geo ni son destacados
                    return !elem.triggers.hotspot_data && !elem.audience.shops.length && elem.category.indexOf("promo-dest") == -1;
                }).sort(function(a, b) {
                    //Por fecha
                    return new Date(a.start_date) > new Date(b.start_date);
                }));

                console.log(filtered);

                /*Filter promos that havent started*/
                filtered = filtered.filter(function(elem){
                    return $rootScope.NOW() > new Date(elem.start_date).toISOString();
                });
                /*Filter promos that havent started*/

                

                return filtered;
            }, function(c){
                $rootScope.updateCallback = c;
                $rootScope.actualizamosCache();
            }, function(s){
                return $compile(s)($scope);
            });

            $scope._detail = function(id, type) {
                if (!l.isScrolling())
                    $rootScope.detail(id, type);
            }

        }
    }, 500));
}]);
