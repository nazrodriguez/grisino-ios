'use strict';

var Eula = angular.module('Eula', []);

Eula.controller('eula',  ['$scope', '$location', '$rootScope', '$mdDialog', '$cordovaSQLite', function ($scope, $location, $rootScope, $mdDialog, $cordovaSQLite) {
	$scope.advance = function(){
		$location.path("/mainPanel");

    var _log='{"fecha":"'+$rootScope.NOW+'","IP":"'+$rootScope.IP+'", "lat:"'+$rootScope.lat+'", "lng":"'+$rootScope.lng+'","accion":"acepta terminos/condiciones"},';

    var query = "INSERT INTO logs (log) VALUES ('"+_log+"')";
      $cordovaSQLite.execute($rootScope.db, query, []).then(function(res) {
              //console.log("insertamos notificacion");
      }, function (err) {
          console.error(err);
      });
	}

	var _exitApp=false;

	$scope.cancel = function(){
		closeApp();
	}

	$scope.closeApp = function(ev) {
      // Appending dialog to document.body to cover sidenav in docs app
      if(_exitApp){
      }else{
        var confirm = $mdDialog.confirm()        
            .title('Salir de la aplicación')
            .content('¿Seguro desea salir de la aplicación?.')
            .ariaLabel('Lucky day')
            .ok('Salir')
            .cancel('Cancelar')
            .targetEvent(ev);
          $mdDialog.show(confirm).then(function() {
            //$scope.alert = 'You decided to get rid of your debt.';
             navigator.app.exitApp();
          }, function() {
            //$scope.alert = 'You decided to keep your debt.';
            _exitApp=false;
          });
      }  
      _exitApp=true;    
    };

    setTimeout(function(){ navigator.splashscreen.hide();} , 200);

}]);
