var Juegos = angular.module('Juegos', []);

Juegos.controller('juegosCtrl', ['$scope', '$location', '$rootScope', '$compile', '$timeout', '$cordovaFacebook', '$q', '$mdToast', '$cordovaInAppBrowser', function($scope, $location, $rootScope, $compile, $timeout, $cordovaFacebook, $q, $mdToast, $cordovaInAppBrowser) {

    angular.element(document).ready(setTimeout(function() {
        
        $scope.open = function(index){
            
            var link = $scope.games[index].links.html;
            console.log(link);

            $cordovaInAppBrowser.open(link, '_blank', IN_APP_BROWSER_OPTIONS).then(function(event) {}).catch(function(event) {});
            $rootScope.$on('$cordovaInAppBrowser:loadstart', function(e, event) {});
            $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event) {});
            $rootScope.$on('$cordovaInAppBrowser:loaderror', function(e, event) {});
            $rootScope.$on('$cordovaInAppBrowser:exit', function(e, event) {
                $rootScope.loading = false;
            });
        }
        
        $scope.games = [
            {
                links: {
                    html: 'https://0.s3.envato.com/files/218856351/index.html',
                    image: 'https://s3-sa-east-1.amazonaws.com/iloveit/promo/c4527d61-3b3d-4907-a29a-4cc8ad8944f2__vuelta-al-cole.jpg'
                },
                name: "Memory Game"
            },

            {
                links: {
                    html: 'https://0.s3.envato.com/files/197200131/index.html',
                    image: 'https://s3-sa-east-1.amazonaws.com/iloveit/promo/c4527d61-3b3d-4907-a29a-4cc8ad8944f2__vuelta-al-cole.jpg'
                },
                name: "Infinite Jumper"
            },

            {
                links: {
                    html: 'https://0.s3.envato.com/files/119192884/index.html',
                    image: 'https://s3-sa-east-1.amazonaws.com/iloveit/promo/c4527d61-3b3d-4907-a29a-4cc8ad8944f2__vuelta-al-cole.jpg'
                },
                name: "Candy"
            }
        ];

    }, 500));

}]);