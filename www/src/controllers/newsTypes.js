'use strict';

var News = angular.module('NewsTypes', []);

News.controller('newsTypesCtrl', ['$scope', '$rootScope', '$compile', '$timeout', function($scope, $rootScope, $compile, $timeout) {

    angular.element(document).ready(setTimeout(function() {

        window.ga.trackView("Content List");

        if ($rootScope.states && $rootScope.states[$rootScope.states.length - 1] == "main.newsTypes") {

            $rootScope.loading = false;

            var l = new ISCROLL('#listWrapper', function(elem) {
                var id = elem._id;

                if (elem.type == "promos" || elem.type == "mobile") {
                    var source = elem.images && elem.images.length ? elem.images[0].url : "https://s3-sa-east-1.amazonaws.com/iloveit/promo/" + id + ".jpg";
                } else if (elem.type == "video") {
                    var sampleUrl = elem.fields.youtubeid;
                    var video_id = sampleUrl;
                    var source = "https://www.youtube.com/embed/" + video_id + "?rel=0";
                } else if (elem.type == undefined) {
                    var source = elem.images && elem.images.length ? elem.images[0].url : "https://s3-sa-east-1.amazonaws.com/iloveit/shop/" + id + ".jpg";
                } else {
                    var source = elem.images && elem.images.length ? elem.images[0].url : "https://s3-sa-east-1.amazonaws.com/iloveit/content/" + id + ".jpg";
                }

                if (elem.type == "news" || elem.type == "trivia" || elem.type == "rss" || elem.type == "lookbook" || elem.type == "image" || elem.type == "product") {

                    var _laDes = elem.description.replace(/'/g, "\\'");

                    var piece = '<li hm-tap="_detail(\'' + elem._id + '\', \'news\')" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' class="newsPiece" layout="column"> <div class="imageWrapper defaultImage" style="height:' + ($(document).width() - 16) + 'px!important; background-image: url(\'assets/images/novedades.png\')"><img src="' + source + '" id="' + elem._id + '" onload="this.style.opacity=1;" class="newsElement"></div><div class="newsElementBottomWrapper no-padding"><span class="newsElementTitle">' + elem.title + '</span> <div class="newsElementDescription">' + _laDes + '...</div></div></li>';

                } else if (elem.type == "video") {

                    var _laDes = elem.description.replace(/"/g, "'");

                    var piece = '<li class="newsPiece" layout="column"><div style="background-image: url(\'assets/images/IconoMapa.jpeg\')" class="imageWrapper defaultImage"><iframe src="' + source + '?rel=0" frameBorder="0" style="height: 100%; width: 100%;" ></iframe><div hm-tap="_startVideo($event)" hm-recognizer-options=\'{"threshold":10,"time":1000,"preventGhosts":true}\' style="position:absolute; top:0;left:0;right:0;bottom:0;background:transparent;text-align:center;vertical-align:middle;font-size: 20vw;color:white;"></div></div><div class="newsElementBottomWrapper no-padding"> <span class="newsElementTitle">' + elem.title + '</span><div class="newsElementDescription">' + _laDes + '</div></div></li>';
                }

                return piece;
            }, function() {

                console.log($rootScope.contentCache);

                //Filtered array
                var destacadas = $rootScope.contentCache.filter(function(el){
                    return el.category.indexOf("content-dest") != -1;
                }).sort(function(a, b) {
                    //Por fecha
                    return new Date(a.start_date) > new Date(b.start_date);
                });

                var nodestacadas = $rootScope.contentCache.filter(function(el){
                    return el.category.indexOf("content-dest") == -1;
                }).sort(function(a, b) {
                    //Por fecha
                    return new Date(a.start_date) > new Date(b.start_date);
                });

                return destacadas.concat(nodestacadas).filter(function(elem){
                    console.log(elem);
                    return $rootScope.NOW() > new Date(elem.start_date).toISOString()
                });
            }, function(c){
                $rootScope.updateCallback = c;
                $rootScope.actualizamosCache();
            }, function(s){
                return $compile(s)($scope);
            });

            $scope._detail = function(id, type) {
                if (!l.isScrolling())
                    $rootScope.detail(id, type);
            }

            $scope._startVideo = function(event){
                if (!l.isScrolling())
                    $rootScope.startVideo(event);
            }
        }
    }, 500));
}]);
